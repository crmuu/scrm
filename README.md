<p align="center">
    <img src="https://gitee.com/crmuu/scrm/raw/master/public/logo.png" width="278" height="69"/>


<p align="center">
    <img src="https://img.shields.io/badge/KtAdmin-1.1.5-orange" />
    <img src="https://img.shields.io/badge/PHP-7.4-green" />
    <img src="https://img.shields.io/badge/Vue-3.0+-yellow" />
    <img src="https://img.shields.io/badge/MySQL-5.7-blueviolet" />
    <img src="https://img.shields.io/badge/ThinkPHP-6.0+-blue" />
</p>

[CRMUU官网](http://www.crmuu.com) | [安装教程](https://www.showdoc.com.cn/2092671753814266/9689100385820903) | [开发文档](https://www.showdoc.com.cn/ktadmin/) | [应用开发](https://www.showdoc.com.cn/ktadmin/9324421497854622) | [数据库字典](https://www.showdoc.com.cn/crmuu/9688875413131974) | [API文档](https://www.showdoc.com.cn/2092671753814266/9688783225619874/) | [使用手册](https://b09e1has81.feishu.cn/wiki/wikcnpFguIBndzdkD3RK9E7lUph)



:star: CRMUU无偿供大家使用，您的 :star: star是我们前进的动力

#### 系统介绍

CRMUU基于狂团KtAdmin框架开发，是一款免费开源的专业级企业微信SCRM源码系统！本系统包含大部分高频常用功能，功能直接对标某伴7000元每年的账号版本。

本系统由深耕企业微信市场多年的专业团队耗时五个月研发而成，颜值高，功能强。项目使用企业微信内部接口开发，无需像代自建应用那样要向官方支付接口费用，且企业员工数量越多越划算。

#### 特别说明

部署完成后，输入域名，使用账号123456  密码123456 登录即可（仅可使用这一个账号,账号可自行修改）


#### 软件架构

CRMUU-SCRM基于狂团KtAdmin框架开发，主要技术栈为TP6+VUE3+ElementUI+Vite

得益于狂团KtAdmin框架优异的扩展插件机制，CRMUU-SCRM系统有机会构建自己的插件市场。

相关软件架构说明详见KtAdmin官网 http://www.ktadmin.cn/

#### 应用市场

CRMUU企微SCRM系统诚邀开发者入驻，将来将开发的新应用将同步上架到CRMUU的狂团店铺 https://www.kt8.cn/shop/view1287.html


#### 功能展示

![输入图片说明](public/static/demo/001.png)

![输入图片说明](public/static/demo/static/img/42.png)

#### 特别说明

CRMUU-SCRM系统使用的KtAdmin框架版本默认不更新，一般不影响使用，如需更新请按KtAdmin官方提供的方式，在线升级

#### 使用须知

1.允许用于个人学习、毕业设计、教学案例、公益事业、商业使用;

2.如果商用必须保留版权信息，请自觉遵守;

3.禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负。


#### 关于商业授权

1.保留CRMUU相关页面版权的情况下，可以商用。

2.如果想去除相关页面版权，请购买商业授权。

3.如果不想将自己新开发的代码按GPL3.0协议开源出去，可以购买商业授权。


#### 版权信息

CRMUU-SCRM遵循GPL3.0开源协议发布

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2022 by CRMUU (http://www.crmuu.com)

All rights reserved。


#### 禁止行为

1.禁止去除CRMUU页面版权后商用。

2.禁止修改后和衍生的代码做为闭源的商业软件发布和销售。

3.禁止任意删除修改代码注释版权或页面版权后商用等任何违反GPL3.0开源协议及《中华人民共和国著作权法》的行为

#### 功能简介

1.首页-数据统计

2.营销获客

1）渠道活码

2）自动拉群

3）批量加好友

4）互动雷达

3.内容中心

1）素材库

2)话术库

4.客户运营

1）客户标签

2）自定义信息

3）客户管理

4）流失提醒

5）删人提醒

5.客户群管理

1）客户群列表

2）客户群标签

6.营销计划

1）数据订阅

2）个人SOP（后续）

3）群SOP（后续）

7.数据统计

1）客户统计

2)客户群统计

3)成员统计

4)一客一码统计

8.配置中心

1）成员管理

2）配置管理

3）自建应用

①一客一码

②客户画像

③快捷回复
