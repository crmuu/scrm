

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `kt_base_agent`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_agent`;
CREATE TABLE `kt_base_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `un` varchar(20) NOT NULL COMMENT '用户名',
  `pwd` varchar(32) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '密码',
  `key_word` varchar(100) DEFAULT NULL COMMENT '网站关键词',
  `describe` varchar(255) DEFAULT NULL COMMENT '网站描述',
  `company_name` varchar(100) DEFAULT NULL COMMENT '公司名称',
  `company_address` varchar(255) DEFAULT NULL COMMENT '公司地址',
  `record_number` varchar(100) DEFAULT NULL COMMENT '备案号',
  `gzh_code` varchar(255) DEFAULT NULL COMMENT '公众号二维码',
  `kf_code` varchar(255) DEFAULT '' COMMENT '客服二维码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `telephone` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `isadmin` tinyint(1) DEFAULT '0' COMMENT '是否是管理员',
  `balance` decimal(10,2) DEFAULT NULL COMMENT '余额',
  `domain` varchar(255) DEFAULT NULL COMMENT '网站域名',
  `webname` varchar(255) DEFAULT '' COMMENT '网站名称',
  `webtitle` varchar(255) DEFAULT NULL COMMENT '网站标题',
  `qq` varchar(1024) DEFAULT NULL COMMENT '联系QQ',
  `pid` int(11) unsigned DEFAULT NULL COMMENT '开通人员id',
  `rtime` datetime DEFAULT NULL COMMENT '注册时间',
  `isstop` tinyint(1) DEFAULT '0' COMMENT '是否禁用 1:开启  0:禁用',
  `level` int(11) DEFAULT '1' COMMENT '代理级别',
  `register_check` tinyint(1) DEFAULT '1' COMMENT '验证码功能（1开启，2关闭）',
  `agency_token` varchar(255) DEFAULT NULL COMMENT '登录时获取的token',
  `expire_time` varchar(255) DEFAULT NULL COMMENT '登录token到期时间',
  `copyright` varchar(255) DEFAULT NULL COMMENT '版权',
  `registration_audit` int(10) DEFAULT NULL COMMENT '注册审核 （1开启，2关闭）',
  `user_logo` varchar(255) DEFAULT NULL COMMENT '用户后台LOGO',
  `login_logo` varchar(255) DEFAULT NULL COMMENT '登录页LOGO',
  `login_background_status` varchar(255) DEFAULT NULL COMMENT '1: 默认 2:自定义',
  `login_background` varchar(255) DEFAULT NULL COMMENT '登录页背景图',
  `status` int(10) DEFAULT '1' COMMENT '0为停用1为启用',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `lasttime` datetime DEFAULT NULL COMMENT '最后登录时间',
  `pc_official` tinyint(1) DEFAULT NULL COMMENT 'PC官网1开启 2关闭',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代理用户表';

-- ----------------------------
-- Records of kt_base_agent
-- ----------------------------
INSERT INTO `kt_base_agent` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '狂团', '狂团', '狂团', '狂团', '粤ICP备18075561号', 'http://demo.kt8.cn/storage/upload/base/2022-09-07/6318bc386de87.png', 'http://demo.kt8.cn/storage/upload/base/2022-09-07/6318bc31cdf5b.png', null, '4001188032', '1', '64954003.00', 'demo.kt8.cn', 'KtAdmin', '狂团框架管理系统', '', '0', '2022-08-13 14:10:33', '1', null, '2', '1aae6036-81c1-11ed-af89-52540086ae5c', '1672295017', '狂团KtAdmin框架', '1', 'http://weidogstest.oss-cn-beijing.aliyuncs.com/base/base_63172fd9d4150.png', 'http://weidogstest.oss-cn-beijing.aliyuncs.com/base/base_63172fdf62914.png', '1', 'http://weidogstest.oss-cn-beijing.aliyuncs.com/949c6004e2ceb22f5fde1f650befa0fa.png', '1', null, '2022-12-22 14:23:09', null);

-- ----------------------------
-- Table structure for `kt_base_agent_apporder`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_agent_apporder`;
CREATE TABLE `kt_base_agent_apporder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `wid` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `app_id` int(11) DEFAULT NULL COMMENT '应用id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `create_time` datetime DEFAULT NULL,
  `openapp_id` int(11) DEFAULT NULL COMMENT '购买应用id',
  `specs_id` int(1) DEFAULT NULL COMMENT '规格id',
  `bh` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `specs_content` text COMMENT '规格内容',
  `discount_price` decimal(10,2) DEFAULT NULL COMMENT '折后价',
  `setmeal_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: 单个开通  2:套餐开通',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_agent_apporder
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_agent_recharge_record`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_agent_recharge_record`;
CREATE TABLE `kt_base_agent_recharge_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'base_agent',
  `money` double NOT NULL COMMENT '充值金额',
  `recharge_time` datetime NOT NULL COMMENT '充值时间',
  `out_trade_no` varchar(255) NOT NULL COMMENT '订单号',
  `type` int(10) DEFAULT '1' COMMENT '1为充值，2为扣除，3为支付',
  `status` int(11) DEFAULT '1',
  `agid` int(10) DEFAULT NULL COMMENT '充值方或者扣除方id',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代理充值记录';

-- ----------------------------
-- Records of kt_base_agent_recharge_record
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_app_package`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_app_package`;
CREATE TABLE `kt_base_app_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '代理ID',
  `name` varchar(255) DEFAULT NULL COMMENT '套餐名称',
  `specs` longtext COMMENT '规格{duration:时长；duration_type:1:天2:月3:年；old_price:原价;price:售价}',
  `apps` longtext COMMENT '包含应用',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `ctime` datetime DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_app_package
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_app_store`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_app_store`;
CREATE TABLE `kt_base_app_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_id` int(11) DEFAULT NULL COMMENT '商品id',
  `pro_name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `app_logo` varchar(255) DEFAULT NULL COMMENT '应用logo',
  `app_name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `price` decimal(10,2) DEFAULT NULL COMMENT '实际售价',
  `install_num` int(11) DEFAULT NULL COMMENT '安装量',
  `lastsj` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_app_store
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_imglib`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_imglib`;
CREATE TABLE `kt_base_imglib` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '账户id',
  `pid` int(11) DEFAULT NULL COMMENT '上级目录',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `url` varchar(255) DEFAULT NULL COMMENT '图片可访问地址',
  `oss_type` tinyint(1) DEFAULT NULL COMMENT '1: 本地 2:阿里云 3:腾讯云 4:七牛云',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='上传的图片库列表';

-- ----------------------------
-- Records of kt_base_imglib
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_kt_plugin`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_kt_plugin`;
CREATE TABLE `kt_base_kt_plugin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `type` tinyint(1) DEFAULT '1' COMMENT '来源途径: 1:本地上传  2:远程安装',
  `version` varchar(20) DEFAULT NULL COMMENT '已安装版本号',
  `identifie` varchar(255) DEFAULT NULL COMMENT '应用标识',
  `file_name` varchar(255) DEFAULT NULL COMMENT '本地上传的文件名',
  `buytime` char(20) DEFAULT NULL COMMENT '应用购买时间',
  `can_update` tinyint(1) DEFAULT '0' COMMENT '是否可以更新',
  `new_version` varchar(255) DEFAULT NULL COMMENT '最新版本',
  `expire_date` datetime DEFAULT NULL COMMENT '免费升级到期时间',
  `orderbh` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `plugin_id` int(11) DEFAULT NULL COMMENT '插件id',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_time` datetime DEFAULT NULL COMMENT '上传时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `code` varchar(255) DEFAULT NULL COMMENT 'code',
  `uid` int(11) DEFAULT NULL COMMENT '上传者id',
  `dir_name` varchar(255) DEFAULT NULL COMMENT '目录名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugin_id` (`plugin_id`) USING BTREE COMMENT '插件id唯一'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='云市场插件记录表';

-- ----------------------------
-- Records of kt_base_kt_plugin
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_ktconfig`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_ktconfig`;
CREATE TABLE `kt_base_ktconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL COMMENT '狂团配置  key',
  `secret` varchar(255) DEFAULT NULL COMMENT '狂团配置  secret',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_ktconfig
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_loginlog`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_loginlog`;
CREATE TABLE `kt_base_loginlog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `uip` char(50) DEFAULT NULL COMMENT 'ip',
  `admin` tinyint(1) DEFAULT NULL COMMENT '1: 代理登录  2:用户后台',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=805 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_loginlog
-- ----------------------------
INSERT INTO `kt_base_loginlog` VALUES ('799', '1', '36.99.250.216', '2', '2022-09-26 12:23:26');
INSERT INTO `kt_base_loginlog` VALUES ('800', '1', '36.99.250.197', '1', '2022-09-26 12:24:29');
INSERT INTO `kt_base_loginlog` VALUES ('801', '1', '153.35.52.224', '1', '2022-09-26 12:26:50');
INSERT INTO `kt_base_loginlog` VALUES ('802', '1', '153.35.52.191', '2', '2022-09-26 12:29:34');
INSERT INTO `kt_base_loginlog` VALUES ('803', '1', '113.89.33.161', '1', '2022-12-22 14:23:09');
INSERT INTO `kt_base_loginlog` VALUES ('804', '1', '113.89.33.161', '2', '2022-12-22 14:23:27');

-- ----------------------------
-- Table structure for `kt_base_market_app`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_market_app`;
CREATE TABLE `kt_base_market_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '代理id',
  `code` varchar(255) DEFAULT NULL COMMENT '应用编码(和配置文件相对应)',
  `name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `logo` varchar(255) DEFAULT NULL COMMENT '应用图标',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `recom` tinyint(1) DEFAULT '0' COMMENT '推荐 0设为不推荐 1设为推荐',
  `try_days` int(11) DEFAULT NULL COMMENT '试用天数',
  `scene` varchar(255) DEFAULT NULL COMMENT '适用场景',
  `type` int(11) DEFAULT NULL COMMENT '分类',
  `version` varchar(255) DEFAULT NULL COMMENT '已安装版本号',
  `shelves` tinyint(1) DEFAULT '0' COMMENT '上下架 0下架 1上架',
  `label` varchar(255) DEFAULT NULL COMMENT '标签',
  `specs` longtext COMMENT '规格{duration:时长；duration_type:1:天2:月3:年；old_price:原价;price:售价}',
  `user_link` varchar(255) DEFAULT NULL COMMENT '用户后台首页',
  `admin_link` varchar(255) DEFAULT NULL COMMENT '管理后台首页',
  `app_type` tinyint(1) DEFAULT '1' COMMENT '应用类型: 1主应用 2插件 3工具; 注: 插件类型时, 文件夹名必须在主应用后面拼接 plugin, 如demo_plugin_demo',
  `describe` text COMMENT '描述',
  `pid` int(11) DEFAULT '0' COMMENT '非主引擎时 上级id, 最上级为0',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  `install_type` tinyint(1) DEFAULT '1' COMMENT '来源途径: 1:本地上传  2:远程安装',
  `file_name` varchar(255) DEFAULT NULL COMMENT '本地上传的文件名',
  `expire_date` datetime DEFAULT NULL COMMENT '免费升级到期时间',
  `orderbh` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `target` tinyint(1) DEFAULT '2' COMMENT '首页打开方式1：当前窗口 2新窗口',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `has_applets` tinyint(1) DEFAULT '0' COMMENT '是否有小程序 0没有 1有',
  `applet_version` varchar(255) DEFAULT NULL COMMENT '小程序版本号',
  `draft_id` varchar(255) DEFAULT NULL COMMENT '草稿id',
  `template_id` varchar(255) DEFAULT NULL COMMENT '模板id',
  `template_update` tinyint(1) DEFAULT '0' COMMENT '最新版本是否已经请求模板更新 0未更新 1已更新',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_market_app
-- ----------------------------
INSERT INTO `kt_base_market_app` VALUES ('11', '1', 'demo', '狂团demo应用', 'https://kt8logo.oss-cn-beijing.aliyuncs.com/app.png', '1', '0', '7', '狂团dmeo应用，基于KtAdmin可以快速开发出各种saas应用\n', '6', '1.0.0', '1', '[\"\\u72c2\\u56e2\",\"\\u6d4b\\u8bd5\\u5e94\\u7528\",\"\\u4f18\\u8d28\\u5e94\\u7528\"]', '[{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}]', '/demo/user/index', '/demo/admin/index', '1', '这是一个demo示例应用', '0', '2022-12-22 14:23:36', '2022-12-22 14:23:36', '1', null, null, null, '1', '狂团', '0', null, null, null, '0');
INSERT INTO `kt_base_market_app` VALUES ('12', '1', 'wework', 'CRMUU-企微SCRM系统', 'https://crmuu.oss-cn-guangzhou.aliyuncs.com/icon.png', null, '0', null, null, null, '1.0.6', '0', null, null, '/app/wework', null, '3', null, '0', null, '2022-12-22 14:23:36', '1', null, null, null, '1', 'CRMUU', '0', null, null, null, '0');

-- ----------------------------
-- Table structure for `kt_base_market_type`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_market_type`;
CREATE TABLE `kt_base_market_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '代理id',
  `name` varchar(255) DEFAULT NULL COMMENT '分类名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `level` int(11) DEFAULT '1' COMMENT '等级 1:一级分类 2：二级分类',
  `pid` int(11) DEFAULT NULL COMMENT '上级分类id',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_market_type
-- ----------------------------
INSERT INTO `kt_base_market_type` VALUES ('6', '1', '默认', '1', '1', '0', '2022-09-08 16:02:13', null);

-- ----------------------------
-- Table structure for `kt_base_pay_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_pay_config`;
CREATE TABLE `kt_base_pay_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `config` text COMMENT '支付配置',
  `type` varchar(100) DEFAULT NULL COMMENT '类型: wx 微信  ali 支付宝',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='支付配置';

-- ----------------------------
-- Records of kt_base_pay_config
-- ----------------------------
INSERT INTO `kt_base_pay_config` VALUES ('3', '1', '1,1,1,1', 'wx');

-- ----------------------------
-- Table structure for `kt_base_recharge`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_recharge`;
CREATE TABLE `kt_base_recharge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bh` char(50) DEFAULT NULL COMMENT '编号',
  `order_bh` char(50) DEFAULT NULL COMMENT '订单编号',
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `uip` char(50) DEFAULT NULL COMMENT '请求ip',
  `amount` decimal(10,2) DEFAULT NULL COMMENT '金额',
  `status` char(50) DEFAULT NULL COMMENT '状态',
  `alipayzt` char(50) DEFAULT NULL,
  `bz` varchar(250) NOT NULL,
  `ifok` int(11) DEFAULT NULL COMMENT '是否支付 1:已支付  0:未支付',
  `wxddbh` char(50) DEFAULT NULL COMMENT '微信订单编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL,
  `jyh` varchar(255) DEFAULT NULL COMMENT '交易单号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_recharge
-- ----------------------------
INSERT INTO `kt_base_recharge` VALUES ('41', '202209081604235125|1', '202209081604235125|1', '1', '', '1.00', '等待买家付款', null, '微信', '0', null, '2022-09-08 16:04:23', '2022-09-08 16:04:23', null);

-- ----------------------------
-- Table structure for `kt_base_requests`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_requests`;
CREATE TABLE `kt_base_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service` varchar(255) DEFAULT NULL COMMENT '接口名称',
  `request_time` datetime DEFAULT NULL COMMENT '请求时间',
  `ip` varchar(60) DEFAULT NULL COMMENT '客户端ip',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_requests
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_robot`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_robot`;
CREATE TABLE `kt_base_robot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL COMMENT 'url连接地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_robot
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_sms_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_sms_config`;
CREATE TABLE `kt_base_sms_config` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL COMMENT '代理id',
  `access_key_id` varchar(255) DEFAULT NULL COMMENT '阿里云短信key',
  `access_key_secret` varchar(255) DEFAULT NULL COMMENT '阿里云短信密钥',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='阿里云短信配置';

-- ----------------------------
-- Records of kt_base_sms_config
-- ----------------------------
INSERT INTO `kt_base_sms_config` VALUES ('1', '1', '123', 'dcs');

-- ----------------------------
-- Table structure for `kt_base_sms_template`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_sms_template`;
CREATE TABLE `kt_base_sms_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bh` varchar(20) DEFAULT NULL,
  `sign_name` varchar(100) DEFAULT NULL,
  `template_code` varchar(100) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_sms_template
-- ----------------------------
INSERT INTO `kt_base_sms_template` VALUES ('3', '1', '001', '1', '1', '1');

-- ----------------------------
-- Table structure for `kt_base_storage_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_storage_config`;
CREATE TABLE `kt_base_storage_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '账户id',
  `type` tinyint(1) DEFAULT '1' COMMENT '1: local本地 2:oss阿里云 3: cos 腾讯云 4: kodo七牛云',
  `oss_id` varchar(100) DEFAULT NULL COMMENT '阿里云 id',
  `oss_secret` varchar(100) DEFAULT NULL COMMENT '阿里云 secret',
  `oss_endpoint` varchar(100) DEFAULT NULL COMMENT '阿里云  访问域名',
  `oss_bucket` varchar(100) DEFAULT NULL COMMENT '阿里云bucket',
  `cos_secretId` varchar(100) DEFAULT NULL COMMENT '腾讯云 id',
  `cos_secretKey` varchar(100) DEFAULT NULL COMMENT '腾讯云 key',
  `cos_bucket` varchar(100) DEFAULT NULL COMMENT '腾讯云 bucket',
  `cos_endpoint` varchar(100) DEFAULT NULL COMMENT '腾讯云 endpoint',
  `kodo_key` varchar(100) DEFAULT NULL COMMENT '七牛云 key',
  `kodo_secret` varchar(100) DEFAULT NULL COMMENT '七牛云 secret',
  `kodo_domain` varchar(100) DEFAULT NULL COMMENT '七牛云 domain',
  `kodo_bucket` varchar(100) DEFAULT NULL COMMENT '七牛云 bucket',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='云存储配置';

-- ----------------------------
-- Records of kt_base_storage_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_user`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_user`;
CREATE TABLE `kt_base_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `un` varchar(200) NOT NULL COMMENT '用户名',
  `pwd` varchar(32) NOT NULL COMMENT '密码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `telephone` varchar(20) DEFAULT NULL COMMENT '电话',
  `qq` int(11) DEFAULT NULL COMMENT 'qq',
  `logtimes` bigint(20) DEFAULT '0' COMMENT '登录次数 每登一次加一',
  `isadmin` tinyint(1) DEFAULT '1' COMMENT '1: 管理账户 2:子用户',
  `level_id` tinyint(1) DEFAULT '1' COMMENT '用户级别',
  `mendtime` datetime DEFAULT NULL COMMENT '到期时间',
  `agid` int(11) unsigned DEFAULT '0' COMMENT '上级代理id',
  `isstop` tinyint(1) DEFAULT '1' COMMENT '账号状态: 1正常   0:停用，2待审核',
  `xiane` smallint(6) DEFAULT '1' COMMENT '账户限额',
  `balance` decimal(10,2) DEFAULT NULL COMMENT '账户余额',
  `alias` varchar(255) DEFAULT NULL COMMENT '别名',
  `token` varchar(255) DEFAULT NULL COMMENT 'token',
  `expire_time` char(20) DEFAULT '' COMMENT 'token过期时间',
  `create_time` datetime DEFAULT NULL COMMENT '注册时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `status` int(10) DEFAULT '1' COMMENT '1为正常，0为禁用，2为作废',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `contacts` varchar(255) DEFAULT NULL COMMENT '联系人',
  `lasttime` datetime DEFAULT NULL COMMENT '最后一次登陆时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `user_un` (`un`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统用户（客户）表';

-- ----------------------------
-- Records of kt_base_user
-- ----------------------------
INSERT INTO `kt_base_user` VALUES ('1', '123456', 'e10adc3949ba59abbe56e057f20f883e', null, '', null, '139', '2', '2', '2022-10-31 00:00:00', '1', '1', '1', '9997.00', '33', '2523588c-81c1-11ed-aa84-52540086ae5c', '1672295160', '2020-08-01 18:09:54', '2022-09-26 12:32:04', '1', null, '123', '2022-12-22 14:23:27');

-- ----------------------------
-- Table structure for `kt_base_user_appauth`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_user_appauth`;
CREATE TABLE `kt_base_user_appauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `title` varchar(255) DEFAULT NULL COMMENT '应用标题',
  `code` varchar(255) DEFAULT NULL COMMENT '应用标识',
  `create_time` datetime DEFAULT NULL,
  `mend_time` datetime DEFAULT NULL COMMENT '到期时间',
  `set_meal` int(10) DEFAULT NULL COMMENT '对应表套餐id',
  `name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='用户应用权限';

-- ----------------------------
-- Records of kt_base_user_appauth
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_base_user_openapp`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_user_openapp`;
CREATE TABLE `kt_base_user_openapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `title` varchar(255) DEFAULT NULL COMMENT '应用标题',
  `code` varchar(255) DEFAULT NULL COMMENT '应用标识',
  `mend_time` datetime DEFAULT NULL COMMENT '到期时间',
  `name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL COMMENT '引擎id',
  `logo` varchar(255) DEFAULT NULL COMMENT 'logo',
  `version` varchar(255) DEFAULT NULL COMMENT '版本',
  `sequence` int(5) DEFAULT NULL COMMENT '序号 越大越靠前',
  `self_title` varchar(255) DEFAULT NULL COMMENT '自定义标题',
  `pid` int(11) DEFAULT '0' COMMENT '上级id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='用户购买的引擎';

-- ----------------------------
-- Records of kt_base_user_openapp
-- ----------------------------
INSERT INTO `kt_base_user_openapp` VALUES ('12', '1', null, 'demo', '2025-09-15 16:05:07', '狂团demo应用', '2022-09-08 16:05:07', '2022-09-26 12:32:04', '11', 'https://kt8logo.oss-cn-beijing.aliyuncs.com/app.png', '1.0.0', '1', '6666', '0');
INSERT INTO `kt_base_user_openapp` VALUES ('14', '1', 'CRMUU-企微SCRM系统', 'wework', '2023-12-22 14:25:24', 'CRMUU', '2022-12-22 14:25:24', '2022-12-22 14:25:24', '12', 'https://crmuu.oss-cn-guangzhou.aliyuncs.com/icon.png', '1.0.6', '1', 'CRMUU-企微SCRM系统', '0');

-- ----------------------------
-- Table structure for `kt_base_user_order`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_user_order`;
CREATE TABLE `kt_base_user_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `app_id` int(11) DEFAULT NULL COMMENT '应用id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `create_time` datetime DEFAULT NULL,
  `openapp_id` int(11) DEFAULT NULL COMMENT '购买应用id',
  `status` tinyint(1) DEFAULT NULL COMMENT '1:待支付  2:支付成功 3:支付失败',
  `balance_pay` decimal(10,2) DEFAULT NULL COMMENT '余额支付金额',
  `specs_id` int(1) DEFAULT NULL COMMENT '规格id',
  `bh` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `specs_content` text COMMENT '规格内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_base_user_order
-- ----------------------------
INSERT INTO `kt_base_user_order` VALUES ('55', '1', null, '11', '1.00', '2022-09-08 16:04:23', null, '1', null, '1', '202209081604235125|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('56', '1', null, '11', '1.00', '2022-09-08 16:04:45', null, '1', null, '1', '202209081604457371|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('57', '1', '用户试用', '11', '0.00', '2022-09-08 16:05:07', '12', '2', null, null, null, null);
INSERT INTO `kt_base_user_order` VALUES ('58', '1', null, '11', '1.00', '2022-09-08 16:14:59', null, '1', null, '1', '202209081614598412|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('59', '1', null, '11', '1.00', '2022-09-08 16:21:44', null, '1', null, '1', '202209081621447164|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('60', '1', null, '11', '1.00', '2022-09-08 16:21:47', null, '1', null, '1', '202209081621471603|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('61', '1', '用户购买规格时长1年的套餐', '11', '1.00', '2022-09-08 16:23:27', null, '2', null, '1', '202209081623276569|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('62', '1', '用户购买规格时长1年的套餐', '11', '1.00', '2022-09-08 16:24:42', null, '2', null, '1', '202209081624429084|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');
INSERT INTO `kt_base_user_order` VALUES ('63', '1', '用户购买规格时长1年的套餐', '11', '1.00', '2022-09-26 12:32:04', null, '2', null, '1', '202209261232049885|1', '{\"id\":1,\"duration\":\"1\",\"price\":\"1\",\"old_price\":\"1000\",\"duration_type\":\"3\",\"index\":0}');

-- ----------------------------
-- Table structure for `kt_base_user_recharge_record`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_user_recharge_record`;
CREATE TABLE `kt_base_user_recharge_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'base_user',
  `money` double NOT NULL COMMENT '充值金额',
  `recharge_time` datetime NOT NULL COMMENT '充值时间',
  `out_trade_no` varchar(255) NOT NULL COMMENT '订单号',
  `type` int(10) DEFAULT '1' COMMENT '1为充值，2为扣除，3为支付',
  `status` int(11) DEFAULT '1',
  `agid` int(10) DEFAULT NULL COMMENT '充值方或者扣除方id',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户充值记录';

-- ----------------------------
-- Records of kt_base_user_recharge_record
-- ----------------------------
INSERT INTO `kt_base_user_recharge_record` VALUES ('27', '1', '10000', '2022-09-08 16:04:38', '20220908160438166262427857771', '1', '1', '1', '');

-- ----------------------------
-- Table structure for `kt_base_user_template`
-- ----------------------------
DROP TABLE IF EXISTS `kt_base_user_template`;
CREATE TABLE `kt_base_user_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '代理id',
  `code` varchar(255) DEFAULT NULL COMMENT '应用标识',
  `ctime` datetime DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_base_user_template
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_batch_addcus`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_batch_addcus`;
CREATE TABLE `kt_wework_batch_addcus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `excel_name` varchar(255) DEFAULT NULL,
  `staffid` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='导入批量加好友文件记录';

-- ----------------------------
-- Records of kt_wework_batch_addcus
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_batch_addcus_info`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_batch_addcus_info`;
CREATE TABLE `kt_wework_batch_addcus_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `staffid` int(11) DEFAULT NULL,
  `add_status` tinyint(1) DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `add_time` datetime DEFAULT NULL,
  `type` int(10) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='批量加好友客户信息列表';

-- ----------------------------
-- Records of kt_wework_batch_addcus_info
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_config`;
CREATE TABLE `kt_wework_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) NOT NULL,
  `corp_name` varchar(255) DEFAULT NULL,
  `corp_id` varchar(255) DEFAULT NULL,
  `user_status` tinyint(1) DEFAULT '0',
  `user_secret` varchar(255) DEFAULT NULL,
  `user_tokent` varchar(255) DEFAULT NULL,
  `user_aes` varchar(255) DEFAULT NULL,
  `customer_secret` varchar(255) DEFAULT NULL,
  `customer_token` varchar(255) DEFAULT NULL,
  `customer_status` tinyint(1) DEFAULT '0',
  `customer_aes` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `wid` (`wid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='企业微信配置';

-- ----------------------------
-- Records of kt_wework_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_customer`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_customer`;
CREATE TABLE `kt_wework_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `external_userid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `unionid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `position` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `corp_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `corp_full_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `external_profile` text CHARACTER SET utf8,
  `staff_id` int(11) DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `createtime` int(11) DEFAULT NULL,
  `tags_info` text CHARACTER SET utf8,
  `tags` text CHARACTER SET utf8,
  `remark_corp_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `remark_mobiles` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `add_way` int(11) DEFAULT NULL,
  `oper_userid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `custom_information` text CHARACTER SET utf8,
  `status` int(11) DEFAULT '0',
  `del_time` int(11) DEFAULT NULL,
  `qq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enterprise` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(10) DEFAULT NULL,
  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weibo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `score` int(10) DEFAULT '0',
  `integral` int(10) DEFAULT '0',
  `way` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `BTREE` (`wid`,`staff_id`,`external_userid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1462 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='企业客户表（外部联系人表）';

-- ----------------------------
-- Records of kt_wework_customer
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_customer_dynamic`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_customer_dynamic`;
CREATE TABLE `kt_wework_customer_dynamic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `customer_userid` varchar(255) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `chat_id` varchar(255) DEFAULT NULL,
  `glian_id` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `content_sup` text,
  `kf_id` int(10) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='客户动态记录表';

-- ----------------------------
-- Records of kt_wework_customer_dynamic
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_customer_info`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_customer_info`;
CREATE TABLE `kt_wework_customer_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `field_id` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_wework_customer_info
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_customer_lossing`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_customer_lossing`;
CREATE TABLE `kt_wework_customer_lossing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `external_userid` varchar(255) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `add_time` datetime DEFAULT NULL,
  `lossing_time` datetime DEFAULT NULL,
  `days` int(6) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='客户流失表';

-- ----------------------------
-- Records of kt_wework_customer_lossing
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_customer_lossing_status`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_customer_lossing_status`;
CREATE TABLE `kt_wework_customer_lossing_status` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) NOT NULL,
  `status` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kt_wework_customer_lossing_status
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_customer_setfield`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_customer_setfield`;
CREATE TABLE `kt_wework_customer_setfield` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) NOT NULL,
  `attr` tinyint(1) DEFAULT NULL,
  `type` tinyint(1) DEFAULT '1',
  `name` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='自定义信息表(客户画像)';

-- ----------------------------
-- Records of kt_wework_customer_setfield
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_customer_setfield_data`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_customer_setfield_data`;
CREATE TABLE `kt_wework_customer_setfield_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) NOT NULL,
  `external_userid` varchar(255) NOT NULL,
  `pid` int(10) NOT NULL,
  `staff_id` int(10) NOT NULL,
  `content` varchar(255) NOT NULL,
  `ctime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_wework_customer_setfield_data
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_customer_sop`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_customer_sop`;
CREATE TABLE `kt_wework_customer_sop` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) NOT NULL,
  `status` int(10) DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `scene_type` tinyint(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `staffid` text,
  `festival_date` date DEFAULT NULL,
  `filter_tag` text,
  `filter_status` tinyint(1) DEFAULT '0',
  `start_type` tinyint(1) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `rule_data` longtext,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='个人sop';

-- ----------------------------
-- Records of kt_wework_customer_sop
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_customer_sop_rule`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_customer_sop_rule`;
CREATE TABLE `kt_wework_customer_sop_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `sop_id` int(11) DEFAULT NULL,
  `trigger_form` tinyint(1) DEFAULT NULL,
  `send_type` tinyint(1) DEFAULT NULL,
  `send_timing_data` text,
  `send_starttime_type` tinyint(1) DEFAULT NULL,
  `send_starttime` datetime DEFAULT NULL,
  `send_endtime_type` tinyint(1) DEFAULT NULL,
  `send_endtime_date` date DEFAULT NULL,
  `send_frequency` int(11) DEFAULT NULL,
  `send_frequency_type` char(20) DEFAULT NULL,
  `send_content` text,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_wework_customer_sop_rule
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_customermsg`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_customermsg`;
CREATE TABLE `kt_wework_customermsg` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `staffer` varchar(255) DEFAULT NULL,
  `customer_status` int(10) DEFAULT NULL,
  `customer_gender` int(10) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `groups` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `exclude_tags` varchar(255) DEFAULT NULL,
  `repetition_status` int(10) DEFAULT NULL,
  `content` text,
  `adjunct_data` text,
  `send_status` int(10) DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='客户群发';

-- ----------------------------
-- Records of kt_wework_customermsg
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_customermsg_detail`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_customermsg_detail`;
CREATE TABLE `kt_wework_customermsg_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `pid` int(10) DEFAULT NULL,
  `msg_id` int(10) DEFAULT NULL,
  `external_userid` varchar(255) DEFAULT NULL,
  `staff_id` int(10) DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7705 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='客户群发详情\r\n';

-- ----------------------------
-- Records of kt_wework_customermsg_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_customermsgid`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_customermsgid`;
CREATE TABLE `kt_wework_customermsgid` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `staff_id` int(10) DEFAULT NULL,
  `pid` int(10) DEFAULT NULL,
  `fail_list` text,
  `msgid` varchar(255) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `status` int(10) DEFAULT '2',
  `send_status` int(10) DEFAULT NULL,
  `send_size` int(10) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=299 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='客户群发的msgid存储表';

-- ----------------------------
-- Records of kt_wework_customermsgid
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_data_subscribe`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_data_subscribe`;
CREATE TABLE `kt_wework_data_subscribe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `customer_total` tinyint(1) DEFAULT NULL,
  `customer_new` tinyint(1) DEFAULT NULL,
  `customer_lossing` tinyint(1) DEFAULT NULL,
  `customer_netnew` tinyint(1) DEFAULT NULL,
  `group_total` tinyint(1) DEFAULT NULL,
  `group_new` tinyint(1) DEFAULT NULL,
  `group_customer_new` tinyint(1) DEFAULT NULL,
  `group_customer_total` tinyint(1) DEFAULT NULL,
  `group_customer_netnew` tinyint(1) DEFAULT NULL,
  `group_lossing_total` tinyint(1) DEFAULT NULL,
  `day_push` tinyint(1) DEFAULT NULL,
  `week_push` tinyint(1) DEFAULT NULL,
  `month_push` tinyint(1) DEFAULT NULL,
  `selfapp_remind` tinyint(1) DEFAULT NULL,
  `mail_remind` tinyint(1) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `staff_id` text,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='数据订阅(推送员工客户数据)';

-- ----------------------------
-- Records of kt_wework_data_subscribe
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_department`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_department`;
CREATE TABLE `kt_wework_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `department_name` varchar(255) DEFAULT NULL,
  `department_name_en` varchar(255) DEFAULT NULL,
  `department_parentid` int(11) DEFAULT NULL,
  `department_order` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `wid` (`wid`,`department_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='部门表';

-- ----------------------------
-- Records of kt_wework_department
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_et_qrcode`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_et_qrcode`;
CREATE TABLE `kt_wework_et_qrcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `external_userid` varchar(255) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `config_id` varchar(255) DEFAULT NULL,
  `qr_code` varchar(255) DEFAULT NULL,
  `ctime` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='一客一码';

-- ----------------------------
-- Records of kt_wework_et_qrcode
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_et_qrcode_statistics`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_et_qrcode_statistics`;
CREATE TABLE `kt_wework_et_qrcode_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `external_userid` varchar(255) DEFAULT NULL,
  `staff_id` int(10) DEFAULT NULL,
  `ctime` varchar(255) DEFAULT NULL,
  `type` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='一客一码数据统计';

-- ----------------------------
-- Records of kt_wework_et_qrcode_statistics
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_group`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_group`;
CREATE TABLE `kt_wework_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `chat_id` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `owner` varchar(100) DEFAULT NULL,
  `notice` text,
  `tags` text,
  `create_time` int(11) DEFAULT NULL,
  `admin` varchar(255) DEFAULT NULL,
  `is_dismissed` tinyint(1) DEFAULT '0',
  `tag_info` text,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='群聊列表';

-- ----------------------------
-- Records of kt_wework_group
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_group_auto`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_group_auto`;
CREATE TABLE `kt_wework_group_auto` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `type` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `grouping_id` int(10) DEFAULT NULL,
  `staffs` text,
  `standby` text,
  `is_tag` int(10) DEFAULT NULL,
  `tags` text,
  `welcome_text` text,
  `create_time` datetime DEFAULT NULL,
  `join_type` int(10) DEFAULT NULL,
  `skip_verify` int(10) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `config_id` varchar(255) DEFAULT NULL,
  `qr_code` varchar(255) DEFAULT NULL,
  `standby_qrcode` varchar(255) DEFAULT NULL,
  `standby_config_id` varchar(255) DEFAULT NULL,
  `delete` int(10) DEFAULT '0',
  `template_id` varchar(255) DEFAULT NULL,
  `is_astrict` int(10) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `group_welcome` varchar(255) DEFAULT NULL,
  `group_image` varchar(255) DEFAULT NULL,
  `is_group_name` int(10) DEFAULT NULL,
  `is_group_welcome` int(10) DEFAULT NULL,
  `upper` varchar(255) DEFAULT NULL,
  `standby_status` int(10) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='自动拉群';

-- ----------------------------
-- Records of kt_wework_group_auto
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_group_auto_astrict`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_group_auto_astrict`;
CREATE TABLE `kt_wework_group_auto_astrict` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `staff_id` int(10) NOT NULL,
  `size` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='自动拉群成员添加上限';

-- ----------------------------
-- Records of kt_wework_group_auto_astrict
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_group_auto_data`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_group_auto_data`;
CREATE TABLE `kt_wework_group_auto_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `size` int(10) DEFAULT NULL,
  `pid` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `orders` int(10) DEFAULT NULL,
  `delete` int(10) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='自动拉群群聊数据';

-- ----------------------------
-- Records of kt_wework_group_auto_data
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_group_auto_grouping`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_group_auto_grouping`;
CREATE TABLE `kt_wework_group_auto_grouping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_wework_group_auto_grouping
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_group_auto_statistics`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_group_auto_statistics`;
CREATE TABLE `kt_wework_group_auto_statistics` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `staff_id` int(10) NOT NULL,
  `external_userid` varchar(255) NOT NULL,
  `ctime` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='自动拉群数据详情';

-- ----------------------------
-- Records of kt_wework_group_auto_statistics
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_group_member`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_group_member`;
CREATE TABLE `kt_wework_group_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `chat_id` varchar(100) DEFAULT NULL,
  `userid` varchar(100) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `join_time` int(11) DEFAULT NULL,
  `join_scene` tinyint(1) DEFAULT NULL,
  `invitor` varchar(255) DEFAULT NULL,
  `group_nickname` varchar(255) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `lossing_time` int(11) DEFAULT NULL,
  `unionid` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `BTREE` (`wid`,`chat_id`,`userid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='群成员表';

-- ----------------------------
-- Records of kt_wework_group_member
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_group_remind`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_group_remind`;
CREATE TABLE `kt_wework_group_remind` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `chat_list` text,
  `img_qrcode_status` tinyint(1) DEFAULT NULL,
  `link_status` tinyint(1) DEFAULT NULL,
  `weapp_status` tinyint(1) DEFAULT NULL,
  `card_status` tinyint(1) DEFAULT NULL,
  `keywords_status` tinyint(1) DEFAULT NULL,
  `keywords` text,
  `status` tinyint(1) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='客户群提醒规则';

-- ----------------------------
-- Records of kt_wework_group_remind
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_group_tag`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_group_tag`;
CREATE TABLE `kt_wework_group_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `orders` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='群标签';

-- ----------------------------
-- Records of kt_wework_group_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_group_taggroup`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_group_taggroup`;
CREATE TABLE `kt_wework_group_taggroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '1',
  `tags` longtext,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='群标签分组';

-- ----------------------------
-- Records of kt_wework_group_taggroup
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_group_welcome`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_group_welcome`;
CREATE TABLE `kt_wework_group_welcome` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) NOT NULL,
  `content` text,
  `welcome_data` text,
  `send_status` int(10) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `type` int(10) DEFAULT '1',
  `template_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='入群欢迎语';

-- ----------------------------
-- Records of kt_wework_group_welcome
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_groupmsg`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_groupmsg`;
CREATE TABLE `kt_wework_groupmsg` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `staffer` varchar(255) DEFAULT NULL,
  `content` text,
  `adjunct_data` text,
  `send_status` int(10) DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='客户群群发';

-- ----------------------------
-- Records of kt_wework_groupmsg
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_groupmsg_detail`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_groupmsg_detail`;
CREATE TABLE `kt_wework_groupmsg_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `pid` int(10) DEFAULT NULL,
  `msg_id` int(10) DEFAULT NULL,
  `staff_id` int(10) DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `chat_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='客户群发详情';

-- ----------------------------
-- Records of kt_wework_groupmsg_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_groupmsgid`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_groupmsgid`;
CREATE TABLE `kt_wework_groupmsgid` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `staff_id` int(10) DEFAULT NULL,
  `pid` int(10) DEFAULT NULL,
  `msgid` varchar(255) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `status` int(10) DEFAULT '2',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_wework_groupmsgid
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_groupsop`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_groupsop`;
CREATE TABLE `kt_wework_groupsop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `groupid` text,
  `status` tinyint(1) DEFAULT '1',
  `group_type` tinyint(1) DEFAULT NULL,
  `group_condition_type` tinyint(1) DEFAULT NULL,
  `group_condition_tag` varchar(255) DEFAULT NULL,
  `group_condition_new` tinyint(1) DEFAULT NULL,
  `group_condition_new_start` datetime DEFAULT NULL,
  `group_condition_new_end` datetime DEFAULT NULL,
  `send_type` tinyint(1) DEFAULT NULL,
  `send_content` text,
  `send_starttime_type` tinyint(1) DEFAULT NULL,
  `send_starttime` datetime DEFAULT NULL,
  `send_endtime_type` tinyint(1) DEFAULT NULL,
  `send_endtime_date` date DEFAULT NULL,
  `send_endtime_time` varchar(100) DEFAULT NULL,
  `send_endtime_size` tinyint(1) DEFAULT NULL,
  `send_endtime_date_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_wework_groupsop
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_imglib`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_imglib`;
CREATE TABLE `kt_wework_imglib` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `oss_type` tinyint(1) DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `wid` int(11) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  `gs_type` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='上传的图片库列表';

-- ----------------------------
-- Records of kt_wework_imglib
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_imglib_group`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_imglib_group`;
CREATE TABLE `kt_wework_imglib_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `gs_type` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='素材库分组';

-- ----------------------------
-- Records of kt_wework_imglib_group
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_moments`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_moments`;
CREATE TABLE `kt_wework_moments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` int(10) DEFAULT NULL,
  `staffs` text,
  `customer_type` int(10) DEFAULT NULL,
  `tags` text,
  `text` text,
  `content` text,
  `send_status` int(10) DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `jobid` varchar(255) DEFAULT NULL,
  `moment_id` varchar(255) DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `staffstr` text,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='企微朋友圈';

-- ----------------------------
-- Records of kt_wework_moments
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_moments_info`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_moments_info`;
CREATE TABLE `kt_wework_moments_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `moment_id` varchar(255) NOT NULL,
  `staff_id` int(10) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `type` int(10) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='个人朋友圈同步详情';

-- ----------------------------
-- Records of kt_wework_moments_info
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_moments_sync`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_moments_sync`;
CREATE TABLE `kt_wework_moments_sync` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) NOT NULL,
  `time` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='上次同步时间表';

-- ----------------------------
-- Records of kt_wework_moments_sync
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_qr`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_qr`;
CREATE TABLE `kt_wework_qr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `skipverify_status` tinyint(1) DEFAULT '1',
  `skipverify_type` tinyint(1) DEFAULT NULL,
  `skipverify_time` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `staff_type` tinyint(1) DEFAULT NULL,
  `staffid` text,
  `staff_data` text,
  `staffup_status` tinyint(1) DEFAULT NULL,
  `staffup_data` text,
  `staffline_status` tinyint(1) DEFAULT NULL,
  `staff_standby` varchar(255) DEFAULT NULL,
  `customerbz_status` tinyint(1) DEFAULT NULL,
  `customerbzqh_status` tinyint(1) DEFAULT NULL,
  `customerbz` varchar(255) DEFAULT NULL,
  `customerdes_status` tinyint(1) DEFAULT NULL,
  `customerdes` varchar(255) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `config_id` varchar(255) DEFAULT NULL,
  `qr_code` varchar(255) DEFAULT NULL,
  `tag_status` tinyint(1) DEFAULT '0',
  `tag` varchar(255) DEFAULT NULL,
  `welcome_type` tinyint(1) DEFAULT '1',
  `welcome_content` text,
  `welcome_other_data` text,
  `dayparting_status` tinyint(1) DEFAULT NULL,
  `dayparting_data` text,
  `welcom_shield` varchar(255) DEFAULT NULL,
  `welcome_shield_status` tinyint(1) DEFAULT NULL,
  `merge_qr_code` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `state` (`state`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='渠道活码列表';

-- ----------------------------
-- Records of kt_wework_qr
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_qrgroup`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_qrgroup`;
CREATE TABLE `kt_wework_qrgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='渠道活码分组';

-- ----------------------------
-- Records of kt_wework_qrgroup
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_radar`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_radar`;
CREATE TABLE `kt_wework_radar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `link_title` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `ex_tag_inform` tinyint(1) DEFAULT NULL,
  `ex_tag` varchar(255) DEFAULT NULL,
  `behavior_inform` tinyint(1) DEFAULT NULL,
  `dynamic_inform` tinyint(1) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `utime` datetime DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='雷达模板列表';

-- ----------------------------
-- Records of kt_wework_radar
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_radar_record`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_radar_record`;
CREATE TABLE `kt_wework_radar_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `externalcontact_id` varchar(255) DEFAULT NULL,
  `radar_id` int(11) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='雷达点击记录表';

-- ----------------------------
-- Records of kt_wework_radar_record
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_reply_group`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_reply_group`;
CREATE TABLE `kt_wework_reply_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `grouping_name` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  `ctime` datetime DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='快捷回复分组';

-- ----------------------------
-- Records of kt_wework_reply_group
-- ----------------------------
INSERT INTO `kt_wework_reply_group` VALUES ('28', '1', '默认分组', '0', '2022-12-22 14:25:53', '0');
INSERT INTO `kt_wework_reply_group` VALUES ('29', '1', '默认分组', '0', '2022-12-22 14:25:53', '0');

-- ----------------------------
-- Table structure for `kt_wework_reply_list`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_reply_list`;
CREATE TABLE `kt_wework_reply_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `grouping_id` int(10) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `orders` int(10) DEFAULT '0',
  `welcome_data` text,
  `send_type` int(10) DEFAULT NULL,
  `size` int(10) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='快捷回复列表';

-- ----------------------------
-- Records of kt_wework_reply_list
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_scan_log`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_scan_log`;
CREATE TABLE `kt_wework_scan_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `external_userid` varchar(100) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `staff_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='扫码统计';

-- ----------------------------
-- Records of kt_wework_scan_log
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_selfapp`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_selfapp`;
CREATE TABLE `kt_wework_selfapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `agent_id` varchar(255) DEFAULT NULL,
  `secret` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `square_logo_url` varchar(255) DEFAULT NULL,
  `description` text,
  `ctime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='企业自建应用配置表';

-- ----------------------------
-- Records of kt_wework_selfapp
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_staff`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_staff`;
CREATE TABLE `kt_wework_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `department_name` varchar(255) DEFAULT NULL,
  `order` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `is_leader_in_dept` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `thumb_avatar` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `extattr` text,
  `status` int(11) DEFAULT NULL,
  `qr_code` varchar(255) DEFAULT NULL,
  `external_profile` text,
  `external_position` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `open_userid` varchar(255) DEFAULT NULL,
  `main_department` int(11) DEFAULT NULL,
  `is_del` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `wid` (`wid`,`userid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='企业员工表';

-- ----------------------------
-- Records of kt_wework_staff
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_tag`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_tag`;
CREATE TABLE `kt_wework_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `group_id` varchar(255) DEFAULT NULL,
  `tag_id` varchar(255) DEFAULT NULL,
  `tag_name` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `web_order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `tag_id` (`tag_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9463 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='企业标签';

-- ----------------------------
-- Records of kt_wework_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_taggroup`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_taggroup`;
CREATE TABLE `kt_wework_taggroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `web_order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `group_id` (`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2464 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='企业标签分组';

-- ----------------------------
-- Records of kt_wework_taggroup
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_wework_welcomemsg`
-- ----------------------------
DROP TABLE IF EXISTS `kt_wework_welcomemsg`;
CREATE TABLE `kt_wework_welcomemsg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `staffer` text,
  `content` text,
  `adjunct_data` text,
  `dayparting_status` tinyint(1) DEFAULT NULL,
  `dayparting_data` text,
  `status` int(1) DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `staffstr` text,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='客户欢迎语';

-- ----------------------------
-- Records of kt_wework_welcomemsg
-- ----------------------------
