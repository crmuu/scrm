<?php
// 这是系统自动生成的event定义文件
return [
	'bind'      => [
    ],

    'listen'    => [
        'AppInit'  => [],
        'HttpRun'  => [],
        'HttpEnd'  => [],
        'LogLevel' => [],
        'LogWrite' => [],
        'CustomerCallback'=>[
        	'app\wework\listener\BuffetPromotion',
            'app\wework\listener\CustomerQrCallback',
            'app\wework\listener\CustomerAutoCallback',
            'app\wework\listener\CustomerQrcodeCallback'
        ],
        'del_external_contact'=>[
            'app\wework\listener\BuffetPromotion',
            'app\wework\listener\DelExternal'
        ],
        'del_external_contact'=>[
            'app\wework\listener\BuffetPromotion',
            'app\wework\listener\DelFollow'
        ],
        'ChangeExternalChat'=>[
            'app\wework\listener\Colonization',
            'app\wework\listener\BuffetPromotion',
            'app\wework\listener\ChangeExternalChat'
        ]
    ],

    'subscribe' => [
    ],
];
