<?php
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

declare (strict_types = 1);

namespace app\wework\listener;

use think\facade\Db;
use think\facade\Log;
use think\facade\Session;
use app\wework\model\QyWechatApi;
use app\wework\model\BaseModel;
use app\wework\model\QrModel;

class CustomerQrcodeCallback
{
	/**
     * 一客一码
     * @return mixedc
     */
    public function handle($arr){
    	if(!array_key_exists("State",$arr))return "不是该渠道";
        $qrcode = Db::table('kt_wework_et_qrcode')->where('state',$arr['State'])->find();
        if(!$qrcode) return "不是该渠道";
        $wid = $qrcode["wid"];
		$staff = Db::table("kt_wework_staff")->where(["wid"=>$wid,"userid"=>$arr['UserID']])->find();
		$set = Db::table("kt_wework_et_qrcode_statistics")->where(["wid"=>$wid,"external_userid"=>$arr['ExternalUserID'],"staff_id"=>$staff["id"]])->find();
		if(!$set){
			$data["wid"] = $wid;
			$data["pid"] = $qrcode["id"];
			$data["external_userid"] = $arr["ExternalUserID"];
			$data["staff_id"] = $staff["id"];
			$data["type"] = 1;
			$data["ctime"] = time();
			Db::table("kt_wework_et_qrcode_statistics")->insertGetId($data);
		}else{
			Db::table("kt_wework_et_qrcode_statistics")->where(["wid"=>$wid,"external_userid"=>$arr['ExternalUserID'],"staff_id"=>$staff["id"]])->update(["type"=>1]);
		}
    }
}