<?php
declare (strict_types = 1);

namespace app\wework\listener;
use think\facade\Db;
use app\wework\model\CallbackModel;
use app\wework\model\QyWechatApi;
use app\wework\model\user\autogroup\AutoPullGroupModel;


class Colonization
{
	public function handle($arr){
		$CorpID = $arr['ToUserName'];
		$config = Db::table("kt_wework_config")->where(['corp_id'=>$CorpID])->find();
		$wid = $config["wid"];
        if($arr["UpdateDetail"] == "add_member"){
        	$is_set = Db::table("kt_wework_tag_group_detail")->where(["wid"=>$wid,"external_userid"=>$arr["group_user"]])->find();
        	if(!$is_set)return "不是该渠道";
        	Db::table("kt_wework_tag_group_detail")->where(["wid"=>$wid,"external_userid"=>$arr["group_user"]])->update(["type"=>1,"chat_id"=>$arr["ChatId"]]);
        }

     	return "操作成功";
    }
}