<?php
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

declare (strict_types = 1);

namespace app\wework\listener;
use think\facade\Db;
use app\wework\controller\Base;
use app\wework\model\CallbackModel;
use app\wework\model\Helper;
use app\wework\model\QyWechatApi;
use think\facade\Log;


class CustomerDelExternal extends Base
{
    /**
     * 事件监听处理
     * @param $config 配置
     * @param $staff 员工
     * @param $customer 客户
     * @return mixed
     */
    public function handle($data)
    {
        $external_userid = $data['ExternalUserID'];
		$userid = $data['UserID'];
		$CorpID = $data['ToUserName'];
		$config = CallbackModel::getConfig($CorpID);
		$wid = $config["wid"];
		$staff = Helper::getUser(['wid'=>$wid,'userid'=>$userid]);
		$qrcode = Db::table("kt_wework_et_qrcode_statistics")->where(["wid"=>$wid,"external_userid"=>$external_userid,"staff_id"=>$staff["id"]])->find();
		$customer = Helper::getCustomer(['wid'=>$wid,'staff_id'=>$staff['id'],'external_userid'=>$external_userid]);
		if(!$customer) return "";
		$lossing["wid"] = $config["wid"];
		$lossing["type"] = 2;
		$lossing["external_userid"] = $external_userid;
		$lossing["staff_id"] = $staff["id"];
		$lossing["days"] = CallbackModel::dateDay($customer["createtime"]);
		$lossing["add_time"] = date("Y-m-d H:i:s",$customer["createtime"]);
		$lossing["lossing_time"] = date("Y-m-d H:i:s",time());
		$lossing["create_time"] = date("Y-m-d H:i:s",time());
		$lossing["update_time"] = date("Y-m-d H:i:s",time());
		$res = CallbackModel::lossing($lossing);
		if(!$res) return "数据库导入失败";
		$lossings["wid"] = $wid;
		$lossings["type"] = 2;
		$lossings["staff_id"] = $staff["id"];
		$lossings["customer_userid"] = $external_userid;
		$lossings["create_time"] = date("Y-m-d H:i:s",time());
		$lossings["update_time"] = date("Y-m-d H:i:s",time());
		CallbackModel::lossingSave($lossings);
		$url = $this->req->domain();
		$sendContent = "员工'{$staff["name"]}'把客户 '{$customer["name"]}'从好友列表中删除了";
		$res = QyWechatApi::send_textcard($userid,$config["wid"],"删人提醒",$sendContent,$this->req->domain().'/wework/h5.CustomerPortrait/customer_info.html?wid='.$wid."&external_userid=".$external_userid."&staff_id=".$staff['id']);
    }
}
