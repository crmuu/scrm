<?php
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller;

use app\BaseController;
use think\facade\Db;
use think\Request;
use think\facade\Session;

class Base extends BaseController
{
	protected $req;
    protected $user;
    protected $wid;
    protected $token;
    protected $appName;
    protected $Jssdk;

    public function  __construct(Request $request){
    	// $url   = strtolower($request->controller().'/'.$request->action());
        $this->req = $request;
        $this->host  = $request->host();
        $url  = $request->url();
        $this->token = $this->req->header('UserToken');
        $this->user = Db::table('kt_base_user')->where([['token', '=', $this->token], ['expire_time', '>',time()]])->find();
        // var_dump($this->user);
        if($this->user){
            $this->wid = $this->user['id'];
            Session::set('wid',$this->user['id']);
        } 
        // var_dump(request()->file());  die;      // $this->Jssdk = \Wechat\Loader::get('Script',config('app.webchat'));
        $this->admin = Db::table('kt_user_agency')->find();
        

    }

    public function getInfo(){
        $admin = Db::table('kt_base_agent')->where('domain',$this->host)->find();
        if(!$admin) $admin = Db::table('kt_base_agent')->where('isadmin',1)->find();

        return success("获取成功",$admin["user_logo"]);
    }
    
    public function index()
    {
        return '您好！这是一个[wework]示例应用';
    }
}