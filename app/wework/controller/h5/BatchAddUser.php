<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\h5;

use app\wework\controller\Base;
use think\facade\Db;
use app\wework\model\BaseModel;
use app\wework\model\BatchAddUserModel;
use app\wework\model\QyWechatApi;
use app\wework\model\JsSdk;
use think\facade\Log;
use think\facade\View;



/**
 * 批量加好友 h5端
*/
class BatchAddUser extends Base
{
	public function index(){
		$wid = $this->req->param("wid");
		$staff_id = $this->req->param("staff_id");
		$id = $this->req->param("id");
		$phone = $this->req->param("phone");
		$status = $this->req->param("status");
		$config = Db::table("kt_wework_config")->where(['wid'=>$wid])->find();
		$selfapp = Db::table("kt_wework_selfapp")->where(['wid'=>$wid])->find();
		$jsSdkServe = new JsSdk($config["corp_id"],$wid);
		$wxConfig = $jsSdkServe->getSignPackage();
		$agentConfig = $jsSdkServe->getSignPackage('','agent');
		$where["pid"] = $id;
		$where["wid"] = $wid;
		if(isset($status))$where["add_status"] = $status;
		$res = Db::table("kt_wework_batch_addcus")->where(["id"=>$id])->json(["tags"])->find();
		$tags = Db::table("kt_wework_tag")->where(["tag_id"=>$res["tags"]])->field("tag_name")->column("tag_name");
		$data = Db::table('kt_wework_batch_addcus_info')
				->where($where);
		if($phone)$data = $data->whereLike('phone','%'.$phone.'%');
				$data = $data->select()
				->toArray();
		View::assign("corp_id", $config["corp_id"]);
		View::assign("agent_id", $selfapp["agent_id"]);
		View::assign("status", $status);
		View::assign("count", count($data));
		View::assign("data", $data);
		View::assign("phone", $phone);
		View::assign("tags", $tags);
		View::assign("wxConfig", $wxConfig);
		View::assign("agentConfig", $agentConfig);
		View::assign("wid", $wid);
		return view();
	}
}