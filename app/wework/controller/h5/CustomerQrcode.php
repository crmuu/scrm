<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\h5;

use app\wework\controller\Base;
use app\wework\model\BaseModel;
use app\wework\model\Helper;
use app\wework\model\QyWechatApi;
use think\facade\Db;
use app\wework\model\JsSdk;
use think\facade\Log;
use think\facade\View;

/**
  * 一客一码
 */
class CustomerQrcode extends Base
{
	public function index(){
		$code = $this->req->param("code");
		$wid = $this->req->param("wid");
		$config = Helper::getConfig($wid);
		$selfapp = Helper::getSelfapp($wid);
		$wid = $config["wid"];
		if($code){
			$resArr = QyWechatApi::getuserinfo($wid,$code);
			$jsSdkServe = new JsSdk($config["corp_id"],$wid);
			$wxConfig = $jsSdkServe->getSignPackage();
			$agentConfig = $jsSdkServe->getSignPackage('','agent');
			$where["wid"] = $wid;
			$where["userid"] = $resArr["UserId"];
			$staff = Helper::getUser($where);
			if(!$staff)return error("请先同步通讯录成员");
			View::assign("corp_id", $config["corp_id"]);
			View::assign("agent_id", $selfapp["agent_id"]);
			View::assign("wxConfig", $wxConfig);
			View::assign("agentConfig", $agentConfig);
			View::assign("wid", $wid);
			View::assign("staff_id", $staff["id"]);
			return view();
		}else{
			$redirect_uri = urlencode($this->req->domain().'/wework/h5.CustomerQrcode/index.html?wid='.$wid);
			$url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$config["corp_id"].'&redirect_uri='.$redirect_uri.'&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect';
			return redirect($url);
		}
	}

	public function customer_qrcode(){
		$etUserId = $this->req->param("etUserId");
		$staff_id = $this->req->param("staff_id");
		$wid = $this->req->param("wid");
		$config = Helper::getConfig($wid);
		$selfapp = Helper::getSelfapp($wid);
		$where["wid"] = $wid;
		$where["id"] = $staff_id;
		$staff = Helper::getUser($where);
		$wheres["wid"] = $wid;
		$wheres["staff_id"] = $staff_id;
		$wheres["external_userid"] = $etUserId;
		$customer = Helper::getCustomer($wheres);
		$qrcode = Db::table("kt_wework_et_qrcode")->where(["wid"=>$wid,"external_userid"=>$etUserId,"staff_id"=>$staff_id])->find();
		if(!$qrcode){
			$data["staff_id"] = $staff_id;
			$data["wid"] = $wid;
			$data["external_userid"] = $etUserId;
			$data["ctime"] = date("Y-m-d H:i:s");
			$id = Db::table("kt_wework_et_qrcode")->insertGetId($data);
			
			$params['type'] = 1;
			$params['scene'] = 2;
			$params['user'] = [$staff["userid"]];
			$params['state'] = "et_qr_".$id;
			$params['skip_verify'] = true;
			$add_way = QyWechatApi::addContactWay($params,$wid);
			if($add_way["errcode"] != 0)return error("操作失败",$add_way["errcode"]);
			$upd["state"] = $params['state'];
			$upd["config_id"] = $add_way['config_id'];
			$upd["qr_code"] = $add_way['qr_code'];
			Db::table("kt_wework_et_qrcode")->where(["id"=>$id])->save($upd);
			$qrcode = Db::table("kt_wework_et_qrcode")->where(["wid"=>$wid,"external_userid"=>$etUserId,"staff_id"=>$staff_id])->find();
		}else{
			$id = $qrcode["id"];
		}
		$statistics = Db::table('kt_wework_et_qrcode_statistics')
				->where(['wid'=>$wid,'pid'=>$id])
				->filter(function($statis) use($wid){
					$statis["ctime"] = date("Y-m-d H:i:s",$statis["ctime"]);
					$statis["customer"] = Db::table("kt_wework_customer")->where(["wid"=>$wid,"external_userid"=>$statis["external_userid"],"staff_id"=>$statis["staff_id"]])->field("name,avatar")->find();
					return $statis;
				})
				->select()
				->toArray();
		$jsSdkServe = new JsSdk($config["corp_id"],$wid);
		$wxConfig = $jsSdkServe->getSignPackage();
		$agentConfig = $jsSdkServe->getSignPackage('','agent');

		View::assign("agentConfig", $agentConfig);
		View::assign("wxConfig", $wxConfig);
		View::assign("staff", $staff);
		View::assign("qrcode", $qrcode);
		View::assign("statistics", $statistics);
		View::assign("counts", count($statistics));
		View::assign("corp_id", $config["corp_id"]);
		View::assign("agent_id", $selfapp["agent_id"]);
		return view();
	}

	public function info(){
		$id = $this->req->param("id");
		$qrcode = Db::table("kt_wework_et_qrcode")->where(["id"=>$id])->find();
		$mediaId = QyWechatApi::mediaUpload($qrcode["wid"],$qrcode["qr_code"],"image")["media_id"];

		return success("获取成功",$mediaId);
	}

	
	public function customer_infos(){
	    return view();
	}
}