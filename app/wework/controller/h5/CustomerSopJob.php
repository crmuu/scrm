<?php

namespace app\wework\controller\h5;

use app\wework\controller\Base;
use app\wework\model\BaseModel;
use app\wework\model\Helper;
use app\wework\model\QyWechatApi;
use think\facade\Db;
use app\wework\model\JsSdk;
use think\facade\Log;
use think\facade\View;

/**
  * sop
 */
class CustomerSopJob extends Base
{
	public function index(){
		$id = $this->req->get("id");
		$status = Db::table("kt_wework_customer_sop_status")->where(["id"=>$id])->json(["external_userid"])->find();
		$rule_data = Db::table("kt_wework_customer_sop_rule")->where(["id"=>$status["pid"]])->json(["send_content"])->find();
        $user = Db::table("kt_wework_customer")->where(["wid"=>$status["wid"]])->where("external_userid","in",$status["external_userid"])->group("external_userid")->column("name");
        
		return view("",["status"=>$status,"rule_data"=>$rule_data,"user"=>implode("，",$user)]);
	}
}