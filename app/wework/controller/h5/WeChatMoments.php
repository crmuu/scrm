<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\h5;

use app\wework\controller\Base;
use app\wework\model\BaseModel;
use app\wework\model\user\reply\ReplyModel;
use app\wework\model\Helper;
use app\wework\model\QyWechatApi;
use app\wework\model\JsSdk;
use think\facade\Log;
use think\facade\Db;
use think\facade\View;


/**
  * 快捷回复，雷达  h5端
 */
class WeChatMoments extends Base
{
	public function index(){
		$id = $this->req->param("id");
		$res = Db::table("kt_wework_moments")->where(["id"=>$id])->json(["text","content"])->find();
        $res["content"]["type"] = isset($res["content"]["type"])?$res["content"]["type"]:0;
		View::assign("res", $res);
		return view();
	}
}