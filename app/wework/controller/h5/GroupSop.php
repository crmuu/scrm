<?php
namespace app\wework\controller\h5;

use app\BaseController;
use app\wework\model\BaseModel;
use app\wework\model\Helper;
use app\wework\model\QyWechatApi;
use think\facade\Db;
use app\wework\model\JsSdk;
use think\facade\Log;
use think\facade\View;

/**
  * 群sop
 */
class GroupSop extends BaseController
{
	public function index(){
		$id = $this->request->get("id");
		$detail = Db::table("kt_wework_groupsop_detail")->where(["id"=>$id])->json(["content"])->find();
        
		return view("",["rule_data"=>$detail["content"]]);
	}
}