<?php
namespace app\wework\controller\user\radar;

use think\facade\Db;
use app\wework\controller\Base;
use app\wework\model\Basemodel;
use app\wework\model\user\radar\InteractiveRadarModel;
use think\facade\Session;

/**
* 互动雷达
**/

class InteractiveRadar extends Base
{
	/**
	* 互动雷达列表
	*/
 	public function index(){
 		$wid = Session::get("wid");
		$page = $this->req->param("page")?:1;
		$size = $this->req->param("size")?:10;
		$title = $this->req->param("title");
		$res = InteractiveRadarModel::info($page,$size,$title);
		$res["url"] = $this->req->domain()."/wework/h5.InteractiveRadar/index.html?wid=".$wid;
		
 		return success("互动雷达列表",$res);
 	}

 	public function info(){
		$id = $this->req->param("id");
		$res = InteractiveRadarModel::infos($id);

 		return success("互动雷达列表",$res);
 	}



 	/**
 	* 添加雷达连接
 	* @param type 雷达类型: 1链接   2文章， 3PDF文件
 	* @param link_title 整条数据的标题
 	* @param link 链接
 	* @param title 标题
 	* @param description 描述
 	* @param image_url 图片
 	* @param ex_tag_inform 是否开启标签验证
 	* @param ex_tag 标签列表
 	* @param behavior_inform 行为通知: 1开启  2关闭
 	* @param dynamic_inform 动态通知: 1开启  2关闭
 	* 
 	*/
 	public function create(){
 		$id = $this->req->param("id");
		$type = $this->req->param("type")?:1;
 		$title = $this->req->param("title");
 		$link_title = $this->req->param("link_title");
 		$link = $this->req->param("link");
 		$description = $this->req->param("description");
 		$image_url = $this->req->param("image_url");
 		if(!$title || !$link_title || !$link || !$description || !$image_url) return error("参数错误");
 		$ex_tag_inform = $this->req->param("ex_tag_inform");
 		$ex_tag = $this->req->param("ex_tag");
 		if(!$ex_tag && $ex_tag_inform == 1) return error("请选择标签");
 		$behavior_inform = $this->req->param("behavior_inform");
 		$dynamic_inform = $this->req->param("dynamic_inform");
 		$res = InteractiveRadarModel::create($link_title,$link,$title,$description,$image_url,$ex_tag_inform,$ex_tag,$behavior_inform,$dynamic_inform,$id);

 		return success("新建雷达链接",$res);
 	}

 	/**
 	* 删除雷达连接
 	* @param id
 	*/
 	public function del(){
 		$id = $this->req->param("id");
 		if(!$id) return error("参数错误");
 		$res = InteractiveRadarModel::del($id);

 		return success("删除雷达链接",$res);
 	}

	/**
	* 获取网址详细信息
	* urls 网址
	*/
 	public function urlData(){
		$urls = $this->req->param("url");
 		if(!$urls) return error("参数错误");
 		if(substr($urls,0,7) !== "http://" && substr($urls,0,8) !== "https://")return error("请输入正确的连接");
 		$content = file_get_contents($urls);
		$img = strpos($content,'<img src=')+10;
		$url = substr($content,$img);
		$imgs = strpos($url,'"');
		$url = substr($url,0,$imgs);
 		if(strpos($url,"http") !== 0)$url = $urls.$url;
 		
		$postb = strpos($content,'<title>')+7;
		$poste = strpos($content,'</title>');
		$length = $poste-$postb;
		$arr["url"] = $url;
		$arr["title"] = substr($content,$postb,$length);
		if(isset(get_meta_tags($urls)["description"]))$arr["desc"] = get_meta_tags($urls)["description"];
 		return success("获取网站详细信息",$arr);
 	}
}