<?php
namespace app\wework\controller\user\reply;

use think\facade\Db;
use app\wework\controller\Base;
use think\facade\Session;
use app\wework\model\QyWechatApi;
use app\wework\model\Basemodel;
use app\wework\model\user\reply\ReplyModel;

/**
* 快捷回复
**/

class Reply extends Base
{
	/**
	* 话术库列表
	* @param page 页码
	* @param title 标题匹配
	*/
	public function index(){
		$page = $this->req->param("page")?:1;
		$size = $this->req->param("size")?:10;
		$title = $this->req->param("title");
		$grouping_id = $this->req->param("grouping_id");
		$orders = $this->req->param("orders")?:"asc";
		$res = ReplyModel::info($page,$size,$title,$grouping_id,$orders);
		
		return success("话术库",$res);
	}

	// down 下移 type
	// up 上移 type
	public function moveGroupManage(){
		$type = $this->req->param("type");
		$id = $this->req->param("id");
		if(!$id || !$type)return error("必填项不可为空");
		$res = ReplyModel::moveGroupManage($id,$type);

		return success("操作成功",$res);
	}

	// down 下移 type
	// up 上移 type
	public function move(){
		$type = $this->req->param("type");
		$id = $this->req->param("id");
		if(!$id || !$type)return error("必填项不可为空");
		$res = ReplyModel::move($id,$type);

		return success("操作成功",$res);
	}
	

	/**
	* 快捷回复分组列表
	* @param page 页码
	* @param name 名称匹配
	*/
	public function groupManage(){
		$res = ReplyModel::groupManage();
		if(!count($res))$res = ReplyModel::groupManage();

		return success("话术库分组列表",$res);
	}

	/**
	* 添加快捷回复分组
	* @param name 名称
	* @param sort 排序
	*/
	public function addgroupManage(){
		$wid = Session::get("wid");
		$grouping_name = $this->req->param("grouping_name");
		$son_grouping = $this->req->param("son_grouping");
		if(!$grouping_name)return error("必填项不可为空");
		$is_set = Db::table("kt_wework_reply_group")->where(["wid"=>$wid,"grouping_name"=>$grouping_name])->find();
		if($is_set) return error("名称重复，请重新输入");
		$res = ReplyModel::addgroupManage($grouping_name,$son_grouping);

		return success("添加分组",$res);
	}

	/*
	* 編輯分组拉取数据
	*/
	public function getgroupManage(){
		$id = $this->req->param("id");
		$res = ReplyModel::getgroupManage($id);

		return success("获取成功",$res);
	}
	

	/**
	* 添加快捷回复分组
	* @param name 名称
	* @param sort 排序
	*/
	public function updgroupManage(){
		$wid = Session::get("wid");
		$id = $this->req->param("id");
		$grouping_name = $this->req->param("grouping_name");
		$add_groupimg = $this->req->param("add_groupimg");
		$del_groupimg = $this->req->param("del_groupimg");
		if(!$id)return error("参数错误");
		if(!$grouping_name)return error("必填项不可为空");
		$grouping = Db::table("kt_wework_reply_group")->where(["id"=>$id])->find();
		if($grouping["pid"] == 0){
			$is_set = Db::table("kt_wework_reply_group")->where(["wid"=>$wid,"grouping_name"=>$grouping_name,"pid"=>0])->where("id","<>",$id)->find();
			if($is_set) return error("分组名称已存在");
		}else{
			$is_set = Db::table("kt_wework_reply_group")->where(["wid"=>$wid,"grouping_name"=>$grouping_name,"pid"=>$grouping["id"]])->where("id","<>",$id)->find();
			if($is_set) return error("子分组名称不可重复");
		}
		$res = ReplyModel::updgroupManage($id,$grouping_name,$add_groupimg,$del_groupimg);

		return success("修改分组",$res);
	}



	/**
	* 添加/编辑 快捷回复
	* @param title 标题
	* @param type 类型 1文本 2图片 3视频 4文件 5链接 6小程序
	* @param groupid grouping 标签集合
	*/
	public function addReply(){
		$wid = Session::get("wid");
		$id = $this->req->param("id");
		$title = $this->req->param("title");
		$send_type = $this->req->param("send_type");
		if(!$title)return error("必填项不可为空");
		if($id) $is_set = Db::table("kt_wework_reply_list")->where(["wid"=>$wid,"title"=>$title])->where("id","<>",$id)->find();
		if(!$id) $is_set = Db::table("kt_wework_reply_list")->where(["wid"=>$wid,"title"=>$title])->find();
		if($is_set) return error("标题重复，请重新输入");
		$grouping_id = $this->req->param("grouping_id");
		$welcome_data = $this->req->param("welcome_data");
		$datas = [];
		$size["text"] = 0;
		$size["image"] = 0;
		$size["video"] = 0;
		$size["file"] = 0;
		$size["link"] = 0;
		$size["miniprogram"] = 0;
		for($i=0;$i<count($welcome_data);$i++){
			$data["type"] = $welcome_data[$i]["type"];
			if($welcome_data[$i]["type"] == 1){
				$data["text"] = $welcome_data[$i]["text"];
				$size["text"] += 1; 
			}elseif($welcome_data[$i]["type"] == 2){
				$data["image_url"] = $welcome_data[$i]["image_url"];
				$size["image"] += 1; 
			}elseif($welcome_data[$i]["type"] == 3){
				$data["video_url"] = $welcome_data[$i]["video_url"];
				$size["video"] += 1; 
			}elseif($welcome_data[$i]["type"] == 4){
				$data["file_name"] = $welcome_data[$i]["file_name"];
				$data["file_url"] = $welcome_data[$i]["file_url"];
				$size["file"] += 1; 
			}elseif($welcome_data[$i]["type"] == 5){
				$data["link_title"] = $welcome_data[$i]["link_title"];
				$data["link_image_url"] = $welcome_data[$i]["link_image_url"];
				$data["link_url"] = $welcome_data[$i]["link_url"];
				$data["link_desc"] = $welcome_data[$i]["link_desc"];
				$size["link"] += 1; 
			}elseif($welcome_data[$i]["type"] == 6){
				$data["miniprogram_title"] = $welcome_data[$i]["miniprogram_title"];
				$data["miniprogram_imgurl"] = $welcome_data[$i]["miniprogram_imgurl"];
				$data["miniprogram_appid"] = $welcome_data[$i]["miniprogram_appid"];
				$data["miniprogram_page"] = $welcome_data[$i]["miniprogram_page"];
				$size["miniprogram"] += 1; 
			}
			$datas[] = $data;
		}
		if($send_type == 1){
			$oftype = "单";
			if(count($welcome_data) > 1)$oftype = "多";
			if($grouping_id)$grouping_name = ReplyModel::find("kt_wework_reply_group",["id"=>$grouping_id])["grouping_name"];
			if(!$grouping_id)$grouping_name = "默认分组";
			$str = "【话术提醒】有企业话术更新啦";
			$str .= "<br>";
			$str .= "分组：".$grouping_name;
			$str .= "<br>";
			$str .= "话术标题：".$title;
			$str .= "<br>";
			$str .= "话术类型：".$oftype;
			$str .= "<br>";
			$str .= "话术内容：";
			if($size["text"])$str .= "文本（{$size["text"]}）";
			if($size["image"])$str .= "图片（{$size["image"]}）";
			if($size["video"])$str .= "视频（{$size["video"]}）";
			if($size["file"])$str .= "文件（{$size["file"]}）";
			if($size["miniprogram"])$str .= "小程序（{$size["miniprogram"]}）";
			if($size["link"])$str .= "连接（{$size["link"]}）";
			$staffs = array_column(Db::table("kt_wework_staff")->where(["wid"=>$wid,"status"=>1,"is_del"=>0])->field("userid")->select()->toArray(), "userid");
			$res = QyWechatApi::sendText($wid,implode("|", $staffs),$str);
		}

		$res = ReplyModel::addReply($title,$grouping_id,$datas,$send_type,$id);

		return success("添加快捷回复",$res);
	}

	//编辑拉取数据
	public function getReply(){
		$id = $this->req->param("id");
		if(!$id)return error("参数缺失");
		$res = ReplyModel::getReply($id);

		return success("获取成功",$res);
	}

	/**
	* 删除快捷回复
	*/
	public function delReply(){
		$id = $this->req->param("id");
		if(!$id)return error("参数缺失");
		$res = ReplyModel::delReply($id,"kt_wework_reply_list");

		return success("删除成功",$res);
	}

	/**
	* 删除分组
	*/
	public function delgroupManage(){
		$id = $this->req->param("id");
		if(!$id)return error("参数缺失");
		$res = ReplyModel::delgroupManage($id);
		if(!$res)return error("不可删除默认分组");

		return success("删除成功",$res);
	}


}