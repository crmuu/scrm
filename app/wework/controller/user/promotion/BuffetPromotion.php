<?php
namespace app\wework\controller\user\promotion;

use think\facade\Db;
use think\facade\Session;
use app\wework\controller\Base;
use app\wework\model\user\promotion\BuffetPromotionModel;
use app\wework\model\Basemodel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use app\wework\model\Helper;
require "../extend/Phpqrcode/phpqrcode.php";

/*
* 自助推广码
*/
class BuffetPromotion extends Base{
	public function index(){
		$type = $this->req->param("type")?:1;
		$page = $this->req->param("page")?:1;
		$size = $this->req->param("size")?:10;
		$res = BuffetPromotionModel::list($type,$page,$size);

		return success("获取成功",$res);
	}

	public function add(){
		$url = $this->req->domain();
		$res = BuffetPromotionModel::add($this->req->post(),$url);

		return $res;
	}

	public function info(){
		$id = $this->req->param("id");
		$res = BuffetPromotionModel::infos($id);

		return success("获取成功",$res);
	}

	/*
	* 修改拉取数据
	*/
	public function edit(){
		$id = $this->req->param("id");
		$res = BuffetPromotionModel::info($id);

		return success("获取成功",$res);
	}

	/*
	* 删除
	*/
	public function del(){
		$id = $this->req->param("id");
		$res = BuffetPromotionModel::del($id);
		
		return $res;
	}

	/*
	* 分享
	*/
	public function share(){
		$id = $this->req->param("id");
		$info = BuffetPromotionModel::infos($id);
		$code = new \QRcode();
		$path = '../public/static/wework/promotion/';
		if(!file_exists($path))mkdir($path,0777,true);
		$url = $this->req->domain()."/wework/h5.BuffetPromotion/index.html?id=".$id;
	    $res = $code->png($url,$path.$info["name"].".png", 6);
	    if(!$res)$res = $this->req->domain()."/static/wework/promotion/".$info["name"].".png";
	    $data["url"] = $url;
	    $data["image"] = $res;

	    return success("获取成功",$data);
	}

	/*
	* 详情
	*/
	public function statistics(){
		$wid = Session::get('wid');
		$id = $this->req->param("id");
		$type = $this->req->param("type")?:1;
		$where["wid"] = $wid;
		$where["type"] = $type;
		$promotions = BuffetPromotionModel::promotions($where);
		if(!$id)$id = $promotions[0]["id"];
		$statistics = BuffetPromotionModel::statistics($id);

		return success("操作成功",$statistics);
	}

	/*
	* 图标数据
	*/
	public function map(){
		$date_type = $this->req->param("date_type");
		$id = $this->req->param("id");
		$type = $this->req->param("type")?:1;
		if(!$id)$id = BuffetPromotionModel::one();
		if($date_type == "day"){
			$ktime = date("Y-m-d");
			$dtime = date("Y-m-d");
		}else if($date_type == "week"){
			$ktime = date("Y-m-d",time()-3600*24*7);
			$dtime = date("Y-m-d");
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 months",time()));
			$dtime = date("Y-m-d");
		}else{
			$ktime = $this->req->param('ktime')?:date("Y-m-d",time()-3600*24*7);
			$dtime = $this->req->param('dtime')?:date("Y-m-d");
		}
		$res = BuffetPromotionModel::map($id,$type,$ktime,$dtime);

		return success("操作成功",$res);
	}

	/*
	* 图标数据
	*/
	public function table(){
		$date_type = $this->req->param("date_type");
		$id = $this->req->param("id");
		$type = $this->req->param("type")?:1;
		if(!$id)$id = BuffetPromotionModel::one();
		if($date_type == "day"){
			$ktime = date("Y-m-d");
			$dtime = date("Y-m-d");
		}else if($date_type == "week"){
			$ktime = date("Y-m-d",time()-3600*24*7);
			$dtime = date("Y-m-d");
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 months",time()));
			$dtime = date("Y-m-d");
		}else{
			$ktime = $this->req->param('ktime')?:date("Y-m-d",time()-3600*24*7);
			$dtime = $this->req->param('dtime')?:date("Y-m-d");
		}
		$res = BuffetPromotionModel::table($id,$type,$ktime,$dtime);

		return success("操作成功",$res);
	}
}