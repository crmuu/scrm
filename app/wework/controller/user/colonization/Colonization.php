<?php
namespace app\wework\controller\user\colonization;

use think\facade\Db;
use think\facade\Session;
use app\wework\controller\Base;
use app\wework\model\user\colonization\ColonizationModel;
use app\wework\model\Basemodel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use app\wework\model\Helper;

/*
* 标签建群
*/
class Colonization extends Base{
	/*
	* 列表
	*/
	public function index(){
		$page = $this->req->param("page")?:1;
		$size = $this->req->param("size")?:10;
		$name = $this->req->param("name");
		$res = ColonizationModel::list($page,$size,$name);

		return success("操作成功",$res);
	}

	/*
	* 新建
	**/
	public function add(){
		$wid = Session::get("wid");
		$group_list = $this->req->post("group_list");
		if(!$group_list)return error("请选择群聊");
		$id = ColonizationModel::add($this->req->post());
		$external_userids = ColonizationModel::getSendUser($this->req->post());
		$group_list = ColonizationModel::groupList($group_list);
		$external_userids = ColonizationModel::sendMsgUserid($group_list,$external_userids);
		ColonizationModel::save($id,["send_msg_userid"=>$external_userids]);
        $max_size = 200;
        $send_index = 0;
        $first_chat_num = $group_list[0]["count"];
        if(count($external_userids) <= ($max_size-$first_chat_num)){
	        $welcome_data[] = [
	            'msgtype'=>'image',
	            'image'=>[
	                'media_id' => QyWechatApi::mediaUpload($wid,$group_list[0]['img'],'image')['media_id'],
	            ]
	        ];
        	$res = QyWechatApi::addMsgTemplate($wid,$external_userids,"",["content"=>$this->req->post("text_content")],$welcome_data);
        	$msgid[] = $res["msgid"];
        }else{
        	foreach ($group_list as $key=>$group){
        		$welcome_data = [];
        		$welcome_data[] = [
		            'msgtype'=>'image',
		            'image'=>[
		                'media_id' => QyWechatApi::mediaUpload($wid,$group['img'],'image')['media_id'],
		            ]
		        ];
        		if($key == count($group_list)-1){
        			//最后一个群聊所发送的数据
                    $send_user = array_slice($external_userids,$send_index);
                    //执行发送逻辑
        			$res = QyWechatApi::addMsgTemplate($wid,$send_user,"",$this->req->post("text_content"),$welcome_data);
        			$msgid[] = $res["msgid"];
                    break;
        		}else{
        			//本次 群剩下可以邀请的数目
                    $group_num = $max_size - $group["count"];
                    //判断  这个群聊是否满足 剩下的客户
                    if( count($external_userids) - $send_index <= $group_num){
                        //已经满足  说明这是最后一批用户
                        $send_user = array_slice($external_userids,$send_index);
                        $send_index = count($external_userids);
                        //执行发送逻辑
        				$res = QyWechatApi::addMsgTemplate($wid,$send_user,"",$this->req->post("text_content"),$welcome_data);
        				$msgid[] = $res["msgid"];
                        break;
                    }else{
                        //获得本次发送的用户
                        $send_user = array_slice($external_userids,$send_index,$group_num);
                        $send_index += $group_num;
                        //执行发送逻辑
        				$res = QyWechatApi::addMsgTemplate($wid,$send_user,"",$this->req->post("text_content"),$welcome_data);
        				$msgid[] = $res["msgid"];
                    }
        		}
        	}
        }
        $res = ColonizationModel::save($id,["msg_id"=>$msgid]);

		return success("操作成功",$res);
	}

	public function del(){
		$id = $this->req->param("id");
		$res = ColonizationModel::del($id);

		return success("操作成功",$res);
	}

    public function detail(){
        $id = $this->req->param("id");
        $detail = ColonizationModel::detail($id);

        return success("操作成功",$detail);
    }

    public function customerStatistics(){
        $id = $this->req->param("id");
        $type = $this->req->param("type");
        $status = $this->req->param("status");
        $chat_id = $this->req->param("chat_id");
        $name = $this->req->param("name");
        $staff_id = $this->req->param("staff_id");
        $res = ColonizationModel::customerStatistics($id,$type,$status,$chat_id,$name,$staff_id);

        return success("操作成功",$res);
    }

    public function staffStatistics(){
        $id = $this->req->param("id");
        $res = ColonizationModel::staffStatistics($id);

        return success("操作成功",$res);
    }
}