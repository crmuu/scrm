<?php
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\user\welcome;

use think\facade\Db;
use app\BaseController;
use app\wework\controller\Base;
use app\wework\model\Basemodel;
use app\wework\model\QyWechatApi;
use think\facade\Session;
use app\wework\model\user\welcome\GroupWelcomeModel;

/*
* 入群欢迎语
*/

class GroupWelcome extends Base
{
	/*
	* 入群欢迎语列表
	*/
    public function index(){
		$page = $this->req->param("page")?:1;
		$size = $this->req->param("size")?:10;
		$content = $this->req->param("content");
		$res = GroupWelcomeModel::getWelcomeList($page,$size,$content);
    	return success('客户群群发列表',$res);
    }

	/**
	* 新建入群欢迎语
	*/
	public function welcomeTemplateCreate(){
		$wid = Session::get('wid');
		$id = $this->req->param("id");
		$content = $this->req->param("content");
		$type = $this->req->param("type");
		if(!$content) return error("缺少参数");
		$data = [];
		if($type)$data["type"] = $type;
		if($type == 1){
			$data["imageurl"] = $this->req->param("image");
		}else if($type == 2){
			$data["link_title"] = $this->req->param("link_title");
			$data["link_url"] = $this->req->param("link_url");
			$data["link_desc"] = $this->req->param("link_desc");
			$data["link_picurl"] = $this->req->param("link_picurl");
		}else if($type == 3){
			$data["miniprogram_title"] = $this->req->param("miniprogram_title");
			$data["miniprogram_appid"] = $this->req->param("miniprogram_appid");
			$data["miniprogram_page"] = $this->req->param("miniprogram_page");
			$data["miniprogram_picurl"] = $this->req->param("miniprogram_picurl");
		}
		$send_status = $this->req->param("send_status"); //0 或者 1
		if(!$id){
		    $welcomeData = [];
			if($data)$welcomeData = GroupWelcomeModel::groupWelcomeOther($data,$wid);
			$res = QyWechatApi::groupWelcomeTemplate($wid,$content,$welcomeData,$send_status);
			if($res["errcode"] != 0) return error("错误",$res); 
			$addWelcom = GroupWelcomeModel::addWelcom($content,$data,$id,$send_status,$res["template_id"]);
		}else{
		    $welcomeData = [];
			if($data)$welcomeData = GroupWelcomeModel::groupWelcomeOther($data,$wid);
			$getWelcome = GroupWelcomeModel::getWelcome($id);
			$res = QyWechatApi::editWelcomeTemplate($wid,$getWelcome["template_id"],$content,$welcomeData);
			if($res["errcode"] != 0) return error("错误",$res); 
			$addWelcom = GroupWelcomeModel::addWelcom($content,$data,$id);
		}
		return success('添加入群欢迎语',$addWelcom);
	}

	public function detail(){
		$id = $this->req->param("id");
		if(!$id) return error("缺少参数");
		$getWelcome = GroupWelcomeModel::getWelcome($id);
		return success('详情',$getWelcome);
	}

	public function delWelcome(){
		$wid = Session::get('wid');
		$id = $this->req->param("id");
		if(!$id) return error("缺少参数");
		$getWelcome = GroupWelcomeModel::getWelcome($id);
		$del = QyWechatApi::delWelcomeTemplate($wid,$getWelcome["template_id"]);
		if($del["errcode"] != 0) return error("错误",$res); 
		$delWelcome = GroupWelcomeModel::delWelcome($id);
		return success('删除成功',$delWelcome);
	}
}