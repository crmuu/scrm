<?php 
namespace app\wework\controller\user\welcome;
use think\facade\Db;
use app\wework\controller\Base;
use app\wework\model\user\welcome\WelcomModel;
use app\wework\model\QyWechatApi;
use think\facade\Session;

class WelcomMessage extends Base{
	/**
	* 好友欢迎语 列表
	*/
	public function index(){
		$wid = Session::get('wid');
		$page = $this->req->param("page")?:1;
        $size = $this->req->param("size")?:10;
        $content = $this->req->param("content");
        $res = WelcomModel::list($page,$size,$content);

        return success("获取成功",$res);
	}


	/**
	* 添加修改 
	* $staffer 使用人员 
	* $content 欢迎语文本内容
	* $dayparting_status 欢迎语文本内容
	* $adjunct_data 欢迎语附件
	* $dayparting_data 分时段欢迎语+附件
	*/
	public function addWelcom(){
        $id = $this->req->param("id");
        $staffer = $this->req->param("staffer");
        $content = $this->req->param("content");
        $dayparting_status = $this->req->param("dayparting_status");
        $adjunct_data = $this->req->param("adjunct_data");
        $dayparting_data = $this->req->param("dayparting_data");
        if(!$staffer || !$content) return error("必填项不可为空");
        $arr = [];
        foreach($staffer as $value){
        	$arr[] = strval($value);
        }
        $res = WelcomModel::addWelcom($staffer,$content,$dayparting_status,$adjunct_data,$dayparting_data,$arr,$id);

        return success("操作成功",$res);
	}

	/**
	* 删除
	*/
	public function delWelcom(){
        $id = $this->req->param("id");
        if(!$id) return error("必填项不可为空");
        $res = WelcomModel::delWelcom($id);

        return success("删除成功",$res);
	}

	/**
	* 编辑拉取数据
	*/
	public function info(){
        $id = $this->req->param("id");
        if(!$id) return error("必填项不可为空");
        $res = WelcomModel::info($id);

        return success("编辑拉取数据",$res);
	}
}