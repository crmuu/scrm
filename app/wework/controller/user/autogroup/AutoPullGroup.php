<?php
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\user\autogroup;

use think\facade\Db;
use think\facade\Session;
use app\wework\controller\Base;
use app\wework\model\user\autogroup\AutoPullGroupModel;
use app\wework\model\QyWechatApi;
use dh2y\qrcode\QRcode;

/*
* 自动拉群
*/

class AutoPullGroup extends Base{
	/*
	* 添加自动拉群
	* 员工添加上限数据为：二维数组，{["id":1,"size":200],["id":1,"size":200]} 
	*/
	public function add(){
		//群二维码，企微拉群活码
		$wid = Session::get("wid");
		$type = $this->req->param("type");//拉群方式
		$join_type = $this->req->param("join_type");//入群方式
		$name = $this->req->param("name");//名称
		$grouping_id = $this->req->param("grouping_id");//分组id 
		$staffs = $this->req->param("staffs");//成员id集合
		$is_astrict = $this->req->param("is_astrict");//员工添加上限状态  1开启 2关闭
		$astrict = $this->req->param("astrict");//员工添加上限
		$standby = $this->req->param("standby");//备用员工
		$is_tag = $this->req->param("is_tag");//是否开启标签
		$tags = $this->req->param("tags");//标签组id
		$welcome_text = $this->req->param("welcome_text");//入群欢迎语
		$skip_verify = $this->req->param("skip_verify");//好友验证，1为开启，2为关闭
		$group_data = $this->req->param("group_data");//群数据
		$is_group_name = $this->req->param("is_group_name");
		$group_name = $this->req->param("group_name");
		$is_group_welcome = $this->req->param("is_group_welcome");
		$group_welcome = $this->req->param("group_welcome");
		$group_image = $this->req->param("group_image");
		if(!$group_data)return error("请上传活码");
		$auto_id = AutoPullGroupModel::add($type,$join_type,$name,$grouping_id,$staffs,$is_astrict,$astrict,$standby,$is_tag,$tags,$welcome_text,$skip_verify,$group_name,$group_welcome,$group_image,$is_group_name,$is_group_welcome);
		if(!$auto_id)return error("添加失败");
		//入群欢迎语配置
		// $res = QyWechatApi::groupWelcomeTemplate($wid,$welcome_text);
		// if($res["errcode"] != 0)return error("操作失败",$res["errcode"]);
		// AutoPullGroupModel::upWelcome($res["template_id"],$auto_id);
		//企微活码拉群
		if($type == 2)AutoPullGroupModel::addDatas($group_data,$auto_id);
		//群二维码拉群
		if($type == 1){
			$groups = AutoPullGroupModel::addData($group_data,$auto_id);
			$groups = AutoPullGroupModel::groupData($groups);
			// 通知成员配置群聊欢迎语
			foreach($groups as $value){
		        $str = "【入群欢迎语】管理员开启了入群欢迎语配置，请为您群聊『".$value["name"]."』配置入群欢迎语";
		        QyWechatApi::sendText($wid,$value["user"]["userid"],$str);
			}
		}
		if($join_type == 2){
			$params['type'] = 2;
			$params['scene'] = 2;
			$params['user'] = AutoPullGroupModel::users($staffs);
			$params['state'] = "auto".$auto_id;
			$params['skip_verify'] = $skip_verify?true:false;
			$add_way = QyWechatApi::addContactWay($params);
			if($add_way["errcode"] != 0)return error("操作失败",$add_way["errcode"]);
			AutoPullGroupModel::upAuto($add_way,$auto_id);
		}
		if(count($standby)){
			$param['type'] = 2;
			$param['scene'] = 2;
			$param['remark'] = NULL;
			$param['user'] = AutoPullGroupModel::users($standby);
			$param['state'] = "auto".$auto_id;
			$param['skip_verify'] = $skip_verify?true:false;
			$add_way = QyWechatApi::addContactWay($param);
			if($add_way["errcode"] != 0)return error("操作失败",$add_way["errcode"]);
			AutoPullGroupModel::upAuto($add_way,$auto_id,1);
		}
		return success("操作成功");		
	}

	/*
	* 编辑拉群数据
	*/
	public function getInfo(){
		$id = $this->req->param("id");
		if(!$id) return error("参数错误");
		$res = AutoPullGroupModel::getInfo($id);

		return success("获取成功",$res);	
	}

	/*
	* 编辑
	*/
	public function upd(){
		//群二维码，企微拉群活码
		$wid = Session::get("wid");
		$id = $this->req->param("id");//主键id
		if(!$id)return error("请上传活码");
		$type = $this->req->param("type");//拉群方式
		$join_type = $this->req->param("join_type");//入群方式
		$name = $this->req->param("name");//名称
		$grouping_id = $this->req->param("grouping_id");//分组id 
		$staffs = $this->req->param("staffs");//成员id集合
		$is_astrict = $this->req->param("is_astrict");//员工添加上限状态  1开启 2关闭
		$astrict = $this->req->param("astrict");//员工添加上限
		$standby = $this->req->param("standby");//备用员工
		$is_tag = $this->req->param("is_tag");//是否开启标签
		$tags = $this->req->param("tags");//标签组id
		$welcome_text = $this->req->param("welcome_text");//入群欢迎语
		$skip_verify = $this->req->param("skip_verify");//好友验证，1为开启']\'2为关闭
		$group_data = $this->req->param("group_data");//群数据
		$is_group_name = $this->req->param("is_group_name");
		$group_name = $this->req->param("group_name");
		$is_group_welcome = $this->req->param("is_group_welcome");
		$group_welcome = $this->req->param("group_welcome");
		$group_image = $this->req->param("group_image");
		if(!count($group_data))return error("请上传活码");
		$add = AutoPullGroupModel::add($type,$join_type,$name,$grouping_id,$staffs,$is_astrict,$astrict,$standby,$is_tag,$tags,$welcome_text,$skip_verify,$group_name,$group_welcome,$group_image,$is_group_name,$is_group_welcome,$id);
		//入群欢迎语配置修改 	
		$auto = AutoPullGroupModel::info($id);
		// $res = QyWechatApi::editWelcomeTemplate($wid,$auto["template_id"],$welcome_text);
		// if($res["errcode"] != 0)return error("操作失败",$res["errcode"]);
		//企微活码拉群
		if($type == 2)AutoPullGroupModel::updatas($group_data,$id);
		//群二维码拉群
		if($type == 1){
			$groups = AutoPullGroupModel::updata($group_data,$id);
			$groups = AutoPullGroupModel::groupData($groups);
			// 通知成员配置群聊欢迎语
			foreach($groups as $value){
		        $str = "【入群欢迎语】管理员开启了入群欢迎语配置，请为您群聊『".$value["name"]."』配置入群欢迎语";
		        QyWechatApi::sendText($wid,$value["user"]["userid"],$str);
			}
		}
		if($join_type == 2){
			$params['user'] = AutoPullGroupModel::users($staffs);
			$params['config_id'] = $auto["config_id"];
			$upd = QyWechatApi::updateContactWay($params);
			if($upd["errcode"] != 0)return error("操作失败",$upd["errcode"]);
		}
		if(count($standby)){
		    if($auto["standby_config_id"]){
		        $param['config_id'] = $auto["standby_config_id"];
    			$param['user'] = AutoPullGroupModel::users($standby);
    			$upd = QyWechatApi::updateContactWay($param);
    			if($upd["errcode"] != 0)return error("操作失败",$upd["errcode"]);
		    }else{
		        $param['type'] = 2;
    			$param['scene'] = 2;
    			$param['remark'] = NULL;
    			$param['user'] = AutoPullGroupModel::users($standby);
    			$param['state'] = $auto["state"];
    			$param['skip_verify'] = $skip_verify?true:false;
    			$add_way = QyWechatApi::addContactWay($param);
    			if($add_way["errcode"] != 0)return error("操作失败",$add_way["errcode"]);
    			AutoPullGroupModel::upAuto($add_way,$auto["id"],1);
		    }
			
		}
		return success("操作成功");		
	}



	/*
	* 自动拉群列表
	*/
	public function index(){
		$page = $this->req->param("page")?:1;
		$size = $this->req->param("size")?:10;
		$name = $this->req->param("name");
		$grouping_id = $this->req->param("grouping_id");
		$group_list = AutoPullGroupModel::list($page,$size,$this->req->domain(),$name,$grouping_id);
		$grouping_list = AutoPullGroupModel::groupingList();
		$data["group_list"] = $group_list;
		$data["grouping_list"] = $grouping_list;

		return success("获取成功",$data);
	}

	/*
	* 分组列表
	*/
	public function grouping(){
		$grouping_list = AutoPullGroupModel::groupingList();

		return success("获取成功",$grouping_list);
	}

	/*
	* 批量分组
	*/
	public function groupings(){
		$ids = $this->req->param("ids");
		$grouping_id = $this->req->param("grouping_id");
		if(!$ids || !$grouping_id)return error("参数错误");
		$res = AutoPullGroupModel::groupings($ids,$grouping_id);

		return success("操作成功",$res);
	}

	/**
	* 自动拉群添加分组,修改
	*/
	public function addGrouping(){
		$name = $this->req->param("name");
		$id = $this->req->param("id");
		if(!$name)return error("参数错误");
		$add_way = AutoPullGroupModel::addGrouping($name,$id);
		if(!$add_way)return error("添加失败");
		if($add_way == "error")return error("请勿添加重复分组名称！");
		
		return success("操作成功");
	}

	/**
	* 删除分组
	*/
	public function delGrouping(){
		$id = $this->req->param("id");
		if(!$id)return error("参数错误");
		AutoPullGroupModel::delGrouping($id);

		return success("操作成功");
	}

	//批量修改拉取所选自动拉群集合详情
	public function getBatch(){
		$ids = $this->req->param("ids");
		if(!$ids)return error("参数错误");
		$data = AutoPullGroupModel::infos($ids);

		return success("获取成功",$data);
	}
	//批量修改
	public function batchUpd(){
		$ids = explode(",",$this->req->param("id"));
		$user_set = $this->req->param("user_set");
		$staffs = $this->req->param("staffs");
		$standby = $this->req->param("standby");
		$is_tag = $this->req->param("is_tag");
		$tags = $this->req->param("tags");
		$skip_verify = $this->req->param("skip_verify");
		$is_astrict = $this->req->param("is_astrict");
		$astrict = $this->req->param("astrict");
		$is_group = $this->req->param("is_group");
		$group_text = $this->req->param("group_text");
		if(!count($staffs) && $user_set == 1)return error("参数错误");
		//做批量修改
		$res = AutoPullGroupModel::batchUpd($ids,$user_set,$staffs,$standby,$is_tag,$tags,$skip_verify,$is_astrict,$astrict,$is_group,$group_text);

		return $res;
	}

	//拉群详情
	public function details(){
		$id = $this->req->param("id");
		if(!$id)return error("参数错误");
		$res = AutoPullGroupModel::getInfo($id,"name,join_type,type,create_time,welcome_text,id,is_astrict,state,staffs,standby,tags,upper");
		if($res["is_astrict"] == 1)$res["below"] = AutoPullGroupModel::below($res);
		$res["qrcode"] = AutoPullGroupModel::qrcode(NULL,$res["name"],$res["id"],$this->req->domain());

		return success("获取成功",$res);	
	}

	/*
	* 自动拉群获取已添加的群聊
	*/
	public function getGroups(){
		$res = AutoPullGroupModel::getGroups();

		return success("获取成功",$res);
	}

	public function del(){
		$id = $this->req->param("id");
		if(!$id)return error("参数错误");
		$res = AutoPullGroupModel::info($id);
		if($res["config_id"]){
			$del = QyWechatApi::delContactWay($res["config_id"]);
			if($del["errcode"] != 0)return error("操作失败",$del["errcode"]);
		}
		if($res["standby_config_id"]){
			$del = QyWechatApi::delContactWay($res["standby_config_id"]);
			if($del["errcode"] != 0)return error("操作失败",$del["errcode"]);
		}
		$res = AutoPullGroupModel::del($id);

		return success("操作成功",$res);
	}
}