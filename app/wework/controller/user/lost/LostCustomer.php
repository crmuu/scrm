<?php
namespace app\wework\controller\user\lost;

include "../extend/PHPExcel/php-excel.class.php";
use think\facade\Db;
use app\BaseController;
use app\wework\controller\Base;
use app\wework\model\Basemodel;
use app\wework\model\user\lost\LostCustomerModel;


class LostCustomer extends Base
{
	/*
	* type 为类型 1: 客户删除员工  2:员工删除客户
	*/
    public function index(){
		$type = $this->req->param("type")?:1;
		$page = $this->req->param("page")?:1;
		$size = $this->req->param("size")?:10;
		$staffs = $this->req->param("staffs");
		$add_time = $this->req->param("add_time");
		$lossing_time = $this->req->param("lossing_time");
		$days = $this->req->param("days");
		$tags = $this->req->param("tags");
    	$res = LostCustomerModel::list($page,$size,$type,$staffs,$add_time,$lossing_time,$days,$tags);

    	return success('获取成功',$res);
    }

    public function downexcel(){
    	$type = $this->req->param("type")?:1;
		$page = $this->req->param("page")?:1;
		$size = $this->req->param("size")?:10;
		$wid = $this->req->param("wid");
		$staffs = $this->req->param("staffs");
		if($staffs)$staffs = explode(",",$staffs);
		$add_time = $this->req->param("add_time");
		if($add_time)$add_time = explode(",",$add_time);
		$lossing_time = $this->req->param("lossing_time");
		if($lossing_time)$lossing_time = explode(",",$lossing_time);
		$days = $this->req->param("days");
		if($days)$days = explode(",",$days);
		$tags = $this->req->param("tags");
		if($tags)$tags = explode(",",$tags);
    	$res = LostCustomerModel::list($page,$size,$type,$staffs,$add_time,$lossing_time,$days,$tags,$wid);
        $str = "客户列表";
    	if($type == 1){
    		$str .= "-流失提醒";
    		$arr = [
	            0 => ['客户名称',"所属客户名称","客户标签","流失时间","添加时间","添加员工时长"]
	        ];
	        $i = 1;
	        foreach($res["item"] as $value){
	            $arr[$i]["客户名称"] = $value["name"];
	            $arr[$i]["所属客户名称"] = $value["staff_name"];
	            $arr[$i]["客户标签"] = implode(",", $value["tag_info"]);
	            $arr[$i]["流失时间"] = $value["lossing_time"];
	            $arr[$i]["添加时间"] = $value["add_time"];
	            $arr[$i]["添加员工时长"] = $value["days"];
	            $i++;
	        }
    	}else{
    		$str .= "-删人提醒";
    		$arr = [
	            0 => ['客户名称',"所属客户名称","客户标签","流失时间","添加时间"]
	        ];
	        $i = 1;
	        foreach($res["item"] as $value){
	            $arr[$i]["客户名称"] = $value["name"];
	            $arr[$i]["所属客户名称"] = $value["staff_name"];
	            $arr[$i]["客户标签"] = implode(",", $value["tag_info"]);
	            $arr[$i]["流失时间"] = $value["lossing_time"];
	            $arr[$i]["添加时间"] = $value["add_time"];
	            $i++;
	        }
    	}
        $xls = new \Excel_XML('utf-8', false, '数据导出');
        $xls->addArray($arr);
        $xls->generateXML($str.date('Y-m-d H:i:s'),time());
        exit();
    }

    public function status(){
		$res = LostCustomerModel::getSet();

    	return success('获取成功',$res);
    }

    //status 1为开启，2为关闭
    public function set(){
		$status = $this->req->post("status");
		$res = LostCustomerModel::updates($status);

    	return success('修改成功',$res);
    }
}
