<?php
declare (strict_types = 1);

namespace app\wework\controller\user\staff;

use app\wework\controller\Base;
use app\wework\model\BaseModel;

class Department extends Base 
{
    /**
     * 当前级别部门下额度部门, 不填默认第一级部门
     * @param id 部门id
     * @return \think\Response
     */
    public function list()
    {   
        $id = $this->req->get('id',0); //部门id, 默认0,最顶级部门
        $where = [];
        $where[] = ['wid', '=' ,$this->wid];
        $where[] = ['department_parentid', '=' ,$id];
        $field = 'department_id as id,department_name as name';    //获取字段数据, 数组|字符串
        $res = BaseModel::getAll('kt_wework_department',$where,false,$field);  //获取数据
        foreach ($res as &$department) {
            $department['haschildren'] = BaseModel::count('kt_wework_department',[['wid','=',$this->wid],['department_parentid','=',$department['id']]]) > 0 ? true : false;
        }

        return success('部门数据',$res);
    }
    /**
     * 模糊查询
     * @param name  部门名
     * @return \think\Response
     */
    public function searchList()
    {   
        $name = $this->req->get('name'); //部门名. 模糊查询
        if(!$name) return error('参数错误');
        $where = [];
        $where[] = ['wid', '=' ,$this->wid];
        $where[] = ['department_name', 'like' ,'%'.$name.'%'];
        $field = 'department_id as id,department_name as name';    //获取字段数据, 数组|字符串
        $res = BaseModel::getAll('kt_wework_department',$where,false,$field);  //获取数据
        return success('部门数据',$res);
    }



    /**
     * 显示资源列表.
     *
     * @return \think\Response
     */
    public function index()
    {
        //
    }
    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
