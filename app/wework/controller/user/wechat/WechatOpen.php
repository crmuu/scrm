<?php
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\user\wechat;

use think\facade\Db;
use think\facade\Session;
use app\wework\controller\Base;
use app\wework\model\user\wechat\WechatOpenModel;
use app\wework\model\Wxopenapi;
use app\wework\model\Helper;
use dh2y\qrcode\QRcode;

/*
* 公众号
*/
class WechatOpen extends Base{
	public function index(){
		$list = WechatOpenModel::list();

		return success("获取成功",$list);
	}

	public function set($table){
		$set = Db::query('SHOW TABLES LIKE '."'".$table."'");

		return $set;
	}

	public function rebind(){
		$wid = Session::get("wid");
		$set = $this->set("kt_open_platform_settings");
		if(!$set)return error("操作失败，请先安装开放平台应用！");
		$res = Wxopenapi::rebind($wid,$this->req->domain());

		return success("操作成功",$res);
	}

	public function call(){
		$auth_code = $this->req->get('auth_code');
		$wid = $this->req->get('wid');
		$res = Wxopenapi::getAuthorizerAccessToken($wid,"",$auth_code);
		
		return success("操作成功",$res);
	}

	public function default(){
		$id = $this->req->post("id");
		$res = WechatOpenModel::default($id);
		
		return success("操作成功",$res);
	}
}