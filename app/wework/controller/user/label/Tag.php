<?php 
namespace app\wework\controller\user\label;

use think\facade\Db;
use app\wework\controller\Base;
use app\wework\model\BaseModel;
use app\wework\model\user\label\TagModel;

/**
* 企业标签
**/

class Tag extends Base
{
	
	 /**
     * 标签列表
     * @return \think\Response
     */
     public function index()
     {
          $name = $this->req->post('name');
          $res = TagModel::list($name);
          return success('标签列表',$res);
     }
      /**
     * 添加标签组
     * @return \think\Response
     */
     public function addGroup()
     {    
          $groupName = $this->req->post('groupName');
          $tagName = $this->req->post('tag');
          if(!$groupName || !$tagName) return error('缺少参数');
          $res = TagModel::addGroup($groupName,$tagName);
          if($res != 'ok') return error($res);
          return success('添加标签组成功');
     }
      /**
     * 修改标签组
     * @return \think\Response
     */
     public function updateGroup()
     {    
          $groupName = $this->req->post('groupName');
          $groupId = $this->req->post('groupId');
          $tag = $this->req->post('tag');
          $removetTag = $this->req->post('removetTag');
          if(!$groupId || !$groupName || !$tag ) return error('缺少参数');
          $res = TagModel::updateGroup($groupId,$groupName,$tag,$removetTag);
          if($res != 'ok') return error($res);
          return success('修改标签组成功');
     }
     /**
     * 标签组详情
     * @param  $groupId 标签组id 
     * @return \think\Response
     */
     public function detailGroup(){
          $groupId=$this->req->post('groupId');
          if(!$groupId) return error('参数错误');
          $res = TagModel::detailGroup($groupId);
          return success('标签组详情',$res);
     }
     /**
     * 添加标签
     * @return \think\Response
     */
     public function addTag()
     {    
          $group_id = $this->req->post('group_id');
          $tag_name = $this->req->post('tag_name');
          if(!$group_id || !$tag_name) return error('缺少参数');
          $res = TagModel::addTag($group_id,$tag_name);
          if($res != 'ok') return error($res);
          return success('添加标签组',$res);
     }
     /**
     * 标签组上下移动
     * @param  $type up 上移  down下移
     * @param  $groupId  分组id
     * @return \think\Response
     */
     public function exchageWeborder()
     {    

          $type = $this->req->post('type');  
          $groupId = $this->req->post('groupId');
          if(!$type || !$groupId) return error('参数错误');
          $res = TagModel::exchageWeborder($type,$groupId);
          if($res=='ok') return success('操作成功',$res);
          return error($res);
     }
     /**
     * 删除标签组
     * @param  $groupId  分组id
     * @return \think\Response
     */
     public function deleteGroup()
     {    
          $groupId = $this->req->post('groupId');
          if(!$groupId) return error('参数错误');
          $res = TagModel::deleteGroup($groupId);
          if($res=='ok') return success('操作成功',$res);
          return error($res);
     }

      /**
     * 同步标签列表
     * @param name  部门名
     * @return \think\Response
     */
     public function sync()
     {    
          $res = TagModel::sync();
          if($res == 'ok') return success('同步成功');
          return error($res);
     }
}