<?php
namespace app\wework\controller\user\label;

use think\facade\Db;
use app\wework\controller\Base;
use app\wework\model\Basemodel;
use app\wework\model\user\label\GroupTagModel;

/**
* 企业标签
**/

class GroupTag extends Base
{
	/**
    * 群标签 列表
    * @param page 页码
    */
    public function index(){
        $page = $this->req->param("page")?:1;
        $size = $this->req->param("size")?:10;
    	$res = GroupTagModel::list($page,$size);

    	return success('群标签列表',$res);
    }

    public function tagList(){
        $res = GroupTagModel::tagList();

        return success('群标签列表',$res);
    }


    /*
    *新建标签组
    * group_name 字符串 标签组名称
    * tag_data 数组 是标签组标签名称的数据
    */
    public function addTag(){
        $group_name = $this->req->param("group_name");
        $tag_data = $this->req->param("tag_data");
    	if(!$tag_data || !$group_name) return error("标签组不可为空");
    	$isSet = GroupTagModel::isSet($group_name);
    	if($isSet) return error("标签组名称重复！");
    	$taggroup = GroupTagModel::tagGroup($group_name);
    	$addTags = GroupTagModel::addTags($taggroup,$tag_data);

    	return success('新建标签组',$addTags);
    }

    /*
    * 新建标签
    * group_id 标签组id 
    * tag_name 标签名
    */
    public function add(){
        $tag_id = $this->req->param("tag_id");
        $group_id = $this->req->param("group_id");
        $tag_name = $this->req->param("tag_name");
        $tagSet = GroupTagModel::tagSet($tag_name,$group_id,$tag_id);
        if($tagSet) return error("标签名称重复！");
        $orders = GroupTagModel::orders($group_id,$tag_id);
        $add = GroupTagModel::add($group_id,$tag_name,$orders,$tag_id);
        $data["id"] = $add["id"];
        $data["name"] = $add["name"];
    	return success('修改子标签',$data);
    }

    /*
    * 修改标签组
    * group_id 标签组id 
    * name 标签组名称
    */
    public function updTag(){
        $add_tag = $this->req->post('add_tag');
        $remove_tag = $this->req->post('remove_tag');
        $group_id = $this->req->param("group_id");
        $group_name = $this->req->param("group_name");
        $isSet = GroupTagModel::isSet($group_name,$group_id);
        if($isSet) return error("标签组名称重复！");
        $add = GroupTagModel::updTag($group_name,$group_id,$add_tag,$remove_tag);

    	return success('修改标签组',$add);
    }

    /*
    * 删除标签组
    * group_id 标签组id 
    */
    public function delTagGroup(){
        $group_id = $this->req->param("group_id");
        $delTagGroup = GroupTagModel::delTagGroup($group_id);

    	return success('删除标签组',$delTagGroup);
    }

    /*
    * 删除标签组
    * group_id 标签组id 
    */
    public function delTag(){
        $tag_id = $this->req->param("tag_id");
        $delTag = GroupTagModel::delTag($tag_id);

        return success('删除子标签',$delTag);
    }

    /*
    * 修改拉取数据
    */
    public function info(){
        $id = $this->req->param("id");
        if(!$id) return error("参数错误");
        $info = GroupTagModel::info($id);

        return success('修改拉取数据',$info);
    }

}