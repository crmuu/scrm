<?php 
namespace app\wework\controller\user\feed;

use think\facade\Db;
use app\BaseController;
use app\wework\controller\Base;
use app\wework\model\user\feed\DataFeedModel;


/**
 *  数据订阅
 */
class DataFeed extends Base
{
	/*
	* 数据订阅入口
	*/
	public function index(){
		$feed = DataFeedModel::find();

		return success("获取成功",$feed);
	}

	/**
	* 数据订阅修改或添加
	* @param `customer_total`'客户总数 1: 选中 0:不选种',
	* @param `customer_new`'新增客户数 1: 选中 0:不选种',
	* @param `customer_lossing`'流失客户数  1: 选中 0:不选种',
	* @param `customer_netnew`'净增客户数: 1: 选中 0:不选种',
	* @param `group_total`'客户群总数 1: 选中 0:不选种',
	* @param `group_new`'新增客户群 1: 选中 0:不选种',
	* @param `group_customer_total`'客户群客户总数 1: 选中 0:不选种',
	* @param `group_lossing_total`'客户群流失客户总数 1: 选中 0:不选种',
	* @param `day_push`'每天9:00 ~10:00推送昨日数据 1: 选中 0:不选种',
	* @param `week_push`'每周一9:00 ~10:00推送上周数据 1: 选中 0:不选种',
	* @param `month_push`'每月一号9:00 ~10:00推送上月数据 1: 选中 0:不选种',
	* @param `selfapp_remind`'推送方式:   企微提醒',
	* @param `mail_remind`'推送方式:   邮件提醒',
	* @param `group_customer_new`'客户群新增客户数
	* @param `group_customer_netnew`'客户净增客户数量
	*/
	public function addFeed(){
		$customer_total = $this->req->param("customer_total");
		$customer_new = $this->req->param("customer_new");
		$customer_lossing = $this->req->param("customer_lossing");
		$customer_netnew = $this->req->param("customer_netnew");
		$group_total = $this->req->param("group_total");
		$group_new = $this->req->param("group_new");
		$group_customer_total = $this->req->param("group_customer_total");
		$group_lossing_total = $this->req->param("group_lossing_total");
		$day_push = $this->req->param("day_push");
		$week_push = $this->req->param("week_push");
		$month_push = $this->req->param("month_push");
		$selfapp_remind = $this->req->param("selfapp_remind");
		$mail_remind = $this->req->param("mail_remind");
		$group_customer_new = $this->req->param("group_customer_new");
		$group_customer_netnew = $this->req->param("group_customer_netnew");
		$status = $this->req->param("status");
		$staff_id = $this->req->param("staff_id");
		if($status == 1 && !$day_push && !$week_push && !$month_push) return error("请先选择开启时间段");
		$feed = DataFeedModel::addFeed($customer_total,$customer_new,$customer_lossing,$customer_netnew,$group_total,$group_new,$group_customer_total,$group_lossing_total,$day_push,$week_push,$month_push,$selfapp_remind,$mail_remind,$group_customer_new,$group_customer_netnew,$status,$staff_id);

		return success("获取成功",$feed);
	}


	/*
	* 修改订阅状态
	*/
	public function upFeed(){
		$status = $this->req->param("status");
		$feed = DataFeedModel::find();
		if(!$feed)return error("请先选择数据订阅内容");
		$upFeed = DataFeedModel::upFeed($status);
		if(!$upFeed) return error("没有选择必要的时间点");
		
		return success("修改成功",$upFeed);
	}

}