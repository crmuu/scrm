<?php
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\user\statistics;

use think\facade\Db;
use think\facade\Session;
use app\wework\controller\Base;
use app\wework\model\user\statistics\StatisticsModel;
use app\wework\model\QyWechatApi;
use app\base\model\BaseModel;
use app\wework\model\Helper;
use dh2y\qrcode\QRcode;

/*
* 首页数据统计
*/

class Statistics extends Base{
	//数据统计
	public function index(){
		$wid = Session::get("wid");
        $res = StatisticsModel::getLoginInfo($this->host);
        $info = StatisticsModel::infos($wid);
		$data["customer_size"] = StatisticsModel::size();
		$data["add_customer_size"] = StatisticsModel::addSize();
		$data["del_customer_size"] = StatisticsModel::delSize();
		$data["group_size"] = StatisticsModel::groupSize();
		$data["add_group_size"] = StatisticsModel::addGroupSize();
		$data["del_group_size"] = StatisticsModel::delGroupSize();
		$data["code"] = $res["kf_code"];
		$data["corp_name"] = isset($info["corp_name"])?$info["corp_name"]:"";

		return success("获取成功",$data);		
	}

	//图表
	public function detail(){
		$wid = Session::get("wid");
		$ktime = $this->req->param('ktime')?:date("Y-m-d",time()-3600*24*7);
		$dtime = $this->req->param('dtime')?:date("Y-m-d",time()-3600*24);
		if(date("Y-m-d") <= $dtime)return error("时间超限");
		$res = QyWechatApi::getDepartmentList($wid);
		if($res["errcode"] !== 0)return error("部门列表获取失败",$res["errmsg"]);
		$department = array_column($res["department"], "id");
		$chunk_result = array_chunk($department, 100);
		$count = count($chunk_result);
		$statistics_date = [];
		$new_contact_cnt = [];
		$chat_cnt = [];
		$message_cnt = [];
		$negative_feedback_cnt = [];
		for ($i = 0; $i < $count; $i++) {
		    $statistics = QyWechatApi::getUserBehaviorData([],$chunk_result[$i], strtotime($ktime), strtotime($dtime));
		    if($statistics["errcode"] !== 0)return error("获取失败",$res["errmsg"]);
		    $behavior_data = $statistics['behavior_data'];
		    foreach ($behavior_data as $data) {
		        $date = date('Y-m-d',$data['stat_time']);
		        if(isset($statistics_date[$date])) {
    				$new_contact_cnt[$date] +=$data['new_contact_cnt'];
    				$chat_cnt[$date] +=$data['chat_cnt'];
    				$message_cnt[$date] +=$data['message_cnt'];
    				$negative_feedback_cnt[$date] +=$data['negative_feedback_cnt']?:0;
		        }else{
		            $statistics_date[$date] = $date;
    				$new_contact_cnt[$date] = $data['new_contact_cnt'];
    				$chat_cnt[$date] = $data['chat_cnt'];
    				$message_cnt[$date] = $data['message_cnt'];
    				$negative_feedback_cnt[$date] = $data['negative_feedback_cnt']?:0;
		        }
		    }
		}
		$data = [
			'statistics_date'=> array_values($statistics_date),
			'new_contact_cnt'=>array_values($new_contact_cnt),
			'chat_cnt'=>array_values($chat_cnt),
			'message_cnt'=>array_values($message_cnt),
			'negative_feedback_cnt'=>array_values($negative_feedback_cnt),
		];
		return success("获取成功",$data);
	}
}