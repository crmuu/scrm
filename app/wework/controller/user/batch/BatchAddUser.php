<?php
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\user\batch;

use think\facade\Db;
use think\facade\Session;
use app\wework\controller\Base;
use app\wework\model\user\batch\BatchAddUserModel;
use app\wework\model\Basemodel;
use app\wework\model\QyWechatApi;
use app\wework\model\Helper;

include "../extend/PHPExcel/PHPExcel.php";
include "../extend/PHPExcel/PHPExcel/IOFactory.php";
include "../extend/PHPExcel/PHPExcel/Reader/Excel5.php";

/**
* 批量加好友
* 批量加好友 设置功能未写
* 批量加好友 邮箱发送未写
* 分配好友没做
* 待分配列表没做
* 分配次数没有
* 需要用到 线索管理
**/
class BatchAddUser extends Base 
{
	/*
	* 客户列表
	*/
	public function index(){
        $page = $this->req->param("page")?:1;
        $size = $this->req->param("size")?:10;
        $add_status = $this->req->param("add_status");
        $phone = $this->req->param("phone");
        $getAddUser = BatchAddUserModel::getAddUser($page,$size,$add_status,$phone);
        $getAddUser["excel"] = $this->req->domain()."/static/wework/excel/Excel.xlsx"; 

        return success("获取导入记录成功",$getAddUser);
	}


	/*
	* 导入记录
	*/
	public function histroy(){
        $page = $this->req->param("page")?:1;
        $size = $this->req->param("size")?:10;
        $histroy = BatchAddUserModel::histroy($page,$size);

        return success("获取导入记录成功",$histroy);
	}
	
	/*
	* 删除导入记录
	*/
	public function delBatch(){
        $id = $this->req->param("id");
        $delBatch = BatchAddUserModel::delBatch("kt_wework_batch_addcus",$id);
        
        return success("删除导入记录成功",$delBatch);
	}

	/*
	* 删除导入记录下的客户
	* id 或是数组 批量删除
	*/
	public function delUser(){
        $id = $this->req->param("id");
        $delBatch = BatchAddUserModel::delBatch("kt_wework_batch_addcus_info",$id);
        
        
        return success("删除客户成功",$delBatch);
	}

	/*
	* 添加提醒，按照客户列表提醒
	* id info  表id
	* id 可以是批量提醒 数组形式
	*/
	public function missionBatch(){
		$wid = Session::get("wid");
		$id = $this->req->param("id");
		if(!$id) return error("参数错误");
		if(is_array($id)){
			$infos = BatchAddUserModel::getInfos("kt_wework_batch_addcus_info",$id);
			foreach($infos as $info){
				$staff = Helper::getUser(["id"=>$info["staffid"]]);
				$remindUrl = $this->req->domain().'/wework/h5.BatchAddUser/index?wid='.$wid."&staff_id=".$staff["id"]."&id=".$info["pid"];
				$sendContent = "【管理员提醒】您有客户未添加哦！";
				$sendContent = "<br>";
				$sendContent = "提醒事项：添加客户";
				$sendContent = "<br>";
				$sendContent = "客户数量：".BatchAddUserModel::getCount(["staffid"=>$staff["id"],"pid"=>$info["pid"],"add_status"=>0])."名";
				$sendContent = "<br>";
				$sendContent = "记得及时添加哦";
				$sendContent = "<br>";
				$sendContent = "<a href='{$remindUrl}'>点击查看详情</a>";
				$res = QyWechatApi::sendText($wid,$staff["userid"],$sendContent);
			}
		}else{
			$info = BatchAddUserModel::getBatch("kt_wework_batch_addcus_info",$id);
			$staff = Helper::getUser(["id"=>$info["staffid"]]);
			$remindUrl = $this->req->domain().'/wework/h5.BatchAddUser/index?wid='.$wid."&staff_id=".$staff["id"]."&id=".$info["pid"];
			$sendContent = "【管理员提醒】您有客户未添加哦！";
			$sendContent = "<br>";
			$sendContent = "提醒事项：添加客户";
			$sendContent = "<br>";
			$sendContent = "客户数量：".BatchAddUserModel::getCount(["staffid"=>$staff["id"],"pid"=>$info["pid"],"add_status"=>0])."名";
			$sendContent = "<br>";
			$sendContent = "记得及时添加哦";
			$sendContent = "<br>";
			$sendContent = "<a href='{$remindUrl}'>点击查看详情</a>";
			$res = QyWechatApi::sendText($wid,$staff["userid"],$sendContent);
		}
		return success("操作成功");
	}


	/*
	* 提醒添加 按照导入记录去提醒
	*/
	public function missionNotify(){
		$wid = Session::get("wid");
        $id = $this->req->param("id");
		if(!$id) return error("参数错误");
        $batch = BatchAddUserModel::getBatch("kt_wework_batch_addcus",$id);
        foreach(json_decode($batch["staffid"],1) as $staff_id){
			$staff = Helper::getUser(["id"=>$staff_id]);
			$add_size = BatchAddUserModel::getCount(["staffid"=>$staff_id,"pid"=>$batch["id"],"add_status"=>1]);
			$size = BatchAddUserModel::getCount(["staffid"=>$staff_id,"pid"=>$batch["id"]]);
			if($add_size == $size) continue;
			$remindUrl = $this->req->domain().'/wework/h5.BatchAddUser/index?wid='.$wid."&staff_id=".$staff_id."&id=".$batch["id"];
			$sendContent = "【管理员提醒】您有客户未添加哦！";
			$sendContent = "<br>";
			$sendContent = "提醒事项：添加客户";
			$sendContent = "<br>";
			$sendContent = "客户数量：".BatchAddUserModel::getCount(["staffid"=>$staff_id,"pid"=>$batch["id"],"add_status"=>0])."名";
			$sendContent = "<br>";
			$sendContent = "记得及时添加哦";
			$sendContent = "<br>";
			$sendContent = "<a href='{$remindUrl}'>点击查看详情</a>";
			$res = QyWechatApi::sendText($wid,$staff["userid"],$sendContent);
		}
        return success("操作成功");
	}

	/**
	* 导入记录详情
	*/
	public function histroyDetail(){
        $id = $this->req->param("id");
        $phone = $this->req->param("phone");
        $status = $this->req->param("status");
		if(!$id) return error("参数错误");
		$histroyDetail = BatchAddUserModel::histroyDetail($id,$phone,$status);

		return success("获取成功",$histroyDetail);
	}

	/**
	* 添加批量加好友
	* 须上传的文件为xlsx 内容为文本形式
	* file 文件路径
	* fileName 文件名称
	* staffs 选择员工列表
	* tags 标签列表
	*/
 	public function addBatch(){
		$wid = Session::get("wid");
        $file_url = $this->req->param("file_url");
        $url = str_replace($this->req->domain(),".",$file_url);
        $file_name = $this->req->param("file_name");
        $staffs = $this->req->param("staffs");
        $tags = $this->req->param("tags");
        $status = $this->req->param("status")?:0;
        $objReader = new \PHPExcel_Reader_Excel2007();
	    $objPHPExcel = $objReader->load($url);
	    $sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow(); // 取得总行数
		$friends = [];
		// 从第六行开始
		for($j=6;$j<=$highestRow;$j++) {
			$phone = $objPHPExcel->getActiveSheet()->getCell("A".$j)->getValue();//获取A列的值
			$remarks = $objPHPExcel->getActiveSheet()->getCell("B".$j)->getValue()?:NULL;//获取B列的值;
			if($phone){
				$friends[$phone] = $remarks;
			}
		}
		if(count($friends)){
			$phones = array_keys($friends);
			$size = $highestRow - 5;
			$count = BatchAddUserModel::getCount(["wid"=>$wid,"phone"=>$phones]);
			if($count >= $size) return error("文档中的客户与已导入客户重复，请重新选择");
			$addBatch = BatchAddUserModel::addBatch($file_name,$staffs,$tags,$friends);
			foreach($staffs as $staff_id){
				$staff = Helper::getUser(["id"=>$staff_id]);
				$remindUrl = $this->req->domain().'/wework/h5.BatchAddUser/index?wid='.$wid."&staff_id=".$staff_id."&id=".$addBatch;
				$sendContent = "【管理员提醒】您有客户未添加哦！";
				$sendContent = "<br>";
				$sendContent = "提醒事项：添加客户";
				$sendContent = "<br>";
				$sendContent = "客户数量：".BatchAddUserModel::getCount(["staffid"=>$staff_id,"pid"=>$addBatch])."名";
				$sendContent = "<br>";
				$sendContent = "记得及时添加哦";
				$sendContent = "<br>";
				$sendContent = "<a href='{$remindUrl}'>点击查看详情</a>";
				$res = QyWechatApi::sendText($wid,$staff["userid"],$sendContent);
			}
		}else{
			unlink($url);
			return error("文件没有内容");
		}
		unlink($url);
		if($count < $size) return success("导入成功，与已导入客户列表有".$count."个重复客户");
        return success("添加成功",$res);
	}

	//数据统计
	public function statistics(){
        $staff_ids = $this->req->param("staff_ids");
        $time = $this->req->param("time");
		$res = BatchAddUserModel::statistics($time,$staff_ids);

		return success("获取成功",$res);
	}

	//数据统计客户详情
	public function details(){
        $staffid = $this->req->param("staffid");
		$res = BatchAddUserModel::details($staffid);

		return success("获取成功",$res);
	}
}