<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\user\customized;
use think\facade\Db;
use app\wework\controller\Base;
use app\wework\model\user\customized\SettingModel;
use think\facade\Session;

class Setting extends Base
{
	/*
	* 自定义信息列表
	*/
	public function index(){
		$page = $this->req->param("page")?:1;
		$size = $this->req->param("size")?:10;
		$data = SettingModel::list($page,$size);

		return success('自定义信息列表获取成功',$data);
	}

	/*
	* 自定义信息 修改状态
	*/
	public function updateStatus(){
		$id = $this->req->param("id");
		$status = $this->req->param("status");
		if(!$id) return error("参数错误");
		$data = SettingModel::updateStatus($id,$status);

		return success('自定义信息状态修改成功',$data);
	}

	/*
	* 自定义信息 排序
	*/
	public function updateStot(){
		$id = $this->req->param("id");
		$sort = $this->req->param("sort");
		if(!$id || !$sort) return error("参数错误");
		$data = SettingModel::updateStot($id,$sort);
		return success('自定义信息排序成功',$data);
	}

	/*
	* 自定义信息 添加
	*/
	public function save(){
		$id = $this->req->param("id");
		$name = $this->req->param("name");
		$attr = $this->req->param("attr");
        $content = $this->req->param("content");
        if($attr == 2 && !$content) return error("请输入选择项");
        if(!$name) return error("名称不可为空");
        $data = SettingModel::save($id,$name,$attr,$content);
		if(!is_int($data)) return error($data);
		return success('操作成功',$data);
	}

	/*
	* 自定义信息 删除
	*/
	public function delete(){
		$id = $this->req->param("id");
		if(!$id) return error("参数不全");
        $data = SettingModel::delete($id);
        
		return success('自定义信息删除成功',$data);
	}
}