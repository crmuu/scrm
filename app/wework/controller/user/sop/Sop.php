<?php
declare (strict_types = 1);

namespace app\wework\controller\user\sop;
use think\facade\Db;
use app\wework\controller\Base;
use app\wework\model\BaseModel;
use app\wework\model\user\sop\SopModel;
use app\wework\model\QyWechatApi;
use think\facade\Session;

class Sop extends Base
{
	public function index()
	{
		return SopModel::list($this->req);
	}
	public function detail()
	{
		return SopModel::detail($this->req);
	}
	public function delte()
	{
		return SopModel::delete($this->req);
	}
	public function add()
	{
		return SopModel::add($this->req);
	}
	public function open()
	{
		return SopModel::open($this->req);
	}
	public function festival()
	{
		return SopModel::festival();
	}

}