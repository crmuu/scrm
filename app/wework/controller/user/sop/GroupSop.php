<?php
declare (strict_types = 1);

namespace app\wework\controller\user\sop;
use think\facade\Db;
use app\wework\controller\Base;
use app\wework\model\BaseModel;
use app\wework\model\user\sop\GroupSopModel;
use app\wework\model\QyWechatApi;
use think\facade\Session;

class GroupSop extends Base
{
	public function index()
	{
		return GroupSopModel::list($this->req);
	}

	public function detail()
	{
		return GroupSopModel::detail($this->req);
	}

	public function delte()
	{
		return GroupSopModel::delete($this->req);
	}

	public function add()
	{
		return GroupSopModel::add($this->req);
	}

	public function open()
	{
		return GroupSopModel::open($this->req);
	}

	public function size(){
		$type = $this->req->param("type");
		$tags = $this->req->param("tags");
		$ktime = $this->req->param("ktime");
		$times = $this->req->param("times");
		if($type == 1)$size = GroupSopModel::size($tags);
		if($type == 2)$size = GroupSopModel::sizes($ktime,$times);

		return success("获取成功",$size);
	}
}