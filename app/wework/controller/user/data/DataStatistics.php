<?php
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\user\data;

use think\facade\Db;
use app\BaseController;
use app\wework\controller\Base;
use think\facade\Session;
use app\wework\model\QyWechatApi;
use app\wework\model\Basemodel;
use app\wework\model\user\statistics\StatisticsModel;
use app\wework\model\user\data\DataStatisticsModel;
include "../extend/PHPExcel/php-excel.class.php";


class DataStatistics extends Base
{
	/*
	* 客户统计 今日增长数据
	*/
	public function customer(){
		$wid = Session::get("wid");
		$data["wid"] = $wid;
		$data["customer_size"] = DataStatisticsModel::addCustomer($wid,date("Y-m-d"),date("Y-m-d H:i:s"));
		$data["add_customer_size"] = DataStatisticsModel::addCustomer($wid,date("Y-m-d"),date("Y-m-d H:i:s"),1);
		$data["del_customer_size"] = DataStatisticsModel::delCustomer($wid,date("Y-m-d"),date("Y-m-d H:i:s"));
		$data["add_size"] = DataStatisticsModel::addCustomerSize($wid,date("Y-m-d"),date("Y-m-d H:i:s"));
		return success("获取成功",$data);
	}


	/*
	* 客户统计，导出数据
	**/
	public function customerDownexcel(){
		$ktime = $this->req->param('ktime')?:date("Y-m-d",time()-3600*24*7);
		$dtime = $this->req->param('dtime')?:date("Y-m-d");
		$wid = $this->req->param('wid');
		$staffs = $this->req->param('staffs');
		if($staffs)$staffs = explode(",",$staffs);
		$type = $this->req->param('type');
		if(!$type || !$wid)return error("参数错误");
		if($type == "times"){
			$time = strtotime($dtime) - strtotime($ktime);
			$time_tow = $ktime;
			$arr = [
	            0 => ['日期', '客户总数', '新增客户数','流失客户数',"净增客户数"]
	        ];
			for ($i=0;$i<=$time/(3600*24);$i++){
				$time_one = date("Y-m-d",strtotime($time_tow));
				$time_tow = date("Y-m-d",strtotime($time_tow)+3600*24);
				$arr[$i+1]["日期"] = $time_one;
				$arr[$i+1]["客户总数"] = DataStatisticsModel::addCustomer($wid,$time_one,$time_tow,$staffs);
				$arr[$i+1]["新增客户数"] = DataStatisticsModel::addCustomer($wid,$time_one,$time_tow,$staffs,1);
				$arr[$i+1]["流失客户数"] = DataStatisticsModel::delCustomer($wid,$time_one,$time_tow,$staffs);
				$arr[$i+1]["净增客户数"] = DataStatisticsModel::addCustomerSize($wid,$time_one,$time_tow,$staffs);
			}

		}else if($type == "staff"){
			$arr = [
	            0 => ['成员名称', '客户总数', '新增客户数','流失客户数',"净增客户数"]
	        ];
			$data = DataStatisticsModel::getStaffs($staffs,$wid);
			$i = 1;
			foreach($data as $key=>$staff){
				$arr[$i]["成员名称"] = $staff["name"];
				$arr[$i]["客户总数"] = DataStatisticsModel::addCustomer($wid,$ktime,$dtime,$staff["id"]);
				$arr[$i]["新增客户数"] = DataStatisticsModel::addCustomer($wid,$ktime,$dtime,$staff["id"],1);
				$arr[$i]["流失客户数"] = DataStatisticsModel::delCustomer($wid,$ktime,$dtime,$staff["id"]);
				$arr[$i]["净增客户数"] = DataStatisticsModel::addCustomerSize($wid,$ktime,$dtime,$staff["id"]);
				$i++;
			}
		}
        $str = "客户统计-";
        if($type == "times")$str .= "时间统计";
        if($type == "staff")$str .= "成员统计";
        $xls = new \Excel_XML('utf-8', false, '数据导出');
        $xls->addArray($arr);
        $xls->generateXML($str.date('Y-m-d H:i:s'),time());
        exit();
	}
	
	/*
	* 表格数据
	**/
	public function customerTable(){
		$wid = $this->req->param('wid');
		$ktime = $this->req->param('ktime')?:date("Y-m-d",time()-3600*24*7);
		$dtime = $this->req->param('dtime')?:date("Y-m-d");
		$staffs = $this->req->param('staffs');
		$type = $this->req->param('type');
		if(!$type)return error("参数错误");
		if($type == "times"){
			$time = strtotime($dtime) - strtotime($ktime);
			$time_tow = $ktime;
			$data = [];
			for ($i=0;$i<=$time/(3600*24);$i++){
				$time_one = date("Y-m-d",strtotime($time_tow));
				$time_tow = date("Y-m-d",strtotime($time_tow)+3600*24);
				$data[$i]["time"] = $time_one;
				$data[$i]["size_all"] = DataStatisticsModel::addCustomer($wid,$time_one,$time_tow,$staffs);
				$data[$i]["add_size"] = DataStatisticsModel::addCustomer($wid,$time_one,$time_tow,$staffs,1);
				$data[$i]["del_size"] = DataStatisticsModel::delCustomer($wid,$time_one,$time_tow,$staffs);
				$data[$i]["customer_size"] = DataStatisticsModel::addCustomerSize($wid,$time_one,$time_tow,$staffs);
			}
		}else if($type == "staff"){
			$data = DataStatisticsModel::getStaffs($staffs);
			foreach($data as $key=>$staff){
				$data[$key]["size_all"] = DataStatisticsModel::addCustomer($wid,$ktime,$dtime,$staff["id"]);
				$data[$key]["add_size"] = DataStatisticsModel::addCustomer($wid,$ktime,$dtime,$staff["id"],1);
				$data[$key]["del_size"] = DataStatisticsModel::delCustomer($ktime,$dtime,$staff["id"]);
				$data[$key]["customer_size"] = DataStatisticsModel::addCustomerSize($ktime,$dtime,$staff["id"]);
			}
		}

		return success("获取成功",$data);
	}


	/*
	* type day 今天的数据，week，一周内数据，month 一月内数据 趋势图 分类
	**/
	public function customerDetail(){
		$wid = $this->req->param('wid');
		$type = $this->req->param('type');
		$date_type = $this->req->param('date_type');
		$staffs = $this->req->param('staffs');
		if(!$type)return error("参数错误");
		if($date_type == "day"){
			$ktime = date("Y-m-d"); 
			$dtime = date("Y-m-d");
		}else if($date_type == "week"){
			$ktime = date("Y-m-d",time()-3600*24*7);
			$dtime = date("Y-m-d");
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 months",time()));
			$dtime = date("Y-m-d");
		}else{
			$ktime = $this->req->param('ktime')?:date("Y-m-d",time()-3600*24*7);
			$dtime = $this->req->param('dtime')?:date("Y-m-d");
		}
		$time = strtotime($dtime) - strtotime($ktime);
		$time_tow = $ktime;
		$data = [];
		for ($i=0;$i<=$time/(3600*24);$i++){
			$time_one = date("Y-m-d",strtotime($time_tow));
			$time_tow = date("Y-m-d",strtotime($time_tow)+3600*24);
			$data[$i]["time"] = $time_one;
			if($type == 1)$data[$i]["size"] = DataStatisticsModel::addCustomer($wid,$time_one,$time_tow,$staffs);
			if($type == 2)$data[$i]["size"] = DataStatisticsModel::addCustomer($wid,$time_one,$time_tow,$staffs,1);
			if($type == 3)$data[$i]["size"] = DataStatisticsModel::delCustomer($wid,$time_one,$time_tow,$staffs);
			if($type == 4)$data[$i]["size"] = DataStatisticsModel::addCustomerSize($wid,$time_one,$time_tow,$staffs);
		}

		return success("获取成功",$data);
	}

	public function group(){
		$ktime = date("Y-m-d",strtotime("-1 day"));
		$dtime = date("Y-m-d");
		$wid = Session::get("wid");
		$data["wid"] = $wid;
		$data["size"] = DataStatisticsModel::groupSize($ktime,$dtime);
		$data["add_customer_size"] = DataStatisticsModel::groupSize($ktime,$dtime,1,2);
		$data["add_size"] = DataStatisticsModel::groupSize($ktime,$dtime,1);
		$data["del_size"] = DataStatisticsModel::groupSize($ktime,$dtime,0);
		$data["add_group_size"] = DataStatisticsModel::addGroupSize($ktime,$dtime);

		return success("获取成功",$data);
	}

	public function groupDetail(){
		$wid = Session::get("wid");
		$date_type = $this->req->param('date_type');
		$staffs = $this->req->param('staffs');
		if($date_type == "day"){
			$ktime = date('Y-m-d',strtotime("-1 day"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else if($date_type == "week"){
			$ktime = date('Y-m-d',strtotime("-1 week"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 month"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else{
			$ktime = $this->req->param('ktime')?:date('Y-m-d',strtotime("-1 week"));
			$dtime = $this->req->param('dtime')?:date('Y-m-d',strtotime("-1 day"));
		}
		if($ktime >= date("Y-m-d"))return error("请选择昨天及以前的时间");
		if(strtotime($ktime)-strtotime($dtime) > 3600*24*31)return error("最大跨度为三十一天");
		$staffs = DataStatisticsModel::getUserids($staffs);
		$staffs = array_chunk($staffs,100);
		$datas = [];
		for($x=0;$x<count($staffs);$x++){
			$res = QyWechatApi::groupBehavior($wid,$staffs[$x], strtotime($ktime), strtotime($dtime));
			if($res['errcode'] != 0)return error('当前时间段无数据',$res['errcode']);
			foreach ($res['items'] as $data) {
				$date = date('Y-m-d',$data['stat_time']);
				if(isset($datas[$date])){
					$datas[$date]["new_chat_cnt"] += $data["data"]['new_chat_cnt'];
					$datas[$date]["chat_total"] += $data["data"]['chat_total'];
					$datas[$date]["chat_has_msg"] += $data["data"]['chat_has_msg'];
					$datas[$date]["new_member_cnt"] += $data["data"]['new_member_cnt'];
					$datas[$date]["member_total"] += $data["data"]['member_total'];
					$datas[$date]["member_has_msg"] += $data["data"]['member_has_msg'];
					$datas[$date]["msg_total"] += $data["data"]['msg_total'];
				} else {
					$datas[$date]["time"] = $date;
					$datas[$date]["new_chat_cnt"] = $data["data"]['new_chat_cnt'];
					$datas[$date]["chat_total"] = $data["data"]['chat_total'];
					$datas[$date]["chat_has_msg"] = $data["data"]['chat_has_msg'];
					$datas[$date]["new_member_cnt"] = $data["data"]['new_member_cnt'];
					$datas[$date]["member_total"] = $data["data"]['member_total'];
					$datas[$date]["member_has_msg"] = $data["data"]['member_has_msg'];
					$datas[$date]["msg_total"] = $data["data"]['msg_total'];
				}
			}
		}
		$datas = array_values($datas);

		return success("获取成功",$datas);
	}

	public function getDepartment(){
		$department_list = DataStatisticsModel::departments();

		return success("获取成功",$department_list);
	}

	public function groupDepartment(){
		$wid = Session::get("wid");
		$date_type = $this->req->param('date_type');
		$department = $this->req->param('department');
		if($date_type == "day"){
			$ktime = date('Y-m-d',strtotime("-1 day"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else if($date_type == "week"){
			$ktime = date('Y-m-d',strtotime("-1 week"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 month"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else{
			$ktime = $this->req->param('ktime')?:date('Y-m-d',strtotime("-1 week"));
			$dtime = $this->req->param('dtime')?:date('Y-m-d',strtotime("-1 day"));
		}
		$department_list = DataStatisticsModel::departments($department);
		foreach($department_list as $key=>$department){
			$staff_list = DataStatisticsModel::staffList($department["department_id"]);
			$res = QyWechatApi::groupUserBehavior($wid,$staff_list, strtotime($ktime), strtotime($dtime));
			if($res["errcode"] != 0)return error("接口错误",$res["errcode"]);
			$item = $res["items"];
			$department_list[$key]["new_chat_cnt"]= 0;
			$department_list[$key]["chat_total"]= 0;
			$department_list[$key]["chat_has_msg"]= 0;
			$department_list[$key]["new_member_cnt"]= 0;
			$department_list[$key]["member_total"]= 0;
			$department_list[$key]["member_has_msg"]= 0;
			$department_list[$key]["msg_total"]= 0;
			foreach($item as $value){
				$department_list[$key]["new_chat_cnt"] += $value["data"]['new_chat_cnt'];
				$department_list[$key]["chat_total"] += $value["data"]['chat_total'];
				$department_list[$key]["chat_has_msg"] += $value["data"]['chat_has_msg'];
				$department_list[$key]["new_member_cnt"] += $value["data"]['new_member_cnt'];
				$department_list[$key]["member_total"] += $value["data"]['member_total'];
				$department_list[$key]["member_has_msg"] += $value["data"]['member_has_msg'];
				$department_list[$key]["msg_total"] += $value["data"]['msg_total'];
			}
		}

		return success("获取成功",$department_list);
	}

	//客户群*-导出Excel
	public function groupDownexcel(){
		$wid = $this->req->param('wid');
		$type = $this->req->param('type');
		$date_type = $this->req->param('date_type');
		$department = $this->req->param('department');
		$staffs = $this->req->param('staffs');
		if($staffs)$staffs = explode(",",$staffs);
		if($date_type == "day"){
			$ktime = date('Y-m-d',strtotime("-1 day"));
			$dtime = date("Y-m-d");
		}else if($date_type == "week"){
			$ktime = date('Y-m-d',strtotime("-1 week"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 month"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else{
			$ktime = $this->req->param('ktime')?:date('Y-m-d',strtotime("-1 week"));
			$dtime = $this->req->param('dtime')?:date('Y-m-d',strtotime("-1 day"));
		}
		if($ktime >= date("Y-m-d"))return error("请选择昨天及以前的时间");
		if(strtotime($ktime)-strtotime($dtime) > 3600*24*31)return error("最大跨度为三十一天");
		$datas = [];
		if($type == "times"){
			$staffs = DataStatisticsModel::getUserids($staffs,"",$wid);
			$staffs = array_chunk($staffs,100);
			for($x=0;$x<count($staffs);$x++){
				$res = QyWechatApi::groupBehavior($wid,$staffs[$x], strtotime($ktime), strtotime($dtime));
				if($res['errcode'] != 0)return error('当前时间段无数据',$res['errcode']);
				foreach ($res['items'] as $data) {
					$date = date('Y-m-d',$data['stat_time']);
					if(isset($datas[$date])){
						$datas[$date]["new_chat_cnt"] += $data["data"]['new_chat_cnt'];
						$datas[$date]["chat_total"] += $data["data"]['chat_total'];
						$datas[$date]["chat_has_msg"] += $data["data"]['chat_has_msg'];
						$datas[$date]["new_member_cnt"] += $data["data"]['new_member_cnt'];
						$datas[$date]["member_total"] += $data["data"]['member_total'];
						$datas[$date]["member_has_msg"] += $data["data"]['member_has_msg'];
						$datas[$date]["msg_total"] += $data["data"]['msg_total'];
					} else {
						$datas[$date]["time"] = $date;
						$datas[$date]["new_chat_cnt"] = $data["data"]['new_chat_cnt'];
						$datas[$date]["chat_total"] = $data["data"]['chat_total'];
						$datas[$date]["chat_has_msg"] = $data["data"]['chat_has_msg'];
						$datas[$date]["new_member_cnt"] = $data["data"]['new_member_cnt'];
						$datas[$date]["member_total"] = $data["data"]['member_total'];
						$datas[$date]["member_has_msg"] = $data["data"]['member_has_msg'];
						$datas[$date]["msg_total"] = $data["data"]['msg_total'];
					}
				}
			}
			$arr = [
	            0 => ['日期', '新增客户群数量', '截至当天客户群总数量','截至当天有发过消息的客户群数量','客户群新增群人数','截至当天客户群总人数','截至当天有发过消息的群成员数','截至当天客户群消息总数',]
	        ];
	        $i = 1;
	        foreach($datas as $value){
	        	$arr[$i]["日期"] = $value["time"];
	        	$arr[$i]["新增客户群数量"] = $value["new_chat_cnt"];
	        	$arr[$i]["截至当天客户群总数量"] = $value["chat_total"];
	        	$arr[$i]["截至当天有发过消息的客户群数量"] = $value["chat_has_msg"];
	        	$arr[$i]["客户群新增群人数"] = $value["new_member_cnt"];
	        	$arr[$i]["截至当天客户群总人数"] = $value["member_total"];
	        	$arr[$i]["截至当天有发过消息的群成员数"] = $value["member_has_msg"];
	        	$arr[$i]["截至当天客户群消息总数"] = $value["msg_total"];
	        	$i++;
	        }
		}else if($type == "staff"){
			$arr = [
	            0 => ['员工名称', '新增客户群数量', '截至当天客户群总数量','截至当天有发过消息的客户群数量','客户群新增群人数','截至当天客户群总人数','截至当天有发过消息的群成员数','截至当天客户群消息总数',]
	        ];
	        $i = 1;
			$datas = DataStatisticsModel::getUserids($staffs,"avatar,name,userid",$wid);
			foreach($datas as $key=>$staff){
				$res = QyWechatApi::groupUserBehavior($wid,$staff["userid"], strtotime($ktime), strtotime($dtime));
				if($res['errcode'] != 0)return error('当前时间段无数据',$res['errcode']);
				$arr[$i]["员工名称"] = $staff["name"];
				$arr[$i]["新增客户群数量"] = $res["items"][0]["data"]["new_chat_cnt"];
	        	$arr[$i]["截至当天客户群总数量"] = $res["items"][0]["data"]["chat_total"];
	        	$arr[$i]["截至当天有发过消息的客户群数量"] = $res["items"][0]["data"]["chat_has_msg"];
	        	$arr[$i]["客户群新增群人数"] = $res["items"][0]["data"]["new_member_cnt"];
	        	$arr[$i]["截至当天客户群总人数"] = $res["items"][0]["data"]["member_total"];
	        	$arr[$i]["截至当天有发过消息的群成员数"] = $res["items"][0]["data"]["member_has_msg"];
	        	$arr[$i]["截至当天客户群消息总数"] = $res["items"][0]["data"]["msg_total"];
	        	$i++;
			}
		}else if($type == "department"){
			$arr = [
	            0 => ['部门名称', '新增客户群数量', '截至当天客户群总数量','截至当天有发过消息的客户群数量','客户群新增群人数','截至当天客户群总人数','截至当天有发过消息的群成员数','截至当天客户群消息总数',]
	        ];
			$i = 1;
	        if($department)$department = explode(",",$department);
			$datas = DataStatisticsModel::departments($department,$wid);
			foreach($datas as $key=>$department){
				$staff_list = DataStatisticsModel::staffList($department["department_id"]);
				$res = QyWechatApi::groupUserBehavior($wid,$staff_list, strtotime($ktime), strtotime($dtime));
				if($res["errcode"] != 0)return error("接口错误",$res["errcode"]);
				$item = $res["items"];
				$datas[$key]["new_chat_cnt"]= 0;
				$datas[$key]["chat_total"]= 0;
				$datas[$key]["chat_has_msg"]= 0;
				$datas[$key]["new_member_cnt"]= 0;
				$datas[$key]["member_total"]= 0;
				$datas[$key]["member_has_msg"]= 0;
				$datas[$key]["msg_total"]= 0;
				foreach($item as $value){
					$datas[$key]["new_chat_cnt"] += $value["data"]['new_chat_cnt'];
					$datas[$key]["chat_total"] += $value["data"]['chat_total'];
					$datas[$key]["chat_has_msg"] += $value["data"]['chat_has_msg'];
					$datas[$key]["new_member_cnt"] += $value["data"]['new_member_cnt'];
					$datas[$key]["member_total"] += $value["data"]['member_total'];
					$datas[$key]["member_has_msg"] += $value["data"]['member_has_msg'];
					$datas[$key]["msg_total"] += $value["data"]['msg_total'];
				}
				$arr[$i]["部门名称"] = $department["department_name"];
	        	$arr[$i]["新增客户群数量"] = $$datas[$key]["new_chat_cnt"];
	        	$arr[$i]["截至当天客户群总数量"] = $$datas[$key]["chat_total"];
	        	$arr[$i]["截至当天有发过消息的客户群数量"] = $$datas[$key]["chat_has_msg"];
	        	$arr[$i]["客户群新增群人数"] = $$datas[$key]["new_member_cnt"];
	        	$arr[$i]["截至当天客户群总人数"] = $$datas[$key]["member_total"];
	        	$arr[$i]["截至当天有发过消息的群成员数"] = $$datas[$key]["member_has_msg"];
	        	$arr[$i]["截至当天客户群消息总数"] = $$datas[$key]["msg_total"];
	        	$i++;
			}
			$i = 1;
		}

		$str = "客户群统计-";
        if($type == "times")$str .= "时间统计";
        if($type == "staff")$str .= "成员统计";
        if($type == "department")$str .= "部门统计";
        $xls = new \Excel_XML('utf-8', false, '数据导出');
        $xls->addArray($arr);
        $xls->generateXML($str.date('Y-m-d H:i:s'),time());
        exit();
	}

	//群数据统计
	public function groupTable(){
		$wid = Session::get("wid");
		$type = $this->req->param('type');
		$date_type = $this->req->param('date_type');
		$staffs = $this->req->param('staffs');
		if($date_type == "day"){
			$ktime = date('Y-m-d',strtotime("-1 day"));
			$dtime = date("Y-m-d");
		}else if($date_type == "week"){
			$ktime = date('Y-m-d',strtotime("-1 week"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 month"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else{
			$ktime = $this->req->param('ktime')?:date('Y-m-d',strtotime("-1 week"));
			$dtime = $this->req->param('dtime')?:date('Y-m-d',strtotime("-1 day"));
		}
		if($ktime >= date("Y-m-d"))return error("请选择昨天及以前的时间");
		if(strtotime($ktime)-strtotime($dtime) > 3600*24*31)return error("最大跨度为三十一天");
		$datas = [];
		if($type == "times"){
			$staffs = DataStatisticsModel::getUserids($staffs);
			$staffs = array_chunk($staffs,100);
			for($x=0;$x<count($staffs);$x++){
				$res = QyWechatApi::groupBehavior($wid,$staffs[$x], strtotime($ktime), strtotime($dtime));
				if($res['errcode'] != 0)return error('当前时间段无数据',$res['errcode']);
				foreach ($res['items'] as $data) {
					$date = date('Y-m-d',$data['stat_time']);
					if(isset($datas[$date])){
						$datas[$date]["new_chat_cnt"] += $data["data"]['new_chat_cnt'];
						$datas[$date]["chat_total"] += $data["data"]['chat_total'];
						$datas[$date]["chat_has_msg"] += $data["data"]['chat_has_msg'];
						$datas[$date]["new_member_cnt"] += $data["data"]['new_member_cnt'];
						$datas[$date]["member_total"] += $data["data"]['member_total'];
						$datas[$date]["member_has_msg"] += $data["data"]['member_has_msg'];
						$datas[$date]["msg_total"] += $data["data"]['msg_total'];
					} else {
						$datas[$date]["time"] = $date;
						$datas[$date]["new_chat_cnt"] = $data["data"]['new_chat_cnt'];
						$datas[$date]["chat_total"] = $data["data"]['chat_total'];
						$datas[$date]["chat_has_msg"] = $data["data"]['chat_has_msg'];
						$datas[$date]["new_member_cnt"] = $data["data"]['new_member_cnt'];
						$datas[$date]["member_total"] = $data["data"]['member_total'];
						$datas[$date]["member_has_msg"] = $data["data"]['member_has_msg'];
						$datas[$date]["msg_total"] = $data["data"]['msg_total'];
					}
				}
			}
			$datas = array_values($datas);
		}else if($type == "staff"){
			$datas = DataStatisticsModel::getUserids($staffs,"avatar,name,userid");
			foreach($datas as $key=>$staff){
				$res = QyWechatApi::groupUserBehavior($wid,$staff["userid"], strtotime($ktime), strtotime($dtime));
				if($res['errcode'] != 0)return error('当前时间段无数据',$res['errcode']);
				$datas[$key]["new_chat_cnt"] = $res["items"][0]["data"]["new_chat_cnt"];
				$datas[$key]["chat_total"] = $res["items"][0]["data"]["chat_total"];
				$datas[$key]["chat_has_msg"] = $res["items"][0]["data"]["chat_has_msg"];
				$datas[$key]["new_member_cnt"] = $res["items"][0]["data"]["new_member_cnt"];
				$datas[$key]["member_total"] = $res["items"][0]["data"]["member_total"];
				$datas[$key]["member_has_msg"] = $res["items"][0]["data"]["member_has_msg"];
				$datas[$key]["msg_total"] = $res["items"][0]["data"]["msg_total"];
			}
		}

		return success("获取成功",$datas);
	}

	public function staff(){
		$wid = Session::get("wid");
		$date_type = $this->req->param('date_type');
		if($date_type == "day"){
			$ktime = date('Y-m-d',strtotime("-1 day"));
			$dtime = date("Y-m-d");
		}else if($date_type == "week"){
			$ktime = date('Y-m-d',strtotime("-1 week"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 month"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else{
			$ktime = $this->req->param('ktime')?:date('Y-m-d',strtotime("-1 week"));
			$dtime = $this->req->param('dtime')?:date('Y-m-d',strtotime("-1 day"));
		}
		$staffs = $this->req->param('staffs');
		$staffs = DataStatisticsModel::getUserids($staffs);
		if(strtotime($ktime)-strtotime($dtime) > 3600*24*31)return error("最大跨度为三十一天");
		$staffs = array_chunk($staffs,100);
		for($x=0;$x<count($staffs);$x++){
			$res = QyWechatApi::getUserBehaviorData($staffs[$x],[],strtotime($ktime),strtotime($dtime));
			if($res['errcode'] != 0)return error('当前时间段无数据',$res['errcode']);
			foreach ($res['behavior_data'] as $behavior_data) {
				if(isset($data["chat_cnt"])){
					$data["chat_cnt"] += $behavior_data["chat_cnt"];
					$data["message_cnt"] += $behavior_data["message_cnt"];
					$data["new_apply_cnt"] += $behavior_data["new_apply_cnt"];
					$data["new_contact_cnt"] += $behavior_data["new_contact_cnt"];
					$data["negative_feedback_cnt"] += $behavior_data["negative_feedback_cnt"];
				}else{
					$data["chat_cnt"] = $behavior_data["chat_cnt"];
					$data["message_cnt"] = $behavior_data["message_cnt"];
					$data["new_apply_cnt"] = $behavior_data["new_apply_cnt"];
					$data["new_contact_cnt"] = $behavior_data["new_contact_cnt"];
					$data["negative_feedback_cnt"] = $behavior_data["negative_feedback_cnt"];
				}
				
			}
		}
		$data["wid"] = $wid;

		return success("获取成功",$data);
	}

	public function staffDownexcel(){
		$wid = $this->req->param('wid');
		$type = $this->req->param('type');
		$department = $this->req->param('department');
		$date_type = $this->req->param('date_type');
		$staffs = $this->req->param('staffs');
		if($staffs)explode(",", $staffs);
		if($date_type == "day"){
			$ktime = date('Y-m-d',strtotime("-1 day"));
			$dtime = date("Y-m-d");
		}else if($date_type == "week"){
			$ktime = date('Y-m-d',strtotime("-1 week"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 month"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else{
			$ktime = $this->req->param('ktime')?:date('Y-m-d',strtotime("-1 week"));
			$dtime = $this->req->param('dtime')?:date('Y-m-d',strtotime("-1 day"));
		}
		if(strtotime($ktime)-strtotime($dtime) > 3600*24*31)return error("最大跨度为三十一天");
		if($type == "times"){
			$staffs = DataStatisticsModel::getUserids($staffs,"",$wid);
			$staffs = array_chunk($staffs,100);
			$data = [];
			for($x=0;$x<count($staffs);$x++){
				$res = QyWechatApi::getUserBehaviorData($staffs[$x],[],strtotime($ktime),strtotime($dtime));
				if($res['errcode'] != 0)return error('当前时间段无数据',$res['errcode']);
				foreach ($res['behavior_data'] as $behavior_data) {
					$date = date('Y-m-d',$behavior_data['stat_time']);
					if(isset($data[$date])){
						$data[$date]["chat_cnt"] += $behavior_data["chat_cnt"];
						$data[$date]["message_cnt"] += $behavior_data["message_cnt"];
						$data[$date]["new_apply_cnt"] += $behavior_data["new_apply_cnt"];
						$data[$date]["new_contact_cnt"] += $behavior_data["new_contact_cnt"];
						$data[$date]["negative_feedback_cnt"] += $behavior_data["negative_feedback_cnt"];
						$data[$date]["reply_percentage"] += isset($behavior_data["reply_percentage"])?$behavior_data["reply_percentage"]:0;
						$data[$date]["avg_reply_time"] += isset($behavior_data["avg_reply_time"])?$behavior_data["avg_reply_time"]:0;
					}else{
						$data[$date]["date"] = $date;
						$data[$date]["chat_cnt"] = $behavior_data["chat_cnt"];
						$data[$date]["message_cnt"] = $behavior_data["message_cnt"];
						$data[$date]["new_apply_cnt"] = $behavior_data["new_apply_cnt"];
						$data[$date]["new_contact_cnt"] = $behavior_data["new_contact_cnt"];
						$data[$date]["negative_feedback_cnt"] = $behavior_data["negative_feedback_cnt"];
						$data[$date]["reply_percentage"] = isset($behavior_data["reply_percentage"])?$behavior_data["reply_percentage"]:0;
						$data[$date]["avg_reply_time"] = isset($behavior_data["avg_reply_time"])?$behavior_data["avg_reply_time"]:0;
					}
				}
			}
			$arr = [
	            0 => ['日期', '聊天总数', '发送消息数','发起申请数','新增客户数','流失客户数','已回复聊天占比','平均首次回复时长',]
	        ];
	        $i = 1;
	        foreach($data as $value){
	        	$arr[$i]["日期"] = $value["date"];
	        	$arr[$i]["聊天总数"] = $value["chat_cnt"];
	        	$arr[$i]["发送消息数"] = $value["message_cnt"];
	        	$arr[$i]["发起申请数"] = $value["new_apply_cnt"];
	        	$arr[$i]["新增客户数"] = $value["new_contact_cnt"];
	        	$arr[$i]["流失客户数"] = $value["negative_feedback_cnt"];
	        	$arr[$i]["已回复聊天占比"] = $value["reply_percentage"]."%";
	        	$arr[$i]["平均首次回复时长"] = $value["avg_reply_time"]."%";
	        	$i++;
	        }
		}else if($type == "staff"){
			$datas = DataStatisticsModel::getUserids($staffs,"avatar,name,userid",$wid);
			$arr = [
	            0 => ['成员名称', '聊天总数', '发送消息数','发起申请数','新增客户数','流失客户数','已回复聊天占比','平均首次回复时长',]
	        ];
	        $i = 1;
			foreach($datas as $key=>$staff){
				$res = QyWechatApi::getUserBehaviorData([$staff["userid"]],[],strtotime($ktime),strtotime($dtime));
				if($res['errcode'] != 0)return error('当前时间段无数据',$res['errcode']);
				$datas[$key]["chat_cnt"] = 0;
				$datas[$key]["message_cnt"] = 0;
				$datas[$key]["new_apply_cnt"] = 0;
				$datas[$key]["new_contact_cnt"] = 0;
				$datas[$key]["negative_feedback_cnt"] = 0;
				$datas[$key]["reply_percentage"] = 0;
				$datas[$key]["avg_reply_time"] = 0;
				foreach($res["behavior_data"] as $behavior_data){
					$datas[$key]["chat_cnt"] += $behavior_data["chat_cnt"];
					$datas[$key]["message_cnt"] += $behavior_data["message_cnt"];
					$datas[$key]["new_apply_cnt"] += $behavior_data["new_apply_cnt"];
					$datas[$key]["new_contact_cnt"] += $behavior_data["new_contact_cnt"];
					$datas[$key]["negative_feedback_cnt"] += $behavior_data["negative_feedback_cnt"];
					$datas[$key]["reply_percentage"] += isset($behavior_data["reply_percentage"])?$behavior_data["reply_percentage"]:0;
					$datas[$key]["avg_reply_time"] += isset($behavior_data["avg_reply_time"])?$behavior_data["avg_reply_time"]:0;
				}
	        	$arr[$i]["日期"] = $staff["name"];
	        	$arr[$i]["聊天总数"] = $datas[$key]["chat_cnt"];
	        	$arr[$i]["发送消息数"] = $datas[$key]["message_cnt"];
	        	$arr[$i]["发起申请数"] = $datas[$key]["new_apply_cnt"];
	        	$arr[$i]["新增客户数"] = $datas[$key]["new_contact_cnt"];
	        	$arr[$i]["流失客户数"] = $datas[$key]["negative_feedback_cnt"];
	        	$arr[$i]["已回复聊天占比"] = $datas[$key]["reply_percentage"]."%";
	        	$arr[$i]["平均首次回复时长"] = $datas[$key]["avg_reply_time"]."%";
	        	$i++;
			}
		}else if($type == "department"){
			if($department)explode(",", $department);
			$datas = DataStatisticsModel::departments($department,$wid);
			$arr = [
	            0 => ['成员名称', '聊天总数', '发送消息数','发起申请数','新增客户数','流失客户数','已回复聊天占比','平均首次回复时长',]
	        ];
	        $i = 1;
			foreach($datas as $key=>$department){
				$res = QyWechatApi::getUserBehaviorData([],[$department["department_id"]],strtotime($ktime),strtotime($dtime));
				if($res["errcode"] != 0)return error("接口错误",$res["errcode"]);
				$behavior_data = $res["behavior_data"];
				$datas[$key]["chat_cnt"]= 0;
				$datas[$key]["message_cnt"]= 0;
				$datas[$key]["negative_feedback_cnt"]= 0;
				$datas[$key]["new_apply_cnt"]= 0;
				$datas[$key]["new_contact_cnt"]= 0;
				$datas[$key]["reply_percentage"]= 0;
				$datas[$key]["avg_reply_time"]= 0;
				foreach($behavior_data as $value){
					$datas[$key]["chat_cnt"] += $value['chat_cnt'];
					$datas[$key]["message_cnt"] += $value['message_cnt'];
					$datas[$key]["negative_feedback_cnt"] += $value['negative_feedback_cnt'];
					$datas[$key]["new_apply_cnt"] += $value['new_apply_cnt'];
					$datas[$key]["new_contact_cnt"] += $value['new_contact_cnt'];
					$datas[$key]["reply_percentage"] += isset($value["reply_percentage"])?$value["reply_percentage"]:0;
					$datas[$key]["avg_reply_time"] += isset($value["avg_reply_time"])?$value["avg_reply_time"]:0;
				}
				$arr[$i]["日期"] = $department["department_name"];
	        	$arr[$i]["聊天总数"] = $datas[$key]["chat_cnt"];
	        	$arr[$i]["发送消息数"] = $datas[$key]["message_cnt"];
	        	$arr[$i]["发起申请数"] = $datas[$key]["new_apply_cnt"];
	        	$arr[$i]["新增客户数"] = $datas[$key]["new_contact_cnt"];
	        	$arr[$i]["流失客户数"] = $datas[$key]["negative_feedback_cnt"];
	        	$arr[$i]["已回复聊天占比"] = $datas[$key]["reply_percentage"]."%";
	        	$arr[$i]["平均首次回复时长"] = $datas[$key]["avg_reply_time"]."%";
	        	$i++;
			}
		}

		$str = "成员统计-";
        if($type == "times")$str .= "时间统计";
        if($type == "staff")$str .= "成员统计";
        if($type == "department")$str .= "部门统计";
        $xls = new \Excel_XML('utf-8', false, '数据导出');
        $xls->addArray($arr);
        $xls->generateXML($str.date('Y-m-d H:i:s'),time());
        exit();
	}

	public function staffDepartment(){
		$wid = Session::get("wid");
		$date_type = $this->req->param('date_type');
		$department = $this->req->param('department');
		if($date_type == "day"){
			$ktime = date('Y-m-d',strtotime("-1 day"));
			$dtime = date("Y-m-d");
		}else if($date_type == "week"){
			$ktime = date('Y-m-d',strtotime("-1 week"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 month"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else{
			$ktime = $this->req->param('ktime')?:date('Y-m-d',strtotime("-1 week"));
			$dtime = $this->req->param('dtime')?:date('Y-m-d',strtotime("-1 day"));
		}
		$department_list = DataStatisticsModel::departments($department);
		foreach($department_list as $key=>$department){
			$res = QyWechatApi::getUserBehaviorData([],[$department["department_id"]],strtotime($ktime),strtotime($dtime));
			if($res["errcode"] != 0)return error("接口错误",$res["errcode"]);
			$behavior_data = $res["behavior_data"];
			$department_list[$key]["chat_cnt"]= 0;
			$department_list[$key]["message_cnt"]= 0;
			$department_list[$key]["negative_feedback_cnt"]= 0;
			$department_list[$key]["new_apply_cnt"]= 0;
			$department_list[$key]["new_contact_cnt"]= 0;
			foreach($behavior_data as $value){
				$department_list[$key]["chat_cnt"] += $value['chat_cnt'];
				$department_list[$key]["message_cnt"] += $value['message_cnt'];
				$department_list[$key]["negative_feedback_cnt"] += $value['negative_feedback_cnt'];
				$department_list[$key]["new_apply_cnt"] += $value['new_apply_cnt'];
				$department_list[$key]["new_contact_cnt"] += $value['new_contact_cnt'];
			}
		}

		return success("获取成功",$department_list);
	}

	//趋势图 成员统计
	public function staffDetail(){
		$wid = Session::get("wid");
		$type = $this->req->param('type');
		$date_type = $this->req->param('date_type');
		$staffs = $this->req->param('staffs');
		if($date_type == "day"){
			$ktime = date('Y-m-d',strtotime("-1 day"));
			$dtime = date("Y-m-d");
		}else if($date_type == "week"){
			$ktime = date('Y-m-d',strtotime("-1 week"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 month"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else{
			$ktime = $this->req->param('ktime')?:date('Y-m-d',strtotime("-1 week"));
			$dtime = $this->req->param('dtime')?:date('Y-m-d',strtotime("-1 day"));
		}
		$data = [];
		$staffs = DataStatisticsModel::getUserids($staffs);
		if(strtotime($ktime)-strtotime($dtime) > 3600*24*31)return error("最大跨度为三十一天");
		$staffs = array_chunk($staffs,100);
		for($x=0;$x<count($staffs);$x++){
			$res = QyWechatApi::getUserBehaviorData($staffs[$x],[],strtotime($ktime),strtotime($dtime));
			if($res['errcode'] != 0)return error('当前时间段无数据',$res['errcode']);
			foreach ($res['behavior_data'] as $behavior_data) {
				$date = date('Y-m-d',$behavior_data['stat_time']);
				if(isset($data[$date])){
					$data[$date]["chat_cnt"] += $behavior_data["chat_cnt"];
					$data[$date]["message_cnt"] += $behavior_data["message_cnt"];
					$data[$date]["new_apply_cnt"] += $behavior_data["new_apply_cnt"];
					$data[$date]["new_contact_cnt"] += $behavior_data["new_contact_cnt"];
					$data[$date]["negative_feedback_cnt"] += $behavior_data["negative_feedback_cnt"];
					$data[$date]["reply_percentage"] += isset($behavior_data["reply_percentage"])?:0;
					$data[$date]["avg_reply_time"] += isset($behavior_data["avg_reply_time"])?:0;
					$data[$date]["reply_percentage"] += isset($behavior_data["reply_percentage"])?$behavior_data["reply_percentage"]:0;
					$data[$date]["avg_reply_time"] += isset($behavior_data["avg_reply_time"])?$behavior_data["avg_reply_time"]:0;
				}else{
					$data[$date]["date"] = $date;
					$data[$date]["chat_cnt"] = $behavior_data["chat_cnt"];
					$data[$date]["message_cnt"] = $behavior_data["message_cnt"];
					$data[$date]["new_apply_cnt"] = $behavior_data["new_apply_cnt"];
					$data[$date]["new_contact_cnt"] = $behavior_data["new_contact_cnt"];
					$data[$date]["negative_feedback_cnt"] = $behavior_data["negative_feedback_cnt"];
					$data[$date]["reply_percentage"] = isset($behavior_data["reply_percentage"])?$behavior_data["reply_percentage"]:0;
					$data[$date]["avg_reply_time"] = isset($behavior_data["avg_reply_time"])?$behavior_data["avg_reply_time"]:0;
				}
			}
		}
		$data = array_values($data);

		return success("获取成功",$data);
	}

	//表格 成员统计
	public function staffTable(){
		$wid = Session::get("wid");
		$type = $this->req->param('type');
		$date_type = $this->req->param('date_type');
		$staffs = $this->req->param('staffs');
		if($date_type == "day"){
			$ktime = date('Y-m-d',strtotime("-1 day"));
			$dtime = date("Y-m-d");
		}else if($date_type == "week"){
			$ktime = date('Y-m-d',strtotime("-1 week"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 month"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else{
			$ktime = $this->req->param('ktime')?:date('Y-m-d',strtotime("-1 week"));
			$dtime = $this->req->param('dtime')?:date('Y-m-d',strtotime("-1 day"));
		}
		$data = [];
		if(strtotime($ktime)-strtotime($dtime) > 3600*24*31)return error("最大跨度为三十一天");
		if($type == "times"){
			$staffs = DataStatisticsModel::getUserids($staffs);
			$staffs = array_chunk($staffs,100);
			for($x=0;$x<count($staffs);$x++){
				$res = QyWechatApi::getUserBehaviorData($staffs[$x],[],strtotime($ktime),strtotime($dtime));
				if($res['errcode'] != 0)return error('当前时间段无数据',$res['errcode']);
				foreach ($res['behavior_data'] as $behavior_data) {
					$date = date('Y-m-d',$behavior_data['stat_time']);
					if(isset($data[$date])){
						$data[$date]["chat_cnt"] += $behavior_data["chat_cnt"];
						$data[$date]["message_cnt"] += $behavior_data["message_cnt"];
						$data[$date]["new_apply_cnt"] += $behavior_data["new_apply_cnt"];
						$data[$date]["new_contact_cnt"] += $behavior_data["new_contact_cnt"];
						$data[$date]["negative_feedback_cnt"] += $behavior_data["negative_feedback_cnt"];
						$data[$date]["reply_percentage"] += isset($behavior_data["reply_percentage"])?$behavior_data["reply_percentage"]:0;
						$data[$date]["avg_reply_time"] += isset($behavior_data["avg_reply_time"])?$behavior_data["avg_reply_time"]:0;
					}else{
						$data[$date]["date"] = $date;
						$data[$date]["chat_cnt"] = $behavior_data["chat_cnt"];
						$data[$date]["message_cnt"] = $behavior_data["message_cnt"];
						$data[$date]["new_apply_cnt"] = $behavior_data["new_apply_cnt"];
						$data[$date]["new_contact_cnt"] = $behavior_data["new_contact_cnt"];
						$data[$date]["negative_feedback_cnt"] = $behavior_data["negative_feedback_cnt"];
						$data[$date]["reply_percentage"] = isset($behavior_data["reply_percentage"])?$behavior_data["reply_percentage"]:0;
						$data[$date]["avg_reply_time"] = isset($behavior_data["avg_reply_time"])?$behavior_data["avg_reply_time"]:0;
					}
				}
			}
			$datas = array_values($data);
		}else if($type == "staff"){
			$datas = DataStatisticsModel::getUserids($staffs,"avatar,name,userid");
			foreach($datas as $key=>$staff){
				$res = QyWechatApi::getUserBehaviorData([$staff["userid"]],[],strtotime($ktime),strtotime($dtime));
				if($res['errcode'] != 0)return error('当前时间段无数据',$res['errcode']);
				$datas[$key]["chat_cnt"] = 0;
				$datas[$key]["message_cnt"] = 0;
				$datas[$key]["new_apply_cnt"] = 0;
				$datas[$key]["new_contact_cnt"] = 0;
				$datas[$key]["negative_feedback_cnt"] = 0;
				$datas[$key]["reply_percentage"] = 0;
				$datas[$key]["avg_reply_time"] = 0;
				foreach($res["behavior_data"] as $behavior_data){
					$datas[$key]["chat_cnt"] += $behavior_data["chat_cnt"];
					$datas[$key]["message_cnt"] += $behavior_data["message_cnt"];
					$datas[$key]["new_apply_cnt"] += $behavior_data["new_apply_cnt"];
					$datas[$key]["new_contact_cnt"] += $behavior_data["new_contact_cnt"];
					$datas[$key]["negative_feedback_cnt"] += $behavior_data["negative_feedback_cnt"];
					$datas[$key]["reply_percentage"] += isset($behavior_data["reply_percentage"])?:0;
					$datas[$key]["avg_reply_time"] += isset($behavior_data["avg_reply_time"])?:0;
				}
			}
		}

		return success("获取成功",$datas);
	}

	/*
	* 客户量排名
 	*/
	public function staffTop(){
		$wid = Session::get("wid");
		$res = DataStatisticsModel::top();
		
		return success("获取成功",$res);
	}

	/*
	* 一客一码数据统计
	*/
	public function qrcode(){
		$wid = Session::get("wid");
		$data["wid"] = $wid;
		$data["day_add_size"] = DataStatisticsModel::size();
		$data["day_del_size"] = DataStatisticsModel::size(2);
		$data["all_add_size"] = DataStatisticsModel::sizeAll();
		$data["all_del_size"] = DataStatisticsModel::sizeAll(2);

		return success("获取成功",$data);
	}

	public function qrcodeDownexcel(){
		$wid = Session::get("wid");
		$type = $this->req->param('type');
		$date_type = $this->req->param('date_type');
		$staffs = $this->req->param('staffs');
		if($date_type == "day"){
			$ktime = date("Y-m-d"); 
			$dtime = date("Y-m-d");
		}else if($date_type == "week"){
			$ktime = date('Y-m-d',strtotime("-1 week"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 month"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else{
			$ktime = $this->req->param('ktime')?:date('Y-m-d',strtotime("-1 week"));
			$dtime = $this->req->param('dtime')?:date('Y-m-d',strtotime("-1 day"));
		}
		if($dtime > date("Y-m-d"))return error("最大时间不能超过今天");
		if($type == "times"){
			$arr = [
	            0 => ['日期', '客户总数', '流失人数','新增人数']
	        ];
			$time = strtotime($dtime) - strtotime($ktime);
			$time_tow = $ktime;
			$datas = [];
			for ($i=0;$i<=$time/(3600*24);$i++){
				$time_one = date("Y-m-d",strtotime($time_tow));
				$time_tow = date("Y-m-d",strtotime($time_tow)+3600*24);
				$arr[$i+1]["日期"] = $time_one;
				$arr[$i+1]["客户总数"] = DataStatisticsModel::qrcodeTable($time_one,$time_tow,$staffs);
				$arr[$i+1]["流失人数"] = DataStatisticsModel::qrcodeTable($time_one,$time_tow,$staffs,1);
				$arr[$i+1]["新增人数"] = DataStatisticsModel::qrcodeTable($time_one,$time_tow,$staffs,2);
			}
		}else if($type == "staff"){
			$arr = [
	            0 => ['成员名称', '客户总数', '流失人数','新增人数']
	        ];
	        $i = 1;
			$datas = DataStatisticsModel::getUserids($staffs,"avatar,name,userid",$wid);
			$dtime = strtotime($dtime)+3600*24;
			foreach($datas as $key=>$staff){
				$arr[$i]["成员名称"] = $staff["name"];
				$arr[$i]["客户总数"] = DataStatisticsModel::qrcodeTable($ktime,$dtime,$staff["id"]);
				$arr[$i]["流失人数"] = DataStatisticsModel::qrcodeTable($ktime,$dtime,$staff["id"],1);
				$arr[$i]["新增人数"] = DataStatisticsModel::qrcodeTable($ktime,$dtime,$staff["id"],2);
				$i++;
			}
		}else if($type == "customer"){
			$add_way = addWay();
			$customer_type = $this->req->param('customer_type');
			$dtime = strtotime($dtime)+3600*24;
			$datas = DataStatisticsModel::qrcodeCustomer($ktime,$dtime,$add_way,$customer_type);
			$arr = [
	            0 => ['客户名称', '成员名称', '客户评分','添加渠道',"添加时间"]
	        ];
	        $i = 1;
	        foreach($datas as $value){
				$arr[$i]["客户名称"] = $value["customer"]["name"];
				$arr[$i]["成员名称"] = $value["staff"]["name"];
				$arr[$i]["客户评分"] = $value["customer"]["score"];
				$arr[$i]["添加渠道"] = $value["customer"]["add_way"];
				$arr[$i]["添加时间"] = $value["customer"]["createtime"];

	        	$i++;
	        }
		}
		$str = "一客一码统计-";
        if($type == "times")$str .= "按日期查看";
        if($type == "staff")$str .= "按员工查看";
        if($type == "customer")$str .= "按客户查看";
        $xls = new \Excel_XML('utf-8', false, '数据导出');
        $xls->addArray($arr);
        $xls->generateXML($str.date('Y-m-d H:i:s'),time());
        exit();
	}

	/*
	* 一客一码图表
	*/
	public function qrcodeTable(){
		$wid = Session::get("wid");
		$type = $this->req->param('type');
		$date_type = $this->req->param('date_type');
		$staffs = $this->req->param('staffs');
		if($date_type == "day"){
			$ktime = date("Y-m-d"); 
			$dtime = date("Y-m-d");
		}else if($date_type == "week"){
			$ktime = date('Y-m-d',strtotime("-1 week"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else if($date_type == "month"){
			$ktime = date("Y-m-d",strtotime("-1 month"));
			$dtime = date('Y-m-d',strtotime("-1 day"));
		}else{
			$ktime = $this->req->param('ktime')?:date('Y-m-d',strtotime("-1 week"));
			$dtime = $this->req->param('dtime')?:date('Y-m-d',strtotime("-1 day"));
		}
		if($dtime > date("Y-m-d"))return error("最大时间不能超过今天");
		if($type == "times"){
			$time = strtotime($dtime) - strtotime($ktime);
			$time_tow = $ktime;
			$datas = [];
			for ($i=0;$i<=$time/(3600*24);$i++){
				$time_one = date("Y-m-d",strtotime($time_tow));
				$time_tow = date("Y-m-d",strtotime($time_tow)+3600*24);
				$datas[$i]["time"] = $time_one;
				$datas[$i]["add_all"] = DataStatisticsModel::qrcodeTable($time_one,$time_tow,$staffs);
				$datas[$i]["del_size"] = DataStatisticsModel::qrcodeTable($time_one,$time_tow,$staffs,2);
				$datas[$i]["add_size"] = DataStatisticsModel::qrcodeTable($time_one,$time_tow,$staffs,1);
			}
		}else if($type == "staff"){
			$datas = DataStatisticsModel::getUserids($staffs,"avatar,name,userid,id");
			$dtime = strtotime($dtime)+3600*24;
			foreach($datas as $key=>$staff){
				$datas[$key]["add_all"] = DataStatisticsModel::qrcodeTable($ktime,$dtime,$staff["id"]);
				$datas[$key]["del_size"] = DataStatisticsModel::qrcodeTable($ktime,$dtime,$staff["id"],2);
				$datas[$key]["add_size"] = DataStatisticsModel::qrcodeTable($ktime,$dtime,$staff["id"],1);
			}
		}else if($type == "customer"){
			$add_way = addWay();
			$customer_type = $this->req->param('customer_type');
			$dtime = strtotime($dtime)+3600*24;
			$datas = DataStatisticsModel::qrcodeCustomer($ktime,$dtime,$add_way,$customer_type);
		}
		
		return success("获取成功",$datas);
	}


	/*
	* 一客一码详情
	**/
	public function qrcodeDetail(){
		$add_way = addWay();
		$id = $this->req->param('id');
		$date_type = $this->req->param('date_type');
		$type = $this->req->param('type');
		$time_type = $this->req->param('time_type');
		$staff_id = $this->req->param('staff_id');
		if($date_type == "day"){
			$time = date("Y-m-d"); 
			$dtime = date("Y-m-d");
		}else if($date_type == "week"){
			$time = date('Y-m-d',strtotime("-1 week"));
			$dtime = date("Y-m-d");
		}else if($date_type == "month"){
			$time = date("Y-m-d",strtotime("-1 month"));
			$dtime = date("Y-m-d");
		}else{
			$time = $this->req->param('time')?:date("Y-m-d");
			$dtime = $this->req->param('dtime')?:date("Y-m-d");
		}
		if($time_type == 1)$dtime = $time;
		$dtime = date("Y-m-d",strtotime($dtime)+3600*24);
		$datas = DataStatisticsModel::qrcodeCustomer($time,$dtime,$add_way,$type,$staff_id);
		$res["data"] = $datas;
		if($time_type == 1)$res["times"] = date("Y-m-d",strtotime($dtime)-3600*24);
		if($time_type != 1)$res["times"] = $time . " ~ " .date("Y-m-d",strtotime($dtime)-3600*24);

		return success("获取成功",$res);
	}
}