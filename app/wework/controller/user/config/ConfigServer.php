<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\user\config;
use think\facade\Db;
use app\wework\controller\Base;
use app\wework\model\QyWechatApi;
use app\wework\model\user\config\ConfigServerModel;
use think\facade\Session;

class ConfigServer extends Base
{
    //externalcontact_secret,user_secret,externalcontact_token,externalcontact_aes,user_tokent,user_aes
    /**
    * 获取基础配置
    **/
    public function basic()
    {
        $wid = Session::get('wid');
        $where = ['wid'=>$wid];
        $res = ConfigServerModel::info('kt_wework_config',$where,'corp_name,corp_id');
        // $res['callback']
        return success('配置信息',$res);
    }
    /**
    * 获取通讯录配置
    **/
    public function staff()
    {
        $wid = Session::get('wid');
        $where = ['wid'=>$wid];
        $res = ConfigServerModel::info('kt_wework_config',$where,'user_secret,user_tokent,user_aes,user_status');

        $res['staff_callback_url'] = $this->req->domain()."/wework/SatffCallback/index.html";
        return success('配置信息',$res);
    }
    /**
    * 获取客户联系配置
    **/
    public function customer()
    {
        $wid = Session::get('wid');
        $where = ['wid'=>$wid];
        $res = ConfigServerModel::info('kt_wework_config',$where,'customer_aes,customer_token,customer_secret,customer_status');

        $res['staff_callback_url'] = $this->req->domain()."/wework/CustomerCallback/index.html";
        return success('配置信息',$res);
    }

    /**
    * 获取自建应用配置
    **/
    public function selfapp()
    {
        $wid = Session::get('wid');
        $where = ['wid'=>$wid];
        $res = ConfigServerModel::info('kt_wework_selfapp',$where,'agent_id,secret,name,square_logo_url,description');
        $res['customer_qr_code'] = $this->req->domain()."/wework/h5.CustomerQrcode/index.html?wid=".$wid;
        $res['customer_qr_portrait'] = $this->req->domain()."/wework/h5.CustomerPortrait/index.html?wid=".$wid;
        $res['reply_url'] = $this->req->domain()."/wework/h5.InteractiveRadar/index.html?wid=".$wid;

        return success('配置信息',$res);
    }

    /**
    * 自建应用配置更新 走接口验证
    **/
    public function selfappSave()
    {
        $data = $this->req->post();
        $field = "agent_id,secret";
        $table = "kt_wework_selfapp";
        $res = ConfigServerModel::UpdateConfig($table,$data,$field);
        $wid = Session::get('wid');
        $res = QyWechatApi::getAgent($wid,$data["agent_id"]);
        if($res["errcode"] != 0)return error("接口错误",$res["errcode"]);
        $res = ConfigServerModel::UpdateConfig($table,["name"=>$res["name"],"square_logo_url"=>$res["square_logo_url"],"description"=>$res["description"]],"name,square_logo_url,description");

        if($res == 0) return error('无修改');
        return success('更新成功');
    }

    /**
    * 基础配置更新 未走接口验证
    **/
    public function basicSave()
    {
        $data = $this->req->post();
        $field = "corp_name,corp_id";
        $table = "kt_wework_config";
        $res = ConfigServerModel::UpdateConfig($table,$data,$field);
        if($res == 0) return error('无修改');
        return success('更新成功');
    }

    /**
    * 通讯录配置更新 未走接口验证
    **/
    public function staffSave()
    {
        $data = $this->req->post();
        $field = "user_secret,user_tokent,user_aes";
        $table = "kt_wework_config";
        $res = ConfigServerModel::UpdateConfig($table,$data,$field);
        if($res == 0) return error('无修改');
        return success('更新成功');
    }

    /**
    * 客户联系更新 走接口验证
    **/
    public function customerSave()
    {
        $data = $this->req->post();
        $field = "customer_secret,customer_token,customer_aes";
        $table = "kt_wework_config";
        $res = ConfigServerModel::UpdateConfig($table,$data,$field);
        $wid = Session::get('wid');
        $token = QyWechatApi::getCustomerAccessToken($wid);
        if($token)$res = ConfigServerModel::UpdateConfig($table,["customer_status"=>1],"customer_status");
        if(!$token)$res = ConfigServerModel::UpdateConfig($table,["customer_status"=>0],"customer_status");

        return success('更新成功');
    }


}