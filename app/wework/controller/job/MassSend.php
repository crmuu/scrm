<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\model\BaseModel;
use app\wework\model\user\send\MassSendModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;
use app\wework\controller\Base;

/**
* 定时任务，实时获取客户群发记录
*/
class MassSend extends Base{

     public function index(){
          $config_list = Db::table('kt_wework_config')->where(["customer_status"=>1])->column("wid");
          if(!$config_list) return success("操作失败");
          foreach ($config_list as $user) {
               $this->groupMsgResult($user);
          }
        return success("操作成功");
     }

     public function groupMsgResult($wid){
          $msgidList = Db::table('kt_wework_customermsgid')->where(["wid"=>$wid,"status"=>2])->select()->toArray();
          foreach ($msgidList as $msg) {
               $user = MassSendModel::getUser($msg["staff_id"]);
               $res = QyWechatApi::getGroupmsgSendResult($wid,$user["userid"],$msg["msgid"]);
               if($res["errcode"] == 0){
                    MassSendModel::addDetail($res["send_list"],$wid,$msg);
               }
               if(count($res["send_list"])) $msgSet = MassSendModel::msgSet($msg,count($res["send_list"]));
          }
          return json(['code'=>'success','msg'=>'成功']);
     }

} 