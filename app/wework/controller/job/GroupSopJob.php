<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\model\BaseModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;


/**
	* 群Sop任务类-存储实时群聊信息-sop规则状态实时更新
*/
class GroupSopJob extends BaseController{
	public function index(){
		$config_list = Db::table('kt_wework_config')->where(["customer_status"=>1])->column("wid");
        if(!$config_list) return success("操作失败");
        foreach ($config_list as $user) {
           $this->sop($user);
        }
        return success("操作成功");
	}

	public function sop($wid){
		$list = Db::table("kt_wework_groupsop")->where(["wid"=>$wid])->json(["group_condition_tag","groupid"])->select()->toArray();
		if(!$list)return success("操作成功");
		foreach ($list as $value){
			if($value["group_type"] == 2){
				$group_list = Db::table("kt_wework_group");
				if($value["group_condition_type"] == 1){
					foreach ($value["group_condition_tag"] as $tag){
						$where = [];
	            		$where[] = ["tags","like","%$tag%"];
	            		$group_list = $group_list->whereOr($where);
					}
         		$group_list = $group_list->where(["wid"=>$wid])->field("id,name,chat_id")->select()->toArray();
				}else{
					if($value["group_condition_new"] == 1)$group_list = $group_list->where(["wid"=>$wid])->whereTime('create_time','>=',$value["group_condition_new_start"])->field("id,name,chat_id")->select()->toArray();
					if($value["group_condition_new"] == 2)$group_list = $group_list->where(["wid"=>$wid])->whereBetweenTime('create_time',$value["group_condition_new_start"],$value["group_condition_new_end"])->field("id,name,chat_id")->select()->toArray();
				}
				$detail_list = Db::table("kt_wework_groupsop_detail")->where(["pid"=>$value["id"]])->select()->toArray();
				foreach ($detail_list as $detail){
					foreach ($group_list as $group){
						$is_set = Db::table("kt_wework_groupsop_time")->where(["pid"=>$value["id"],"group_id"=>$group["chat_id"],"detail_id"=>$detail["id"]])->find();
						if(!$is_set){
							$time["pid"] = $value["id"];
							$time["detail_id"] = $detail["id"];
							$time["group_id"] = $group["chat_id"];
							$time["ctime"] = date("Y-m-d H:i:s");
							Db::table("kt_wework_groupsop_time")->insertGetId($time);
						}
					}
				}
			}
			if($value["send_type"] == 2){
				if($value["send_starttime_type"] == 2 && $value["send_starttime"] >= date("Y-m-d H:i:s")){
					Db::table("kt_wework_groupsop")->where(["id"=>$value["id"]])->update(["status"=>1]);
				}
				if($value["send_endtime_type"] == 2 && $value["send_endtime_date"] >= date("Y-m-d") && $value["send_endtime_time"] >= date("H:i:s")){
					Db::table("kt_wework_groupsop")->where(["id"=>$value["id"]])->update(["status"=>0]);
				}
				if($value["send_endtime_type"] == 3){
					$send_starttime = $value["send_starttime"];
					if($value["send_starttime_type"] == 1)$send_starttime = $value["create_time"];
					if($value["send_endtime_date_type"] == 1)$dtime = $send_starttime + $value["send_endtime_size"]*86400;
					if($value["send_endtime_date_type"] == 2)$dtime = $send_starttime + $value["send_endtime_size"]*604800;
					if($value["send_endtime_date_type"] == 3)$dtime = $send_starttime + $value["send_endtime_size"]*2592000;
					if($send_starttime >= $dtime)Db::table("kt_wework_groupsop")->where(["id"=>$value["id"]])->update(["status"=>0]);
				}
			}
		}
		return success("操作成功");
	}
}