<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\model\BaseModel;
use app\wework\model\user\staff\StaffModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;
use app\wework\controller\Base;


/**
  * 任务类
  */
 class StaffJob extends Base
 {
 	 /**
     * fire方法是消息队列默认调用的方法
     * @param Job $job 当前的任务对象
     * @param array $data 发布任务时自定义的数据
     */
    public function fire(Job $job, array $data)
    {
     	
        $isJobDone = $this->doHelloJob($data);
        if ($isJobDone){
            $job->delete();
			Log::info("执行完毕,删除任务" . $job->attempts() . '\n');
        }else{
            if ($job->attempts() > 3){
                $job->delete();
                Log::info("超时任务删除" . $job->attempts() . '\n');
            }
        }
 		
    }

    private function doHelloJob(array $data)
    {
        $wid = $data['wid'];
        $config = Db::table('kt_wework_selfapp')->where('wid',$wid)->find();
 		if(!$config) return true;
 		$departmentid = $data['departmentid'];
 		foreach ($departmentid as $id) {
 			$this->syncQyuser($id,$wid);
 		}
        return true;
    }

     /**
    * 同步成员
    **/
    private function syncQyuser($id,$wid)
    {   
        $qyuserLists = [];
        $qyuserInfos = QyWechatApi::getStaffList($id,$wid);
        if($qyuserInfos['errcode'] != 0 || count($qyuserInfos['userlist']) == 0){
        	return 0;
        }
        $qyuserLists = $qyuserInfos['userlist'];
        // $departmentArr = array_column(UserDepartmentList::all(['user_id'=>$userId])->toArray(),'department_name','department_id');
        foreach($qyuserLists as $info){ 
        	$data = [];
            
            // $data['user_id'] = $userId;
            // $data['userid'] = $info['userid'];
            // $data['name'] = filterEmoji($info['name']);
            // $data['mobile'] = $info['mobile'];
            // $data['department'] = json_encode($info['department']);
            // $data['department_name'] = json_encode($departmentName,320);
            // $data['order'] = json_encode($info['order']);
            // $data['position'] = $info['position'];
            // $data['gender'] = $info['gender'];
            // $data['email'] = $info['email'];
            // $data['is_leader_in_dept'] = json_encode($info['is_leader_in_dept']);
            // $data['avatar'] = $info['avatar'];
            // $data['thumb_avatar'] = $info['thumb_avatar'];
            // $data['telephone'] = $info['telephone'];
            // $data['alias'] = $info['alias'];
            // $data['extattr'] = json_encode($info['extattr']);
            // $data['status'] = $info['status'];
            // $data['qr_code'] = $info['qr_code'];
            // $data['external_profile'] = isset($info['external_profile']) ? json_encode($info['external_profile']) : '' ;
            // $data['external_position'] = $info['external_position'] ?? '';
            // $data['address'] = $info['address'] ?? '';
            // $data['open_userid'] = $info['open_userid'] ?? '';
            // $data['main_department'] = $info['main_department'] ?? '';
            // $data['is_del'] = 0;
            // Db::table('kt_wework_staff')->save($data);
         
        }
        return json(['code'=>'success','msg'=>'成功']);
    }

 }  
