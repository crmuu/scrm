<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\model\BaseModel;
use app\wework\model\user\send\GroupSendModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;
use app\wework\controller\Base;


/**
* 定时任务，实时获取客户群发记录
*/
class GroupMassSend extends Base{

     public function index(){
          $config_list = Db::table('kt_wework_config')->where(["customer_status"=>1])->column("wid");
          if(!$config_list) return success("操作失败");
          foreach ($config_list as $user) {
               $this->groupMsg($user);
          }
          return success("操作成功");
     }

     public function groupMsg($wid){
          $ids = Db::table('kt_wework_groupmsg')->where(["wid"=>$wid])->column("id");
          $msgidList = Db::table('kt_wework_groupmsgid')->where(["wid"=>$wid,"status"=>2,"pid"=>$ids])->select()->toArray();
          foreach ($msgidList as $msg) {
               $user = GroupSendModel::getUser($msg["staff_id"]);
               $res = QyWechatApi::getGroupmsgSendResult($wid,$user["userid"],$msg["msgid"]);
               if(count($res["send_list"])){
                    GroupSendModel::addDetail($res["send_list"],$wid,$msg);
                    $msgSet = GroupSendModel::msgSet($msg["id"]);
               }
               GroupSendModel::updateTime($msg["pid"]);
          }
     }
}