<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\model\BaseModel;
use app\wework\model\MassSendModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;
use app\wework\controller\Base;


/**
  * 定时任务， 渠道活码 员工添加上限
*/
class QrTime extends Base{

	public function index(){
		$config_list = Db::table('kt_wework_config')->where(["customer_status"=>1])->column("wid");
 		if(!$config_list) return success("操作失败");
 		foreach ($config_list as $user) {
 			$this->staffup($user);
 		}
        return success("操作成功");
	}

	public function staffup($wid){
		$qr_list = Db::table("kt_wework_qr")->where(["skipverify_type"=>2])->json(["skipverify_time"])->select()->toArray();
		foreach ($qr_list as $qr){
			if($qr["skipverify_time"][0] <= date("H:i") && $qr["skipverify_time"][1] > date("H:i")){
				$upd['skip_verify'] = false;
				$upd['config_id'] = $qr['config_id'];
				$res = QyWechatApi::updateContactWay($upd,$wid);
			}else{
				$upd['skip_verify'] = true;
				$upd['config_id'] = $qr['config_id'];
				$res = QyWechatApi::updateContactWay($upd,$wid);
			}
			if($res["errcode"] != 0)Log::error($res);
		}
	}

}