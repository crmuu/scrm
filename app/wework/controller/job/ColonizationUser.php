<?php 
namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\model\Helper;
use app\wework\model\BaseModel;
use app\wework\model\user\send\GroupSendModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;
use app\wework\controller\Base;


/**
* 定时任务，实时发送提醒
*/
class ColonizationUser extends Base{
	public function index(){
		$config_list = Db::table('kt_wework_config')->where(["customer_status"=>1])->column("wid");
		if(!$config_list) return success("操作失败");
		foreach ($config_list as $user) {
			$this->coloniz($user);
		}

		return success("操作成功");
	}

	public function coloniz($wid){
		$user = Db::table("kt_wework_tag_group_user")->where(["wid"=>$wid,"send_status"=>0])->select()->toArray();
		if(!$user)return success("操作完成");
		foreach($user as $value){
			$user = Helper::getUser(["wid"=>$wid,"id"=>$value["staff_id"]]);
			$res = QyWechatApi::getGroupmsgSendResult($wid,$user["userid"],$value["msg_id"]);
			if(!$res["send_list"])continue;
			foreach ($res["send_list"] as $send){
				$is_set = Db::table("kt_wework_tag_group_detail")->where(["user_id"=>$value["id"],"external_userid"=>$send["external_userid"]])->find();
				$data["user_id"] = $value["id"];
				$data["external_userid"] = $send["external_userid"];
				$data["status"] = $send["status"];
				$data["pid"] = $value["pid"];
				if($is_set)Db::table("kt_wework_tag_group_detail")->where(["user_id"=>$value["id"],"external_userid"=>$send["external_userid"]])->save($data);
				if(!$is_set)Db::table("kt_wework_tag_group_detail")->insertGetId($data);
			}
			$size = Db::table("kt_wework_tag_group_detail")->where(["user_id"=>$value["id"],"status"=>0])->count();
			if(!$size)Db::table("kt_wework_tag_group_user")->where(["id"=>$value["id"]])->update(["send_status"=>1]);
		}
		return success("操作成功");
	}
}