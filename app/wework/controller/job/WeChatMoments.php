<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\controller\Base;
use app\wework\model\BaseModel;
use app\wework\model\GroupSendModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;


/**
* 定时任务，企微朋友圈 同步个人
*/
class WeChatMoments extends Base
{
    public function index(){
        $res = ['code'=>'ok'];
        $config_list = Db::table('kt_wework_config')->where(["customer_status"=>1])->column("wid");
 		if(!$config_list) return success("操作失败");
 		foreach ($config_list as $config) {
 			$this->syncQyuser($config);
 		}
        return success("操作成功");
    }

    public function syncQyuser($wid){
    	$data = QyWechatApi::getMomentList($wid);
    	foreach ($data["moment_list"] as $moment){
			if(!array_key_exists("text",$moment))continue;
			$moments = Db::table("kt_wework_moments")->where(["wid"=>$wid,"text"=>json_encode($moment["text"]["content"],320)])->find();
			if($moments){
				$is_set = Db::table("kt_wework_moments_info")->where(["moment_id"=>$moment["moment_id"],"pid"=>$moments["id"]])->find();
				if($is_set)continue;
				$data["pid"] = $moments["id"];
				$data["wid"] = $wid;
				$data["moment_id"] = $moment["moment_id"];
				$data["create_time"] = date("Y-m-d H:i:s",$moment["create_time"]);
				$data["staff_id"] = Db::table("kt_wework_staff")->where(["wid"=>$wid,"userid"=>$moment["creator"]])->find()["id"];
				Db::table("kt_wework_moments_info")->insertGetId($data);
			}
		}
 		return json(['code'=>'success','msg'=>'成功']);
    }
}