<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\model\BaseModel;
use app\wework\model\MassSendModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;
use app\wework\controller\Base;


/**
  * 定时任务， 渠道活码 员工添加上限
*/
class Qr extends Base{

	public function index(){
		$config_list = Db::table('kt_wework_config')->where(["customer_status"=>1])->column("wid");
 		if(!$config_list) return success("操作失败");
 		foreach ($config_list as $user) {
 			$this->staffup($user);
 		}
        return success("操作成功");
	}

	public function staffup($wid){
		$qr_list = Db::table("kt_wework_qr")->where(["staff_type"=>1,"staffup_status"=>1])->json(["staffup_data","staffid","staff_standby"])->select()->toArray();
		foreach ($qr_list as $qr){
			$staffid = $qr["staffid"];
			foreach($qr["staffup_data"] as $staffup){
				$count = Db::table("kt_wework_scan_log")->where(["type"=>"qr","pid"=>$qr["id"],"staff_id"=>$staffup["id"]])->whereTime("create_time",">=",date("Y-m-d"))->count();
				if(!isset($staffup["sx"]))continue;
				if($count >= $staffup["sx"]){
					unset($staffid[array_search($staffup["id"],$staffid,true)]);
				}
			}
			if(!$staffid)$staffid = $qr["staff_standby"];
			if(!$staffid) continue;
			$upd['config_id'] = $qr['config_id'];
			$upd['user'] = Db::table('kt_wework_staff')->where(['id'=>$staffid])->column('userid');
			$res = QyWechatApi::updateContactWay($upd,$wid);
			if($res["errcode"] != 0)Log::error($res);
		}
	}

}