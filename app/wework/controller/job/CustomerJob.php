<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\model\BaseModel;
use app\wework\model\user\staff\StaffModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;
use app\wework\controller\Base;


/**
  * 任务类
  */
 class CustomerJob extends Base
 {
     /**
     * fire方法是消息队列默认调用的方法
     * @param Job $job 当前的任务对象
     * @param array $data 发布任务时自定义的数据
     */
    public function fire(Job $job, array $data)
    {
        
        $isJobDone = $this->doHelloJob($data);
        if ($isJobDone){
            $job->delete();
            Log::info("执行完毕,删除任务" . $job->attempts() . '\n');
        }else{
            if ($job->attempts() > 3){
                $job->delete();
                Log::info("超时任务删除" . $job->attempts() . '\n');
            }
        }
        
    }

    private function doHelloJob(array $data)
    {
        $wid = $data['wid'];
        $where["wid"] = $wid;
        $where["status"] = 1;
        $where["is_del"] = 0;
        $config = Db::table('kt_wework_staff')->field("userid,id")->where($where)->select();
        if(!count($config)) return true;
        foreach ($config as $value) {
            $this->syncCustomer($value["userid"],$value["userid"],$wid);
        }
        return true;
    }

     /**
    * 同步客戶
    **/
    private function syncCustomer($user_id,$wid)
    {   
        $CustomerList = QyWechatApi::getCustomerList($wid,$id,$user_id);
        if(!count($CustomerList)){
            return 0;
        }
        foreach($CustomerList as $customer_info){
            $data = [];
            $customer = Db::table('kt_wework_customer')->where(['wid'=>$wid,'external_userid'=>$customer_info['external_contact']['external_userid']])->find();
            if($customer){
                $data['id'] = $customer['id'];
            }
            $users = $customer_info['follow_info'];
            $external_contact = $customer_info['external_contact'];
            $data['name'] = filterEmoji($external_contact['name']);
            $data['remark'] = filterEmoji($users['remark']);
            $data['external_profile'] = json_encode($external_contact['external_profile'],320);;
            $data['external_userid'] = $external_contact['external_userid'];
            $data['avatar'] = $external_contact['avatar'];
            $data['type'] = $external_contact['type'];
            $data['gender'] = $external_contact['gender'];
            $data['unionid'] = $external_contact['unionid'];
            $data['position'] = $external_contact['position'];
            $data['corp_name'] = $external_contact['corp_name'];
            $data['corp_full_name'] = $external_contact['corp_full_name'];
            $data['description'] = $users['description'];
            $data['createtime'] = $users['createtime'];
            $data['tags_info'] = json_encode($users['tag_id'],320);
            $data['tags'] = json_encode($users['tag_id'],320);
            $data['remark_corp_name'] = $users['remark_corp_name'];
            $data['remark_mobiles'] = json_encode($users['remark_mobiles'],320);
            $data['add_way'] = $users['add_way'];
            $data['oper_userid'] = $users['oper_userid'];
            $data['state'] = $users['state'];
            $data['wid'] = $wid;
            $data['staff_id'] = $id;
            $data['status'] = 0;
            Db::table('kt_wework_customer')->save($data);
        }
    }

 }  
