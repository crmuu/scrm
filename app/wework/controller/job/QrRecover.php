<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\model\BaseModel;
use app\wework\model\MassSendModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;
use app\wework\controller\Base;


/**
  * 定时任务， 渠道活码 每日恢复成员限制
*/
class QrRecover extends Base{

	public function index(){
		$config_list = Db::table('kt_wework_config')->where(["customer_status"=>1])->column("wid");
		$time = date("H");
 		if(!$config_list || $time >= 1) return success("操作失败");
 		foreach ($config_list as $user) {
 			$this->staffup($user);
 		}
        return success("操作成功");
	}

	public function staffup($wid){
		$qr_list = Db::table("kt_wework_qr")->where(["staff_type"=>1,"staffup_status"=>1])->json(["staffid"])->select()->toArray();
		foreach ($qr_list as $qr){
			$staffid = $qr["staffid"];
			$upd['config_id'] = $qr['config_id'];
			$upd['user'] = Db::table('kt_wework_staff')->where(['id'=>$staffid])->column('userid');;
			$res = QyWechatApi::updateContactWay($upd,$wid);
			if($res["errcode"] != 0)Log::error($res);
		}
	}

}