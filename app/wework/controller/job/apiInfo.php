<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

/**
 * 常量配置
 */ 
class apiInfo
{
	static public $info = [
		"qr" => 1,					//渠道活码
		"recover" => 1,				//渠道活码
		"time" => 1,				//渠道活码
		"staff" => 1,				//渠道活码
		"datafeed" => 1,
		"wechatmoments" => 1,
		"groupmassnotify" => 1,
		"massnotify" => 1,
		"groupmasssend" => 1,
		"festival" => 1,
		"customersop" => 1,
		"groupsop" => 1,
		"groupsopjob" => 1,
		"sop" => 1,
		"masssend" => 1
	];
 
}