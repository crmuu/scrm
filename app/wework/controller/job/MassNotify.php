<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\model\BaseModel;
use app\wework\model\user\send\MassSendModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;
use app\wework\controller\Base;


/**
  * 定时任务，实时发送提醒 客户群发
  */
class MassNotify extends Base{
    public function index(){
        $config_list = Db::table('kt_wework_config')->where(["customer_status"=>1])->column("wid");
        if(!$config_list) return success("操作失败");
        foreach ($config_list as $user) {
            $this->sendMsg($user);
        }
        
        return success("操作成功");
    }

    public function sendMsg($wid){
        $msgidList = Db::table('kt_wework_customermsg')->where(["wid"=>$wid,"send_status"=>2])->select()->toArray();
        foreach ($msgidList as $msg) {
            if($msg["send_time"] <= date("Y-m-d H:i:s",time())){
                $msgs = ["content"=>json_decode($msg["content"],1)];
                $attachments = MassSendModel::assembleOther(json_decode($msg["adjunct_data"],1),$wid);
                $repetition_status = $msg["repetition_status"];
                $addMsg = $msg["id"];
                $customer_gender = $msg["customer_gender"];
                $staffer = json_decode($msg["staffer"],1);
                $groups = json_decode($msg["groups"],1);
                $exclude_tags = json_decode($msg["exclude_tags"],1);
                $tags = json_decode($msg["tags"],1);
                if($msg["start_time"])$times[0] = $msg["start_time"];
                if($msg["end_time"])$times[1] = $msg["end_time"];
                $customer_status = $msg["customer_status"];
                if($customer_status == 2) $externalList = MassSendModel::externalList($staffer,$customer_gender,$groups,$tags,$exclude_tags,$times,$repetition_status,$wid);
                if($customer_status == 1) $externalList = MassSendModel::externalLists($staffer,$repetition_status,$wid);
                if($repetition_status == 1){
                    foreach($externalList as $value){
                        $arr[$value["staff_id"]]["staff_id"] = $value["staff_id"];
                        $arr[$value["staff_id"]]["userid"] = $value["userid"];
                        $arr[$value["staff_id"]]["externalList"][] = $value["external_userid"];
                    }
                    foreach($arr as $value){
                        if(count($value["externalList"])){
                            $res = QyWechatApi::addMsgTemplate($wid,$value["externalList"],$value["userid"],$msg,$attachments);
                            if($res["errcode"] == 0){
                                MassSendModel::addMsgid($value["staff_id"],$addMsg,$res["fail_list"],$res["msgid"],2,count($value["externalList"]),$wid);
                            }
                        }else{
                            MassSendModel::addMsgid($value["staff_id"],$addMsg,NULL,NULL,3,0,$wid);
                        }
                    }
                }else{
                    if($customer_status == 2){
                        $arr = [];
                        foreach($externalList as $value){
                            $arr[$value["staff_id"]]["staff_id"] = $value["staff_id"];
                            $arr[$value["staff_id"]]["userid"] = $value["userid"];
                            $arr[$value["staff_id"]]["externalList"][] = $value["external_userid"];
                        }
                        foreach($arr as $value){
                            if(count($value["externalList"])){
                                $res = QyWechatApi::addMsgTemplate($wid,$value["externalList"],$value["userid"],$msgs,$attachments);
                                if($res["errcode"] == 0){
                                    MassSendModel::addMsgid($value["staff_id"],$addMsg,$res["fail_list"],$res["msgid"],2,count($value["externalList"]),$wid);
                                }
                            }else{
                                MassSendModel::addMsgid($value["staff_id"],$addMsg,NULL,NULL,3,0,$wid);
                            }
                        }
                    }else{
                        $arr = MassSendModel::getAll(NULL,$wid);
                        foreach($arr as $value){
                            if($value["detail_count"] >= 1){
                                $res = QyWechatApi::addMsgTemplate($wid,NULL,$value["userid"],$msgs,$attachments);
                                MassSendModel::addMsgid($value["id"],$addMsg,$res["fail_list"],$res["msgid"],2,$value["detail_count"],$wid);
                            }else{
                                MassSendModel::addMsgid($value["id"],$addMsg,NULL,NULL,3,0,$wid);
                            }
                        }
                    }
                }
                $msgUpd = MassSendModel::msgUpd($msg["id"]);
            }
        }
        return json(['code'=>'success','msg'=>'成功']);
    }
}