<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\model\BaseModel;
use app\wework\model\GroupSendModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;
use app\wework\controller\Base;

/**
* 定时任务，实时发送提醒
*/
class DataFeed extends Base{

    public function index(){
		$mok = date('h');   //得到今天几点
        $subscribe = Db::table('kt_wework_data_subscribe')->where(["status"=>1])->select()->toArray();
 		if(!$subscribe) return success("操作成功");
 		if($mok == 9){
 			foreach ($subscribe as $feed) {
	 			$this->feeds($feed);
	 		}
 		}
        return success("操作成功");
    }

    public function feeds($feed){
    	$mt_day = date('d');   //得到今天是几号
    	$wk_day = date('w');   //得到今天是星期几
    	$status = Db::table("kt_wework_data_status")->where(["wid"=>$feed["wid"]])->whereTime("ctime",">",strtotime(date("Y-m-d")))->find();
    	if($status)return json(['code'=>'success','msg'=>'成功']);
    	$data["wid"] = $feed["wid"];
    	$data["ctime"] = time();
    	Db::table("kt_wework_data_status")->insertGetId($data);
    	$staffList = Db::table("kt_wework_staff")->where(["wid"=>$feed["wid"],"is_del"=>0,"status"=>1,"id"=>json_decode($feed["staff_id"])])->field("userid,id")->select()->toArray();
    	foreach($staffList as $staff){
    		if($feed["day_push"]){
    			$dayStr = "【数据日报】";
    			if($feed["customer_total"]){
    				$customer_total = Db::table("kt_wework_customer")->where(["staff_id"=>$staff["id"]])->whereDay("createtime",date("Y-m-d",strtotime("yesterday")))->count();
    				$dayStr .= "客户总数".$customer_total;
					$dayStr .= "<br>";
    			}
	    		if($feed["customer_new"]){
	    			$customer_new = Db::table("kt_wework_customer")->where(["staff_id"=>$staff["id"],"status"=>0])->whereDay("createtime",date("Y-m-d",strtotime("yesterday")))->count();
	    			$dayStr .= "新增客户数".$customer_new;
					$dayStr .= "<br>";
    			}
    			if($feed["customer_lossing"]){
    				$customer_lossing = Db::table("kt_wework_customer_lossing")->where(["staff_id"=>$staff["id"]])->whereDay("lossing_time",date("Y-m-d",strtotime("yesterday")))->group("external_userid")->count();
    				$dayStr .= "流失客户数".$customer_lossing;
					$dayStr .= "<br>";
    			}
	    		if($feed["customer_netnew"]){
	    			$dayStr .= "净增客户数". ($customer_new - $customer_lossing);
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_total"]){
	    			$group_total = Db::table("kt_wework_group")->where(["staff_id"=>$staff["id"]])->whereDay("create_time",date("Y-m-d",strtotime("yesterday")))->count();
	    			$dayStr .= "客户群总数".$group_total;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_new"]){
	    			$group_new = Db::table("kt_wework_group")->where(["staff_id"=>$staff["id"],"is_dismissed"=>0])->whereDay("create_time",date("Y-m-d",strtotime("yesterday")))->count();
	    			$dayStr .= "新增客户群".$group_new;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_customer_total"]){
	    			$group_customer_total = array_sum(array_column(Db::table("kt_wework_group")
			   		->alias('g')
					->where(["g.staff_id"=>$staff["id"]])
					->whereBetweenTime('g.create_time', strtotime("yesterday"), strtotime("today"))
					->join(['kt_wework_group_member'=>'m'],'m.chat_id = g.chat_id')
					->whereBetweenTime('m.join_time', strtotime("yesterday"), strtotime("today"))
					->field('g.id,count(m.id) as number')
					->group("id")
					->select()
					->toArray(),"number"));
					$dayStr .= "客户群客户总数".$group_customer_total;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_customer_new"]){
	    			$group_customer_new = array_sum(array_column(Db::table("kt_wework_group")
			   		->alias('g')
					->where(["g.staff_id"=>$staff["id"],"m.status"=>1])
					->whereBetweenTime('g.create_time', strtotime("yesterday"), strtotime("today"))
					->join(['kt_wework_group_member'=>'m'],'m.chat_id = g.chat_id')
					->whereBetweenTime('m.join_time', strtotime("yesterday"), strtotime("today"))
					->field('g.id,count(m.id) as number')
					->group("id")
					->select()
					->toArray(),"number"));
					$dayStr .= "客户群新增客户数".$group_customer_new;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_lossing_total"]){
	    			$group_lossing_total = array_sum(array_column(Db::table("kt_wework_group")
			   		->alias('g')
					->where(["g.staff_id"=>$staff["id"],"m.status"=>0])
					->whereBetweenTime('g.create_time', strtotime("yesterday"), strtotime("today"))
					->join(['kt_wework_group_member'=>'m'],'m.chat_id = g.chat_id')
					->whereBetweenTime('m.join_time', strtotime("yesterday"), strtotime("today"))
					->field('g.id,count(m.id) as number')
					->group("id")
					->select()
					->toArray(),"number"));
					$dayStr .= "客户群流失客户总数".$group_lossing_total;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_customer_netnew"]){
	    			$group_customer_netnew = $group_customer_new - $group_lossing_total;
	    			$dayStr .= "客户净增客户数量".$group_customer_netnew;
					$dayStr .= "<br>";
	    		}
	    		$res = QyWechatApi::sendText($feed["wid"],$staff["userid"],$dayStr);
    		}
    		if($feed["week_push"] && $wk_day == "1"){
    			$dayStr = "【数据周报】";
    			if($feed["customer_total"]){
    				$customer_total = Db::table("kt_wework_customer")->where(["staff_id"=>$staff["id"]])->whereDay("createtime",date("Y-m-d",strtotime("-7 day")))->count();
    				$dayStr .= "客户总数".$customer_total;
					$dayStr .= "<br>";
    			}
	    		if($feed["customer_new"]){
	    			$customer_new = Db::table("kt_wework_customer")->where(["staff_id"=>$staff["id"],"status"=>0])->whereDay("createtime",date("Y-m-d",strtotime("-7 day")))->count();
	    			$dayStr .= "新增客户数".$customer_new;
					$dayStr .= "<br>";
    			}
    			if($feed["customer_lossing"]){
    				$customer_lossing = Db::table("kt_wework_customer_lossing")->where(["staff_id"=>$staff["id"]])->whereDay("lossing_time",date("Y-m-d",strtotime("-7 day")))->group("customer_id")->count();
    				$dayStr .= "流失客户数".$customer_lossing;
					$dayStr .= "<br>";
    			}
	    		if($feed["customer_netnew"]){
	    			$dayStr .= "净增客户数". ($customer_new - $customer_lossing);
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_total"]){
	    			$group_total = Db::table("kt_wework_group")->where(["staff_id"=>$staff["id"]])->whereDay("create_time",date("Y-m-d",strtotime("-7 day")))->count();
	    			$dayStr .= "客户群总数".$group_total;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_new"]){
	    			$group_new = Db::table("kt_wework_group")->where(["staff_id"=>$staff["id"],"is_dismissed"=>0])->whereDay("create_time",date("Y-m-d",strtotime("-7 day")))->count();
	    			$dayStr .= "新增客户群".$group_new;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_customer_total"]){
	    			$group_customer_total = array_sum(array_column(Db::table("kt_wework_group")
			   		->alias('g')
					->where(["g.staff_id"=>$staff["id"]])
					->whereBetweenTime('g.create_time', strtotime("-7 day"), strtotime("today"))
					->join(['kt_wework_group_member'=>'m'],'m.chat_id = g.chat_id')
					->whereBetweenTime('m.join_time', strtotime("-7 day"), strtotime("today"))
					->field('g.id,count(m.id) as number')
					->group("id")
					->select()
					->toArray(),"number"));
					$dayStr .= "客户群客户总数".$group_customer_total;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_customer_new"]){
	    			$group_customer_new = array_sum(array_column(Db::table("kt_wework_group")
			   		->alias('g')
					->where(["g.staff_id"=>$staff["id"],"m.status"=>1])
					->whereBetweenTime('g.create_time', strtotime("-7 day"), strtotime("today"))
					->join(['kt_wework_group_member'=>'m'],'m.chat_id = g.chat_id')
					->whereBetweenTime('m.join_time', strtotime("-7 day"), strtotime("today"))
					->field('g.id,count(m.id) as number')
					->group("id")
					->select()
					->toArray(),"number"));
					$dayStr .= "客户群新增客户数".$group_customer_new;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_lossing_total"]){
	    			$group_lossing_total = array_sum(array_column(Db::table("kt_wework_group")
			   		->alias('g')
					->where(["g.staff_id"=>$staff["id"],"m.status"=>0])
					->whereBetweenTime('g.create_time', strtotime("-7 day"), strtotime("today"))
					->join(['kt_wework_group_member'=>'m'],'m.chat_id = g.chat_id')
					->whereBetweenTime('m.join_time', strtotime("-7 day"), strtotime("today"))
					->field('g.id,count(m.id) as number')
					->group("id")
					->select()
					->toArray(),"number"));
					$dayStr .= "客户群流失客户总数".$group_lossing_total;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_customer_netnew"]){
	    			$group_customer_netnew = $group_customer_new - $group_lossing_total;
	    			$dayStr .= "客户净增客户数量".$group_customer_netnew;
					$dayStr .= "<br>";
	    		}
	    		$res = QyWechatApi::sendText($feed["wid"],$staff["userid"],$dayStr);
    		}
    		if($feed["month_push"] && $mt_day == "1"){
    			$dayStr = "【数据月报】";
    			if($feed["customer_total"]){
    				$customer_total = Db::table("kt_wework_customer")->where(["staff_id"=>$staff["id"]])->whereDay("createtime",date("Y-m-d",strtotime("last month")))->count();
    				$dayStr .= "客户总数".$customer_total;
					$dayStr .= "<br>";
    			}
	    		if($feed["customer_new"]){
	    			$customer_new = Db::table("kt_wework_customer")->where(["staff_id"=>$staff["id"],"status"=>0])->whereDay("createtime",date("Y-m-d",strtotime("last month")))->count();
	    			$dayStr .= "新增客户数".$customer_new;
					$dayStr .= "<br>";
    			}
    			if($feed["customer_lossing"]){
    				$customer_lossing = Db::table("kt_wework_customer_lossing")->where(["staff_id"=>$staff["id"]])->whereDay("lossing_time",date("Y-m-d",strtotime("last month")))->group("customer_id")->count();
    				$dayStr .= "流失客户数".$customer_lossing;
					$dayStr .= "<br>";
    			}
	    		if($feed["customer_netnew"]){
	    			$dayStr .= "净增客户数". ($customer_new - $customer_lossing);
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_total"]){
	    			$group_total = Db::table("kt_wework_group")->where(["staff_id"=>$staff["id"]])->whereDay("create_time",date("Y-m-d",strtotime("last month")))->count();
	    			$dayStr .= "客户群总数".$group_total;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_new"]){
	    			$group_new = Db::table("kt_wework_group")->where(["staff_id"=>$staff["id"],"is_dismissed"=>0])->whereDay("create_time",date("Y-m-d",strtotime("last month")))->count();
	    			$dayStr .= "新增客户群".$group_new;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_customer_total"]){
	    			$group_customer_total = array_sum(array_column(Db::table("kt_wework_group")
			   		->alias('g')
					->where(["g.staff_id"=>$staff["id"]])
					->whereBetweenTime('g.create_time', strtotime("last month"), strtotime("today"))
					->join(['kt_wework_group_member'=>'m'],'m.chat_id = g.chat_id')
					->whereBetweenTime('m.join_time', strtotime("last month"), strtotime("today"))
					->field('g.id,count(m.id) as number')
					->group("id")
					->select()
					->toArray(),"number"));
					$dayStr .= "客户群客户总数".$group_customer_total;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_customer_new"]){
	    			$group_customer_new = array_sum(array_column(Db::table("kt_wework_group")
			   		->alias('g')
					->where(["g.staff_id"=>$staff["id"],"m.status"=>1])
					->whereBetweenTime('g.create_time', strtotime("last month"), strtotime("today"))
					->join(['kt_wework_group_member'=>'m'],'m.chat_id = g.chat_id')
					->whereBetweenTime('m.join_time', strtotime("last month"), strtotime("today"))
					->field('g.id,count(m.id) as number')
					->group("id")
					->select()
					->toArray(),"number"));
					$dayStr .= "客户群新增客户数".$group_customer_new;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_lossing_total"]){
	    			$group_lossing_total = array_sum(array_column(Db::table("kt_wework_group")
			   		->alias('g')
					->where(["g.staff_id"=>$staff["id"],"m.status"=>0])
					->whereBetweenTime('g.create_time', strtotime("last month"), strtotime("today"))
					->join(['kt_wework_group_member'=>'m'],'m.chat_id = g.chat_id')
					->whereBetweenTime('m.join_time', strtotime("last month"), strtotime("today"))
					->field('g.id,count(m.id) as number')
					->group("id")
					->select()
					->toArray(),"number"));
					$dayStr .= "客户群流失客户总数".$group_lossing_total;
					$dayStr .= "<br>";
	    		}
	    		if($feed["group_customer_netnew"]){
	    			$group_customer_netnew = $group_customer_new - $group_lossing_total;
	    			$dayStr .= "客户净增客户数量".$group_customer_netnew;
					$dayStr .= "<br>";
	    		}
	    		$res = QyWechatApi::sendText($feed["wid"],$staff["userid"],$dayStr);
    		}
    	}
 		return json(['code'=>'success','msg'=>'成功']);
    }
 }