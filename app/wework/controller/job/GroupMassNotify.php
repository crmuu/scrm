<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\model\BaseModel;
use app\wework\model\user\send\GroupSendModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;
use app\wework\controller\Base;


/**
* 定时任务，实时发送提醒
*/
class GroupMassNotify extends Base{
     public function index(){
          $config_list = Db::table('kt_wework_config')->where(["customer_status"=>1])->column("wid");
          if(!$config_list) return success("操作失败");
          foreach ($config_list as $user) {
               $this->sendMsg($user);
          }

          return success("操作成功");
     }

     public function sendMsg($wid){
          $msgidList = Db::table('kt_wework_groupmsg')->where(["wid"=>$wid,"send_status"=>2,"status"=>0])->json(["content"])->select()->toArray();
          foreach ($msgidList as $msg) {
               if($msg["send_time"] <= date("Y-m-d H:i:s",time())){
                    $content = $msg["content"];
                    $attachments = GroupSendModel::assembleOther(json_decode($msg["adjunct_data"],1),$wid);
                    $staffer = json_decode($msg["staffer"],1);
                    $groiup_list = GroupSendModel::getStaffer($staffer);
                    foreach($groiup_list as $value){
                         $res = QyWechatApi::addMsgGroup($wid,$value["userid"],$content,$attachments,"group");
                         if($res["errcode"] == 0){
                              GroupSendModel::addMsgid($value["id"],$msg["id"],$res["msgid"],$wid);
                         }
                    }
                    $msgUpd = GroupSendModel::msgUpd($msg["id"]);
               }
          }
          return success("操作成功");
     }
}