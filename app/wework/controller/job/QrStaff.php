<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\controller\job;

use app\BaseController;
use think\facade\Db;
use app\wework\model\BaseModel;
use app\wework\model\MassSendModel;
use app\wework\model\QyWechatApi;
use think\facade\Log;
use think\queue\Job;
use app\wework\controller\Base;


/**
  * 定时任务， 渠道活码 员工添加上限
*/
class QrStaff extends Base{

	public function index(){
		$config_list = Db::table('kt_wework_config')->where(["customer_status"=>1])->column("wid");
 		if(!$config_list) return success("操作失败");
 		foreach ($config_list as $user) {
 			$this->staffup($user);
 		}
        return success("操作成功");
	}

	public function staffup($wid){
		$qr_list = Db::table("kt_wework_qr")->where(["staff_type"=>2])->json(["staff_data","staffid","staff_standby"])->select()->toArray();
		foreach ($qr_list as $qr){
			$arr = [];
			$wk_day = date('w');
			foreach ($qr["staff_data"] as $staff_data){
				$staff_data["week"] = explode(",",$staff_data["week"]);
				if(in_array($wk_day,$staff_data["week"])){
					if($staff_data["startime"] <= date("H:i") && $staff_data["endtime"] > date("H:i")){
						$arr = array_merge($arr,$staff_data["staffid"]);
					}
				}
			}
			if(!$arr)$arr = $qr["staff_standby"];
			if(!$arr) continue;
			$upd['config_id'] = $qr['config_id'];
			$upd['user'] = Db::table('kt_wework_staff')->where(['id'=>$arr])->column('userid');;
			$res = QyWechatApi::updateContactWay($upd,$wid);
			if($res["errcode"] != 0)Log::error($res);
		}
	}

}