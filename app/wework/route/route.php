<?php
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

use think\facade\Route;
//用户后台路由
Route::group('job',function(){
	Route::any('index', 'job.TimedTask/index');
	Route::any('wechatmoments', 'job.WeChatMoments/index');
	Route::any('masssend', 'job.MassSend/index');
	Route::any('massnotify', 'job.MassNotify/index');
	Route::any('groupmasssend', 'job.GroupMassSend/index');
	Route::any('groupmassnotify', 'job.GroupMassNotify/index');
	Route::any('datafeed', 'job.DataFeed/index');
	Route::any('qr', 'job.Qr/index');
	Route::any('recover', 'job.QrRecover/index');
	Route::any('time', 'job.QrTime/index');
	Route::any('staff', 'job.QrStaff/index');
	Route::any('sop', 'job.CustomerSopJob/index');
	Route::any('groupsopjob', 'job.GroupSopJob/index');
	Route::any('groupsop', 'job.GroupSop/index');
	Route::any('festival', 'job.CustomerSopFestival/index');
	Route::any('customersop', 'job.CustomerSop/index');
	Route::any('colonization', 'job.Colonization/index');
	Route::any('coloniz', 'job.ColonizationUser/index');
});

Route::group('user',function(){
	// 好友欢迎语 入群欢迎语
	Route::group('welcome', function () {
		Route::post('list', 'user.welcome.WelcomMessage/index');
		Route::post('add', 'user.welcome.WelcomMessage/addWelcom');
		Route::post('del', 'user.welcome.WelcomMessage/delWelcom');
		Route::post('info', 'user.welcome.WelcomMessage/info');


		Route::post('grouplist', 'user.welcome.GroupWelcome/index');
		Route::post('detail', 'user.welcome.GroupWelcome/detail');
		Route::post('delwelcom', 'user.welcome.GroupWelcome/delWelcome');
		Route::post('addwelcom', 'user.welcome.GroupWelcome/welcomeTemplateCreate');
	});

	//企业标签
	Route::group('tag', function () {
		Route::post('list', 'user.label.Tag/index');
		Route::post('addgroup', 'user.label.Tag/addGroup');
		Route::post('updgroup', 'user.label.Tag/updateGroup');
		Route::post('detail', 'user.label.Tag/detailGroup');
		Route::post('add', 'user.label.Tag/addTag');
		Route::post('order', 'user.label.Tag/exchageWeborder');
		Route::post('del', 'user.label.Tag/deleteGroup');
		Route::post('sync', 'user.label.Tag/sync');
		Route::post('grouplist', 'user.label.GroupTag/index');
		Route::post('addtags', 'user.label.GroupTag/addTag');
		Route::post('addtag', 'user.label.GroupTag/add');
		Route::post('updtag', 'user.label.GroupTag/updTag');
		Route::post('deltags', 'user.label.GroupTag/delTagGroup');
		Route::post('deltag', 'user.label.GroupTag/delTag');
		Route::post('info', 'user.label.GroupTag/info');
		Route::post('taglist', 'user.label.GroupTag/tagList');
	});

	//企业成员
	Route::group('staff', function () {
		Route::post('pop', 'user.staff.Staff/pop');  //弹框 获取成员与部门信息 
		Route::get('departmentlist', 'user.staff.Department/list');  // 成员管理页 获取部门列表
		Route::get('departmentsearch', 'user.staff.Department/searchList');  // 成员管理页 获取部门列表
		Route::post('index', 'user.staff.Staff/index');
		Route::post('detail', 'user.staff.Staff/detail');
		Route::post('statistics', 'user.staff.Staff/statistics');
		Route::post('sync', 'user.staff.Staff/sync');
		Route::post('users', 'user.staff.Staff/users');
		Route::post('department', 'user.staff.Staff/department');
	});
	//渠道活码
	Route::group('qr', function () {
		Route::get('group', 'user.qr.Qr/group');  //渠道活码分组
		Route::post('groupsave', 'user.qr.Qr/groupSave');  //添加|更新 渠道活码分组
		Route::post('groupdel', 'user.qr.Qr/groupDelete');  //删除渠道活码分组
		Route::post('list', 'user.qr.Qr/index');  //删除渠道活码分组
		Route::post('add', 'user.qr.Qr/add');  //删除渠道活码分组
		Route::any('detail', 'user.qr.Qr/detail');  //删除渠道活码分组
		Route::any('statistics', 'user.qr.Qr/statisticsQrList');  //删除渠道活码分组
		Route::any('statisticssize', 'user.qr.Qr/statisticsInfo');  //删除渠道活码分组
		Route::any('map', 'user.qr.Qr/statisticsChartData');  //删除渠道活码分组
		Route::any('table', 'user.qr.Qr/statisticsTableData');  //删除渠道活码分组
		Route::any('stafflist', 'user.qr.Qr/statisticsStaff');  //删除渠道活码分组
		Route::any('infos', 'user.qr.Qr/infos');  //删除渠道活码分组
		Route::get('downexcel', 'user.qr.Qr/downexcel');  //导出Excel
		Route::get('downexceltable', 'user.qr.Qr/downexcelTable');  //导出Excel
		Route::post('delete', 'user.qr.Qr/delete');  //导出Excel
	});

	//自定义信息
	Route::group('customized', function () {
		Route::post('list', 'user.customized.Setting/index');
		Route::post('updstatus', 'user.customized.Setting/updateStatus');
		Route::post('updstot', 'user.customized.Setting/updateStot');
		Route::post('save', 'user.customized.Setting/save');
		Route::post('del', 'user.customized.Setting/delete');
	});

	//客户管理
	Route::group('client', function () {
		Route::post('list', 'user.client.Customer/index');
		Route::post('sync', 'user.client.Customer/sync');
		Route::post('info', 'user.client.Customer/Info');
		Route::post('customerinfo', 'user.client.Customer/customerInfo');
		Route::post('dynamics', 'user.client.Customer/dynamics');
		Route::post('grouplist', 'user.client.Customer/groupList');
		Route::post('detailuser', 'user.client.Customer/customerUser');
		Route::post('customer', 'user.client.Customer/customer');
		Route::post('updscore', 'user.client.Customer/updScore');
		Route::post('updintegral', 'user.client.Customer/updIntegral');
		Route::post('addtag', 'user.client.Customer/addTags');
		Route::post('deltag', 'user.client.Customer/delTags');
		Route::post('way', 'user.client.Customer/way');
		Route::get('downexcel', 'user.client.Customer/downexcel');
		Route::post('upload', 'user.client.Customer/upload');
		Route::post('setfield', 'user.client.Customer/setfield');
		Route::post('setfieldlist', 'user.client.Customer/setfieldList');
		Route::post('upduserfields', 'user.client.Customer/updUserFields');
	});

	//群列表
	Route::group('group', function () {
		Route::post('list', 'user.group.Group/index');
		Route::post('detail', 'user.group.Group/detail');
		Route::post('map', 'user.group.Group/detailMap');
		Route::post('sync', 'user.group.Group/sync');
		Route::post('groupsync', 'user.group.Group/syncGroup');
		Route::post('clientdetail', 'user.group.Group/detailClient');
		Route::post('batch', 'user.group.Group/batchTag');
		Route::post('chatsync', 'user.group.Group/chatSync');
		Route::get('downexcel', 'user.group.Group/downexcel');
	});

	//客户群群发
	Route::group('send', function () {
		Route::post('list', 'user.send.GroupSend/index');
		Route::post('add', 'user.send.GroupSend/addMsg');
		Route::post('getmsg', 'user.send.GroupSend/getMsg');
		Route::post('grouplist', 'user.send.GroupSend/getGroupAll');
		Route::post('userlist', 'user.send.GroupSend/getUserAll');
		Route::post('upds', 'user.send.GroupSend/upds');
		Route::post('alert', 'user.send.GroupSend/alertGroup');
		Route::post('alerts', 'user.send.GroupSend/alertGroups');
		Route::post('groupsync', 'user.send.GroupSend/sync');
		Route::post('groupsyncs', 'user.send.GroupSend/syncs');
		Route::get('groupdownexcel', 'user.send.GroupSend/downexcel');


		Route::post('sendlist', 'user.send.MassSend/index');
		Route::post('addmsg', 'user.send.MassSend/addMsg');
		Route::post('usermsg', 'user.send.MassSend/getMsg');
		Route::post('detailuser', 'user.send.MassSend/detailUser');
		Route::post('detailcustomer', 'user.send.MassSend/detailCustomer');
		Route::post('alertcustomers', 'user.send.MassSend/alertCustomers');
		Route::post('alertcustomer', 'user.send.MassSend/alertCustomer');
		Route::post('size', 'user.send.MassSend/getSize');
		Route::post('sync', 'user.send.MassSend/sync');
		Route::post('syncs', 'user.send.MassSend/syncs');
		Route::get('downexcel', 'user.send.MassSend/downexcel');

	});


	//数据订阅
	Route::group('feed', function () {
		Route::post('index', 'user.feed.DataFeed/index');
		Route::post('add', 'user.feed.DataFeed/addFeed');
		Route::post('upd', 'user.feed.DataFeed/upFeed');
	});

	//首页数据统计
	Route::group('statistics', function () {
		Route::post('index', 'user.statistics.Statistics/index');
		Route::post('detail', 'user.statistics.Statistics/detail');
	});

	//批量加好友
	Route::group('batch', function () {
		Route::post('list', 'user.batch.BatchAddUser/index');
		Route::post('histroy', 'user.batch.BatchAddUser/histroy');
		Route::post('delbatch', 'user.batch.BatchAddUser/delBatch');
		Route::post('deluser', 'user.batch.BatchAddUser/delUser');
		Route::post('mission', 'user.batch.BatchAddUser/missionBatch');
		Route::post('missions', 'user.batch.BatchAddUser/missionNotify');
		Route::post('detail', 'user.batch.BatchAddUser/histroyDetail');
		Route::post('details', 'user.batch.BatchAddUser/details');
		Route::post('add', 'user.batch.BatchAddUser/addBatch');
		Route::post('statistics', 'user.batch.BatchAddUser/statistics');//1
	});

	Route::group('config', function () {
		Route::post('basic', 'user.config.ConfigServer/basic');
		Route::post('staff', 'user.config.ConfigServer/staff');
		Route::post('customer', 'user.config.ConfigServer/customer');
		Route::post('selfapp', 'user.config.ConfigServer/selfapp');
		Route::post('updselfapp', 'user.config.ConfigServer/selfappSave');
		Route::post('updbasic', 'user.config.ConfigServer/basicSave');
		Route::post('updstaff', 'user.config.ConfigServer/staffSave');
		Route::post('updcustomer', 'user.config.ConfigServer/customerSave');
	});

	//互动雷达
	Route::group('radar', function () {
		Route::post('list', 'user.radar.InteractiveRadar/index');
		Route::post('add', 'user.radar.InteractiveRadar/create');
		Route::post('del', 'user.radar.InteractiveRadar/del');
		Route::post('urldata', 'user.radar.InteractiveRadar/urlData');
		Route::post('info', 'user.radar.InteractiveRadar/info');
	});

	//流失/删人提醒
	Route::group('lost', function () {
		Route::post('list', 'user.lost.LostCustomer/index');
		Route::post('set', 'user.lost.LostCustomer/set');
		Route::get('downexcel', 'user.lost.LostCustomer/downexcel');
		Route::post('status', 'user.lost.LostCustomer/status');
	});

	//自动拉群
	Route::group('autogroup', function () {
		Route::post('add', 'user.autogroup.AutoPullGroup/add');
		Route::post('upd', 'user.autogroup.AutoPullGroup/upd');
		Route::post('getinfo', 'user.autogroup.AutoPullGroup/getInfo');
		Route::post('list', 'user.autogroup.AutoPullGroup/index');
		Route::post('groupings', 'user.autogroup.AutoPullGroup/groupings');
		Route::post('addgrouping', 'user.autogroup.AutoPullGroup/addGrouping');
		Route::post('getbatch', 'user.autogroup.AutoPullGroup/getBatch');
		Route::post('batch', 'user.autogroup.AutoPullGroup/batchUpd');
		Route::post('details', 'user.autogroup.AutoPullGroup/details');
		Route::post('del', 'user.autogroup.AutoPullGroup/del');
		Route::post('delgrouping', 'user.autogroup.AutoPullGroup/delGrouping');
		Route::post('grouping', 'user.autogroup.AutoPullGroup/grouping');
		Route::post('groupids', 'user.autogroup.AutoPullGroup/getGroups');
	});

	//快捷回复
	Route::group('reply', function () {
		Route::post('list', 'user.reply.Reply/index');
		Route::post('groupinglist', 'user.reply.Reply/groupManage');
		Route::post('addgrouping', 'user.reply.Reply/addgroupManage');
		Route::post('getgrouping', 'user.reply.Reply/getgroupManage');
		Route::post('updgrouping', 'user.reply.Reply/updgroupManage');
		Route::post('movegrouping', 'user.reply.Reply/moveGroupManage');
		Route::post('move', 'user.reply.Reply/move');
		Route::post('delgrouping', 'user.reply.Reply/delgroupManage');
		Route::post('get', 'user.reply.Reply/getReply');
		Route::post('add', 'user.reply.Reply/addReply');
		Route::post('del', 'user.reply.Reply/delReply');
	});
	
	//sop
	Route::group('sop', function () {
		Route::post('grouplist', 'user.sop.GroupSop/index');
		Route::post('groupdetail', 'user.sop.GroupSop/detail');
		Route::post('groupdelete', 'user.sop.GroupSop/delte');
		Route::post('groupadd', 'user.sop.GroupSop/add');
		Route::post('size', 'user.sop.GroupSop/size');
		Route::post('groupopen', 'user.sop.Sop/open');
		Route::post('list', 'user.sop.Sop/index');
		Route::post('detail', 'user.sop.Sop/detail');
		Route::post('delete', 'user.sop.Sop/delte');
		Route::post('add', 'user.sop.Sop/add');
		Route::post('open', 'user.sop.Sop/open');
		Route::post('festival', 'user.sop.Sop/festival');
	});

	//数据统计
	Route::group('data', function () {
		Route::post('customer', 'user.data.DataStatistics/customer');
		Route::post('customerdetail', 'user.data.DataStatistics/customerDetail');
		Route::post('customertable', 'user.data.DataStatistics/customerTable');
		Route::post('group', 'user.data.DataStatistics/group');
		Route::post('groupdetail', 'user.data.DataStatistics/groupDetail');
		Route::post('grouptable', 'user.data.DataStatistics/groupTable');
		Route::post('staff', 'user.data.DataStatistics/staff');
		Route::post('staffdetail', 'user.data.DataStatistics/staffDetail');
		Route::post('stafftable', 'user.data.DataStatistics/staffTable');
		Route::post('stafftop', 'user.data.DataStatistics/staffTop');
		Route::post('qrcode', 'user.data.DataStatistics/qrcode');
		Route::post('qrcodetable', 'user.data.DataStatistics/qrcodeTable');
		Route::post('qrcodedetail', 'user.data.DataStatistics/qrcodeDetail');
		Route::post('groupdepartment', 'user.data.DataStatistics/groupDepartment');
		Route::post('staffdepartment', 'user.data.DataStatistics/staffDepartment');
		Route::post('department', 'user.data.DataStatistics/getDepartment');
		Route::get('customerdownexcel', 'user.data.DataStatistics/customerDownexcel');
		Route::get('groupdownexcel', 'user.data.DataStatistics/groupDownexcel');
		Route::get('staffdownexcel', 'user.data.DataStatistics/staffDownexcel');
		Route::get('qrcodedownexcel', 'user.data.DataStatistics/qrcodeDownexcel');
	});

	//企业朋友圈
	Route::group('moments', function () {
		Route::post('list', 'user.moments.WeChatMoments/index');
		Route::post('detail', 'user.moments.WeChatMoments/detail');
		Route::post('add', 'user.moments.WeChatMoments/addMoments');
		Route::post('sync', 'user.moments.WeChatMoments/sync');
		Route::post('remind', 'user.moments.WeChatMoments/remind');
		Route::post('syncs', 'user.moments.WeChatMoments/syncs');
		Route::post('qysyncs', 'user.moments.WeChatMoments/qySync');
		Route::get('downexcel', 'user.moments.WeChatMoments/downexcel');
		Route::get('downexcellist', 'user.moments.WeChatMoments/downexcelList');
	});

	//标签建群
	Route::group('colonization', function () {
		Route::post('list', 'user.colonization.Colonization/index');
		Route::post('add', 'user.colonization.Colonization/add');
		Route::post('del', 'user.colonization.Colonization/del');
		Route::post('detail', 'user.colonization.Colonization/detail');
		Route::post('customer', 'user.colonization.Colonization/customerStatistics');
		Route::post('staff', 'user.colonization.Colonization/staffStatistics');
	});

	//自助推广
	Route::group('promotion', function () {
		Route::post('list', 'user.promotion.BuffetPromotion/index');
		Route::post('add', 'user.promotion.BuffetPromotion/add');
		Route::post('info', 'user.promotion.BuffetPromotion/info');
		Route::post('edit', 'user.promotion.BuffetPromotion/edit');
		Route::post('share', 'user.promotion.BuffetPromotion/share');
		Route::post('del', 'user.promotion.BuffetPromotion/del');
		Route::post('statistics', 'user.promotion.BuffetPromotion/statistics');
		Route::post('map', 'user.promotion.BuffetPromotion/map');
		Route::post('table', 'user.promotion.BuffetPromotion/table');
	});

	//公众号
	Route::group('wechat', function () {
		Route::post('list', 'user.wechat.WechatOpen/index');
		Route::post('rebind', 'user.wechat.WechatOpen/rebind');
		Route::any('call', 'user.wechat.WechatOpen/call');
	});


});
