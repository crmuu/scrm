<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\model;
use think\facade\Db;
use think\facade\Session;

class BaseModel
{	
	/**
	 * @param table 表名
	 * @param where array|string, 查询条件
	 * @param whereRaw string, 查询条件字符串
	 * @param field string,默认全部, sting|array
	 * @param page  int 页码 
	 * @param size  int 每页条数 
	 * @return 带有分页的数据 
	 */

 	static public function getList($table, $where=[],$whereRaw=false,$field=true, $page=1, $size=10){
		$data = [];
		$res = Db::table($table)->field($field);
		if($where) $res = $res->where($where);
		if($whereRaw) $res = $res->whereRaw($whereRaw);
		$data['count'] = $res->count();
		$data['item'] = $res->page($page,$size)->select()->toArray();
		$data['page'] = $page;
		$data['size'] = $size;
		return $data;
	}

	/**
	 * @param table 表名
	 * @param where array|string, 查询条件
	 * @param whereRaw string, 查询条件字符串
	 * @param field string,默认全部, sting|array
	 * @return 全部数据 
	 */
	static public function getAll($table, $where = [], $whereRaw=false,$field=true){
		$res = Db::table($table)->field($field);
		if($where) $res = $res->where($where);
		if($whereRaw) $res = $res->whereRaw($whereRaw);
		$res = $res->select()->toArray();
		return $res;
	}

	/**
	 *	获取一条数据
	 * @param table 表名
	 * @param where array|string, 查询条件
	 * @param field string,默认全部, sting|array
	 * @return 全部数据 
	 */
	static public function find($table, $where = [],$field=true,$jsonField=[]){
		$res = Db::table($table)->field($field);
		if($jsonField) $res = $res->json($jsonField);
		if($where){
			$res = $res->where($where);
		} 
		$res = $res->find() ?: [];
		return $res;
	}

	/**
	 * 添加一条数据
	 * @param table 表名
	 * @param data 数据 array
	 * @return 返回自动递增主键id
	 */
	static public function insertGetId($table, $data){
		$res = Db::table($table)->insertGetId($data);
		return $res;
	}
	/**
	 * 添加多条数据
	 * @param table 表名
	 * @param data 数据 array
	 * @return 返回添加条数
	 */
	static public function insertAll($table, $data){
		$res = Db::table($table)->insertAll($data);
		return $res;
	}

	/**
	 * 删除数据
	 * @param table 表名
	 * @param where 数据 array|int 主键id数组,或单个id
	 * @return 返回删除条数
	 */
	static public function deleteById($table, $where){
		$res = Db::table($table)->delete($where);
		return $res;
	}
	/**
	 * 更新|添加方法 save
	 * @param table 表名
	 * @param data 数据 
	 * @return 返回更新条数
	 */
	static public function save($table, $data){
		$res = Db::table($table)->save($data);
		return $res;
	}
	/**
	 *  column
	 * @param table 表名
	 * @param where 数据 
	 * @return 返回更新条数
	 */
	static public function column($table, $where,$field){
		$res = Db::table($table)->where($where)->column($field);
		return $res;
	}

	/**
	 * @param table 表名
	 * @param where array|string, 查询条件
	 * @return 统计数量 
	 */
	static public function count($table, $where = [],$whereRaw=false){
		$data = [];
		$res = Db::table($table);
		if($where) $res = $res->where($where);
		if($whereRaw) $res =$res->whereRaw($whereRaw);
		$data = $res->count();
		return $data;
	}
	
}
