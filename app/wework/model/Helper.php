<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\model;
use think\facade\Db;
use think\facade\Session;

/**
* 公共使用model
**/
class Helper {
	static public $wid;
	
	/**
	 * 获取企业配置信息
	 * @param $corp_id 企业id
	 * @return 
	*/
 	static public function getConfig($wid){
 		$res = Db::table("kt_wework_config")->where(["wid"=>$wid])->find();
 		return $res;
	}

	/**
	 * 获取企业自建应用信息
	 * @param $agent_id 企业自建应用id
	 * @param $wid 企业唯一标识
	 * @return 
	*/
 	static public function getSelfapp($wid){
 		$res = Db::table("kt_wework_selfapp")->where(["wid"=>$wid])->find();
 		return $res;
	}

	/**
	* 获取员工信息
	*/
	static public function getUser($where){
		$res = Db::table("kt_wework_staff")->where($where)->find();
		return $res;
	}

	/**
	* 获取员工信息
	*/
	static public function getCustomer($where){
		$res = Db::table("kt_wework_customer")->where($where)->find();
		return $res;
	}

	static public function saveCustomer($where,$data){
		$res = Db::table("kt_wework_customer")->where($where)->save($data);
		return $res;
	}
}