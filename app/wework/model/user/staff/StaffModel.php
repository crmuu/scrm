<?php 
namespace app\wework\model\user\staff;
use think\facade\Db;
use think\facade\Session;
use app\wework\model\QyWechatApi;

/**
* 成员
**/
class StaffModel
{	
	static public $wid;
	/**
	 * 成员列表
	 * @param $wid 账户id
	 * @return 
	 */
 	static public function list($where=[],$department=false, $page=1, $size=10){
 		$wid = Session::get("wid");
 		$follow_user = QyWechatApi::get_follow_user_list($wid);
		$res = Db::table('kt_wework_staff')
			   ->alias('s')
			   ->field('s.id,s.wid,s.name,s.avatar,s.userid,s.department,s.department_name,s.status,count(c.id) as customer_number')
			   ->json(["department","department_name"])
			   ->leftjoin('kt_wework_customer c','s.id=c.staff_id');
		if($where) $res = $res->where($where);
		if($department){
		    $department_name = Db::table("kt_wework_department")->where(["wid"=>$wid,"department_id"=>$department])->find()["department_name"];
		    $res = $res->where('s.department','like','%'.$department.'%');
		    $res = $res->where('s.department_name','like','%'."$department_name".'%');
		}
		$res = $res->group('id');
		$data['count'] = $res->count();
		$data['item'] = $res->page($page,$size)
							->filter(function($staff) use($follow_user,$wid){
								$staff["department"] = array_column(Db::table("kt_wework_department")->where(["wid"=>$wid,"department_id"=>$staff["department"]])->field("department_name")->select()->toArray(), "department_name");
								$staff["type"] = "无权限";
								if(in_array($staff["userid"],$follow_user))$staff["type"] = "正常";

								return $staff;
							})
							->select()
							->toArray();
		$data['page'] = $page;
		$data['size'] = $size;
		return $data;
	}

	static public function departments($departmentid){
 		$wid = Session::get("wid");
		$ids = [];
		$ids[] = $departmentid;
		self::getDepartmentsId($departmentid,$ids);
		return $ids;
	}

	static public function getDepartmentsId($departmentids,&$ids){
 		$wid = Session::get("wid");
		$res = Db::table("kt_wework_department")->where(["wid"=>$wid,"department_parentid"=>$departmentids])->field("department_id,department_name")->column("department_id");
		if(count($res)>0){
			foreach ($res as $id) {
				$ids[] = $id;
				self::getDepartmentsId($id,$ids);
			}
		}
		return $ids;
	}

	static public function department($department_id=NULL,$department_name=NULL){
 		$wid = Session::get("wid");
 		if(!$department_id){
            $department_id = Db::table("kt_wework_department")->where(["wid"=>$wid])->min("department_parentid");
 		}
 		if($department_name)$res = Db::table("kt_wework_department")->where(["wid"=>$wid,"department_parentid"=>$department_id])->where("department_name","like","%".$department_name."%")->field("department_id,department_name")->order("department_parentid ASC")->select()->toArray();
 		if(!$department_name)$res = Db::table("kt_wework_department")->where(["wid"=>$wid,"department_parentid"=>$department_id])->field("department_id,department_name")->order("department_parentid ASC")->select()->toArray();

 		return $res;
	}

	/**
	 * 成员详情
	 * @param $id 更新数据 array
	 * @return 
	 */
 	static public function detail($id){
 		$wid = Session::get("wid");
		$res = Db::table('kt_wework_staff')
			   ->alias('s')
			   ->field('s.id,s.wid,s.name,s.avatar,s.department,s.status,count(c.id) as customer_number')
			   ->json(["department"])
			   ->leftjoin('kt_wework_customer c','s.id=c.staff_id')
			   ->filter(function($staff) use($wid){
			   		if($staff["department"])$staff["department"] = Db::table("kt_wework_department")->where(["wid"=>$wid,"department_id"=>$staff["department"]])->field("department_id,department_name")->select()->toArray();

			   		return $staff;
			   })
			   ->group('id')
			   ->find($id);
		$time = strtotime(date('Y-m-d'));
		$res['today_new'] = Db::table('kt_wework_customer')->where(['wid'=>$res['wid'],'staff_id'=>$id])->where('createtime','>',$time)->count();
		return $res;
	}
	/**
	 * 获取联系客户统计数据
	 * @param $id array 企业成员主键id
	 * @param $partyid array 企业部门id
	 * @param $start_time int 开始时间
	 * @param $end_time int 结束时间
	 * @return 
	 */
 	static public function behaviorData($id=array(), $partyid=array(),$start_time, $end_time){
 		$userid = [];
 		if($id) $userid = Db::table('kt_wework_staff')->where(['id'=>$id])->column("userid");
 		$res = QyWechatApi::getUserBehaviorData($userid,$partyid,$start_time,$end_time);
		$data = [];
		if($res['errcode'] == 0){
			$data = $res['behavior_data'];
		}

		return $data;
	}


	/**
	 * 更新成员
	 * @param $data 更新数据 array
	 * @return 
	 */
 	static public function update($data){
		$res = Db::table('kt_wework_staff')->insert($data);
		return $res;
	}
	/**
	 * 同步部门
	 * @param $data 更新数据 array
	 * @return 
	 */
 	static public function updateDepart($department_list){
 		$wid = Session::get('wid');
 		foreach ($department_list as $value){
 			$department = Db::table("kt_wework_department")->where(["wid"=>$wid,"department_id"=>$value["id"]])->find();
 			$data["wid"] = $wid;
 			$data["department_id"] = $value["id"];
 			$data["department_name"] = $value["name"];
 			$data["department_parentid"] = $value["parentid"];
 			$data["department_order"] = $value["order"];
 			if($department){
 				$res = Db::table("kt_wework_department")->where(["wid"=>$wid,"department_id"=>$value["id"]])->update($data);
 			}else{
 				$data["create_time"] = date("Y-m-d H:i:s");
 				$res = Db::table("kt_wework_department")->where(["wid"=>$wid,"department_id"=>$value["id"]])->insertGetId($data);
 			}
 		}
		return $res;
	}


}