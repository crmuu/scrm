<?php 
namespace app\wework\model\user\data;
use think\facade\Db;
use think\facade\Session;
use app\wework\model\QyWechatApi;
use dh2y\qrcode\QRcode;

/**
* 数据统计 model
**/
class DataStatisticsModel {
	static public function addCustomer($wid,$time_one,$time_tow,$staffs=NULL,$status=NULL){
		$wid = Session::get("wid")?:$wid;
		$where["wid"] = $wid;
		if($status)$where["status"] = 0;
		if($staffs)$where["staff_id"] = $staffs;
		$res = Db::table("kt_wework_customer")->where($where)->whereTime('createtime', 'between', [$time_one, $time_tow])->count();

		return $res;
	}

	static public function delCustomer($wid,$time_one,$time_tow,$staffs=NULL){
		$wid = Session::get("wid")?:$wid;
		$where["wid"] = $wid;
		if($staffs)$where["staff_id"] = $staffs;
		$res = Db::table("kt_wework_customer")->where($where)->whereTime('del_time', 'between', [$time_one, $time_tow])->where("status",">=","1")->count();

		return $res;
	}

	static public function addCustomerSize($wid,$time_one,$time_tow,$staffs=NULL){
		$wid = Session::get("wid")?:$wid;
		$where["wid"] = $wid;
		if($staffs)$where["staff_id"] = $staffs;
		$add_size = Db::table("kt_wework_customer")->where($where)->whereTime('createtime', 'between', [$time_one, $time_tow])->where("status",0)->whereNull("del_time")->count();

		return $add_size;
	}

	static public function getStaffs($staffs=NULL,$wid=NULL){
		$wid = Session::get("wid")?:$wid;
		$where["wid"] = $wid;
		if($staffs)$where["id"] = $staffs;
		$res = Db::table("kt_wework_staff")->where($where)->field("name,avatar,id")->select()->toArray();

		return $res;
	}

	static public function groupSize($ktime,$dtime,$status=NULL,$type=NULL){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		if($status)$where["type"] = $type;
		if($status)$where["status"] = $status;
		$res = Db::table("kt_wework_group_member")->where($where)->whereTime('join_time', 'between', [$ktime, $dtime])->count();

		return $res;

	}

	static public function addGroupSize($ktime,$dtime){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		$res = Db::table("kt_wework_group_member")->where($where)->whereTime('join_time', 'between', [$ktime, $dtime])->where("status",1)->whereNull("lossing_time")->count();

		return $res;

	}

	static public function getUserids($staffs=NULL,$field=NULL,$wid=NULL){
		$column = $field?:"userid";
		$wid = Session::get("wid")?:$wid;
		$where["wid"] = $wid;
		$where["status"] = 1;
		if($staffs)$where["id"] = $staffs;
		$res = Db::table("kt_wework_staff")->where($where)->column($column);

		return $res;
	}

	static public function top(){
		$wid = Session::get("wid");
		$list = Db::query("select staff_id,count(*) num from kt_wework_customer where wid={$wid} group by staff_id order by num desc limit 10 ");
		$size = 0;
		foreach ($list as $key => $value) {
			$size += $value["num"];
			$list[$key]["name"] = Db::table("kt_wework_staff")->where(["wid"=>$wid,"id"=>$value["staff_id"]])->find()["name"];
		}

		return ["list"=>$list,"size"=>$size];
	}

	static public function size($type=NULL){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		if($type)$where["type"] = 2;
		$res = Db::table("kt_wework_et_qrcode_statistics")->where($where)->where("ctime",">=",strtotime(date("Y-m-d")))->count();

		return $res;
	}

	static public function sizeAll($type=NULL){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		if($type)$where["type"] = 2;
		$res = Db::table("kt_wework_et_qrcode_statistics")->where($where)->count();

		return $res;
	}

	static public function qrcodeTable($ktime,$dtime,$staff=NULL,$type=NULL){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		if($type)$where["type"] = $type;
		if($staff)$where["staff_id"] = $staff;
		$res = Db::table("kt_wework_et_qrcode_statistics")->where($where)->whereTime('ctime', 'between', [$ktime, $dtime])->count();

		return $res;
	}

	static public function qrcodeCustomer($ktime,$dtime,$add_way,$type,$staff=NULL){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		if($staff)$where["staff_id"] = $staff;
		if($type)$where["type"] = $type;
		$res = Db::table("kt_wework_et_qrcode_statistics")
						->where($where)
						->field("staff_id,external_userid,type")
						->whereTime('ctime', 'between', [$ktime, $dtime])
						->filter(function($data) use($wid,$add_way){
							$data["customer"] = Db::table("kt_wework_customer")->where(["wid"=>$wid,"external_userid"=>$data["external_userid"],"staff_id"=>$data["staff_id"]])->json(["tags"])->field("name,avatar,id,createtime,tags,add_way,score")->find();
							$data["customer"]["createtime"] = date("Y-m-d H:i:s",$data["customer"]["createtime"]);
							if($data["customer"]["tags"])$data["customer"]["tags"] = Db::table("kt_wework_tag")->where(["wid"=>$wid,"tag_id"=>$data["customer"]["tags"]])->field("tag_name,deleted")->select()->toArray();
							$data["customer"]["add_way"] = $add_way[$data["customer"]["add_way"]];
							$data["staff"] = Db::table("kt_wework_staff")->where(["wid"=>$wid,"id"=>$data["staff_id"]])->field("name,avatar")->find();
							return $data;
						})
						->select()
						->toArray();

		return $res;
	}

	static public function staffList($department_id){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		$where["is_del"] = 0;
		$where["status"] = 1;
		$department_name = Db::table("kt_wework_department")->where(["wid"=>$wid,"department_id"=>$department_id])->find()["department_name"];
		$res = Db::table("kt_wework_staff")->where($where)->where('department','like','%'.$department_id.'%')->where('department_name','like','%'."$department_name".'%')->column("userid");

		return $res;
	}

	static public function departments($department=NULL,$wid=NULL){
		$wid = Session::get("wid")?:$wid;
		$where["wid"] = $wid;
		if($department)$where["department_id"] = $department;
		$res = Db::table("kt_wework_department")->where($where)->field("department_name,department_id")->select()->toArray();

		return $res;
	}
}