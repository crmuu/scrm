<?php 
namespace app\wework\model\user\wechat;
use think\facade\Db;
use think\facade\Session;
use app\wework\model\QyWechatApi;

class WechatOpenModel {
	static public function list(){
		$wid = Session::get("wid");
		$res = Db::table("kt_wework_authorizer_info")
				->where(["wid"=>$wid])
				->json(["message"])
				->select()
				->toArray();

		return $res;
	}

	static public function default($id){
		$wid = Session::get("wid");
		Db::table("kt_wework_authorizer_info")->where(["wid"=>$wid])->update(["type"=>0]);
		$res = Db::table("kt_wework_authorizer_info")->where(["id"=>$id])->update(["type"=>1]);

		return $res;
	}
}