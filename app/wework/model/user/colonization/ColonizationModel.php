<?php 
namespace app\wework\model\user\colonization;
use think\facade\Db;
use think\facade\Session;
use app\wework\model\QyWechatApi;

class ColonizationModel {
	static public function list($page,$size,$name=NULL){
		$wid = Session::get("wid");
		$res = Db::table("kt_wework_tag_group")
				->where(["wid"=>$wid]);
		$data["count"] = $res->count();
		$data["page"] = $page;
		$data["size"] = $size;
		$data["item"] = $res->page($page,$size)
				->field("group_list,staffs,id,ctime,name")
				->json(["group_list","staffs"])
				->filter(function($group){
					$group["group_name"] = implode(",", array_column($group["group_list"], "name"));
					$group["staff_name"] = Db::table("kt_wework_staff")->where(["id"=>$group["staffs"]])->column("name");
					$group["customer_size"] = 0;
					$group["customer_not_size"] = 0;
					$group["staff_not_size"] = 0;
					$group["group_customer_size"] = 0;

					return $group;
				})
				->select()
				->toArray();

		return $data;
	}

	static public function add($data){
		$wid = Session::get("wid");
		$data["ctime"] = date("Y-m-d H:i:s");
		$data["wid"] = $wid;
		$res = Db::table("kt_wework_tag_group")->json(["group_list","staffs","tags"])->insertGetId($data);

		return $res;
	}

	static public function getSendUser($data){
		$wid = Session::get("wid");
		if($data["type"] == 2){
			$external_userid = Db::table("kt_wework_customer")->where(["wid"=>$wid,"status"=>0])->column("external_userid");
		}else{
			$external_userid = Db::table("kt_wework_customer");
			if(isset($data["tags"])){
				foreach ($data["tags"] as $value){
					$where = [];
		            $where[] = ["staff_id","in",$data["staffs"]];
		            $where[] = ["wid","=",$wid];
					if($data["gender"] == 2)$where[] = ["gender","=",1];
					if($data["gender"] == 3)$where[] = ["gender","=",2];
					if($data["gender"] == 4)$where[] = ["gender","=",0];
		            $where[] = ["tags","like","%\"{$value}\"%"];
					$external_userid = $external_userid->whereOr($where);
				}
			}else{
				$where["staff_id"] = $data["staffs"];
				$where["wid"] = $wid;
				if($data["gender"] == 2)$where["gender"] = 1;
				if($data["gender"] == 3)$where["gender"] = 2;
				if($data["gender"] == 4)$where["gender"] = 0;
				$external_userid = $external_userid->where($where);
			}
			if(isset($data["time"]))$external_userid = $external_userid->whereBetweenTime("createtime",$data["time"][0],$data["time"][1]);
			$external_userid = $external_userid->column("external_userid");
		}

		return $external_userid;
	}

	static public function groupList($group_list){
		$wid = Session::get("wid");
		$chatid = array_column($group_list, "chatid");
		$res = Db::table("kt_wework_group")
				->where(["wid"=>$wid,"chat_id"=>$chatid])
				->filter(function($data) use($group_list,$wid){
					foreach($group_list as $group){
						if($data["chat_id"] == $group["chatid"])$data["img"] = $group['img'];
					}
					$data["count"] =  Db::table("kt_wework_group_member")->where(["wid"=>$wid,"chat_id"=>$data["chat_id"],"status"=>1])->count();

					return $data;
				})
				->select()
				->toArray();

		return $res;
	}

	static public function sendMsgUserid($group_list,$external_userids){
		$wid = Session::get("wid");
        foreach ($group_list as $group){
        	$group_chat_userid = Db::table("kt_wework_group_member")->where(["wid"=>$wid,"chat_id"=>$group["chat_id"],"status"=>1])->column("userid");
        	$external_userids = array_diff($external_userids,$group_chat_userid);
        }

        return $external_userids;
	}

	static public function save($id,$data){
		$res = Db::table("kt_wework_tag_group")->where(["id"=>$id])->json(["group_list","staffs","tags","msg_id"])->save($data);

		return $res;
	}

	static public function del($id){
		$res = Db::table("kt_wework_tag_group")->where(["id"=>$id])->delete();

		return $res;
	}

	static public function detail($id){
		$wid = Session::get("wid");
		$res["add_size"] = Db::table("kt_wework_tag_group_detail")->where(["wid"=>$wid,"pid"=>$id,"type"=>1])->count();
		$res["not_size"] = Db::table("kt_wework_tag_group_detail")->where(["wid"=>$wid,"pid"=>$id,"type"=>0])->count();
		$res["add_invite"] = Db::table("kt_wework_tag_group_detail")->where(["wid"=>$wid,"pid"=>$id,"status"=>1])->count();
		$res["not_invite"] = Db::table("kt_wework_tag_group_detail")->where(["wid"=>$wid,"pid"=>$id,"status"=>0])->count();
		$res["accomplish"] = Db::table("kt_wework_tag_group_user")->where(["wid"=>$wid,"pid"=>$id,"status"=>1])->count();
		$res["not_accomplish"] = Db::table("kt_wework_tag_group_user")->where(["wid"=>$wid,"pid"=>$id,"status"=>0])->count();

		return $res;
	}

	static public function customerStatistics($id,$type=NULL,$status=NULL,$chat_id=NULL,$name=NULL,$staff_id=NULL){
		$wid = Session::get("wid");
		$where["d.pid"] = $id;
		if($type)$where["d.type"] = $type;
		if($status)$where["d.status"] = $status;
		if($chat_id)$where["d.chat_id"] = $chat_id;
		if($staff_id)$where["u.staff_id"] = $staff_id;
		if($name){
			$external_userid = Db::table("kt_wework_customer")->where(["wid"=>$wid])->whereLike("name","%".$name."%")->group("external_userid")->column("external_userid");
			$where["d.external_userid"] = $external_userid;
		}
		$res = Db::table("kt_wework_tag_group_detail")
					->alias("d")
					->join(['kt_wework_tag_group_user'=>'u'],'d.user_id=u.id')
					->where($where)
					->field("d.user_id,d.external_userid,d.status,d.type,u.staff_id,d.chat_id")
					->filter(function($detail) use($wid){
						$detail["customer"] = Db::table("kt_wework_customer")->where(["wid"=>$wid,"external_userid"=>$detail["external_userid"]])->field("name,avatar,type")->find();
						$staff = Db::table("kt_wework_staff")->where(["wid"=>$wid,"id"=>$detail["staff_id"]])->field("name")->find();
						$detail["staff_name"] = $staff["name"];
						if($detail["chat_id"])$detail["group"] = Db::table("kt_wework_group")->where(["wid"=>$wid,"chat_id"=>$detail["chat_id"]])->field("name")->find();

						return $detail;
					})
					->select()
					->toArray();
		return $res;
	}

	static public function staffStatistics($id){
		$wid = Session::get("wid");
		$res = Db::table("kt_wework_tag_group_user")
				->where(["pid"=>$id])
				->group("staff_id")
				->field("id,staff_id")
				->filter(function($user) use($wid,$id){
					$user["staff"] = Db::table("kt_wework_staff")->where(["wid"=>$wid,"id"=>$user["staff_id"]])->field("name,avatar")->find();
					$size = Db::table("kt_wework_tag_group_user")->where(["wid"=>$wid,"staff_id"=>$user["staff_id"],"pid"=>$id])->column("id");
					$user["size"] = count($size);
					$user["customer_size"] = Db::table("kt_wework_tag_group_detail")->where(["user_id"=>$size])->count();
					$user["add_customer"] = Db::table("kt_wework_tag_group_detail")->where(["user_id"=>$size,"type"=>1])->count();
					$status = Db::table("kt_wework_tag_group_user")->where(["wid"=>$wid,"staff_id"=>$user["staff_id"],"pid"=>$id,"status"=>0])->column("id");
					$user["status"] = 0;
					if($status)$user["status"] = 1;

					return $user;
				})
				->select()
				->toArray();

		return $res;
	}
}