<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\model\user\radar;
use think\facade\Db;
use think\facade\Session;

/**
* 企业雷达
**/
class InteractiveRadarModel {
	static public $wid;
	/**
	 * 雷达列表 使用于后台跟H5
	 * @param $page 分页
	 * @param $title 标题
	 * @param 暂时只做一种类型
	 * @return 
	*/
 	static public function info($page,$size,$title=NULL){
 		$wid = Session::get('wid');
 		$where["wid"] = $wid;
 		$where["status"] = 0;
 		$res = Db::table("kt_wework_radar")
 							->where($where);
		if($title) $res = $res->whereLike('title','%'.$title.'%');
		$data['count'] = $res->count();
		$data['item'] = $res->page($page,$size)	
							->filter(function($data){
								if($data["ex_tag"]){
									$data['ex_tag'] = array_column(Db::table('kt_wework_tag')->field('tag_name')->where(['tag_id'=>json_decode($data["ex_tag"],1)])->select()->toArray(), "tag_name");
								}
								$data["size"] = Db::table("kt_wework_radar_record")->where(["radar_id"=>$data["id"]])->group("externalcontact_id")->count();
								return $data;
							})
 							->select()
 							->toArray();
 		$data['page'] = $page;
		$data['size'] = $size;
 		return $data;
	}

	static public function infos($id){
		$res = Db::table("kt_wework_radar")->where(["id"=>$id])->json(["ex_tag"])->find();
		if($res['ex_tag'])$res['tag_name'] = Db::table('kt_wework_tag')->field("tag_name,tag_id")->where(['tag_id'=>$res["ex_tag"]])->select()->toArray();

		return $res;
	}



	/**
	 * 创建雷达连接
	 * @param 暂时只做一种类型
	 * @return 
	*/
	static public function create($link_title,$link,$title,$description,$image_url,$ex_tag_inform,$ex_tag,$behavior_inform,$dynamic_inform,$id=NULL){
		$wid = Session::get('wid');
		$where = [];
		$data["wid"] = $wid;
		$data["link_title"] = $link_title;
		$data["title"] = $title;
		$data["link"] = $link;
		$data["image_url"] = $image_url;
		$data["description"] = $description;
		$data["dynamic_inform"] = $dynamic_inform;
		$data["behavior_inform"] = $behavior_inform;
		$data["ex_tag_inform"] = $ex_tag_inform;
		$data["ex_tag"] = json_encode($ex_tag,320);
		if($id) $where["id"] = $id;
		$data["ctime"] = date("Y-m-d H:i:s",time());
		$data["utime"] = date("Y-m-d H:i:s",time());
		$res = Db::table('kt_wework_radar')->where($where)->save($data);
		return $res;
	}


	/***
	* 删除
	*/
	static public function del($id){
		$where["id"] = $id;
		$res = Db::table('kt_wework_radar')->where($where)->save(["status"=>1]);
		return $res;
	}
	
	/**
	* 获取互动雷达的列表
	*/
	static public function radarList($wid,$type=1){
		if(!$wid)$wid = Session::get("wid");
 		$where["wid"] = $wid;
 		$where["type"] = $type;
 		$where["status"] = 0;
 		$res = Db::table("kt_wework_radar")
 							->where($where)
							->filter(function($data){
								$data["click_tatol"] = Db::table("kt_wework_radar_record")->where(["radar_id"=>$data["id"]])->whereBetweenTime('ctime',date("Y-m-d",time()),date("Y-m-d H:i:s",time()))->group("externalcontact_id")->count();
								$data["click_tedaytatol"] = Db::table("kt_wework_radar_record")->where(["radar_id"=>$data["id"]])->group("externalcontact_id")->count();
								return $data;
							})
 							->select()
 							->toArray();
 		return $res;
	}
}