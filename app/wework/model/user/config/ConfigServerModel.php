<?php 
namespace app\wework\model\user\config;
use think\facade\Db;
use think\facade\Session;

/**
* 配置类
**/
class ConfigServerModel {

	public $wid;
	/**
	 * 获取基本配置信息
	 * @param $table 表名
	 * @param $wid 账户id
	 * @return 
	 */
 	static public function info($table,$where,$field=true){
 		self::inisetConfig();
 		$wid = Session::get('wid');
		$res = Db::table($table)->field($field)->where($where)->find();
		return $res;
	}
	/**
	 * 初始化基础配置表
	 * @return 
	 */
	static private function inisetConfig(){
		$wid = Session::get('wid');
		$config = Db::table('kt_wework_config')->where('wid',$wid)->find();
		if(!$config){
			Db::table('kt_wework_config')->insert(
			 	[
			 		'wid' => $wid,
	                'customer_token' => getRandStr(15),
	                'customer_aes' => getRandStr(42),
	                'user_tokent' => getRandStr(15),
	                'user_aes' => getRandStr(42),
	                'create_time' => date("Y-m-d H:i:s"),
	                'update_time' => date("Y-m-d H:i:s"),
			 	]
			);
		} 
	}

	/**
	 * 修改配置文件
	 *	@param $table  表名
	 *	@param $data   表参数，修改的内容
	 *	@param $field  表参数 字段名
	 */
 	static public function UpdateConfig($table,$data,$field){
		$wid = Session::get('wid');
		$res = Db::table($table)->field($field)->where('wid',$wid)->find();
		if(!$res){
		    $data["wid"] = $wid;
		    $res = Db::table($table)->insertGetId($data);
		}else{
		    $res = Db::table($table)->field($field)->where('wid',$wid)->save($data);
		}
		return $res;
	}
}