<?php 
namespace app\wework\model\user\customized;
use think\facade\Db;
use think\facade\Session;

/**
* 自定义信息类
**/
class SettingModel {
	
	static public $wid;
	/**
	 * 获取自定义信息列表
	 * @param $wid 账户id
	 * @return 
	 */
 	static public function list($page,$size){
 		$wid = Session::get('wid');
 		$where["wid"] = $wid;
 		$res = Db::table('kt_wework_customer_setfield')	
 						->where($where);
 		$data["count"] = $res->count();
 		$data["item"] = $res->page($page,$size)
 						->order("sort asc")
 						->json(["content"])
 						->select()
 						->toArray();
 		$data["size"] = $size;
 		$data["page"] = $page;
 		if($data["count"] == 0){
 			$arr = ["来源","时间","性别","电话","年龄","邮箱","生日","微博","QQ","企业","地址","描述","职位"];
 			foreach ($arr as $key=>$value){
 				$datas[$key]["wid"] = $wid;
		 		$datas[$key]["name"] = $value;
		 		$datas[$key]["type"] = 1;
		 		$datas[$key]["sort"] = $key+1;
		 		$datas[$key]["create_time"] = date("Y-m-d H:i:s",time());
		 		$datas[$key]["update_time"] = date("Y-m-d H:i:s",time());
 			}
 			Db::table("kt_wework_customer_setfield")->insertAll($datas);
 			$res = Db::table('kt_wework_customer_setfield')	
	 						->where($where);
	 		$data["count"] = $res->count();
	 		$data["item"] = $res->page($page,$size)
	 						->order("sort asc")
	 						->json(["content"])
	 						->select()
	 						->toArray();
	 		$data["size"] = $size;
	 		$data["page"] = $page;
 		}
		return $data;
 	}
 	/**
	 * 
	 * @param $wid 账户id
	 * @param $id 自定义信息的id
	 * @param $status 开启或关闭
	 * @return 
	*/
 	static public function updateStatus($id,$status){
 		$wid = Session::get('wid');
 		$where["wid"] = $wid;

 		$where["id"] = $id;
 		$res = Db::table('kt_wework_customer_setfield')
					->where($where)
					->update(["status"=>$status]);
		return $res;
 	}

 	/**
	 * 
	 * @param $wid 账户id
	 * @param $id 自定义信息的id
	 * @param $sort 1上升  2者下降
	 * @return 
	*/
 	static public function updateStot($id,$sort){
 		$wid = Session::get('wid');
 		$where["wid"] = $wid;
 		$where["id"] = $id;
 		$res = Db::table('kt_wework_customer_setfield')
					->where($where)
					->find();
 		if($sort == 1){
			Db::table('kt_wework_customer_setfield')
					->where(["wid"=>$wid,"sort"=>$res["sort"]-1])
					->update(["sort"=>$res["sort"]]);
			Db::table('kt_wework_customer_setfield')
					->where(["wid"=>$wid,"id"=>$res["id"]])
					->update(["sort"=>$res["sort"]-1]);
 		}else{
 			Db::table('kt_wework_customer_setfield')
					->where(["wid"=>$wid,"sort"=>$res["sort"]+1])
					->update(["sort"=>$res["sort"]]);
			Db::table('kt_wework_customer_setfield')
					->where(["wid"=>$wid,"id"=>$res["id"]])
					->update(["sort"=>$res["sort"]+1]);
 		}
 		return true;
 	}

 	/**
	 * 
	 * @param 添加
	 * @param $id 为真的时候就是修改
	 * @return 
	*/
 	static public function save($id,$name,$attr,$content){
 		$wid = Session::get('wid');
 		$data["wid"] = $wid;
 		$data["name"] = $name;
 		$data["attr"] = $attr;
 		$data["type"] = 2;
 		$data["status"] = 1;
 		$data["sort"] = self::sort() + 1;
 		$data["create_time"] = date("Y-m-d H:i:s",time());
 		$data["update_time"] = date("Y-m-d H:i:s",time());
 		if($attr == 2) $data["content"] = json_encode($content,320);
 		$where = [];
 		if(!$id) $isset = Db::table('kt_wework_customer_setfield')->where(["name"=>$name])->find();
 		if($id) $isset = Db::table('kt_wework_customer_setfield')->where('id','<>',$id)->where('name',$name)->find();
 		if($isset) return "已存在相同名称的自定义字段";
 		if($id) $where["id"] = $id;
 		$res = Db::table('kt_wework_customer_setfield')->where($where)->save($data);
 		return 1;
 	}

 	/**
	 * 
	 * @param 删除
	 * @param $id 唯一id
	 * @return 
	*/
 	static public function delete($id){
 		$wid = Session::get('wid');
		$where["wid"] = $wid;
		$where["id"] = $id;
 		$res = Db::table('kt_wework_customer_setfield')->where($where)->delete();
 		return $res;
 	}

 	/**
	 * 
	 * @param 获取最大的sort
	 * @return 
	*/
 	static public function sort(){
 		$wid = Session::get('wid');
 		$where["wid"] = $wid;
 		$res = Db::table('kt_wework_customer_setfield')
					->where($where)
					->max("sort");
		return $res;
 	}
 	


}