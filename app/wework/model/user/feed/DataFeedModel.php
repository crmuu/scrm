<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\model\user\feed;
use think\facade\Db;
use think\facade\Session;

/**
* 数据订阅类
**/
class DataFeedModel {
	
	static public function find(){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		$res = Db::table("kt_wework_data_subscribe")->where($where)->json(["staff_id"])->find();
		if(isset($res["staff_id"]))$res["staff"] = Db::table("kt_wework_staff")->field("name,avatar,id")->where(["id"=>$res["staff_id"]])->select()->toArray();

		return $res;
	}

	/*
	* 数据订阅入库
	* 默认全部成员
	*/
	static public function addFeed($customer_total,$customer_new,$customer_lossing,$customer_netnew,$group_total,$group_new,$group_customer_total,$group_lossing_total,$day_push,$week_push,$month_push,$selfapp_remind,$mail_remind,$group_customer_new,$group_customer_netnew,$status,$staff_id){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		$data["wid"] = $wid;
		$data["customer_total"] = $customer_total;
		$data["customer_new"] = $customer_new;
		$data["customer_lossing"] = $customer_lossing;
		$data["customer_netnew"] = $customer_netnew;
		$data["group_total"] = $group_total;
		$data["group_new"] = $group_new;
		$data["group_customer_total"] = $group_customer_total;
		$data["group_lossing_total"] = $group_lossing_total;
		$data["day_push"] = $day_push;
		$data["week_push"] = $week_push;
		$data["month_push"] = $month_push;
		$data["selfapp_remind"] = $selfapp_remind;
		$data["group_customer_netnew"] = $group_customer_netnew;
		$data["group_customer_new"] = $group_customer_new;
		$data["mail_remind"] = $mail_remind;
		$data["status"] = $status;
		$data["staff_id"] = json_encode($staff_id,320);
		$data["create_time"] = date("Y-m-d H:i:s",time());
		if(!$day_push && !$week_push && !$month_push) $data["status"] = 0;
		$subscribe = $res = Db::table("kt_wework_data_subscribe")->where($where)->find();
		if($subscribe)$res = Db::table("kt_wework_data_subscribe")->where($where)->save($data);
		if(!$subscribe)$res = Db::table("kt_wework_data_subscribe")->save($data);

		return $res;
	}

	/*
	* 数据订阅修改状态
	*/
	static public function upFeed($status){
		$wid = Session::get("wid");
		$res = Db::table("kt_wework_data_subscribe")->where(["wid"=>$wid])->find();
		if($status == 1 && !$res["day_push"] && !$res["week_push"] && !$res["month_push"]) return false;
		$res = Db::table("kt_wework_data_subscribe")->where(["wid"=>$wid])->save(["status"=>$status]);
		
		return true;
	}
}