<?php 
namespace app\wework\model\user\reply;
use think\facade\Db;
use think\facade\Session;
use think\facade\Cache;

/**
* 快捷回复
**/
class ReplyModel 
{

	/**
	* 快捷回复列表
	*/
	static public function info($page,$size,$title=NULL,$grouping_id=NULL,$orders=NULL){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		if($grouping_id){
			$group = Db::table("kt_wework_reply_group")->where("id",$grouping_id)->find();
			$groupings = [];
			if($group["pid"] == 0){
				$groupings = Db::table("kt_wework_reply_group")->where("pid",$group["id"])->column("id");
			}
			array_push($groupings,$grouping_id);
			$where["grouping_id"] = $groupings;
		}
		$str = "";
		$str = substr($str, 0,-3);
		$res = Db::table("kt_wework_reply_list")
							->where($where);
		if($title) $res = $res->where('title','like',"%{$title}%");
		$data['count'] = $res->count();
		$res = $res->page($page,$size)
						->json(["welcome_data"])
						->filter(function($data) use($wid){
								$data["grouping_name"] = Db::table("kt_wework_reply_group")->where(["id"=>$data["grouping_id"]])->field("grouping_name")->find()["grouping_name"];
								if(count($data["welcome_data"]) > 1)$data["type"] = "复合消息";
								if(count($data["welcome_data"]) == 1){
									if($data["welcome_data"][0]["type"] == 1)$data["type"] = "文本";
									if($data["welcome_data"][0]["type"] == 2)$data["type"] = "图片";
									if($data["welcome_data"][0]["type"] == 3)$data["type"] = "视频";
									if($data["welcome_data"][0]["type"] == 4)$data["type"] = "文件";
									if($data["welcome_data"][0]["type"] == 5)$data["type"] = "链接";
									if($data["welcome_data"][0]["type"] == 6)$data["type"] = "小程序";
								}
								return $data;
							})
						->order("orders","DESC")
						->select()
						->toArray();
		$data['item'] = $res;
		$data['page'] = $page;
		$data['size'] = $size;
		return $data;
	}


	static public function getReply($id){
		$res =  Db::table("kt_wework_reply_list")->where("id",$id)->json(["welcome_data"])->find();
		// $group = Db::table("kt_wework_reply_group")->where("id",$res["grouping_id"])->find();
		// $grouping_id = $res["grouping_id"];
		// if($group["pid"] == 0)$res["grouping_id"] = [$grouping_id];
		// if($group["pid"] != 0){
		// 	$groups = Db::table("kt_wework_reply_group")->where("id",$group["pid"])->find();
		// 	$res["grouping_id"] = [];
		// 	array_push($res["grouping_id"],$groups["id"]);
		// 	array_push($res["grouping_id"],intval($grouping_id));
		// }

		return $res;
	}

	//kt_wework_reply_list
	static public function move($id,$type){
		$wid = Session::get("wid");
		$res =  Db::table("kt_wework_reply_list")->where("id",$id)->field("id,orders")->find();
		if($type == "down"){
			$up_order = Db::table("kt_wework_reply_list")->where(["wid"=>$wid])->where("orders","<",$res["orders"])->max("orders");
			$up_reply= Db::table("kt_wework_reply_list")->where(["wid"=>$wid,"orders"=>$up_order])->find();
			Db::table("kt_wework_reply_list")->where(["id"=>$id])->update(["orders"=>$up_order]);
			Db::table("kt_wework_reply_list")->where(["id"=>$up_reply["id"]])->update(["orders"=>$res["orders"]]);
		}else{
			$down_order = Db::table("kt_wework_reply_list")->where(["wid"=>$wid])->where("orders",">",$res["orders"])->min("orders");
			$down_reply= Db::table("kt_wework_reply_list")->where(["wid"=>$wid,"orders"=>$down_order])->find();
			Db::table("kt_wework_reply_list")->where(["id"=>$id])->update(["orders"=>$down_order]);
			Db::table("kt_wework_reply_list")->where(["id"=>$down_reply["id"]])->update(["orders"=>$res["orders"]]);
		}
	}

	/**
	* 快捷回复分组
	*/
	static public function groupManage(){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		$where["pid"] = 0;
		$res = Db::table("kt_wework_reply_group")
				->where($where)
				->field("grouping_name,id,pid,sort")
				->order("sort ASC")
				->filter(function($data) use($wid){
					$son_grouping = Db::table("kt_wework_reply_group")
							->where(["pid"=>$data["id"]])
							->order("sort ASC")
							->filter(function($son){
								$son["size"] = Db::table("kt_wework_reply_list")->where(["grouping_id"=>$son["id"]])->count();
								return $son;
							})
							->select()
							->toArray();
					$son_id = array_column($son_grouping, "id");
					array_push($son_id,$data["id"]);
					$data["son_grouping"] = $son_grouping;
					$data["size"] = Db::table("kt_wework_reply_list")->where(["grouping_id"=>$son_id])->count();
					return $data;
				})
				->select()
				->toArray();
		if(!count($res)){
			$data["wid"] = $wid;
			$data["grouping_name"] = "默认分组";
			$data["pid"] = 0;
			$data["sort"] = 0;
			$data["ctime"] = date("Y-m-d H:i:s");
			Db::table("kt_wework_reply_group")->insertGetId($data);
		}
		return $res;
	}

	static public function moveGroupManage($id,$type){
		$wid = Session::get("wid");
		$res =  Db::table("kt_wework_reply_group")->where("id",$id)->find();
		if($type == "up"){
			if($res["pid"] == 0){
				$up_sort = Db::table("kt_wework_reply_group")->where(["wid"=>$wid,"pid"=>0])->where("sort","<",$res["sort"])->max("sort");
				$up_grouping = Db::table("kt_wework_reply_group")->where(["wid"=>$wid,"pid"=>0,"sort"=>$up_sort])->find();
				Db::table("kt_wework_reply_group")->where(["id"=>$id])->update(["sort"=>$up_sort]);
				Db::table("kt_wework_reply_group")->where(["id"=>$up_grouping["id"]])->update(["sort"=>$res["sort"]]);
			}else{
				$down_sort = Db::table("kt_wework_reply_group")->where(["wid"=>$wid,"pid"=>$res["pid"]])->where("sort","<",$res["sort"])->max("sort");
				$up_grouping = Db::table("kt_wework_reply_group")->where(["wid"=>$wid,"pid"=>$res["pid"],"sort"=>$down_sort])->find();
				Db::table("kt_wework_reply_group")->where(["id"=>$id])->update(["sort"=>$down_sort]);
				Db::table("kt_wework_reply_group")->where(["id"=>$up_grouping["id"]])->update(["sort"=>$res["sort"]]);
			}
		}else{
			if($res["pid"] == 0){
				$up_sort = Db::table("kt_wework_reply_group")->where(["wid"=>$wid,"pid"=>0])->where("sort",">",$res["sort"])->min("sort");
				$up_grouping = Db::table("kt_wework_reply_group")->where(["wid"=>$wid,"pid"=>0,"sort"=>$up_sort])->find();
				Db::table("kt_wework_reply_group")->where(["id"=>$id])->update(["sort"=>$up_sort]);
				Db::table("kt_wework_reply_group")->where(["id"=>$up_grouping["id"]])->update(["sort"=>$res["sort"]]);
			}else{
				$down_sort = Db::table("kt_wework_reply_group")->where(["wid"=>$wid,"pid"=>$res["pid"]])->where("sort",">",$res["sort"])->min("sort");
				$up_grouping = Db::table("kt_wework_reply_group")->where(["wid"=>$wid,"pid"=>$res["pid"],"sort"=>$down_sort])->find();
				Db::table("kt_wework_reply_group")->where(["id"=>$id])->update(["sort"=>$down_sort]);
				Db::table("kt_wework_reply_group")->where(["id"=>$up_grouping["id"]])->update(["sort"=>$res["sort"]]);
			}
		}
		return '1';
	}

	/**
	* 添加快捷回复 修改
	* 只有title 标题不可为空
	*/
	static public function addReply($title,$grouping_id,$datas,$send_type,$id=NULL){
		if($id) $data["id"] = $id;
		$wid = Session::get("wid");
		$data["wid"] = $wid;
		$data["title"] = $title;
		$data["send_type"] = $send_type;
		$data["grouping_id"] = $grouping_id;
		$data["orders"] = Db::table("kt_wework_reply_list")->where("wid",$wid)->max("orders")+1;
		$data["welcome_data"] = json_encode($datas,320);
		if(!$id)$data["ctime"] = date("Y-m-d H:i:s",time());
		$res = Db::table("kt_wework_reply_list")->save($data);
		return $res;
	}

	static public function getgroupManage($id){
		$res =  Db::table("kt_wework_reply_group")->where("id",$id)->field("grouping_name,pid,id")->find();
		if($res["pid"] == 0){
			$res["son_grouping"] = Db::table("kt_wework_reply_group")->where(["pid"=>$res["id"]])->field("grouping_name,id")->order("sort asc")->select()->toArray();
		}

		return $res;
	}

	static public function delgroupManage($id){
		$wid = Session::get("wid");
		$res =  Db::table("kt_wework_reply_group")->where("id",$id)->find();
		$ids = [];
		if($res["grouping_name"] == "默认分组")return false;
		if($res["pid"] == 0){
			$ids = array_column(Db::table("kt_wework_reply_group")->where(["pid"=>$res["id"]])->select()->toArray(), "id");
		}
		array_push($ids,$res["id"]);
		$grouping = Db::table("kt_wework_reply_group")->where(["wid"=>$wid,"grouping_name"=>"默认分组"])->find();
		Db::table("kt_wework_reply_list")->where(["grouping_id"=>$ids])->update(["grouping_id"=>$grouping["id"]]);
		$res =  Db::table("kt_wework_reply_group")->where(["id"=>$ids])->delete();

		return $res;
	}

	/**
	* 新增分组
	*/
	static public function addgroupManage($grouping_name,$son_grouping=NULL){
		$wid = Session::get("wid");
		$data["wid"] = $wid;
		$data["pid"] = 0;
		$data["grouping_name"] = $grouping_name;
		$data["sort"] = Db::table("kt_wework_reply_group")->where("wid",$wid)->where("pid",0)->max("sort")+1;
		$data["ctime"] = date("Y-m-d H:i:s",time());
		$res = Db::table("kt_wework_reply_group")->insertGetId($data);
		$groupings = [];
		foreach ($son_grouping as $key=>$value){
			$grouping["grouping_name"] = $value;
			$grouping["wid"] = $wid;
			$grouping["pid"] = $res;
			$grouping["sort"] = $key+1;
			$grouping["ctime"] = date("Y-m-d H:i:s",time());
			$groupings[] = $grouping;
		}
		if($son_grouping)Db::table("kt_wework_reply_group")->insertAll($groupings);

		return $res;
	}

	static public function updgroupManage($id,$grouping_name,$add_groupimg=NULL,$del_groupimg=NULL){
		$wid = Session::get("wid");
		$res =  Db::table("kt_wework_reply_group")->where("id",$id)->find();
		if($res["pid"] == 0){
			if($del_groupimg)Db::table("kt_wework_reply_group")->where(["id"=>$del_groupimg])->delete();
			if($add_groupimg){
				$groupings = [];
				foreach($add_groupimg as $value){
					$grouping["grouping_name"] = $value["grouping_name"];
					if(array_key_exists("id",$value)){
						Db::table("kt_wework_reply_group")->where(["id"=>$value["id"]])->update($grouping);
					}else{
						$grouping["wid"] = $wid;
						$grouping["pid"] = $res["id"];
						$grouping["sort"] = Db::table("kt_wework_reply_group")->where(["pid"=>$res["id"]])->max("sort")+1;
						$grouping["ctime"] = date("Y-m-d H:i:s",time());
						Db::table("kt_wework_reply_group")->insertGetId($grouping);
					}
				}
			}
		}
		$res =  Db::table("kt_wework_reply_group")->where("id",$id)->update(["grouping_name"=>$grouping_name]);

		return $res;
	}

	/**
	* 刪除快捷回复
	*/
	static public function delReply($id,$table){
		$res = Db::table($table)->where(["id"=>$id])->delete();
		return $res;
	}

	/**
	* list 获取话术，快捷回复的信息
	*/
	static public function replyList($wid,$title=NULL,$group=NULL,$type=NULL,$page=1,$size=10){
		if(!$wid)$wid = Session::get("wid");
		$where["wid"] = $wid;
		if($type)$where["type"] = $type;
		$res = Db::table("kt_wework_reply_list")
							->where($where);
		if($title) $res = $res->whereLike('title','%'.$title.'%');
		$data['count'] = $res->count();
		$data['item'] = $res->page($page,$size)
							->select()
							->order("sort DESC")
							->toArray();
		$data['page'] = $page;
		$data['size'] = $size;
		return $data;
	}

	

	/**
	* 获取本条数据
	*/
	static public function find($table,$id){
		$res = Db::table($table)->where(["id"=>$id])->find();
		return $res;
	}

	/*
	* 统计雷达点击记录表
	*/

	static public function addRadar($wid,$externalcontact,$id){
		$data["wid"] = $wid;
		$data["externalcontact_id"] = $externalcontact;
		$data["radar_id"] = $id;
		$data["ctime"] = date("Y-m-d H:i:s",time());
		$res = Db::table("kt_wework_radar_record")->insertGetId($data);

		return $res;
	}

	/**
	* 入库客户动态
	*/
	static public function add_Dynamic($wid,$external_userid,$type,$id,$userid){
		$data["wid"] = $wid;
		$data["customer_userid"] = $external_userid;
		$data["type"] = $type;
		$data["glian_id"] = $id;
		$data["staff_id"] = $userid;
		$data["create_time"] = date("Y-m-d H:i:s",time());
		$data["update_time"] = date("Y-m-d H:i:s",time());
		$res = Db::table("kt_wework_customer_dynamic")->insertGetId($data);

		return $res;
	}
}