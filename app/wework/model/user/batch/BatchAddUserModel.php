<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\model\user\batch;
use think\facade\Db;
use think\facade\Session;

/**
* 批量加好友
**/
class BatchAddUserModel 
{
	/**
	* 获取潜在客户列表 添加状态 0未添加 1已添加,2待分配 3待通过
	*/
	static public function getAddUser($page=1,$size=10,$add_status=NULL,$phone=NULL){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		if($add_status == 1)$where["add_status"] = 0;
		if($add_status == 2)$where["add_status"] = 1;
		if($add_status == 3)$where["add_status"] = 2;
		if($add_status == 4)$where["add_status"] = 3;
		$where["wid"] = $wid;
		$res = Db::table('kt_wework_batch_addcus_info')
					->where($where);
		if($phone)$res = $res->whereLike('phone','%'.$phone.'%');
		$data['count'] = $res->count();
		$data['item'] = $res->page($page,$size)
					->filter(function($datas){
						$datas["staff_name"] = Db::table("kt_wework_staff")->where(["id"=>$datas["staffid"]])->field("name")->find()["name"];
						$customer = Db::table("kt_wework_customer")->where("remark_mobiles","like",'%'.$datas["phone"].'%')->find();
						$datas["customer_id"] = 0;
						if($customer)$datas["customer_id"] = $customer["id"];
						$addcus = Db::table("kt_wework_batch_addcus")->where(["id"=>$datas["pid"]])->field("tags")->find();
						if($addcus["tags"]){
							$datas['tags'] = array_column(Db::table('kt_wework_tag')->field('tag_name')->where(['tag_id'=>json_decode($addcus["tags"],1)])->select()->toArray(), "tag_name");
						}
						$datas['size'] = 1;//分配次数暂定为1
						return $datas;
					})
					->select()
					->toArray();
		$data['page'] = $page;
		$data['size'] = $size;
		return $data;
	}
	/***
	* 导入记录
	*/
	static public function histroy($page=1,$size=10){
		$wid = Session::get("wid");
		$res = Db::table('kt_wework_batch_addcus')
						->where(["wid"=>$wid]);
		$data['count'] = $res->count();
		$data['item'] = $res->page($page,$size)->json(["tags","staffid"])
			 			->filter(function($data){
			 				$data['tags'] = array_column(Db::table('kt_wework_tag')
			 									->field('tag_name')
			 									->where(['tag_id'=>$data["tags"]])
			 									->select()->toArray(),"tag_name");
			 				$data["staff"] = array_column(Db::table("kt_wework_staff")
			 									->field('name')
			 									->where(['id'=>$data["staffid"]])
			 									->select()->toArray(),"name");
			 				$data["batch_users"] = Db::table("kt_wework_batch_addcus_info")
			 									->field('name')
			 									->where(['pid'=>$data["id"]])
			 									->count();
			 				$data["add_users"] = Db::table("kt_wework_batch_addcus_info")
			 									->field('name')
			 									->where(['pid'=>$data["id"],"add_status"=>1])
			 									->count();
			 				$data["performance"] = "0％";
			 				if($data["add_users"] > 0){
			 					$data["performance"] = round($data["batch_users"]/$data["add_users"]*100)."％";
			 				}
			 				$data["status"] = 0;
			 				if($data["batch_users"] == $data["add_users"]) $data["status"] = 1;
			 				return $data;
			 			})
			 			->select()
			 			->toArray();
		$data['page'] = $page;
		$data['size'] = $size;
		return $data;
	}

	/*
	* 删除导入记录
	*/
	static public function delBatch($table,$id){
		$wid = Session::get("wid");
		$res = Db::table($table)->where(["wid"=>$wid,"id"=>$id])->delete();
		if($table == "kt_wework_batch_addcus")$res = Db::table('kt_wework_batch_addcus_info')->where(["wid"=>$wid,"pid"=>$id])->delete();
		return $res;
	}

	/*
	* 获取本条数据
	*/
	static public function getBatch($table,$id){
		$wid = Session::get("wid");
		$res = Db::table($table)->where(["wid"=>$wid,"id"=>$id])->find();
		return $res;
	}

	/*
	* 获取倒入记录详情数据
	*/
	static public function histroyDetail($id,$phone=NULL,$status=NULL){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		$where["pid"] = $id;
		if($status == 1)$where["add_status"] = 0;
		if($status == 2)$where["add_status"] = 1;
		if($status == 3)$where["add_status"] = 2;
		if($status == 4)$where["add_status"] = 3;
		$res = Db::table('kt_wework_batch_addcus_info')
							->where($where);
		if($phone)$res = $res->whereLike('phone','%'.$phone.'%');
							$res = $res->filter(function($data){
								$customer = Db::table("kt_wework_customer")->where("remark_mobiles","like",'%'.$data["phone"].'%')->find();
								$data["customer_id"] = 0;
								if($customer)$data["customer_id"] = $customer["id"];
								$data["staff_name"] = Db::table("kt_wework_staff")->where(["id"=>$data["staffid"]])->field("name")->find()["name"];
								return $data;
							})
							->select()->toArray();
		return $res;
	}

	/*
	* 添加批量加好友
	*/
	static public function addBatch($file_name,$staffs,$tags,$friends){
		$wid = Session::get("wid");
		$data["wid"] = $wid;
		$data["excel_name"] = $file_name;
		$data["create_time"] = date("Y-m-d H:i:s");
		$data["staffid"] = json_encode($staffs,320);
		$data["tags"] = json_encode($tags,320);
		$addcus = Db::table('kt_wework_batch_addcus')->insertGetId($data);
		$start = 0;
		$end = count($staffs) - 1;
		foreach($friends as $phone=>$remarks){
			$isfriend = Db::table('kt_wework_customer')->where(["wid"=>$wid,"staff_id"=>$staffs[$start]])->whereLike("remark_mobiles",'%'.$phone.'%')->count();
			if(!$isfriend){
				$info = Db::table('kt_wework_batch_addcus_info')->where(["wid"=>$wid,"phone"=>$phone,'staffid'=>$staffs[$start]])->find();
				if($info) continue;
				$infoData["wid"] = $wid;
				$infoData["pid"] = $addcus;
				$infoData["phone"] = $phone;
				$infoData["remarks"] = $remarks;
				$infoData["staffid"] = $staffs[$start];
				$infoData["create_time"] = date("Y-m-d H:i:s");
				Db::table('kt_wework_batch_addcus_info')->insertGetId($infoData);
				if($start < $end) $start++;
				if($start >= $end) $start = 0;
			}
		}
		return $addcus;
	}

	/**
	* 获取本次上传文件中有几个是已经存在的
	*/
	static public function getCount($where){
		$wid = Session::get("wid");
		$coount = Db::table('kt_wework_batch_addcus_info')->where($where)->count();
		return $coount;
	}

	/**
	* 批量提醒中的员工去重
	*/
	static public function getInfos($id){
		$wid = Session::get("wid");
		$staffids = Db::table('kt_wework_batch_addcus_info')
								->where(["wid"=>$wid,"id"=>$id])
								->field("staffid")
								->group("staffid")
								->select()
								->toArray();
		return $staffids;
	}

	//->whereBetweenTime('g.create_time', strtotime("yesterday"), strtotime("today"))
	static public function statistics($time=NULL,$staff_ids=NULL){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		if($staff_ids)$where["staffid"] = $staff_ids;
		if($time){
			$data["all_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid])->whereBetweenTime('create_time',$time[0], $time[1])->count();
			$data["allocation_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"add_status"=>2])->whereBetweenTime('create_time',$time[0], $time[1])->count();
			$data["add_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"add_status"=>1])->whereBetweenTime('create_time',$time[0], $time[1])->count();
			$data["to_add_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"add_status"=>0])->whereBetweenTime('create_time',$time[0], $time[1])->count();
			$data["to_pass_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"add_status"=>3])->whereBetweenTime('create_time',$time[0], $time[1])->count();
			$data["performance"] = "0%";
			if($data["add_count"] > 0)$data["performance"] = round($data["all_count"]/$data["add_count"]*100)."％";
			$staffs = array_column(Db::table("kt_wework_batch_addcus_info")->where($where)->field("staffid")->whereBetweenTime('create_time',$time[0], $time[1])->select()->toArray(), "staffid");
			$staffs = array_values(array_unique($staffs));
			$data["staff_list"] = Db::table("kt_wework_staff")
							->where(["wid"=>$wid,"id"=>$staffs])
							->field("name,id")
							->filter(function($staff) use($wid){
								$staff["all_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"staffid"=>$staff["id"]])->count();
								$staff["to_add_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"staffid"=>$staff["id"],"add_status"=>0])->count();
								$staff["to_pass_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"staffid"=>$staff["id"],"add_status"=>3])->count();
								$staff["add_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"staffid"=>$staff["id"],"add_status"=>1])->count();
								$staff["allocation_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"staffid"=>$staff["id"],"add_status"=>2])->count();
								$staff["performance"] = "0%";
								if($staff["add_count"] > 0)$staff["performance"] = round($staff["all_count"]/$staff["add_count"]*100)."％";
								return $staff;
							})
							->select()
							->toArray();
		}else{
			$data["all_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid])->count();
			$data["allocation_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"add_status"=>2])->count();
			$data["add_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"add_status"=>1])->count();
			$data["to_add_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"add_status"=>0])->count();
			$data["to_pass_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"add_status"=>3])->count();
			$data["performance"] = "0%";
			if($data["add_count"] > 0)$data["performance"] = round($data["all_count"]/$data["add_count"]*100)."％";
			$staffs = array_column(Db::table("kt_wework_batch_addcus_info")->where($where)->field("staffid")->select()->toArray(), "staffid");
			$staffs = array_values(array_unique($staffs));
			$data["staff_list"] = Db::table("kt_wework_staff")
							->where(["wid"=>$wid,"id"=>$staffs])
							->field("name,id")
							->filter(function($staff) use($wid){
								$staff["all_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"staffid"=>$staff["id"]])->count();
								$staff["to_add_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"staffid"=>$staff["id"],"add_status"=>0])->count();
								$staff["to_pass_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"staffid"=>$staff["id"],"add_status"=>3])->count();
								$staff["add_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"staffid"=>$staff["id"],"add_status"=>1])->count();
								$staff["allocation_count"] = Db::table("kt_wework_batch_addcus_info")->where(["wid"=>$wid,"staffid"=>$staff["id"],"add_status"=>2])->count();
								$staff["performance"] = "0%";
								if($staff["add_count"] > 0)$staff["performance"] = round($staff["all_count"]/$staff["add_count"]*100)."％";
								return $staff;
							})
							->select()
							->toArray();
		}
		return $data;
	}

	static public function details($staffid){
		$wid = Session::get("wid");
		$res = Db::table("kt_wework_staff")->where(["id"=>$staffid])->field("name,avatar,id")->find();
		$res["all_count"] = Db::table("kt_wework_customer")->where(["wid"=>$wid,"staff_id"=>$res["id"]])->count();
		$res["add_count"] = Db::table("kt_wework_customer")->where(["wid"=>$wid,"staff_id"=>$res["id"]])->whereTime('createtime','>=',strtotime(date("Y-m-d")))->count();
		$res["customer"] = Db::table("kt_wework_customer")
							->where(["wid"=>$wid,"staff_id"=>$res["id"]])
							->json(["tags"])
							->field("tags,avatar,name,createtime,id")
							->filter(function($data) use($wid){
								$data["createtime"] = date("Y-m-d H:i:s",$data["createtime"]);
								$data["tags"] = array_column(Db::table("kt_wework_tag")->where(["tag_id"=>$data["tags"]])->field("tag_name")->select()->toArray(),"tag_name");
								return $data;
							})
							->select()
							->toArray();
		return $res;

	}
}