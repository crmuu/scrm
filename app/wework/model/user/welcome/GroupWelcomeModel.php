<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\model\user\welcome;
use think\facade\Db;
use think\facade\Session;
use app\wework\model\QyWechatApi;

/**
* 入群欢迎语
**/
class GroupWelcomeModel {
	static public $wid;

	/**
	 * 入群欢迎语列表
	 * @param page 页码
	 * @return
 	*/
	static public function getWelcomeList($page,$size,$content=NULL){
		$wid = Session::get('wid');
 	 	$res = Db::table("kt_wework_group_welcome")
 	 					->where(["wid"=>$wid])
 	 					->json(["content","welcome_data"]);
		if($content) $res = $res->whereLike("content","%".$content."%");
		$data['count'] = $res->count();
		$data['item'] = $res->page($page,$size)
 	 					->order("ctime desc")
 	 					->select()
 	 					->toArray();
 	 	$data['page'] = $page;
		$data['size'] = $size;
 	 	return $data;
	}

	/**
	 * 获取详情
 	*/
	static public function getWelcome($id){
		$wid = Session::get('wid');
 	 	$res = Db::table("kt_wework_group_welcome")->where(["wid"=>$wid,"id"=>$id])->json(["content","welcome_data"])->find();

		return $res;
	}

	/**
	 * 删除
 	*/
	static public function delWelcome($id){
 	 	$res = Db::table("kt_wework_group_welcome")->delete($id);
 	 	
		return $res;
	}

	/**
	* 新建入群欢迎语 修改
	*/
	static public function addWelcom($content,$welcome_data,$id=NUll,$send_status=NULL,$template_id=NULL){
		$wid = Session::get('wid');
		$data["wid"] = $wid;
		$data["content"] = json_encode($content,320);
		$data["welcome_data"] = json_encode($welcome_data,320);
		if(!$id){
			$data["template_id"] = $template_id;
			$data["send_status"] = $send_status;
			$data["ctime"] = date("Y-m-d H:i:s",time());
			$res = Db::table("kt_wework_group_welcome")->insertGetId($data);
		}else{
			$where["id"] = $id;
			$res = Db::table("kt_wework_group_welcome")->where($where)->save($data);
		}
		
		return $res;
	}


	//组装 换迎语附件 数据
    static public function groupWelcomeOther($other,$wid){
        switch ($other['type']) {
            case '1':
                $data["image"] = [
                    'media_id' => QyWechatApi::mediaUpload($wid,$other['imageurl'],'image')['media_id']
                ];
                break;
            case '2':
                $data["link"] = [
                    'title' => $other['link_title'],
                    'picurl' => $other['link_picurl'],
                    'desc' => $other['link_desc'],
                    'url' => $other['link_url']
                ];
                break;
            case '3':
                $data["miniprogram"] = [
                    'title' => $other['miniprogram_title'],
                    'pic_media_id' => QyWechatApi::mediaUpload($wid,$other['miniprogram_picurl'],'image')['media_id'],
                    'appid' => $other['miniprogram_appid'],
                    'page' => $other['miniprogram_page']
                ];
                break;
        }
        return $data;
    }
}