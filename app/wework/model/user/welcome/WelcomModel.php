<?php
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\model\user\welcome;
use think\facade\Db;
use think\facade\Session;

/**
* 好友欢迎语
**/
class WelcomModel{
	static public $wid;
	/**
	 * 获取欢迎语列表数据
	 * @param $wid 账户id
	 */
 	static public function list($page=1,$size=10,$content=NULL){
 		$wid = Session::get("wid");
 		$where["wid"] = $wid;
		$res = Db::table("kt_wework_welcomemsg")
						->where($where);
		if($content)$res = $res->whereLike('content','%'.$content.'%');
		$data['count'] = $res->count();
		$data['item'] = $res->page($page,$size)->field("id,create_time,staffer,update_time,adjunct_data,content")->json(["staffer","content","adjunct_data"])
							->filter(function($data){
								$data["staff_info"] = Db::table("kt_wework_staff")->where(["id"=>$data["staffer"]])->field("name,id")->select()->toArray();
								return $data;
							})->select()
							->toArray();
		$data['page'] = $page;
		$data['size'] = $size;
		return $data;
 	}

 	static public function addWelcom($staffer,$content,$dayparting_status,$adjunct_data,$dayparting_data=NULL,$arr=[],$id=NULL){
 		$wid = Session::get("wid");
 		$where = [];
 		if($id) $where["id"] = $id;
 		$data["staffer"] = json_encode($staffer,320);
 		$data["adjunct_data"] = json_encode($adjunct_data,320);
 		if($dayparting_status == 1)$data["dayparting_data"] = json_encode($dayparting_data,320);
 		$data["wid"] = $wid;
 		$data["dayparting_status"] = $dayparting_status;
 		$data["staffstr"] = json_encode($arr,320);
 		$data["content"] = json_encode($content,320);
 		if(!$id) $data["create_time"] = date("Y-m-d H:i:s",time());
 		$data["update_time"] = date("Y-m-d H:i:s",time());
 		$res = Db::table("kt_wework_welcomemsg")->where($where)->save($data);
 		return $res;
 	}

 	static public function delWelcom($id){
 		$res = Db::table("kt_wework_welcomemsg")->where(["id"=>$id])->delete();
 		return $res;
 	}

 	static public function info($id){
 		$res = Db::table("kt_wework_welcomemsg")->where(["id"=>$id])->json(["staffer","content","adjunct_data","dayparting_data"])->find();
 		$res["staffer"] = Db::table("kt_wework_staff")->where(["id"=>$res["staffer"]])->field("name,id,avatar")->select()->toArray();
 		return $res;
 	}

}