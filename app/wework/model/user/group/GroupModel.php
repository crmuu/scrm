<?php 
namespace app\wework\model\user\group;
use think\facade\Db;
use think\facade\Session;
use app\wework\model\QyWechatApi;

/**
* 群管理
**/

class GroupModel 
{
	/**
	 * 群列表
	 * @return 
	 */
	static public function list($page=1,$size=10,$staff=NULL,$name=NULL,$tags=NULL,$status=NULL,$order="desc",$times=NULL,$wid=NULL){
		$wid = Session::get('wid')?:$wid;
		$res = Db::table('kt_wework_group');
		if($tags){
			foreach ($tags as $value){
				$where = [];
	            $where[0] = ["wid","=",$wid];
				if($status === 0)$where[0] = ["is_dismissed","=",$status];
				if($status === 1)$where[0] = ["is_dismissed","=",$status];
				if($staff)$where[0] = ["staff_id","in",$staff];
	            $where[0] = ["tag_info","like","%\"{$value}\"%"];
				$res = $res->whereOr($where);
			}
		}else{
			$where["wid"] = $wid;
			if($status === 0)$where["is_dismissed"] = $status;
			if($status === 1)$where["is_dismissed"] = $status;
			if($staff)$where["staff_id"] = $staff;
			$res = $res->where($where);
		}
		$res = $res->where("staff_id","not null");
		if($name)$res = $res->whereLike('name','%'.$name.'%');
		if($times)$res = $res->whereTime('create_time','>=',strtotime($times));
			$data["count"] = $res->count();
			$data["page"] = $page;
			$data["size"] = $size;
			$data["item"] = $res->page($page,$size)
					->json(["tags"])
					->order("create_time",$order)
				 	->filter(function($group)use($wid){
				 		$group["staff"] = Db::table("kt_wework_staff")->where(["id"=>$group["staff_id"]])->json(["department"])->field("name,department")->find();
				 		if($group["staff"]["department"])$group["staff"]["department"] = array_column(Db::table("kt_wework_department")->where(["department_id"=>$group["staff"]["department"],"wid"=>$wid])->field("department_name")->select()->toArray(), "department_name");
				 		$group["tags"] = Db::table("kt_wework_group_tag")->where(["id"=>$group["tags"]])->field("name,deleted")->select()->toArray();
				 		$group["size"] = Db::table("kt_wework_group_member")->where(["chat_id"=>$group["chat_id"],"status"=>1])->count();
				 		$group["day_add"] = Db::table("kt_wework_group_member")->where(["chat_id"=>$group["chat_id"]])->whereTime('join_time','>=',strtotime(date("Y-m-d",time())))->count();
				 		$group["day_del"] = Db::table("kt_wework_group_member")->where(["chat_id"=>$group["chat_id"]])->whereTime('lossing_time','>=',strtotime(date("Y-m-d",time())))->count();
				 		$group["create_time"] = date("Y-m-d H:i:s",$group["create_time"]);
				 		return $group;
				 	})
				 	->select()
				 	->toArray();
		$data["group_szie"] = Db::table("kt_wework_group_member")->where(["status"=>1])->count();
		return $data;
	}

	/**
	 * 群详情
	 * @return 
	 */
	static public function detail($chatId){
		$wid = Session::get('wid');
		$res = Db::table('kt_wework_group')
				->where(["wid"=>$wid,"chat_id"=>$chatId])
				->json(["tags","notice"])
				->filter(function($group){
					$group["staff"] = Db::table("kt_wework_staff")->where(["id"=>$group["staff_id"]])->field("name,avatar")->find();
					$group["tags"] = Db::table("kt_wework_group_tag")->where(["id"=>$group["tags"]])->field("name,deleted")->select()->toArray();
					$group["size"] = Db::table("kt_wework_group_member")->where(["chat_id"=>$group["chat_id"],"status"=>1])->count();
					$group["client_size"] = Db::table("kt_wework_group_member")->where(["chat_id"=>$group["chat_id"],"status"=>1,"type"=>2])->count();
					$group["day_add"] = Db::table("kt_wework_group_member")->where(["chat_id"=>$group["chat_id"]])->whereTime('join_time','>=',strtotime(date("Y-m-d",time())))->count();
			 		$group["day_del"] = Db::table("kt_wework_group_member")->where(["chat_id"=>$group["chat_id"]])->whereTime('lossing_time','>=',strtotime(date("Y-m-d",time())))->count();
			 		return $group;
				})
				->find();
		return $res;
	}

	/**
	* 客户群详情
	*/
	static public function detailClient($chatId,$page,$size,$name=NULL){
		$wid = Session::get('wid');
		$group = Db::table("kt_wework_group")->where(["chat_id"=>$chatId,"wid"=>$wid])->find();
		$staff = Db::table("kt_wework_staff")->where(["id"=>$group["staff_id"]])->find();
		$res = Db::table('kt_wework_group_member')
					->where(["chat_id"=>$chatId,"wid"=>$wid])
					->json(["invitor"]);
		if($name)$res->whereLike('name','%'.$name.'%');
		$data["count"] = $res->count();
		$data["page"] = $page;
		$data["size"] = $size;
		$data["item"] = $res->page($page,$size)
					->field("type,join_time,join_scene,invitor,unionid,userid")
					->filter(function($member)use($wid,$staff){
						$member["join_time"] = date("Y-m-d H:i:s",$member["join_time"]);
						$member["group_owner"] = 0;
						if($staff["userid"] == $member["userid"])$member["group_owner"] = 1;
						if($member["type"] == 1)$member["user"] = Db::table("kt_wework_staff")->where(["wid"=>$wid,"userid"=>$member["userid"]])->field("name,avatar,id")->find();
						if($member["type"] == 2)$member["user"] = Db::table("kt_wework_customer")->where(["wid"=>$wid,"external_userid"=>$member["userid"]])->field("name,avatar,id")->find();
						if($member["invitor"])$member["invitor"] = Db::table("kt_wework_staff")->where(["userid"=>$member["invitor"]["userid"]])->field("name,avatar")->find();
						return $member;
					})
					->select()
					->toArray();
		return $data;
	}

	static public function groupSize($chatId,$time_tow){
		$wid = Session::get('wid');
		$res = Db::table('kt_wework_group_member')
				->where(["Wid"=>$wid,"chat_id"=>$chatId,"status"=>1])
				->whereTime('join_time','<=',strtotime($time_tow))
				->count();
		return $res;
	}

	static public function clientSize($chatId,$time_tow){
		$wid = Session::get('wid');
		$res = Db::table('kt_wework_group_member')
				->where(["Wid"=>$wid,"chat_id"=>$chatId,"status"=>1,"type"=>2])
				->whereTime('join_time','<=',strtotime($time_tow))
				->count();
		return $res;
	}

	static public function addSize($chatId,$time_one,$time_tow){
		$wid = Session::get('wid');
		$res = Db::table('kt_wework_group_member')
				->where(["Wid"=>$wid,"chat_id"=>$chatId,"status"=>1])
				->whereTime('join_time','<=',strtotime($time_tow))
				->whereTime('join_time','>=',strtotime($time_one))
				->count();
		return $res;
	}

	static public function delSize($chatId,$time_one,$time_tow){
		$wid = Session::get('wid');
		$res = Db::table('kt_wework_group_member')
				->where(["Wid"=>$wid,"chat_id"=>$chatId,"status"=>0])
				->whereTime('lossing_time','<=',strtotime($time_tow))
				->whereTime('lossing_time','>=',strtotime($time_one))
				->count();
		return $res;
	}

	static public function group($chatId){
		$wid = Session::get('wid');
		$res = Db::table('kt_wework_group')->where(["wid"=>$wid,"chat_id"=>$chatId])->find();
		return $res;
	}

	/*
	* 批量打标签
	*/
	static public function batchTag($chatIds,$tags,$tag_info){
		$wid = Session::get('wid');
		$data["tags"] = json_encode($tags,320);
		$data["tag_info"] = json_encode($tag_info,320);
		$res = Db::table('kt_wework_group')->where(["wid"=>$wid,"chat_id"=>$chatIds])->update($data);

		return $res;
	}
}