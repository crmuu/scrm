<?php 
namespace app\wework\model\user\label;
use think\facade\Db;
use think\facade\Session;

/**
* 群标签类
**/
class GroupTagModel {
	static public $wid;
	
	/**
	* 群标签列表
	* @param  $page 页码
	* @param  $size 条数
	*/
	static public function list($page,$size){
		$wid = Session::get('wid');
		$where["wid"] = $wid;
		$where["deleted"] = 1;
		$res = Db::table('kt_wework_group_taggroup')
			   ->where($where)
			   ->field("name,id");
		$data['count'] = $res->count();
		$data['item'] = $res->page($page,$size)
						->order("create_time DESC")
						->filter(function($data){
							$data["tag_info"] = Db::table("kt_wework_group_tag")->where(["group_id"=>$data["id"],"deleted"=>1])->field("name,id")->order("orders asc")->select()->toArray();
							return $data;
						})->select()
						->toArray();
		$data['page'] = $page;
		$data['size'] = $size;
		return $data;
	}

	static public function tagList(){
		$wid = Session::get('wid');
		$where["wid"] = $wid;
		$where["deleted"] = 1;
		$res = Db::table('kt_wework_group_taggroup')
			   ->where($where)
			   ->field("name,id")
			   ->filter(function($data){
					$data["group_name"] = $data["name"];
					$data["group_id"] = $data["id"];
					$data["tag"] = Db::table("kt_wework_group_tag")
						->where(["group_id"=>$data["id"],"deleted"=>1])
						->field("name,id")
						->filter(function($tags){
							$tags["tag_name"] = $tags["name"];
							$tags["tag_id"] = $tags["id"];
							return $tags;
						})
						->select()
						->toArray();
					return $data;
				})->select()
				->toArray();
		return $res;
	}

	/**
	* 添加群标签组
	* @param  name 标签组名称
	*/
	static public function tagGroup($name){
		$wid = Session::get('wid');
		$data["wid"] = $wid;
		$data["name"] = $name;
		$data["create_time"] = date("Y-m-d H:i:s",time());
		$data["update_time"] = date("Y-m-d H:i:s",time());
		$res = Db::table('kt_wework_group_taggroup')->insertGetId($data);
		return $res;
	}

	/**
	* 修改群标签组
	* @param  name 标签组名称
	*/
	static public function updTag($group_name,$group_id,$add_tag,$remove_tag){
		$wid = Session::get('wid');
		$group = Db::table('kt_wework_group_taggroup')->where('wid',$wid)->where('id',$group_id)->find();
		Db::table('kt_wework_group_taggroup')->update(['id'=>$group['id'],'name'=>$group_name]);
		Db::table('kt_wework_group_tag')->where(['id'=>$remove_tag])->update(["deleted"=>0]);
		foreach ($add_tag as $k => $t) {
			if(!isset($t['id']) || !$t['id']){
				$res = self::add($group_id,$t["name"],$k+1);
			}else{
				$res = self::add($group_id,$t["name"],$k+1,$t['id']);
			}
		}
		return $res;
	}

	/**
	* 验证标签组名称是否重复
	*  @param tagGroup_id 标签组id
	*  @param name 标签组名称
	*/
	static public function isSet($name,$tagGroup_id=NULL){
		$wid = Session::get('wid');
		$where["wid"] = $wid;
		$where["name"] = $name;
		if($tagGroup_id) $res = Db::table('kt_wework_group_taggroup')->where($where)->where('id','<>',$tagGroup_id)->find();
		if(!$tagGroup_id)$res = Db::table('kt_wework_group_taggroup')->where($where)->find();
		return $res;
	}

	/**
	* 验证名称是否重复
	*  @param  tagGroup_id 标签组id
	*  @param  tag_data 标签数据
	*/
	static public function addTags($tagGroup_id,$tag_data){
		$wid = Session::get('wid');
		foreach($tag_data as $key=>$tags){
			$data[$key]["wid"] = $wid;
			$data[$key]["name"] = $tags;
			$data[$key]["group_id"] = $tagGroup_id;
			$data[$key]["create_time"] = date("Y-m-d H:i:s",time());
			$data[$key]["update_time"] = date("Y-m-d H:i:s",time());
		}
		$res = Db::table('kt_wework_group_tag')->insertAll($data);
		return $res;
	}

	/**
	* 添加标签
	*  @param  group_id 标签组id
	*  @param  tag_name 标签数据
	*/
	static public function add($group_id,$tag_name,$orders,$tag_id=NULL){
		$wid = Session::get('wid');
		$where = [];
		$data["wid"] = $wid;
		$data["name"] = $tag_name;
		$data["orders"] = $orders;
		$data["group_id"] = $group_id;
		$data["create_time"] = date("Y-m-d H:i:s",time());
		$data["update_time"] = date("Y-m-d H:i:s",time());
		if($tag_id) $res = Db::table('kt_wework_group_tag')->where(["id"=>$tag_id])->save($data);
		if(!$tag_id) $tag_id = Db::table('kt_wework_group_tag')->insertGetId($data);
		$res = Db::table('kt_wework_group_tag')->where(["id"=>$tag_id])->find();
		
		return $res;
	}

	static public function orders($group_id,$tag_id){
		if($tag_id)$res = Db::table('kt_wework_group_tag')->where(["id"=>$tag_id])->filed("orders")->find()["orders"];
		if(!$tag_id)$res = Db::table('kt_wework_group_tag')->where(["group_id"=>$group_id])->max("orders");
		
		return $res;
	}

	/**
	* 验证标签名称是否重复
	*  @param name 标签组名称
	*/
	static public function tagSet($name,$tagGroup_id,$tag_id=NULL){
		$wid = Session::get('wid');	
		$where["wid"] = $wid;
		$where["name"] = $name;
		$where["group_id"] = $tagGroup_id;
		if($tag_id) $res = Db::table('kt_wework_group_tag')->where($where)->where('id','<>',$tag_id)->find();
		if(!$tag_id) $res = Db::table('kt_wework_group_tag')->where($where)->find();
		return $res;
	}

	/**
	* 删除标签组跟子标签
	*  @param  tagGroup_id 标签组id
	*/
	static public function delTagGroup($group_id){
		$wid = Session::get('wid');
		$where["wid"] = $wid;
		$where["group_id"] = $group_id;
		Db::table('kt_wework_group_tag')->where($where)->update(["deleted"=>0]);
		$res = Db::table('kt_wework_group_taggroup')->where(["id"=>$group_id])->update(["deleted"=>0]);
		return $res;
	}

	/**
	* 删除子标签
	*  @param  tag_id 标签组id
	*/
	static public function delTag($tag_id){
		$res = Db::table('kt_wework_group_tag')->where(["id"=>$tag_id])->update(["deleted"=>0]);
		return $res;
	}

	/**
	* 删除子标签
	*  @param  tag_id 标签组id
	*/
	static public function info($id){
		$res = Db::table('kt_wework_group_taggroup')->where(["id"=>$id])->field("id,name")->find();
		$res["info"] = Db::table("kt_wework_group_tag")->where(["group_id"=>$id])->field("name,id")->order("orders asc")->select()->toArray();
		return $res;
	}

}