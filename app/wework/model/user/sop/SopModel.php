<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\model\user\sop;
use think\facade\Db;
use think\facade\Session;
use app\wework\model\QyWechatApi;

/**
* sop类
**/
class SopModel {
	static public $wid;
	/**
	 * 获取数据
	 * @param $wid 账户id
	 * @param $data 接受的数据
	 * @return 
	 */
	static public function list($req){
		$wid = Session::get('wid');
		$page = $req->post('page',1);
		$size = $req->post('size',10);
		$name = $req->post('name');
		$scene_type = $req->post('scene_type');
		$res = Db::table('kt_wework_customer_sop')
			   ->field('id,wid,status,create_time,scene_type,name,staffid')
			   ->json(['staffid'])
			   ->where('wid',$wid);
		if($name) $res->whereLike('name',"%$name%");
		if($scene_type) $res->where('scene_type',$scene_type);
		$data['count'] = $res->count();
		$data['item'] = $res ->page($page,$size)
			->filter(function($r){
				$r['satff_name'] = Db::table('kt_wework_staff')->where('id','in',$r['staffid'])->column('name');
				return $r;
			})
			->select();
		$data['page'] = $page;
		$data['size'] = $size;
		return success('sop',$data);
	}

	/*
	* 获取所有节日
	*/
	static public function festival(){
		$list = Db::table("kt_wework_sop_festival")->select()->toArray();

		return success('节日',$list);
	}

	/**
	 * 获取详情
	 * @param $wid 账户id
	 * @param $data 接受的数据
	 * @return 
	 */
	static public function detail($req){
		$wid = Session::get('wid');
		$id = $req->post('id');
		$res = Db::table('kt_wework_customer_sop')->json(['staffid','filter_tag',"rule_data"])->find($id);
		$res['filter_tag'] = Db::table('kt_wework_tag')->where(["wid"=>$wid])->where('tag_id','in',$res['filter_tag'])->field("tag_name,tag_id")->select()->toArray();
		$res['staffid'] = Db::table('kt_wework_staff')->where('id','in',$res['staffid'])->field('name,id,avatar')->select()->toArray();
		$res["rule_data"] = Db::table('kt_wework_customer_sop_rule')->where(["sop_id"=>$res["id"]])->json(["send_content","send_timing_data"])->select()->toArray();

		return success('sop详情',$res);
	}
	/**
	 * 删除
	 * @param $wid 账户id
	 * @param $data 接受的数据
	 * @return 
	 */
	static public function delete($req){
		$wid = Session::get('wid');
		$id = $req->post('id');
		$res = Db::table('kt_wework_customer_sop')->where('wid',$wid)->delete($id);
		if($res){
			Db::table('kt_wework_customer_sop_rule')->where('wid',$wid)->where('sop_id',$id)->delete();
			return success('sop删除成功');
		}	   
		return error('sop删除失败');
	}
	/**
	 * 开关
	 * @param $wid 账户id
	 * @return 
	 */
	static public function open($req){
		$wid = Session::get('wid');
		$id = $req->post('id');
		if(!$id) return error('参数错误');
		$status = $req->post('status',0);
		$res = Db::table('kt_wework_customer_sop')->where('id',$id)->update([
			'status' => $status
		]);
		return success('操作成功');
	}
	/**
	 * 添加
	 * @param $wid 账户id
	 * @param $data 接受的数据
	 * @return 
	 */
	static public function add($req){
		$wid = Session::get('wid');
		$data = [];
		$data['wid'] = $wid;
		$data['name'] = $req->post('name');
		if(!$data['name']) return error('缺少参数name');
		$data['scene_type'] = $req->post('scene_type');
		if(!$data['scene_type']) return error('缺少场景');
		$data['staffid'] = $req->post('staffid',[]);
		if(!$data['staffid']) return error('缺少成员');
		$data['filter_status'] = $req->post('filter_status',0);
		$data['filter_tag'] = $req->post('filter_tag',[]);
		if($data['filter_status'] == 1 && !$data['filter_tag']) return error('缺少过滤条件');
		$data['start_type'] = $req->post('start_type');
		$data['festival'] = $req->post('festival');
		$data['start_date'] = $req->post('start_date');
		if($data['start_type'] == 1) $data['start_date'] = date("Y-m-d H:i:s");
		$data['update_time'] = date("Y-m-d H:i:s");
		$rule_data = $req->post('rule_data',[]);
		if(!$rule_data) return error('缺少规则');
		if($req->post('id')){
			$id = $req->post('id');
			$res = Db::table('kt_wework_customer_sop')->where(["id"=>$id])->json(['staffid','filter_tag'])->save($data);
		} else {
			$data['create_time'] = date("Y-m-d H:i:s");
			$id = Db::table('kt_wework_customer_sop')->json(['staffid','filter_tag'])->insertGetId($data);
			$res = $id;
		}
		$del_rule = $req->post('del_rule',[]);
		if($del_rule)Db::table('kt_wework_customer_sop_rule')->where("id","in",$del_rule)->delete();
		foreach ($rule_data as $value){
		    $rule = [];
		    $rule["wid"] = $wid;
		    $rule["sop_id"] = $id;
		    $rule["trigger_form"] = $value['trigger_form'];
		    $rule["send_type"] = $value['send_type'];
		    $rule["send_content"] = json_encode($value['send_content'],320);
		    if($value['send_type'] == 1){
		        $rule["send_timing_data"] = json_encode($value["send_timing_data"],320);
		    }else{
                $rule["send_frequency_type"] = $value["send_frequency_type"];
                $rule["send_frequency"] = $value["send_frequency"];
                $rule["send_starttime_type"] = $value["send_starttime_type"];
                if($rule["send_starttime_type"] == 2)$rule["send_starttime"] = $value["send_starttime"];
                $rule["send_endtime_type"] = $value["send_endtime_type"];
                if($rule["send_endtime_type"] == 2)$rule["send_endtime_date"] = $value["send_endtime_date"];
		    }
            if(isset($value["id"]))Db::table('kt_wework_customer_sop_rule')->where(["id"=>$value["id"]])->save($data);
            if(!isset($value["id"]))Db::table('kt_wework_customer_sop_rule')->insertGetId($rule);
		}
		if($res) return success('更新sop成功');
		return error('更新sop失败');
	}
}
