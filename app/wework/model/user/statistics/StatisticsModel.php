<?php 
namespace app\wework\model\user\statistics;
use think\facade\Db;
use think\facade\Session;
use app\wework\model\QyWechatApi;
use dh2y\qrcode\QRcode;

/**
* 首页数据统计 model
**/
class StatisticsModel {
	
	static public function info(){
		$wid = Session::get("wid");

	}

	//客户总数
	static public function size(){
		$wid = Session::get("wid");
		$size = Db::table("kt_wework_customer")->where(["wid"=>$wid])->count();

		return $size;
	}

	static public function delSize(){
		$wid = Session::get("wid");
		$size = Db::table("kt_wework_customer")->where(["wid"=>$wid])->whereTime('createtime','<=',strtotime(date("Y-m-d")))->where("status","<>","0")->count();

		return $size;
	}

	static public function addSize(){
		$wid = Session::get("wid");
		$size = Db::table("kt_wework_customer")->where(["wid"=>$wid,"status"=>0])->whereTime('createtime','>=',strtotime(date("Y-m-d")))->count();

		return $size;
	}

	static public function groupSize(){
		$wid = Session::get("wid");
		$size = Db::table("kt_wework_group")->where(["wid"=>$wid])->count();

		return $size;
	}

	//今日新增
	static public function addGroupSize(){
		$wid = Session::get("wid");
		$size = Db::table("kt_wework_group_member")->where(["wid"=>$wid,"status"=>1])->whereTime('join_time','<=',strtotime(date("Y-m-d")))->count();

		return $size;
	}
	
	//今日流失
	static public function delGroupSize(){
		$wid = Session::get("wid");
		$size = Db::table("kt_wework_group_member")->where(["wid"=>$wid,"status"=>0])->whereTime('lossing_time','<=',strtotime(date("Y-m-d")))->count();

		return $size;
	}

	//获取总配置
	static public function getLoginInfo($host){
		$admin = Db::table('kt_base_agent')->where('domain',$host)->find();
		if(!$admin) $admin = Db::table('kt_base_agent')->where('isadmin',1)->find();

		return $admin;
	}

	//获取所选中的成员userid
	static public function getUsers($steff){
		$res = array_column(Db::table("kt_wework_staff")->where(["id"=>$steff])->field("userid")->select()->toArray(), "userid");

		return $res;
	}

	static public function infos($wid){
		$res = Db::table("kt_wework_config")->where(["id"=>$wid])->find();

		return $res;
	}

}