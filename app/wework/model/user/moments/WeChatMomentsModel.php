<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\model\user\moments;
use think\facade\Db;
use think\facade\Session;
use app\wework\model\QyWechatApi;
use think\facade\Log;

/**
* 企微朋友圈
**/
class WeChatMomentsModel {

	static public function list($page,$size,$staffs=NULL,$times=NULL,$name=NULL,$type,$wid=NULL){
		$wid = Session::get("wid")?:$wid;
		$res = Db::table("kt_wework_moments");
		if($staffs){
			foreach ($staffs as $value){
				$where = [];
	            $where[0] = ["wid","=",$wid];
	            if($type)$where[0] = ["type","=",$type];
	            $where[0] = ["staffstr","like","%\"{$value}\"%"];
				$res = $res->whereOr($where);
			}
		}else{
			$where["wid"] = $wid;
			if($type)$where["type"] = $type;
			$res = $res->where($where);
		}
		if($name)$res = $res->where("name","like","%".$name."%");
		if($times)$res = $res->whereBetweenTime('ctime', $times[0], $times[1]);
		$data["count"] = $res->count();
		$data["item"] = $res->json(["staffs","tags","text","content"])
						->filter(function($moments) use($wid){
							$moments["staff_list"] = Db::table("kt_wework_staff")->where(["id"=>$moments["staffs"]])->field("name,id,avatar,userid")->select()->toArray();
							$yes_send = Db::table("kt_wework_moments_info")->where(["pid"=>$moments["id"],"staff_id"=>$moments["staffs"],"type"=>1])->count();
							$not_send = count($moments["staff_list"]) - $yes_send;
							$moments["yes_send"] = $yes_send;
							$moments["not_send"] = $not_send;
							if($moments["not_send"] == 0)Db::table("kt_wework_moments")->where(["id"=>$moments["id"]])->update(["status"=>1]);

							return $moments;
						})
						->page($page,$size)
						->select()
						->toArray();
		$data["page"] = $page;
		$data["size"] = $size;

		return $data;
	}

	static public function syncList(){
		$wid = Session::get("wid");
		$where["wid"] = $wid;
		$where["status"] = 0;
		$where["type"] = 1;
		$res = Db::table("kt_wework_moments")->where($where)->json(["staffs"])->select()->toArray();

		return $res;
	}

	static public function add($name,$type,$staffs,$customer_type,$tags=NULL,$text,$content,$send_status,$send_time=NULL,$staffstr){
		$wid = Session::get("wid");
		$data["wid"] = $wid;
		$data["name"] = $name;
		$data["type"] = $type;
		$data["staffstr"] = json_encode($staffstr,320);
		$data["tags"] = json_encode($tags,320);
		$data["staffs"] = json_encode($staffs,320);
		$data["customer_type"] = $customer_type;
		$data["text"] = json_encode($text);
		$data["content"] = json_encode($content,320);
		$data["send_status"] = $send_status;
		if($send_status == 2)$data["send_time"] = $send_time;
		$data["ctime"] = date("Y-m-d H:i:s");

		$res = Db::table("kt_wework_moments")->insertGetId($data);

		return $res;
	}

	static public function users($staffs){
		$res = Db::table("kt_wework_staff")->where(["id"=>$staffs])->column("userid");

		return $res;
	}

	static public function staffs($id){
		$res = Db::table("kt_wework_moments_info")->where(["pid"=>$id])->column("staff_id");
		$data = Db::table("kt_wework_staff")->where(["id"=>$res])->column("userid");

		return $data;
	}

	static public function save($id,$jobid,$moment_id){
		$res = Db::table("kt_wework_moments")->where(["id"=>$id])->update(["jobid"=>$jobid,"moment_id"=>$moment_id]);

		return $res;
	}

	static public function infos($id){
		$res = Db::table("kt_wework_moments")->where(["id"=>$id])->json(["staffs"])->find();

		return $res;
	}

	static public function info($id,$type){
		$wid = Session::get("wid");
		$res = Db::table("kt_wework_moments")->where(["id"=>$id])->json(["tags","staffs","text","content"])->find();
		$customer_type = $res["customer_type"];
		$tags = $res["tags"];
		if($customer_type == 1)$res["customer_szie"] = Db::table("kt_wework_customer")->where(["staff_id"=>$res["staffs"]])->count();
		if($customer_type == 2){
			$res["tags"] = Db::table("kt_wework_tag")->where(["tag_id"=>$tags])->field("tag_name,deleted")->select()->toArray();
			$whereor = [];
			$size = Db::table("kt_wework_customer");
			foreach ($tags as $value){
				$map = [];
				$map[] = [
			        ["staff_id","in",$res["staffs"]],
			        ["tags","like","%{$value}%"]
			    ];
				$size = $size->whereOr($map);
			}
			$res["customer_szie"] = $size->count();
		}
		$customer_szie = 0;//客户人数
		$res["send_size"] = count($res["staffs"]); //全部发送成员人数
		if($res["type"] == 1 && $res["status"] == 0){
			$result = QyWechatApi::getMoment($wid,$res["moment_id"]);
			if($result["errcode"] != 0)return "error";
			$task_list = $result["task_list"];
			foreach ($task_list as $value){
				$datas = [];
				$datas["pid"] = $res["id"];
				$datas["wid"] = $wid;
				$datas["moment_id"] = $res["moment_id"];
				$datas["type"] = $value["publish_status"];
				$staff_id = Db::table("kt_wework_staff")->where(["userid"=>$value["userid"]])->column("id");
				$datas["staff_id"] = $staff_id[0];
				if($value["publish_status"] == 1)$datas["create_time"] = date("Y-m-d H:i:s");
				$is_set = Db::table("kt_wework_moments_info")->where(["staff_id"=>$staff_id,"pid"=>$res["id"]])->find();
				if($is_set)Db::table("kt_wework_moments_info")->where(["staff_id"=>$staff_id,"pid"=>$res["id"]])->update($datas);
				if(!$is_set)Db::table("kt_wework_moments_info")->insertGetId($datas);
			}
		}
		$yes_send = Db::table("kt_wework_moments_info")->where(["pid"=>$id,"staff_id"=>$res["staffs"],"type"=>1])->count();
		$not_send = count($res["staffs"]) - $yes_send;
		$res["yes_send"] = $yes_send;
		$res["not_send"] = $not_send;
		if($res["not_send"] == 0)Db::table("kt_wework_moments")->where(["id"=>$res["id"]])->update(["status"=>1]);
		$where["pid"] = $id;
		$where["staff_id"] = $res["staffs"];
		if(isset($type))$where["type"] = $type;
		$res["staff_list"] = Db::table("kt_wework_moments_info")
							->where($where)
							->filter(function($info) use($wid,$customer_type,$tags,$res){
								$info["customer_szie"] = 0;
								if($customer_type == 1 && $info["type"] == 1)$info["customer_szie"] = Db::table("kt_wework_customer")->where(["staff_id"=>$res["staffs"]])->count();
								if($customer_type == 2 && $info["type"] == 1){
									$size = Db::table("kt_wework_customer");
									foreach ($tags as $value){
										$map = [];
										$map[] = [
									        ["staff_id","in",$res["staffs"]],
									        ["tags","like","%{$value}%"]
									    ];
										$size = $size->whereOr($map);
									}
									$info["customer_szie"] = $size->count();
								}
								$info["staff"] =  Db::table("kt_wework_staff")->where(["id"=>$info["staff_id"]])->field(["avatar,name,userid"])->find();
								return $info; 
							})
							->select()
							->toArray();

		$res["t_send_size"] = count($res["staff_list"]); //图表中发送人数
		$res["not_send"] = $not_send;
		$res["yes_send"] = $yes_send;
		$res["send_customer_szie"] = $customer_szie; //已送达客户朋友圈人数
		if($res["not_send"] == 0)Db::table("kt_wework_moments")->where(["id"=>$res["id"]])->update(["status"=>1]);

		return $res;
	}

	static public function staffList($where,$wid,$customer_type,$tags,$res){
		$staff_list = Db::table("kt_wework_moments_info")
				->where($where)
				->filter(function($info) use($wid,$customer_type,$tags,$res){
					$info["customer_szie"] = 0;
					if($customer_type == 1 && $info["type"] == 1)$info["customer_szie"] = Db::table("kt_wework_customer")->where(["staff_id"=>$res["staffs"]])->count();
					if($customer_type == 2 && $info["type"] == 1){
						$size = Db::table("kt_wework_customer");
						foreach ($tags as $value){
							$map = [];
							$map[] = [
						        ["staff_id","in",$res["staffs"]],
						        ["tags","like","%{$value}%"]
						    ];
							$size = $size->whereOr($map);
						}
						$info["customer_szie"] = $size->count();
					}
					$info["staff"] =  Db::table("kt_wework_staff")->where(["id"=>$info["staff_id"]])->field(["name"])->find();
					return $info; 
				})
				->select()
				->toArray();
		return $staff_list;
	}

	//同步企业朋友圈
	static public function syncs($data,$wid){
		$send_list = Db::table("kt_wework_moments_info")->where(["pid"=>$data["id"],"staff_id"=>$data["staffs"],"type"=>1])->column("staff_id");
		$result = QyWechatApi::getMoment($wid,$data["moment_id"]);
		if($result["errcode"] != 0)return "error";
		$task_list = $result["task_list"];
		foreach ($task_list as $value){
			$datas["pid"] = $data["id"];
			$datas["wid"] = $wid;
			$datas["moment_id"] = $data["moment_id"];
			$datas["type"] = $value["publish_status"];
			$staff_id = Db::table("kt_wework_staff")->where(["userid"=>$value["userid"]])->column("id");
			if(in_array($staff_id,$send_list))continue;
			$datas["staff_id"] = $staff_id;
			$is_set = Db::table("kt_wework_moments_info")->where(["staff_id"=>$staff_id,"pid"=>$data["id"]])->find();
			if($is_set)Db::table("kt_wework_moments_info")->where(["staff_id"=>$staff_id,"pid"=>$data["id"]])->update($datas);
			if(!$is_set)Db::table("kt_wework_moments_info")->insertGetId($datas);
		}
		$size = Db::table("kt_wework_moments_info")->where(["pid"=>$data["id"],"staff_id"=>$data["staffs"],"type"=>1])->count();
		if($size == count($data["staffs"]))Db::table("kt_wework_moments")->where(["id"=>$data["id"]])->update(["status"=>1]);

		return "ok";

	}

	//同步 个人朋友圈 数据
	static public function sync($moment_list){
		$wid = Session::get("wid");
		foreach ($moment_list as $moment){
			if(!array_key_exists("text",$moment))continue;
			$moments = Db::table("kt_wework_moments")->where(["wid"=>$wid,"text"=>json_encode($moment["text"]["content"],320)])->find();
			if($moments){
				$is_set = Db::table("kt_wework_moments_info")->where(["moment_id"=>$moment["moment_id"],"pid"=>$moments["id"]])->find();
				if($is_set)continue;
				$data["pid"] = $moments["id"];
				$data["wid"] = $wid;
				$data["moment_id"] = $moment["moment_id"];
				$data["create_time"] = date("Y-m-d H:i:s",$moment["create_time"]);
				$data["staff_id"] = Db::table("kt_wework_staff")->where(["wid"=>$wid,"userid"=>$moment["creator"]])->find()["id"];
				Db::table("kt_wework_moments_info")->insertGetId($data);
			}
		}

		return 1;
	}

}