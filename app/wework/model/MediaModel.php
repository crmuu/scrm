<?php 
// +----------------------------------------------------------------------
// | CRMUU-企微SCRM是专业的企业微信第三方源码系统.
// +----------------------------------------------------------------------
// | [CRMUU] Copyright (c) 2022 http://crmuu.com All rights reserved.
// +----------------------------------------------------------------------

namespace app\wework\model;
use think\facade\Db;
use think\facade\Session;

/**
* 存储类
**/
class MediaModel
{	
	static public $wid;
	/**
	 * 素材库 分类
	 * @param $wid 账户id
	 * @param $data 接受的数据
	 * @return 
	 */
	static public function group($gsType){
		self::inisetImg($gsType);
		$wid = Session::get('wid');
		$res = Db::table('kt_wework_imglib_group')
			   ->alias('g')
			   ->field('g.id,g.title,g.gs_type,count(q.id) as total')
			   ->leftjoin('kt_wework_imglib q','g.id=q.groupid')
			   ->where('g.wid',$wid)
			   ->where('g.gs_type',$gsType)
			   ->group('id')
			   ->select();
		return $res;
	}
	/**
	 * 素材库 添加分类
	 * @param $wid 账户id
	 * @param $data 接受的数据
	 * @return 
	 */
	static public function groupAdd($title,$gsType){
		$wid = Session::get('wid');
		$check = Db::table('kt_wework_imglib_group')->where(['wid'=>$wid,'title'=>$title,'gs_type'=>$gsType])->find();
		if($check) return '已存在';
		Db::table('kt_wework_imglib_group')->insert([
			'wid' => $wid,
			'title' => $title,
			'gs_type' => $gsType,
			'create_time' => date("Y-m-d H:i:s"),
			'update_time' => date("Y-m-d H:i:s"),
		]);
		return 'ok';
	}
	/*
	* 删除图片/视频
	*/
	static public function deletes($id,$url){
		$check = Db::table('kt_wework_imglib')->where(['id'=>$id])->find();
	    if(substr($check["url"],0,strlen($url)) == $url)unlink(str_replace($url,".",$check["url"]));
	    $res = Db::table('kt_wework_imglib')->where(['id'=>$id])->delete();

	    return $res;
	}
	/**
	 * 素材库分组 更新||删除分类名
	 * @param $update 更新
	 * @param $detele 删除
	 * @return 
	 */
	static public function groupUpdate($update=array(),$detele=array()){
		$wid = Session::get('wid');
		if($update){
			foreach ($update as $updataData) {
				if(!$updataData['id']) continue;
				$updataData['update_time'] = date("Y-m-d H:i:s");
				Db::table('kt_wework_imglib_group')->update($updataData);
			}
			
		}
		if($detele){
			Db::table('kt_wework_imglib_group')->where('wid',$wid)->where('id','in',$detele)->delete();
		}
		
		return 'ok';
	}
	/**
	 * 获取 单个图片信息
	 * @param $wid 账户id
	 * @return 
	 */
 	static public function imgInfo($wid,$where){
 		// self::$wid = $wid;
 		// self::inisetImg();
		$res = Db::table('kt_wework_imglib')->field('name,url,create_time,oss_type')->where($where)->find();
		return $res;
	}
	/**
	 * 获取 图片库列表
	 * @param $wid 账户id
	 * @return 
	 */
 	static public function imgList($page,$size,$groupId,$gsType,$name){
 		// self::$wid = $wid;
 		// self::inisetImg();
 		$wid = Session::get('wid');
 		// $groupId = 
		$res = Db::table('kt_wework_imglib')->field('name,url,create_time,oss_type,id')
					->where('wid',$wid)
					->where('groupid',$groupId)
					->where('gs_type',$gsType);
		if($name)$res = $res->whereLike("name","%".$name."%");
		$data['page'] = $page;
		$data['size'] = $size;
		$data['count'] = $res->count();
		$data['items'] = $res->page($page,$size)->select();
		return $data;
	}
	/**
	 * 修改 图片库
	 * @param $id 主键id 
	 * @param $data array
	 * @return 
	 */
 	static public function imgUpdate($id,$data){
		$res = Db::table('kt_wework_imglib')->where('id',$id)->update($data);
		return $res;
	}
	/**
	 * 存储 图片库
	 * @param $wid 账户id
	 * @return 
	 */
 	static public function imgSave($data){
		$res = Db::table('kt_wework_imglib')->insert($data);
		return $res;
	}

	/**
	 * 初始化图片库数据, 添加默认分组
	 * @return 
	 */
	static public function inisetImg($gsType){
 		$wid = Session::get('wid');
		$imglib = Db::table('kt_wework_imglib_group')->where('wid',$wid)->where('title','未分类')->where('gs_type',$gsType)->find();
		if(!$imglib){
		 $res = Db::table('kt_wework_imglib_group')->insertGetId(
			 	[
			 		'wid' => $wid,
			 		'title' => '未分类',
			 		'gs_type' => $gsType,
			 		'create_time'=>date("Y-m-d H:i:s"),
			 		'update_time'=>date("Y-m-d H:i:s"),
			 	]
			);
		}else{
			$res = $imglib['id'];
		}
		return $res;
	}

	/**
	 * 获取 云存储配置
	 * @param $wid 账户id
	 * @return 
	 */
 	static public function storageInfo($wid){
 		self::$wid = $wid;
 		$user = Db::table("kt_base_user")->where(["id"=>$wid])->find();
 		self::inisetStorage($user["agid"]);
		$res = Db::table('kt_base_storage_config')->where('uid',$user["agid"])->find();
		return $res;
	}

	/**
	 * 获取 云存储配置
	 * @param $data 更新数据 array
	 * @return 
	 */
 	static public function storageUpdate($wid,$data){
 		$user = Db::table("kt_base_user")->where(["id"=>$wid])->find();
		$res = Db::table('kt_base_storage_config')->where('uid',$user["agid"])->update($data);
		return $res;
	}

	/**
	 * 初始化云存储数据
	 * @return 
	 */
	static private function inisetStorage($id){
		$config = Db::table('kt_base_storage_config')->where('uid',$id)->find();
		if(!$config){
			Db::table('kt_base_storage_config')->insert(
			 	[
			 		'uid' => $id,
			 		'type' => 1,
			 		'create_time'=>date("Y-m-d H:i:s"),
			 		'update_time'=>date("Y-m-d H:i:s"),
			 	]
			);
		} 
	}
}
