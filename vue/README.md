# 路由

布局内部的页面：路由定义写在 app.js 里，页面写在 pages/app 下
文件名和路由名保持一致，name 拼接上父路由（驼峰命名：例如：weworkQr）

scss 统一变量在 css-variable.scss

通用组件@引入
页面组件相对路径引入

某个文件的所有组件直接就是当前组件的文件名对应的文件夹里

data 文件夹是

1. 原始数据（含 orginal 命名），类似于 ts 的接口定义
2. 其他含 data 的是模拟数据
3. form 是 object，list 是 arr

app-gap-words-left
app-gap-margin-top
app-max-width

app-words-tip-weak

app-flex-left-top

@change="changeFormItem"
@input="changeFormItem"

### 图表 Echarts

使用封装后的 Vue-ECharts 会省去很多事情以及 bug 的产生。文档：https://www.npmjs.com/package/vue-echarts ，中文：https://github.com/ecomfe/vue-echarts/blob/HEAD/README.zh-Hans.md

# Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar)
