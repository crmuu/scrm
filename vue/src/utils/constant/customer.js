export const customerStatus = [
  {
    id:0,
    name:'正常',
  },
  {
    id:1,
    name:'员工删除客户',
  },
  {
    id:2,
    name:'员工被客户删除',
  }
]
export const customerLossingStatus = [
  {
    id:1,
    name:'已流失',
  },
  {
    id:0,
    name:'未流失',
  },
]

export const customerGender = [
  {
    id:0,
    name:'未知',
  },
  {
    id:1,
    name:'男',
  },
  {
    id:2,
    name:'女',
  },
]