/**
 * 节流函数
 */
export function throttle(fn, wait = 150) {
	let last = 0
	return function (...args) {
		const now = Date.now()
		if (now - last > wait) {
			fn.apply(this, args)
			last = now
		}
	}
}

/**
 * 防抖函数
 */
export function debounce(fn, wait = 200, immediate = false) {
	let timer

	return function (...args) {
		if (timer) clearTimeout(timer)
		if (immediate) {
			timer = null
			return fn.apply(this, args)
		}
		timer = setTimeout(() => fn.apply(this, args), wait)
	}
}

/**
 * 深拷贝
 */
export function deepClone(source) {
  if (!source && typeof source !== 'object') {
    throw new Error('error arguments', 'deepClone')
  }
  const targetObj = source.constructor === Array ? [] : {}
  Object.keys(source).forEach(keys => {
    if (source[keys] && typeof source[keys] === 'object') {
      targetObj[keys] = deepClone(source[keys])
    } else {
      targetObj[keys] = source[keys]
    }
  })
  return targetObj
}

/**
 * 是否为空
 */
 export function isEmpty(val) {
	if (val==''){
		return true
	}else if(val==null){
		return true
	}else if (val==undefined){
		return true
	}
  return false
}


export function formatDate(time, patten) {
	if(time==undefined||time==null) return ''
	if (typeof(time) == 'string'){
		time = parseInt(time)
	}
  if (time !== ''&&time !=undefined) {
    var date = new Date(time)
    var o = {
      'M+': date.getMonth() + 1, // 月份
      'd+': date.getDate(), // 日
      'h+': date.getHours(), // 小时
      'm+': date.getMinutes(), // 分
      's+': date.getSeconds(), // 秒
      'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
      'S': date.getMilliseconds() // 毫秒
    }
    var rtn
    if (/(y+)/.test(patten)) {
      rtn = patten.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    }

    for (var k in o) {
      if (new RegExp('(' + k + ')').test(patten)) {
        rtn = rtn.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
      }
    }
    return rtn
  }
  return ''
}
