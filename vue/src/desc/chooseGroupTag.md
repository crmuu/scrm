<span class="app-gap-words-left app-select-right">选择群标签：</span>
  <span v-if="isRefresh">
    <span @click="showLabels" class="app-flex-2 app-choose-staff" v-if="tagData.length>1">
      <span class="app-flex-left">
        
        <span>{{ tagData[0].name }}等{{tagData.length}}个标签</span>
      </span>
      <el-icon>
        <ArrowDown />
      </el-icon>
    </span>
    <span @click="showLabels" class="app-flex-2 app-choose-staff" v-else>
      <span class="app-flex-left" v-for="(item,index) in tagData" :key="index">
        <span>{{ item.name }}</span>
      </span>
      <span v-if="tagData.length==0"></span>
      <el-icon>
        <ArrowDown />
      </el-icon>
    </span>
  </span>



<labels ref="TestRef" :labeldataapi="labeldataapi"></labels>

import labels from '@/components/app/label.vue'
const TestRef = ref(null)


    const showLabels = ()=>{
      // console.log('111')
      // tableselect.value=true
      var data = {
    show:true,
    type:1
      }
      TestRef.value.tableshow(data)
    }


	const tagData = ref([])
	const labeldataapi=(val)=>{
	var arr = []
	val.list.forEach(item=>{
		arr.push(toRaw(item))
	})
	tagData.value = arr
	}



1是群，2是个人