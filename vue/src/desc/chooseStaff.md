<span v-if="isRefresh">
  <span @click="chooseStaff" class="app-flex-2 app-choose-staff" v-if="staffData.length>2">
    <span class="app-flex-left">
      <el-avatar v-if="staffData[0].avatar" class="cho-margin" shape="square" :size="30" :src="staffData[0].avatar" />
      <span>{{ staffData[0].name }}等{{staffData.length}}个账号</span>
    </span>
    <el-icon>
      <ArrowDown />
    </el-icon>
  </span>
  <span @click="chooseStaff" class="app-flex-2 app-choose-staff" v-else>
    <span class="app-flex-left" v-for="(item,index) in staffData" :key="index">
      <el-avatar v-if="item.avatar" class="cho-margin" shape="square" :size="30" :src="item.avatar" />
      <span>{{ item.name }}</span>
    </span>
    <span v-if="staffData.length==0"> </span>
    <el-icon>
      <ArrowDown />
    </el-icon>
  </span>
</span>



<ChooseDialogue :dialogVisible="dialogVisible" @getChooseData="getChooseData($event, val)" ref="chooseDialogue" />

import ChooseDialogue from '@/components/app/chooseDialogue.vue'
const isRefresh = ref(true)
const handleRefresh = () => {
  isRefresh.value = false
  isRefresh.value = true
}
const dialogVisible = ref(false)
const staffData = ref([])
const chooseStaff = () => {
  dialogVisible.value = true
  chooseDialogue.value.show('staff', staffData.value)
}
const getChooseData = (val) => {
  console.log(val, '数据回显')
  staffData.value = val
  handleRefresh()
}
const chooseDialogue = ref(null)

