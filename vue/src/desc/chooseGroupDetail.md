
import { toRaw } from '@vue/reactivity'
import editsop from '@/components/app/groupchat.vue'
const groupData = ref([])
const editData =ref(null)
const handleDeleGroup  = (val) => {
	var arr = groupData.value
  arr.splice(val,1)
  groupData._value = arr
  handleRefresh()
}
const groupchatapi = (val) => {
	var arr = []
	toRaw(val._value).forEach(item=>{
		arr.push(toRaw(item))
	})
    groupData._value = arr
    handleRefresh()
    console.log(groupData.value)
}
const editsopapi = (val) => {
	var data = {
		show: true,
		id: val,
		name: val.name
	}
	editData.value.addapi(data)
}

 <div style="margin-top:-10px;margin-left:120px;margin-bottom:20px">
                            <span v-if="isRefresh" class="app-tag app-gap-tr" v-for="(select, index) in groupData" :key="index">
										<span class="app-flex-left">
											<span class="app-gap-words-left">{{ select.name }}</span>
										</span>
										<el-icon
											@click="handleDeleGroup(index)"
											class="app-tag-close"
											:size="16"
											color="#999"
										>
											<CircleClose />
										</el-icon>
                                    </span>
                                    </div>