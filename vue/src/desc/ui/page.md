

```html
<el-pagination style="margin-top:10px;margin-bottom:10px" :current-page="page" class="app-page" @current-change="getData" background layout="prev, pager, next" :total="total" />
```


```javascript
const tableData = ref([])
const total = ref(0)
const page = ref(1)
const getData = (val)=>{
if (val) page.value = val
postCustomizedList({
   page:page.value,size:10
}).then((res) => {
    total.value = res.data.count
    tableData.value = res.data.item
})
} 
getData()
```


@import '../../../style/app-component.scss';