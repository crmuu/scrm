<el-tag type="primary">
								<div class="app-flex-left">
									<div>
										<MyIcon class="app-icon app-primary-font" name="Star" />
									</div>
									<div class="app-gap-words-left-xs">
										<span class="">
											{{ scope.row.score }}
										</span>
										<div class="app-star-bottom"></div>
									</div>
								</div>
							</el-tag>