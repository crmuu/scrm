<el-date-picker
    class="app-date-picker"
    size="large"
    value-format="YYYY-MM-DD"
		format="YYYY-MM-DD"
    v-model="selfTime"
    type="daterange"
    
    range-separator="~"
    start-placeholder="开始日期"
    end-placeholder="结束日期"
    
  />


const selfTime = ref([])
