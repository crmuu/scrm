import EditLink from '@/components/app/chooseAdjunct.vue'



<EditLink @getDialogueData="getDialogueData($event,val)" ref="editLink"/>


<Draggable
                      v-model="drag"
                      :list="welcomeOtherData"
                      :animation="100"
                      item-key="id"
                      class="list-group"
                      :forceFallback="true"
                      ghost-class="ghost"
                    >
                      <template #item="{ element,index }">
                        <div class="app-flex-2">
                          <div class="app-flex-left">
                            <span class="app-close-circle">
                              -
                            </span>
                            <span>
                              【<span v-if="element['type'] == '1'">图片</span>
                              <span v-else-if="element['type'] == '2'">链接</span>
                               <span v-else-if="element['type'] == '3'">小程序</span>】:
                            </span>
                            <span class="app-gap-words-left-s">{{ element['title'] }}</span>
                          </div>
                          <MyIcon @click="showDialog(element['type'],index)" class="app-pointer app-icon " name="Edit" />
                        </div>
                      </template>
                    </Draggable>