export const userSopOrignalForm = {
  "name": "",
  "staffid": [],
  "scene_type": 1,
  "filter_status": 1,
  "filter_tag": [],
  "start_type": 1,
  "start_date": "",
  "rule_data": [],
}
export const userSopOrule_dataForm = {
  "trigger_form": "1",
  send_type:1,
  send_starttime_type:'1',
  "send_starttime": "", //send_starttime_type 为2, 开始时间
  "send_endtime_type": 1,   //sendtype 为2 结束时间类型: 1:永久有效 2:定时结束
  "send_endtime_date": "",  //send_endtime_type为2, 结束时间
  "send_frequency": '1',  //推送频率
  "send_frequency_type": "minute", //推送频率单位,  minute 分钟 hour小时 day天
  "send_content": []    //推送内容
}
export const userSopOrule_dataFormsend_timing_data = {
  type:'day',
  number:'1',
  time:[],
}

