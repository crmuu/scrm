/**
 * 初始form
 */
// 图片 小程序 链接的弹窗数据
export const qrOrignalForm = {
	id: 1,
	name: 'test',
	staffid: [],
	staff_standby: [],
	// tag: [],
	tag_status: 0,
	groupid: 1,
	skipverify_type: 1,
	staff_type: 0,
	staffline_status: 0,
	customerbz_status: 0,
	customerdes_status: 0,
	customerdes: '',
	customerbz: '',
	customerbzqh_status: 2,
	welcome_type: 1,
	welcomeContent: '',
	welcomeOtherData: [],
	// 员工分时段添加
	staff_data: [
		{
			selectStaffData: [],
			selectstaffids: [],
			time: [],
			week: []
		}
	],
	// 员工上限
	staffupData: [{ staffid: '', name: '', avatar: '', sx: 0 }],
	dayparting_status: 1
}

/**
 * 初始qrImgOrignalForm
 */
export const qrImgOrignalForm = {
	type: '1',
	title: '',
	imgurl: 'shanchu.png'
}

/**
 * 初始qrImgOrignalForm
 */
export const qrLinkOrignalForm = {
	type: '2',

	title: '',
	desc: '',
	picurl: '',
	setStatus: '1',
	linkSet: '1',

	behavior: '1',
	dynamic: '1',
	tag: '',
	score: '1',
	saveRadar: '1'
}

export const qrMiniprogramOrignalForm = {
	type: '3',
	title: '',
	appId: '',
	appUrl: '',
	img: ''
}
