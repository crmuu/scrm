export const memberChartTab = [
	{
    id:1,
		name: 'total',
		title: '聊天总数'
	},
	{
    id:2,
		name: 'total1',
		title: '发送消息数'
	},
	{
    id:7,
		name: 'total2',
		title: '已回复聊天占比'
	},
	{
    id:8,
		name: 'total3',
		title: '平均首次回复时长'
	}
]

export const materialType = [
  {
    id:0,
    name:''
  },
  {
    id:1,
    name:'图片'
  },
  {
    id:2,
    name:'视频'
  },
  {
    id:3,
    name:'语音'
  },
  {
    id:4,
    name:'混剪'
  },
  {
    id:5,
    name:'文本'
  }
]