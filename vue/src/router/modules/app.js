import { RouterView } from 'vue-router'
//二级路由-------------
// const appListRoutes = [
// 	{
// 		path: 'table',
// 		name: 'appListTable',
// 		meta: { title: 'Demo Table', hiddenInSideMenu: false },
// 		component: () => import('@/pages/wework/list/table.vue')
// 	},
// 	{
// 		path: 'tree',
// 		name: 'appListTree',
// 		meta: { title: 'Demo Tree' },
// 		component: () => import('@/pages/wework/list/tree.vue')
// 	}
// ]
// const dataRoutes = [
// 	{
// 		path: 'dataCustomer',
// 		name: 'dataCustomer',
// 		meta: { title: '客户统计' },
// 		component: () => import('@/pages/wework/data/customer.vue')
// 	},
// 	{
// 		path: 'group',
// 		name: 'dataGroup',
// 		meta: { title: '客户群统计' },
// 		component: () => import('@/pages/wework/data/group.vue')
// 	},
// 	{
// 		path: 'member',
// 		name: 'dataMember',
// 		meta: { title: '成员统计' },
// 		component: () => import('@/pages/wework/data/member.vue')
// 	},
// 	{
// 		path: 'qr',
// 		name: 'dataQr',
// 		meta: { title: '一客一码统计' },
// 		component: () => import('@/pages/wework/data/qr.vue')
// 	}
// ]
const weworkRoutes = [
	{
		path: 'qr',
		name: 'weworkQrFirst',
		meta: { title: '渠道活码' },
		component: () => import('@/pages/wework/wework/qr.vue')
	},
	{
		path: 'autogroup',
		name: 'autogroup',
		meta: { title: '自动拉群' },
		component: () => import('@/pages/wework/wework/autogroup.vue')
	},
	{
		path: 'addautogroup',
		name: 'addautogroup',
		meta: { title: '新增自动拉群', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/wework/addautogroup.vue')
	},
	{
		path: 'batchFriend',
		name: 'batchFriend',
		meta: { title: '批量加好友' },
		component: () => import('@/pages/wework/wework/batch.vue')
	},
	{
		path: 'record',
		name: 'record',
		meta: { title: '导入记录', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/wework/record.vue')
	},
	{
		path: 'recorddetails',
		name: 'recorddetails',
		meta: { title: '详情', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/wework/details.vue')
	},
	{
		path: 'promote',
		name: 'promote',
		meta: { title: '自助推广码',  },
		component: () => import('@/pages/wework/groupManagement/promote/index.vue')
	},
	{
		path: 'craeteCode',
		name: 'craeteCode',
		meta: { title: '创建推广码', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/groupManagement/promote/craeteCode.vue')
	},
	{
		path: 'seat',
		name: 'seat',
		meta: { title: '席位管理',  },
		component: () => import('@/pages/wework/seat/index.vue')
	},
	{
		path: 'building',
		name: 'building',
		meta: { title: '标签建群',  },
		component: () => import('@/pages/wework/seat/building.vue')
	},
	{
		path: 'addbuilding',
		name: 'addbuilding',
		meta: { title: '添加标签建群', hiddenInSideMenu: true},
		component: () => import('@/pages/wework/seat/addbuilding.vue')
	},
	// {
	// 	path: 'taggroupInvitate',
	// 	name: 'taggroupInvitate',
	// 	meta: { title: '标签建群', icon: '', hiddenInSideMenu: false },
	// 	component: () => import('@/pages/wework/groupManagement/taggroupInvitate/index.vue')
	// },
	// {
	// 	path: 'createTagGruop',
	// 	name: 'createTagGruop',
	// 	meta: { title: '创建标签建群', icon: '', hiddenInSideMenu: true },
	// 	component: () => import('@/pages/wework/groupManagement/taggroupInvitate/createTagGruop.vue')
	// },
	{
		path: 'invitateDetails',
		name: 'invitateDetails',
		meta: { title: '标签详情', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/groupManagement/taggroupInvitate/invitateDetails.vue')
	},
	{
		path: 'InteractiveRadar',
		name: 'InteractiveRadar',
		meta: { title: '互动雷达', icon: '' },
		component: () => import('@/pages/wework/marketingplan/InteractiveRadar.vue')
	}
]
const customerRoutes = [
	{
		path: 'customerManage',
		name: 'customerManage',
		meta: { title: '客户管理' },
		component: () => import('@/pages/wework/customer/index.vue')
	}
]
const materialRoutes = [
	{
		path: '',
		name: 'material',
		meta: { title: '素材中心' },
		component: () => import('@/pages/wework/material/index.vue')
	}
]

const PersonListRoutes = [
	{
		path: 'memberManage',
		name: 'appListmemberManage',
		meta: { title: '成员管理 ', icon: '' },
		component: () => import('@/pages/wework/persionmessage/memberManage.vue')
	}
]
const cloudStorageRoutes = [
	{
		path: 'cloudStorage',
		name: 'appCloudStorage',
		meta: { title: '云存储 ', icon: '' },
		component: () => import('@/pages/wework/cloudstorage/cloudStorage.vue')
	}
]
const basicSettingsRoutes = [
	{
		path: 'basicSettings',
		name: 'appBasicSettings',
		meta: { title: '基础设置 ', icon: '' },
		component: () => import('@/pages/wework/basicsettings/basicSettings.vue')
	}
]
const customerLabelRoutes = [
	{
		path: 'customerLabel',
		name: 'appCustomerLabel',
		meta: { title: '客户标签 ', icon: '' },
		component: () => import('@/pages/wework/customermanagement/customerLabel.vue')
	},
	// 添加客户标签组页面
	{
		path: 'customerTagGroup',
		name: 'customerTagGroup',
		meta: { title: '添加客户标签组 ', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/customermanagement/customerTagGroup.vue')
	},
	//修改客户标签组
	{
		path: 'ModifyCustomerLabelGroup',
		name: 'ModifyCustomerLabelGroup',
		meta: { title: '修改客户标签组 ', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/customermanagement/ModifyCustomerLabelGroup.vue')
	},
	{
		path: 'addLabelGroup',
		name: 'addLabelGroup',
		meta: { title: '添加群标签组 ', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/customermanagement/addLabelGroup.vue')
	},
	{
		path: 'customInformation',
		name: 'customInformation',
		meta: { title: '自定义信息 ', icon: '' },
		component: () => import('@/pages/wework/customermanagement/customInformation.vue')
	},
	{
		path: 'customerManage',
		name: 'customerManage',
		meta: { title: '客户管理' },
		component: () => import('@/pages/wework/customer/index.vue')
	},
	{
		path: 'lossReminder',
		name: 'customermanagementLossReminder',
		meta: { title: '流失提醒 ', icon: '' },
		component: () => import('@/pages/wework/customermanagement/lossReminder.vue')
	},
	{
		path: 'deleteReminder',
		name: 'customermanagementDeleteReminder',
		meta: { title: '删人提醒', icon: '' },
		component: () => import('@/pages/wework/customermanagement/deleteReminder.vue')
	},

	// {
	// 	path: 'customerGroupLabel',
	// 	name: 'customermanagementCustomerGroupLabel',
	// 	meta: { title: '客户群标签', icon: '' },
	// 	component: () => import('@/pages/wework/customermanagement/customerGroupLabel.vue')
	// },
	{
		path: 'addGroupLabelGroup',
		name: 'customermanagementAddGroupLabelGroup',
		meta: { title: '修改群标签组', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/customermanagement/addGroupLabelGroup.vue')
	}
	// {
	// 	path: 'massSending',
	// 	name: 'customermanagementMassSending',
	// 	meta: { title: '客户群发', icon: '' },
	// 	component: () => import('@/pages/wework/customermanagement/massSending.vue')
	// },
	// {
	// 	path: 'newGroupSending',
	// 	name: 'customermanagementnewGroupSending',
	// 	meta: { title: '新建群发', icon: '', hiddenInSideMenu: true },
	// 	component: () => import('@/pages/wework/customermanagement/newGroupSending.vue')
	// }
]

const customerOperationsRoutes = [
	{
		path: 'customerOperations',
		name: 'appCustomerOperations',
		meta: { title: '好友欢迎语', icon: '' },
		component: () => import('@/pages/wework/customeroperations/customerOperations.vue')
	},
	{
		path: 'customerBaseSend',
		name: 'appCustomerBaseSend',
		meta: { title: '客户群群发', icon: '' },
		component: () => import('@/pages/wework/customeroperations/customerBaseSend.vue')
	},
	{
		path: 'welcomeToGroup',
		name: 'appwelcomeToGroup',
		meta: { title: '入群欢迎语', icon: '' },
		component: () => import('@/pages/wework/customeroperations/welcomeToGroup.vue')
	},
	// {
	// 	path: 'staffGroupSendRecord',
	// 	name: 'StaffGroupSendRecord',
	// 	meta: { title: '员工群发记录', icon: '' },
	// 	component: () => import('@/pages/wework/customeroperations/staff.vue')
	// }
]
const dataSubscriptionRoutes = [
	{
		path: 'dataSubscription',
		name: 'dataSubscription',
		meta: { title: '数据订阅', icon: '' },
		component: () => import('@/pages/wework/marketingplan/dataSubscription.vue')
	},
	{
		path: 'InteractiveRadar',
		name: 'InteractiveRadar',
		meta: { title: '互动雷达', icon: '' },
		component: () => import('@/pages/wework/marketingplan/InteractiveRadar.vue')
	},
	// {
	// 	path: 'groupSop',
	// 	name: 'groupSop',
	// 	meta: { title: '群 SOP', icon: '' },
	// 	component: () => import('@/pages/wework/marketingplan/groupSop.vue')
	// },
	// {
	// 	path: 'addSop',
	// 	name: 'addSop',
	// 	meta: { title: '新建群SOP', icon: '', hiddenInSideMenu: true },
	// 	component: () => import('@/pages/wework/marketingplan/addSop.vue')
	// },
	{
		path: 'details',
		name: 'details',
		meta: { title: '任务详情', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/marketingplan/details.vue')
	},
	{
		path: 'group',
		name: 'group',
		meta: { title: '群详情', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/marketingplan/group.vue')
	},
	{
		path: 'personalSop',
		name: 'personalSop',
		meta: { title: '个人SOP', icon: '' },
		component: () => import('@/pages/wework/marketingplan/personalSop.vue')
	},
	{
		path: 'addpersonal',
		name: 'addpersonal',
		meta: { title: '新建个人SOP', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/marketingplan/addpersonal.vue')
	},
	{
		path: 'executiondata',
		name: 'executiondata',
		meta: { title: '执行数据', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/marketingplan/executiondata.vue')
	}
]
//会话存档
const dialogueRoutes = [
	{
		path: 'chat',
		name: 'dialogueChat',
		meta: { title: '会话存档' },
		component: () => import('@/pages/wework/dialogue/index.vue')
	}
]
const messageRoutes = [
	{
		path: 'chat',
		name: 'messageChat',
		meta: { title: '私信管理' },
		component: () => import('@/pages/wework/message/index.vue')
	}
]
//客户群管理
const groupManagementRoutes = [
	// {
	// 	path: 'customerList',
	// 	name: 'CustomerList',
	// 	meta: { title: '客户群列表', icon: '' },
	// 	component: () => import('@/pages/wework/groupManagement/customerList.vue')
	// },
	// {
	// 	path: 'workMoment',
	// 	name: 'WorkMoment',
	// 	meta: { title: '企微朋友圈', icon: '' },
	// 	component: () => import('@/pages/wework/groupManagement/index.vue')
	// },
	// {
	// 	path: 'create',
	// 	name: 'create',
	// 	meta: { title: '发布朋友圈', icon: '', hiddenInSideMenu: true },
	// 	component: () => import('@/pages/wework/groupManagement/component/create.vue')
	// },
	// {
	// 	path: 'friendsConfig',
	// 	name: 'friendsConfig',
	// 	meta: { title: '朋友圈', icon: '' },
	// 	component: () => import('@/pages/wework/groupManagement/workMoment/friendsConfig.vue')
	// },
	// {
	// 	path: 'friendsDetail',
	// 	name: 'friendsDetail',
	// 	meta: { title: '朋友圈详情', icon: '', hiddenInSideMenu: true },
	// 	component: () => import('@/pages/wework/groupManagement/workMoment/friendsDetail.vue')
	// },
	// {
	// 	path: 'setMomentUrl',
	// 	name: 'setMomentUrl',
	// 	meta: { title: '朋友圈配置', icon: '', hiddenInSideMenu: true },
	// 	component: () => import('@/pages/wework/groupManagement/setMomentUrl.vue')
	// },
	{
		path: 'corpReply',
		name: 'corpReply',
		meta: { title: '话术库', icon: '', hiddenInSideMenu: false },
		component: () => import('@/pages/wework/groupManagement/corpReply.vue')
	},
	{
		path: 'material',
		name: 'material',
		meta: { title: '素材库' },
		component: () => import('@/pages/wework/material/index.vue')
	}
	// {
	// 	path: 'taggroupInvitate',
	// 	name: 'taggroupInvitate',
	// 	meta: { title: '标签建群', icon: '', hiddenInSideMenu: false },
	// 	component: () => import('@/pages/wework/groupManagement/taggroupInvitate/index.vue')
	// },
	// {
	// 	path: 'createTagGruop',
	// 	name: 'createTagGruop',
	// 	meta: { title: '标签建群', icon: '', hiddenInSideMenu: true },
	// 	component: () => import('@/pages/wework/groupManagement/taggroupInvitate/createTagGruop.vue')
	// },
	// {
	// 	path: 'invitateDetails',
	// 	name: 'invitateDetails',
	// 	meta: { title: '标签详情', icon: '', hiddenInSideMenu: true },
	// 	component: () => import('@/pages/wework/groupManagement/taggroupInvitate/invitateDetails.vue')
	// }
]
const customerLabelRoutesyi = [
	{
		path: 'massSending',
		name: 'customermanagementMassSending',
		meta: { title: '客户群发', icon: '' },
		component: () => import('@/pages/wework/customermanagement/massSending.vue')
	},
	{
		path: 'newGroupSending',
		name: 'customermanagementnewGroupSending',
		meta: { title: '新建群发', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/customermanagement/newGroupSending.vue')
	},
	{
		path: 'customerOperations',
		name: 'appCustomerOperations',
		meta: { title: '好友欢迎语', icon: '' },
		component: () => import('@/pages/wework/customeroperations/customerOperations.vue')
	},
	// {
	// 	path: 'staffGroupSendRecord',
	// 	name: 'StaffGroupSendRecord',
	// 	meta: { title: '员工群发记录', icon: '' },
	// 	component: () => import('@/pages/wework/customeroperations/staff.vue')
	// },
	{
		path: 'customerBaseSend',
		name: 'appCustomerBaseSend',
		meta: { title: '客户群群发', icon: '' },
		component: () => import('@/pages/wework/customeroperations/customerBaseSend.vue')
	},
	{
		path: 'welcomeToGroup',
		name: 'appwelcomeToGroup',
		meta: { title: '入群欢迎语', icon: '' },
		component: () => import('@/pages/wework/customeroperations/welcomeToGroup.vue')
	},
	{
		path: 'workMoment',
		name: 'WorkMoment',
		meta: { title: '企微朋友圈', icon: '' },
		component: () => import('@/pages/wework/groupManagement/index.vue')
	},
	{
		path: 'create',
		name: 'create',
		meta: { title: '发布朋友圈', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/groupManagement/component/create.vue')
	},
	// {
	// 	path: 'friendsConfig',
	// 	name: 'friendsConfig',
	// 	meta: { title: '朋友圈', icon: '' },
	// 	component: () => import('@/pages/wework/groupManagement/workMoment/friendsConfig.vue')
	// },
	{
		path: 'friendsDetail',
		name: 'friendsDetail',
		meta: { title: '朋友圈详情', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/groupManagement/workMoment/friendsDetail.vue')
	},
	{
		path: 'setMomentUrl',
		name: 'setMomentUrl',
		meta: { title: '朋友圈配置', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/groupManagement/setMomentUrl.vue')
	}
]
const groupManagementyi = [
	{
		path: 'customerList',
		name: 'CustomerList',
		meta: { title: '客户群列表', icon: '' },
		component: () => import('@/pages/wework/groupManagement/customerList.vue')
	},
	{
		path: 'customerGroupLabel',
		name: 'customermanagementCustomerGroupLabel',
		meta: { title: '客户群标签', icon: '' },
		component: () => import('@/pages/wework/customermanagement/customerGroupLabel.vue')
	},
	{
		path: 'addLabelGroup',
		name: 'addLabelGroup',
		meta: { title: '添加群标签组 ', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/customermanagement/addLabelGroup.vue')
	},
	{
		path: 'addGroupLabelGroup',
		name: 'customermanagementAddGroupLabelGroup',
		meta: { title: '修改群标签组', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/customermanagement/addGroupLabelGroup.vue')
	}
]
const marketingyi = [
	{
		path: 'dataSubscription',
		name: 'dataSubscription',
		meta: { title: '数据订阅', icon: '' },
		component: () => import('@/pages/wework/marketingplan/dataSubscription.vue')
	},
	{
		path: 'groupSop',
		name: 'groupSop',
		meta: { title: '群 SOP', icon: '' },
		component: () => import('@/pages/wework/marketingplan/groupSop.vue')
	},
	{
		path: 'addSop',
		name: 'addSop',
		meta: { title: '新建群SOP', icon: '',hiddenInSideMenu: true },
		component: () => import('@/pages/wework/marketingplan/addSop.vue')
	},
	{
		path: 'details',
		name: 'details',
		meta: { title: '任务详情', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/marketingplan/details.vue')
	},
	{
		path: 'group',
		name: 'group',
		meta: { title: '群详情', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/marketingplan/group.vue')
	},
	{
		path: 'addpersonal',
		name: 'addpersonal',
		meta: { title: '新建个人SOP', icon: '',hiddenInSideMenu: true },
		component: () => import('@/pages/wework/marketingplan/addpersonal.vue')
	},
	{
		path: 'personalSop',
		name: 'personalSop',
		meta: { title: '个人SOP', icon: '' },
		component: () => import('@/pages/wework/marketingplan/personalSop.vue')
	},
	{
		path: 'executiondata',
		name: 'executiondata',
		meta: { title: '执行数据', icon: '', hiddenInSideMenu: true },
		component: () => import('@/pages/wework/marketingplan/executiondata.vue')
	},
	// {
	// 	path: 'chat',
	// 	name: 'dialogueChat',
	// 	meta: { title: '会话存档' },
	// 	component: () => import('@/pages/wework/dialogue/index.vue')
	// }
]
const dataStatisticsyi = [
	{
		path: 'dataCustomer',
		name: 'dataCustomer',
		meta: { title: '客户统计' },
		component: () => import('@/pages/wework/data/customer.vue')
	},
	{
		path: 'group',
		name: 'dataGroup',
		meta: { title: '客户群统计' },
		component: () => import('@/pages/wework/data/group.vue')
	},
	{
		path: 'member',
		name: 'dataMember',
		meta: { title: '成员统计' },
		component: () => import('@/pages/wework/data/member.vue')
	},
	{
		path: 'qr',
		name: 'dataQr',
		meta: { title: '一客一码统计' },
		component: () => import('@/pages/wework/data/qr.vue')
	}
]
const configureyi = [
	{
		path: 'memberManage',
		name: 'appListmemberManage',
		meta: { title: '成员管理 ', icon: '' },
		component: () => import('@/pages/wework/persionmessage/memberManage.vue')
	},
	{
		path: 'basicSettings',
		name: 'appBasicSettings',
		meta: { title: '配置管理 ', icon: '' },
		component: () => import('@/pages/wework/basicsettings/basicSettings.vue')
	}
]
// 一级路由-----------------
export const configure = [
	{
		path: 'groupManagement',
		meta: { title: '配置中心', icon: 'Setting', alwaysShow: true, hidePageHeader: true },
		redirect: '/wework/groupManagement/memberManage',
		component: RouterView,
		children: configureyi
	}
]
export const dataStatistics = [
	{
		path: 'groupManagement',
		meta: { title: '数据统计', icon: 'DataAnalysis', alwaysShow: true, hidePageHeader: true },
		redirect: '/wework/groupManagement/dataCustomer',
		component: RouterView,
		children: dataStatisticsyi
	}
]
export const weworkRoutesyi = [
	{
		path: 'groupManagement',
		meta: { title: '内容中心', icon: 'Memo', alwaysShow: true, hidePageHeader: true },
		redirect: '/wework/groupManagement/customerList',
		component: RouterView,
		children: groupManagementRoutes
	}
]
export const groupManagement = [
	// {
	// 	path: 'customerList',
	// 	name: 'CustomerList',
	// 	meta: { title: '客户群列表', icon: '' },
	// 	component: () => import('@/pages/wework/groupManagement/customerList.vue')
	// },
	{
		path: 'groupManagement',
		meta: { title: '客户群管理', icon: 'Avatar', alwaysShow: true, hidePageHeader: true },
		redirect: '/wework/groupManagement/customerList',
		component: RouterView,
		children: groupManagementyi
	}
]
export const marketing = [
	// {
	// 	path: 'customerList',
	// 	name: 'CustomerList',
	// 	meta: { title: '客户群列表', icon: '' },
	// 	component: () => import('@/pages/wework/groupManagement/customerList.vue')
	// },
	{
		path: 'groupManagement',
		meta: { title: '营销计划', icon: 'Calendar', alwaysShow: true, hidePageHeader: true },
		redirect: '/wework/groupManagement/dataSubscription',
		component: RouterView,
		children: marketingyi
	}
]
export const customerRoutesyi = [
	{
		path: 'customermanagement',
		meta: { title: '客户运营', icon: 'UserFilled', alwaysShow: true },
		redirect: '/customer/customermanagement/customerLabel',
		component: RouterView,
		children: customerLabelRoutesyi
	}
]
export const customerManagement = [
	{
		path: 'customermanagement',
		meta: { title: '客户管理', icon: 'TrophyBase', alwaysShow: true },
		redirect: '/customer/customermanagement/customerLabel',
		component: RouterView,
		children: customerLabelRoutes
	}
]
export const appRoutes = [
	{
		path: 'wework',
		meta: { title: '营销获客', icon: 'DataLine', alwaysShow: true, hidePageHeader: true },
		redirect: '/wework/qr',
		component: RouterView,
		children: weworkRoutes
	}
	// {
	// 	path: 'message',
	// 	meta: { title: '私信管理', icon: 'AntDesignBarsOutlined', alwaysShow: true, hidePageHeader: true },
	// 	redirect: '/wework/message',
	// 	component: RouterView,
	// 	children: messageRoutes
	// },
	// {
	// 	path: 'dialogue',
	// 	meta: { title: '会话存档', icon: 'AntDesignBarsOutlined', alwaysShow: true, hidePageHeader: true },
	// 	redirect: '/wework/dialogue',
	// 	component: RouterView,
	// 	children: dialogueRoutes
	// },
	// {
	// 	path: 'data',
	// 	meta: { title: '数据统计', icon: 'AntDesignBarsOutlined', alwaysShow: true, hidePageHeader: true },
	// 	redirect: '/wework/data',
	// 	component: RouterView,
	// 	children: dataRoutes
	// },

	// {
	// 	path: 'customer',
	// 	meta: { title: '客户管理', icon: 'AntDesignBarsOutlined', alwaysShow: true, hidePageHeader: true },
	// 	redirect: '/wework/customer',
	// 	component: RouterView,
	// 	children: customerRoutes
	// },
	// {
	// 	path: 'material',
	// 	meta: { title: '素材中心', icon: 'AntDesignBarsOutlined', alwaysShow: true, hidePageHeader: true },
	// 	redirect: '/wework/material',
	// 	component: RouterView,
	// 	children: materialRoutes
	// },

	// {
	// 	path: 'list',
	// 	meta: { title: 'Demo List', icon: 'AntDesignBarsOutlined', alwaysShow: true },
	// 	redirect: '/wework/list/table',
	// 	component: RouterView,
	// 	children: appListRoutes
	// },
	// {
	// 	path: 'persionessage',
	// 	meta: { title: '成员管理', icon: 'AntDesignBarsOutlined', alwaysShow: true },
	// 	redirect: '/wework/persionmessage/memberMessage',
	// 	component: RouterView,
	// 	children: PersonListRoutes
	// },
	// {
	// 	path: 'cloudstorage',
	// 	meta: { title: '云存储', icon: 'AntDesignBarsOutlined', alwaysShow: true },
	// 	redirect: '/wework/cloudstorage/cloudStorage',
	// 	component: RouterView,
	// 	children: cloudStorageRoutes
	// },
	// {
	// 	path: 'basicsettings',
	// 	meta: { title: '基础设置', icon: 'AntDesignBarsOutlined', alwaysShow: true },
	// 	redirect: '/wework/basicsettings/basicSettings',
	// 	component: RouterView,
	// 	children: basicSettingsRoutes
	// },
	// {
	// 	path: 'customermanagement',
	// 	meta: { title: '客户联系', icon: 'AntDesignBarsOutlined', alwaysShow: true },
	// 	redirect: '/wework/customermanagement/customerLabel',
	// 	component: RouterView,
	// 	children: customerLabelRoutes
	// },
	// {
	// 	path: 'customeroperations',
	// 	meta: { title: '客户运营', icon: 'AntDesignBarsOutlined', alwaysShow: true },
	// 	redirect: '/wework/customeroperations/customerOperations',
	// 	component: RouterView,
	// 	children: customerOperationsRoutes
	// },
	// {
	// 	path: 'marketingplan',
	// 	meta: { title: '营销计划', icon: 'AntDesignBarsOutlined', alwaysShow: true },
	// 	redirect: '/wework/marketingplan/InteractiveRadar',
	// 	component: RouterView,
	// 	children: dataSubscriptionRoutes
	// },
	// {
	// 	path: 'groupManagement',
	// 	meta: { title: '客户群管理', icon: 'AntDesignBarsOutlined', alwaysShow: true, hidePageHeader: true },
	// 	redirect: '/wework/groupManagement/customerList',
	// 	component: RouterView,
	// 	children: groupManagementRoutes
	// }
]
