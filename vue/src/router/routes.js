import { RouterView } from 'vue-router'
import MyLayout from '@/components/my-layout/index.vue'
import { authRoutes, appRoutes,weworkRoutesyi,customerRoutesyi,customerManagement,groupManagement,marketing,dataStatistics,configure } from './modules'

// meta 中可选字段
// declare module 'vue-router' {
// 	// 扩展 meta 字段
// 	interface RouteMeta {
// 		// 菜单栏、面包屑等需要
// 		title?: string
// 		// 菜单栏加载时菜单图标，详见 @/components/my-icon 支持的 name 值
// 		icon?: string
// 		// 权限
// 		permissions?: string[] | string
// 		// 是否总是显示。如果不设置，默认当其 children.length=1 时会被这个 child 替代
// 		alwaysShow?: boolean
// 		// 设置为 true 则不在菜单栏中显示
// 		hiddenInSideMenu?: boolean
// 		// 设置为 true 则不在面包屑中显示
// 		hiddenInBreadcrumb?: boolean
// 		// 设置为 true 则不显示页面标题栏
// 		hidePageHeader?: boolean
// 		// 设置为 true 则在页面标题栏显示返回
// 		pageHeaderBack?: boolean
// 		// todo: if set true, the tag will affix in the tags-view
// 		affix?: boolean
// 		// todo: 设置为 true 则用 <keep-alive /> 缓存该路由的 name
// 		cache?: boolean
// 		// 手动设置左侧菜单栏要选中的菜单路由path，默认是自身
// 		activeMenu?: string
// 	}
// }

// <keep-alive /> 时 name 必须
export const routes = [
	{
		path: '/',
		redirect: '/index',
		meta: { hiddenInSideMenu: true }
	},
	{
		path: '/index',
		meta: { title: '首页', icon: 'House' },
		component: MyLayout,
		children: [
			{
				path: '',
				name: 'dashboard',
				meta: { title: '首页', hiddenInSideMenu: true, hidePageHeader: true },
				component: () => import('@/pages/wework/dashboard/index.vue')
			}
		]
	},
	{
		path: '/auth',
		redirect: '/auth/login',
		meta: { hiddenInSideMenu: true },
		component: () => import('@/pages/auth/components/auth-container.vue'),
		children: authRoutes
	},
	{
		path: '/app',
		redirect: '/app/wework/qr',
		meta: { title: '营销获客', icon: 'DataLine' },
		component: MyLayout,
		children: appRoutes
	},
	{
		path: '/wework',
		redirect: '/wework/groupManagement/corpReply',
		meta: { title: '内容中心', icon: 'Memo' },
		component: MyLayout,
		children: weworkRoutesyi
	},
	{
		path: '/customer',
		redirect: '/customer/customermanagement/massSending',
		meta: { title: '客户运营', icon: 'UserFilled' },
		component: MyLayout,
		children: customerRoutesyi
	},
	{
		path: '/customerManagement',
		redirect: '/customerManagement/customermanagement/customerLabel',
		meta: { title: '客户管理', icon: 'TrophyBase' },
		component: MyLayout,
		children: customerManagement
	},
	{
		path: '/groupManagement',
		redirect: '/groupManagement/groupManagement/customerList',
		meta: { title: '客户群管理', icon: 'Avatar' },
		component: MyLayout,
		children: groupManagement
	},
	{
		path: '/marketing',
		redirect: '/marketing/groupManagement/dataSubscription',
		meta: { title: '营销计划', icon: 'Calendar' },
		component: MyLayout,
		children: marketing
	},
	{
		path: '/dataStatistics',
		redirect: '/dataStatistics/groupManagement/dataCustomer',
		meta: { title: '数据统计', icon: 'DataAnalysis' },
		component: MyLayout,
		children: dataStatistics
	},
	{
		path: '/configure',
		redirect: '/configure/groupManagement/memberManage',
		meta: { title: '配置中心', icon: 'Setting' },
		component: MyLayout,
		children: configure
	},
	{
		path: '/setMomentUrl',
		meta: { hiddenInSideMenu: true },
		component: () => import('@/pages/wework/groupManagement/setMomentUrl.vue')
	},

	{
		path: '/:pathMatch(.*)*',
		name: '404',
		meta: { hiddenInSideMenu: true },
		component: () => import('@/pages/results/404.vue')
	}
]
