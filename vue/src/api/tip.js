import { request } from '@/utils'
// 渠道活码
// 获取渠道活码分组
export function postLostList(data) {
	return request.post('/wework/user/lost/list', data)
}
export function postLostStatus(data) {
	return request.post('/wework/user/lost/status', data)
}
export function postLostSet(data) {
	return request.post('/wework/user/lost/set', data)
}