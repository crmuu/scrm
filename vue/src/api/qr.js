import { request } from '@/utils'
// 渠道活码
// 获取渠道活码分组
export function reqQrGroupList(data) {
	return request.get('/wework/user/qr/group', data)
}
// 修改|添加 渠道活码分组
export function reqQrGroupUpdate(data) {
	return request.post('/wework/user/qr/groupsave', data)
}
// 集合数据统计
export function reqQrStatistics(data) {
	return request.post('/wework/user/qr/statistics', data)
}
// 数据统计人数统计
export function reqQrStatisticssize(data) {
	return request.post('/wework/user/qr/statisticssize', data)
}
export function reqQrMap(data) {
	return request.post('/wework/user/qr/map', data)
}

export function reqQrTable(data) {
	return request.post('/wework/user/qr/table', data)
}



export function reqQrGroupdel(data) {
	return request.post('/wework/user/qr/groupdel', data)
}

export function reqQrList(data) {
	return request.post('/wework/user/qr/list', data)
}
// 添加渠道活码
export function reqQrUpdate(data) {
	return request.post('/wework/user/qr/add', data)
}

export function reqQrGetone(data) {
	return request.get('/wework/user/qr/detail?id=' + data)
}
// 自动拉群
// 自动拉群列表
export function reqAutoList(data) {
	return request.post('/wework/user/autogroup/list', data)
}
// 列表详情
export function autogroupDetails(data) {
	return request.post('/wework/user/autogroup/details', data)
}
// 添加/修改分组
export function reqAutoaddGrouping(data) {
	return request.post('/wework/user/autogroup/addgrouping', data)
}
// 批量修改获取所选群聊数据
export function reqAutoaddGetbatch(data) {
	return request.post('/wework/user/autogroup/getbatch', data)
}
// 批量分组
export function reqAutoaddGroupings(data) {
	return request.post('/wework/user/autogroup/groupings', data)
}
// 刪除分组
export function reqAutoDelgrouping(data) {
	return request.post('/wework/user/autogroup/delgrouping', data)
}
// 删除自动拉群
export function reqAutoDel(data) {
	return request.post('/wework/user/autogroup/del', data)
}
// 添加自动拉群
export function requestAutoAdd(data) {
	return request.post('/wework/user/autogroup/add', data)
}
// 修改自动拉群
export function reqAutoUpd(data) {
	return request.post('/wework/user/autogroup/upd', data)
}
// 编辑拉取数据
export function reqAutoGetInfo(data) {
	return request.post('/wework/user/autogroup/getinfo', data)
}
// 群sop
// 群sop列表
export function reqSopGroupList(data) {
	return request.post('/wework/user/sop/grouplist', data)
}
// 群sop开关
export function reqSopgroupopen(data) {
	return request.post('/wework/user/sop/groupopen', data)
}
// 群sop删除
export function reqSopGroupLete(data) {
	return request.post('/wework/user/sop/groupdelete', data)
}
// 群sop详情
export function reqSopGroupDetail(data) {
	return request.post('/wework/user/sop/groupdetail', data)
}
// 个人sop列表
export function userSopList(data) {
	return request.post('/wework/user/sop/list', data)
}
// 个人sop开关
export function userSopOpen(data) {
	return request.post('/wework/user/sop/open', data)
}
// 个人sop详情
export function userSopDetail(data) {
	return request.post('/wework/user/sop/detail', data)
}
// 自动拉群获取分组列表
export function userAutogroupGrouping(data) {
	return request.post('/wework/user/autogroup/grouping', data)
}
// 自动拉群获取分组列表
export function userAutogroupGroupings(data) {
	return request.post('/wework/user/autogroup/groupings', data)
}
// 自动拉群批量修改
export function userAutogroupBatch(data) {
	return request.post('/wework/user/autogroup/batch', data)
}
// 潜在客户列表
export function userBatchList(data) {
	return request.post('/wework/user/batch/list', data)
}
// 删除潜在客户
export function userBatchDeluser(data) {
	return request.post('/wework/user/batch/deluser', data)
}
// 导入记录列表
export function userBatchDelHistroy(data) {
	return request.post('/wework/user/batch/histroy', data)
}
//删除导入记录以及潜在客户
export function userBatchDelDelbatch(data) {
	return request.post('/wework/user/batch/delbatch', data)
}
//添加提醒/导入记录
export function userBatchDelMissions(data) {
	return request.post('/wework/user/batch/mission', data)
}
//导入记录详情
export function userBatchDetail(data) {
	return request.post('/wework/user/batch/detail', data)
}
//数据统计
export function userBatchStatistics(data) {
	return request.post('/wework/user/batch/statistics', data)
}
//数据统计客户详情
export function userBatchDetails(data) {
	return request.post('/wework/user/batch/details', data)
}
//数据统计客户详情
export function batchAdd(data) {
	return request.post('/wework/user/batch/add', data)
}

//话术库
//话术库分组列表
export function postGroupinglist(data) {
	return request.post('/wework/user/reply/groupinglist', data)
}
//新增分组
export function postAddgrouping(data) {
	return request.post('/wework/user/reply/addgrouping', data)
}
//话术库列表
export function postReplyList(data) {
	return request.post('/wework/user/reply/list', data)
}
//编辑拉取数据
export function postReplyGet(data) {
	return request.post('/wework/user/reply/get', data)
}
export function postIntelligentdelete(data) {
	return request.post('/wework/user/qr/delete', data)
}
//添加/修改话术
export function postReplyAdd(data) {
	return request.post('/wework/user/reply/add', data)
}
//话术库上下移动
export function postReplyMove(data) {
	return request.post('/wework/user/reply/move', data)
}
//删除分组
export function postReplyDelgrouping(data) {
	return request.post('/wework/user/reply/delgrouping', data)
}
//分组上下移动
export function postReplyMovegrouping(data) {
	return request.post('/wework/user/reply/movegrouping', data)
}
//删除快捷回复
export function postReplyDel(data) {
	return request.post('/wework/user/reply/del', data)
}
//修改分组拉取数据
export function postReplyGetgrouping(data) {
	return request.post('/wework/user/reply/getgrouping', data)
}
//修改分组
export function postReplyUpdgrouping(data) {
	return request.post('/wework/user/reply/updgrouping', data)
}


