import { request } from '@/utils'

// //获取部门成员信息
// export function postVideoPop(data) {
// 	return request.post('/wework/user/staff/pop', data)
// }

// 成员管理 获取部门列表
export function getDepartmentList(data) {
	return request.get('/wework/user/staff/departmentlist', data)
}
export function getDepartmentLists(data) {
	return request.post('/wework/user/staff/sync', data)
}

//成员管理 部门列表搜索
export function getDepartmentSearch(data) {
	// return request.get('/wework/user/staff/departmentsearch?id= ' + data.id)
	return request.get('/wework/user/staff/departmentsearch?name=d', data)
}

//成员列表
export function postStaffIndex(data) {
	return request.post('/wework/user/staff/index', data)
}

// 自定义信息列表
export function postCustomizedList(data) {
	return request.post('/wework/user/customized/list', data)
}
// 自定义信息列表
export function postCustomizedSave(data) {
	return request.post('/wework/user/customized/save', data)
}
// 删除自定义信息
export function postCustomizedDel(data) {
	return request.post('/wework/user/customized/del', data)
}

// 添加/修改子标签列表
export function postTagAddtag(data) {
	return request.post('/wework/user/tag/addtag', data)
}

// 群标签列表信息页
export function postTagGrouplist(data) {
	return request.post('/wework/user/tag/grouplist', data)
}
// 群标签列表信息页
export function postTagTaglist(data) {
	return request.post('/wework/user/tag/taglist', data)
}
// 群列表
export function postGroupLists(data) {
	return request.post('/wework/user/group/list', data)
}
// 新建群标签
export function postTagAddTags(data) {
	return request.post('/wework/user/tag/addtags', data)
}
// 删除标签组
export function postTagDeltags(data) {
	return request.post('/wework/user/tag/deltags', data)
}
// 修改标签组
export function postTagUpdtag(data) {
	return request.post('/wework/user/tag/updtag', data)
}
// 删除子标签
export function postTagDeltag(data) {
	return request.post('/wework/user/tag/deltag', data)
}
// 修改拉取数据
export function postTagInfo(data) {
	return request.post('/wework/user/tag/info', data)
}

// 客户标签管理列表页
// 客户标签列表
export function postTagList(data) {
	return request.post('/wework/user/tag/list', data)
}
//添加客户子标签
export function postTagAdd(data) {
	return request.post('/wework/user/tag/add', data)
}
// 添加客户标签组
export function postTagAddGroup(data) {
	return request.post('/wework/user/tag/addgroup', data)
}
// 修改客户标签组
export function postTagUpdGroup(data) {
	return request.post('/wework/user/tag/updgroup', data)
}
// 删除客户标签组
export function postTagDel(data) {
	return request.post('/wework/user/tag/del', data)
}
// 同步标签组详情
export function postTagDetail(data) {
	return request.post('/wework/user/tag/detail', data)
}
// 客户标签组上下移动
export function postTagOrder(data) {
	return request.post('/wework/user/tag/order', data)
}
// 同步标签列表
export function postTagSync(data) {
	return request.post('/wework/user/tag/sync', data)
}

// 入群欢迎语
// 入群欢迎语列表
export function postGroupList(data) {
	return request.post('/wework/user/welcome/grouplist', data)
}
// 入群欢迎语详情
export function postDeTail(data) {
	return request.post('/wework/user/welcome/detail', data)
}
// 新建入群欢迎语
export function postAddWelcom(data) {
	return request.post('/wework/user/welcome/addwelcom', data)
}
// // 链接获取url详情
// export function postRadarUrldata(data) {
// 	return request.post('/wework/user/radar/urldata', data)
// }

// 客户群发页
// 群发列表页
export function postSendList(data) {
	return request.post('/wework/user/send/list', data)
}
// 新建群发页
export function postSendAdd(data) {
	return request.post('/wework/user/send/add', data)
}
// 互动雷达
// 互动雷达
export function postRadarList(data) {
	return request.post('/wework/user/radar/list', data)
}
// 删除雷达
export function postRadarDel(data) {
	return request.post('/wework/user/radar/del', data)
}
// 新建/修改 互动雷达（暂只有连接类型）
export function postRadarAdd(data) {
	return request.post('/wework/user/radar/add', data)
}
// // 获取url详情
// export function postRadarUrlData(data) {
// 	return request.post('/wework/user/radar/urldata', data)
// }

// 客户群列表
// 群列表
export function postUserGroupList(data) {
	return request.post('/wework/user/group/list', data)
}
// 客户群详情
export function postUserGroupDetail(data) {
	return request.post('/wework/user/group/detail', data)
}
// 详情图表数据
export function postUserGroupMap(data) {
	return request.post('/wework/user/group/map', data)
}
// 同步客户群列表
export function postUserGroupSync(data) {
	return request.post('/wework/user/group/sync', data)
}
// 数据详情（按照成员查看）
// 数据详情（按照时间查看）
export function postUserClientdetail(data) {
	return request.post('/wework/user/group/clientdetail', data)
}
// 批量打标签
export function postUserGroupBatch(data) {
	return request.post('/wework/user/group/batch', data)
}



export function reqMaterialGroupList(data) {
	return request.post('/wework/Media/group', data)
}

export function reqMaterialGroupAdd(data) {
	return request.post('/wework/Media/groupadd', data)
}

export function reqMaterialGroupUpdate(data) {
	return request.post('/wework/Media/groupUpdate', data)
}

export function reqMaterialList(data) {
	return request.post('/wework/Media/imgIndex', data)
}

export function reqMaterialDelete(data) {
	return request.post('/wework/Media/deletes', data)
}
export function reqUploadcover(data) {
	return request.post('/svideo/media/uploadcover', data)
}
export function postcolonizationlist(data) {
	return request.post('/wework/user/colonization/list', data)
}
export function postcolonizationadd(data) {
	return request.post('/wework/user/colonization/add', data)
}