import { request } from '@/utils'

export function postRadarAddA(data) {
	return request.post('/wework/user/radar/add', data)
}
export function postRadarInfo(data) {
	return request.post('/wework/user/radar/info', data)
}