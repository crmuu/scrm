import { request } from '@/utils'

export function reqClientList(data) {
	return request.post('/wework/user/client/list', data)
}
export function clientaddtag(data) {
	return request.post('/wework/user/client/addtag', data)
}
export function clientupdintegral(data) {
	return request.post('/wework/user/client/updintegral', data)
}
export function clientupdscore(data) {
	return request.post('/wework/user/client/updscore', data)
}
export function clientdeltag(data) {
	return request.post('/wework/user/client/deltag', data)
}

export function reqClientInfo(data) {
	return request.post('/wework/user/client/info', data)
}
export function reqCustomerList(data) {
	return request.post('/wework/user/client/customerinfo', data)
}

export function reqClientDynamics(data) {
	return request.post('/wework/user/client/dynamics', data)
}
// 好友欢迎语列表
export function friendWelcomeList(data) {
	return request.post('/wework/user/welcome/list', data)
}

// 删除好友欢迎语
export function friendWelcomeDel(data) {
	return request.post('/wework/user/welcome/del', data)
}
// 添加修改欢迎语
export function friendWelcomeAdd(data) {
	return request.post('/wework/user/welcome/add', data)
}
// 编辑拉取欢迎语数据
export function friendWelcomeInfo(data) {
	return request.post('/wework/user/welcome/info', data)
}

export function postClientWay(data) {
	return request.post('/wework/user/client/way', data)
}
//数据订阅拉取数据
export function feedDataList() {
	return request.post('/wework/user/feed/index')
}
//修改数据订阅
export function feedAdd(data) {
	return request.post('/wework/user/feed/add', data)
}
//数据订阅开启关闭
export function feedUpdate(data) {
	return request.post('/wework/user/feed/upd', data)
}

export function reqClientGroup(data) {
	return request.post('/wework/user/client/grouplist', data)
}
export function userSopSize(data) {
	return request.post('/wework/user/sop/size', data)
}
export function userSopGroupadd(data) {
	return request.post('/wework/user/sop/groupadd', data)
}
export function userSopGroupdetail(data) {
	return request.post('/wework/user/sop/groupdetail', data)
}


export function postCustomerDetailStaff(data){
	return request.post('/wework/user/client/detailuser',data)
}

export function postCustomerDetailField(data){
	return request.post('/wework/user/client/setfield',data)
}
export function postClientUpload(data){
	return request.post('/wework/user/client/upload',data)
}
export function postClientSetfieldlist(data){
	return request.post('/wework/user/client/setfieldlist',data)
}
export function postClientUpduserfields(data){
	return request.post('/wework/user/client/upduserfields',data)
}

export function postClientSync(data){
	return request.post('/wework/user/client/sync',data)
}

export function postClientCustomizedUpdstatus(data){
	return request.post('/wework/user/customized/updstatus',data)
}
export function postusergroup(data){
	return request.post('/chatdata/user/group',data)
}
export function postusergroupdata(data){
	return request.post('/chatdata/user/groupdata',data)
}
export function postusercustomer(data){
	return request.post('/chatdata/user/customer',data)
}
export function postusercustomegroup(data){
	return request.post('/chatdata/user/customegroup',data)
}
export function postusercustomestaff(data){
	return request.post('/chatdata/user/customestaff',data)
}
export function postusercustomedata(data){
	return request.post('/chatdata/user/customedata',data)
}
export function postuserstafflist(data){
	return request.post('/chatdata/user/stafflist',data)
}
export function postusercustomes(data){
	return request.post('/chatdata/user/customes',data)
}