import { request } from '@/utils'

export function postUserSopAdd(data) {
	return request.post('/wework/user/sop/add', data)
}
export function postUserSopFestival(data) {
	return request.post('/wework/user/sop/festival', data)
}

