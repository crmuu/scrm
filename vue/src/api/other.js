import { request } from '@/utils'

// -------成员管理--------
// 获取部门列表
export function departmentList(data) {
	return request.post('/wework/user/staff/department', data)
}
// -------客户群群发--------
// 详情获取基本信息
export function customerGroupendingGetmsg(data) {
	return request.post('/wework/user/send/getmsg', data)
}
// 客户群接收详情
export function customerGroupendingGrouplist(data) {
	return request.post('/wework/user/send/grouplist', data)
}
// 群主发送详情
export function customerGroupendingUserlist(data) {
	return request.post('/wework/user/send/userlist', data)
}
// 同步更新数据
export function customerGroupendingUpdate(data) {
	return request.post('/wework/user/send/upds', data)
}
