import { request } from '@/utils'

export function basic(data) {
	return request.post('/wework/user/config/basic', data)
}
export function updbasic(data) {
	return request.post('/wework/user/config/updbasic', data)
}
export function staff(data) {
	return request.post('/wework/user/config/staff', data)
}
export function updstaff(data) {
	return request.post('/wework/user/config/updstaff', data)
}
export function customer(data) {
	return request.post('/wework/user/config/customer', data)
}
export function updcustomer(data) {
	return request.post('/wework/user/config/updcustomer', data)
}
export function selfapp(data) {
	return request.post('/wework/user/config/selfapp', data)
}
export function updselfapp(data) {
	return request.post('/wework/user/config/updselfapp', data)
}
export function statisticsDetail(data) {
	return request.post('/wework/user/statistics/detail', data)
}
export function statisticsIndex(data) {
	return request.post('/wework/user/statistics/index', data)
}
