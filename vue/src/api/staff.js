import { request } from '@/utils'

export function reqStaffList(data) {
	return request.post('/wework/user/staff/index', data)
}
export function autogroupGroupids(data) {
	return request.post('/wework/user/autogroup/groupids', data)
}


export function reqStaffDetail(data) {
	return request.post('/wework/user/staff/detail', data)
}

export function reqStaffDetailChart(data) {
	return request.post('/wework/user/data/staffdetail', data)
}

