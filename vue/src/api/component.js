import { request } from '@/utils'
export function reqRadarList(data) {
	return request.post('/wework/user/radar/list', data)
}

export function postRadarUrlData(data) {
	return request.post('/wework/user/radar/urldata', data)
}