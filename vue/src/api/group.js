import { request } from '@/utils'

export function postGroupBatch(data) {
	return request.post('/wework/user/group/batch', data)
}

export function postGroupList2(data) {
	return request.post('/wework/user/group/list', data)
}

export function postGroupDetail(data) {
	return request.post('/wework/user/group/detail', data)
}

export function postGroupMap(data) {
	return request.post('/wework/user/group/map', data)
}

export function postGroupSync(data) {
	return request.post('/wework/user/group/sync', data)
}

export function postGroupDetailMember(data) {
	return request.post('/wework/user/group/clientdetail', data)
}

export function postGroupDetailTime(data) {
	return request.post('/wework/user/group/clientdetail', data)
}
//企微朋友圈列表
export function momentList(data) {
	return request.post('/wework/user/moments/list', data)
}
//企微朋友圈详情
export function momentsDetail(data) {
	return request.post('/wework/user/moments/detail', data)
}
//企业朋友圈提醒成员发送
export function momentsRemind(data) {
	return request.post('/wework/user/moments/remind', data)
}
//导出数据
export function momentsDownexcel(data) {
	// return request.get(`/wework/user/moments/downexcel?id=${data.id}&type=${data.type}`)
}
//同步企业朋友圈
export function momentsSyncs() {
	return request.post(`/wework/user/moments/syncs`)
}
//同步个人朋友圈
export function momentsSync() {
	return request.post(`/wework/user/moments/sync`)
}
//添加朋友圈
export function momentsAdd(data) {
	return request.post(`/wework/user/moments/add`, data)
}
//自助推广码列表
export function promotionlist(data) {
	return request.post(`/wework/user/promotion/list`, data)
}
//新建（修改）自助推广码
export function promotionadd(data) {
	return request.post(`/wework/user/promotion/add`, data)
}
//自助推广码分享
export function promotionshare(data) {
	return request.post(`/wework/user/promotion/share`, data)
}
//删除自助推广码
export function promotiondel(data) {
	return request.post(`/wework/user/promotion/del`, data)
}
//删除自助推广码
export function promotioninfo(data) {
	return request.post(`/wework/user/promotion/info`, data)
}