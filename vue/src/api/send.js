import { request } from '@/utils'

export function postCustomerSend(data) {
	return request.post('/wework/user/send/addmsg', data)
}

export function postCustomerSize(data) {
	return request.post('/wework/user/send/size', data)
}

export function postCustomerList(data) {
	return request.post('/wework/user/send/sendlist', data)
}
export function postCustomerAlert(data) {
	return request.post('/wework/user/send/alertcustomers', data)
}



export function postCustomerGroupSend(data) {
	return request.post('/wework/user/send/add', data)
}


export function postCustomerGroupList(data) {
	return request.post('/wework/user/send/list', data)
}
export function postCustomerGroupAlert(data) {
	return request.post('/wework/user/send/alerts', data)
}


export function postCustomerSendDetail(data) {
	return request.post('/wework/user/send/usermsg', data)
}


export function postCustomerSendDetailUser(data) {
	return request.post('/wework/user/send/detailuser', data)
}
export function postCustomerSendDetailCustomer(data) {
	return request.post('/wework/user/send/detailcustomer', data)
}

export function postCustomerSendDetailUserAlert(data) {
	return request.post('/wework/user/send/alertcustomers', data)
}







