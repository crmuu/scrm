import { request } from '@/utils'

export function reqGroupDetail(data) {
	return request.post('/wework/user/group/detail', data)
}

export function reqGroupDetailSync(data) {
	return request.post('/wework/user/group/chatsync', data)
}
export function reqGroupDetailMap(data) {
	return request.post('/wework/user/group/map', data)
}

export function reqGroupDetailStaffTable(data) {
	return request.post('/wework/user/group/clientdetail', data)
}

export function reqGroupDetailDateTable(data) {
	return request.post('/wework/user/group/clientdetail', data)
}


