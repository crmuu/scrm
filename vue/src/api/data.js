import { request } from '@/utils'

export function postDataCustomer(data) {
	return request.post('/wework/user/data/customer', data)
}

export function postDataCustomerChart(data) {
	return request.post('/wework/user/data/customerdetail', data)
}

export function postDataCustomerTable(data) {
	return request.post('/wework/user/data/customertable', data)
}

export function postDataGroup(data) {
	return request.post('/wework/user/data/group', data)
}

export function postDataGroupChart(data) {
	return request.post('/wework/user/data/groupdetail', data)
}

export function postDataGroupTable(data) {
	return request.post('/wework/user/data/grouptable', data)
}

export function postDataGroupTableDepartment(data) {
	return request.post('/wework/user/data/groupdepartment', data)
}



export function postDataStaff(data) {
	return request.post('/wework/user/data/staff', data)
}

export function postDataStaffChart(data) {
	return request.post('/wework/user/data/staffdetail', data)
}

export function postDataStaffTable(data) {
	return request.post('/wework/user/data/stafftable', data)
}

export function postDataStaffTableDepartment(data) {
	return request.post('/wework/user/data/staffdepartment', data)
}

export function postDataStaffBar(data) {
	return request.post('/wework/user/data/stafftop', data)
}


export function postDataQr(data) {
	return request.post('/wework/user/data/qrcode', data)
}



export function postDataQrTable(data) {
	return request.post('/wework/user/data/qrcodetable', data)
}

export function postDataQrTableDetail(data) {
	return request.post('/wework/user/data/qrcodedetail', data)
}



