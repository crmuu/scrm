import { request } from '@/utils'

export function reqSms() {
	// return request.get('/api/sms')
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve()
		}, 1000)
	})
}

export function reqLogin() {
	// return request.get('/api/login')
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve({ data: { token: 'test' } })
		}, 1000)
	})
}

export function reqPermissions() {
	// return request.get('/api/user/permission')
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve({ data: ['p1', 'p2'] })
		}, 1000)
	})
}
export function logindata() {
	return request.post('/wework/Base/getInfo')
}