import { request } from '@/utils'

export function postWelcomeAdd(data) {
	return request.post('/wework/user/welcome/addwelcom', data)
}

export function postWelcomeGroupList(data) {
	return request.post('/wework/user/welcome/grouplist', data)
}

export function postWelcomeDetail(data) {
	return request.post('/wework/user/welcome/detail', data)
}

export function postWelcomeDele(data) {
	return request.post('/wework/user/welcome/delwelcom', data)
}



