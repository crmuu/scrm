export const LineOption = {
	// title: {
	// 	text: 'Stacked Line'
	// },
	tooltip: {
		trigger: 'axis'
	},
	legend: {
		data: ['页面浏览次数', '添加客户数', '流失客户数', '净增客户数']
	},
	grid: {
		left: '0%',
		right: '4%',
		bottom: '0%',
		containLabel: true
	},
	// toolbox: {
	// 	feature: {
	// 		saveAsImage: {}
	// 	}
	// },
	xAxis: {
		type: 'category',
		boundaryGap: true,
		// nameGap: 15,
		data: ['2022-11-22', '2022-11-23', '2022-11-24', '2022-11-25', '2022-11-26', '2022-11-27', '2022-11-28']
	},
	yAxis: {
		type: 'value',
		min: function (value) {
			return value.max + 10
		}
	},
	series: [
		{
			name: '页面浏览次数',
			type: 'line',
			stack: 'Total',
			data: [0, 0, 0, 0, 0, 0, 0]
		},
		{
			name: '添加客户数',
			type: 'line',
			stack: 'Total',
			data: [0, 0, 0, 0, 0, 0, 0]
		},
		{
			name: '流失客户数',
			type: 'line',
			stack: 'Total',
			data: [0, 0, 0, 0, 0, 0, 0]
		},
		{
			name: '净增客户数',
			type: 'line',
			stack: 'Total',
			data: [0, 0, 0, 0, 0, 0, 0]
		}
	]
}
export const PromotionRankings = [
	{
		accepted: 0,
		activity_description: '您有一个推广任务已经开始，快点击下方链接领取你的专属海报 ，邀请客户添加你的企业微信吧～ ',
		activity_source_ids: null,
		activity_type: 'internal_add_staff',
		company_description: '风雅',
		company_logo_url:
			'https://weibanzhushou.oss-cn-shenzhen.aliyuncs.com/1749725042939516929/promotion_qr_activity_fission/wogizUDQAALP7FQHyxCG3fEKCMGJ5bGA/lauxeweq1b2b_lauxew4gy9z1669285376800',
		company_name: '风雅',
		corp_id: '1749725042939516929',
		create_body:
			'{"post_url": null, "qr_size": 82, "qr_x": 130, "qr_y": 245, "name": "\\u6d4b\\u8bd512344", "group_list": null, "group_welcome": null, "activity_type": "internal_add_staff", "is_open_company_description": true, "company_description": "\\u98ce\\u96c5", "company_name": "\\u98ce\\u96c5", "scan_qr_guide": "\\u6d4b\\u8bd5\\u63a8\\u5e7f\\u7801", "poster_title": "\\u6d4b\\u8bd5\\u63a8\\u5e7f\\u7801", "company_logo_url": "https://weibanzhushou.oss-cn-shenzhen.aliyuncs.com/1749725042939516929/promotion_qr_activity_fission/wogizUDQAALP7FQHyxCG3fEKCMGJ5bGA/lauxeweq1b2b_lauxew4gy9z1669285376800", "poster_style": "default", "msg_data": "\\u60a8\\u6709\\u4e00\\u4e2a\\u63a8\\u5e7f\\u4efb\\u52a1\\u5df2\\u7ecf\\u5f00\\u59cb\\uff0c\\u5feb\\u70b9\\u51fb\\u4e0b\\u65b9\\u94fe\\u63a5\\u9886\\u53d6\\u4f60\\u7684\\u4e13\\u5c5e\\u6d77\\u62a5 \\uff0c\\u9080\\u8bf7\\u5ba2\\u6237\\u6dfb\\u52a0\\u4f60\\u7684\\u4f01\\u4e1a\\u5fae\\u4fe1\\u5427\\uff5e ", "activity_description": "\\u60a8\\u6709\\u4e00\\u4e2a\\u63a8\\u5e7f\\u4efb\\u52a1\\u5df2\\u7ecf\\u5f00\\u59cb\\uff0c\\u5feb\\u70b9\\u51fb\\u4e0b\\u65b9\\u94fe\\u63a5\\u9886\\u53d6\\u4f60\\u7684\\u4e13\\u5c5e\\u6d77\\u62a5 \\uff0c\\u9080\\u8bf7\\u5ba2\\u6237\\u6dfb\\u52a0\\u4f60\\u7684\\u4f01\\u4e1a\\u5fae\\u4fe1\\u5427\\uff5e ", "enable_daily_notification": true, "daily_notification_time": "12:00", "staff_ext_id_list": ["wogizUDQAALP7FQHyxCG3fEKCMGJ5bGA"], "activity_sources": null, "enable_group_name": true, "group_name": null, "end_date": null, "end_time": null, "is_unlimited": true, "tag_enable": false, "customer_remark_enable": false, "customer_desc_enable": false, "auto_mark_id": [], "customer_desc": null, "customer_remark": null, "auto_reply_rule": "special", "auto_reply": "{\\"attachments\\":[],\\"text\\":{\\"content\\":\\"#\\u7528\\u6237\\u6635\\u79f0#\\"}}", "kanban_stage_id": null, "radar_id": null}',
		created_at: 1669285410,
		ctime: '2022-11-28',
		creator_id: 19777611,
		creator_name: '夏小飞',
		avatar: 'https://wework.qpic.cn/wwpic/186339_3kO_N4tbShSL3lF_1665369145/0',
		daily_notification_time: '12:00',
		deleted: false,
		enable_daily_notification: true,
		enable_group_name: true,
		end_time: 4070880000,
		entry_url:
			'https://weibanzhushou.com/api/promotion_qr_activity_fission/staff/entry?_c=X1BrWlhARW8GAgsCV1JuVVZASQ&id=rlukv6nq7',
		group_name: null,
		group_welcome: null,
		hid: 'rlukv6nq7',
		id: 3333,
		is_open_company_description: true,
		is_unlimited: true,
		join_count: 0,
		msg_data: '您有一个推广任务已经开始，快点击下方链接领取你的专属海报 ，邀请客户添加你的企业微信吧～ ',
		name: '测试12344',
		post_url: null,
		poster_style: 'default',
		poster_title: '测试推广码',
		qr_size: 82,
		qr_x: 130,
		qr_y: 245,
		radar_id: null,
		scan_qr_guide: '测试推广码',
		staff_ext_id_list: ['wogizUDQAALP7FQHyxCG3fEKCMGJ5bGA'],
		staff_name_list: ['夏小飞'],
		status: 'ongoing',
		updated_at: 1669285410,
		view_count: 0,
		ranking: 1
	}
]
