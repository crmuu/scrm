export const customerDataDesc = [
	'1、客户总数：成员的全部客户数量',
	'2、今日新增：成员今日新添加的客户数量，包含流失数据',
	'3、今日流失：将成员删除的客户数'
]
export const groupDataDesc = [
	'群成员总数：群内的全部成员数量，包括企业员工数量',
	'群客户总数：群内的全部客户数量',
	'入群人数：进入群聊的成员数量，包括企业员工',
	'退群人数：退出群聊的成员数量，包括企业员工数量',
	'活跃人数：在群内发言过的成员记做活跃用户，包含企业员工'
]
export const groupAttetionDataDesc = ['请注意：', '【客户群统计】与【客户群列表】数据计算方式不一样']

export const memberDataDesc = [
	'1、聊天总数：成员有发送过消息的单聊的数量',
	'2、发送消息数：成员在单聊中发送的消息总数',
	'3、已回复聊天占比：客户主动发起聊天后，成员在一个自然日内有回复过消息的聊天数/客户主动发起的聊天比例',
	'4、平均首次回复时长：客户主动发起聊天后，成员在一个自然日内首次回复的时长间隔为首次回复时长'
]

export const qrDataDesc = [
	'1、今日添加客户数（人）：今日通过一客一码添加的客户，若客户重复添加将不会重复计数',
	'2、今日流失客户数（人）：今日通过一客一码添加后将成员删除的客户数',
	'3、总添加客户数（人）：通过一客一码添加的客户总数，若客户重复添加将不会重复计数',
	'4、总流失客户数（人）：通过一客一码添加后将成员删除的客户总数'
]

// name字段名，title显示
export const groupChartTab = [
	{
		name: 'total',
		title: '总人数'
	},
	{
		name: 'total1',
		title: '客户总数'
	},
	{
		name: 'total2',
		title: '入群人数'
	},
	{
		name: 'total3',
		title: '退群人数'
	},
	{
		name: 'total4',
		title: '活跃人数'
	},
	{
		name: 'total5',
		title: '活跃人数'
	}
]
export const memberChartTab = [
	{
		name: 'total',
		title: '聊天总数'
	},
	{
		name: 'total1',
		title: '发送消息数'
	},
	{
		name: 'total2',
		title: '已回复聊天占比'
	},
	{
		name: 'total3',
		title: '平均首次回复时长'
	}
]
