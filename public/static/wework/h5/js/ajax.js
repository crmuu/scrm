function myAjax(msg, loading, url, data, method, successCallback) {
  layui.use(['layer'], function () {
    var layer = layui.layer;

    function realAjax(loading, url, data, method, successCallback) {
      $.ajax({
        url: url,
        data: data,
        method: method.toUpperCase(),
        beforeSend: function () {
          if (loading) {
            layer.load(1);
          }
        }, complete: function () {
          if (loading) {
            layer.closeAll('loading');
          }
          if (alertHandle) {
            layer.close(alertHandle);
          }
        }, success: function (resp) {
          if (successCallback) {
            successCallback(resp);
          } else {
            if (resp.status == 'success') {
              layer.msg(resp.msg, {time: 1000}, function () {
                location.reload(1);
              });
            } else {
              layer.msg(resp.msg);
            }
          }
        }, error: function () {
          layer.msg('网络错误');
        }
      });
    }

    var alertHandle = null;
    if (msg) {
      alertHandle = layer.alert(msg, {
        btn: ['确定', '取消'],
        yes: function () {
          realAjax(loading, url, data, method, successCallback);
        }
      });
    } else {
      realAjax(loading, url, data, method, successCallback);
    }
  });
}

function myAjaxPost(msg, loading, url, data, successCallback) {
  myAjax(msg, loading, url, data, 'POST', successCallback);
}